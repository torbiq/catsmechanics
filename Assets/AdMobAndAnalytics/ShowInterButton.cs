﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//[RequireComponent ( typeof ( Button ) )]
public class ShowInterButton :MonoBehaviour/*,IPointerClickHandler*/
{
	private const string
		admob_test_banner = "ca-app-pub-3940256099942544/6300978111",
		admob_test_interstitial = "ca-app-pub-3940256099942544/4411468910";


	Button btn;
	Text lbl;

	Dropdown
		dropdown;


	public enum AdNetworks
	{
		AdMob,
		ChartBoost,
		UnityAds,
		MobFox,
		InMobi,
		Vungle,
		AdColony,
		HomeAds,
	}

	public class AdID
	{
		public string
			banner_id,
			inter_id;
		public AdID (string banner, string inter)
		{
			banner_id = banner;
			inter_id = inter;
		}
	}


	Dictionary<AdNetworks, AdID> AdNetworkParams = new Dictionary<AdNetworks, AdID>
	{
		{AdNetworks.AdMob, new AdID("","")},
		{AdNetworks.ChartBoost, new AdID("","ca-app-pub-1022958838828668/2002891837")},
		{AdNetworks.UnityAds, new AdID("","ca-app-pub-1022958838828668/4964861435")},
		{AdNetworks.InMobi, new AdID("ca-app-pub-1022958838828668/5843688633","ca-app-pub-1022958838828668/8797155037")},
		{AdNetworks.MobFox, new AdID("ca-app-pub-1022958838828668/4024035032","ca-app-pub-1022958838828668/1070568633")},
		{AdNetworks.Vungle, new AdID("","ca-app-pub-1022958838828668/6135243034")},
		{AdNetworks.AdColony, new AdID("","ca-app-pub-1022958838828668/2630583034")},
		{AdNetworks.HomeAds, new AdID("ca-app-pub-1022958838828668/3557542231","ca-app-pub-1022958838828668/8816622632")},
	};


	void Awake ()
	{
		btn = transform.GetChild ( 2 ).GetComponent<Button> ( );
		lbl = btn.GetComponentInChildren<Text> ( );
		DontDestroyOnLoad ( gameObject );
		dropdown = GetComponentInChildren<Dropdown> ( );
	}





	void SetOptions ()
	{
		if (dropdown != null && AdmobManager.Instance != null)
		{
			AdNetworkParams [AdNetworks.AdMob] = new AdID ( AdmobManager.Instance.BannerIdAndroid, AdmobManager.Instance.InterstitialIdAndroid );


			List<Dropdown.OptionData> options = new List<Dropdown.OptionData> ( );

			foreach (var item in AdNetworkParams)
			{
				if (string.IsNullOrEmpty ( item.Value.banner_id ))
					item.Value.banner_id = admob_test_banner;
				if (string.IsNullOrEmpty ( item.Value.inter_id ))
					item.Value.inter_id = admob_test_interstitial;
				options.Add ( new Dropdown.OptionData ( item.Key.ToString ( ) ) );
			}
			dropdown.ClearOptions ( );
			dropdown.AddOptions ( options );

		}
	}


	void Start ()
	{
		SetOptions ( );
		EnableButton ( false );
	}
	public void ShowInter ()
	{
		AdmobManager.Instance.ShowInterstitial ( );
	}

	public void ShowBanner ()
	{
		AdmobManager.Instance.ShowBanner ( );
	}


	public void OnDisable ()
	{
		AdmobAdAgent.OnReceiveAdInterstitial -= OnInterLoaded;
		AdmobAdAgent.OnDismissScreenInterstitial -= OnInterLoading;
	}

	public void OnEnable ()
	{
		AdmobAdAgent.OnReceiveAdInterstitial += OnInterLoaded;
		AdmobAdAgent.OnDismissScreenInterstitial += OnInterLoading;
	}



	void OnInterLoaded ()
	{
		EnableButton ( true );
	}


	void OnInterLoading ()
	{
		EnableButton ( false );
	}


	void EnableButton (bool param)
	{
		btn.interactable = param;
		if (lbl)
			lbl.text = param ? "Show ad" : "Ad is loading";
	}



	public void OnDropdownChange (int val)
	{
		if (dropdown != null)
		{
			AdNetworks net = (AdNetworks) System.Enum.Parse ( typeof ( AdNetworks ), dropdown.options [val].text, true );
			SetAdmobParams ( net );
		}
	}



	void SetAdmobParams (AdNetworks net)
	{
		Debug.Log ( "SetAdmobParams for " + net );
		if (AdNetworkParams.ContainsKey ( net ))
		{
			OnInterLoading ( );
			AdmobManager.Instance.BannerIdAndroid = AdNetworkParams [net].banner_id;
			AdmobManager.Instance.InterstitialIdAndroid = AdNetworkParams [net].inter_id;
			AdmobManager.Instance.Init ( );
		}
	}


}
