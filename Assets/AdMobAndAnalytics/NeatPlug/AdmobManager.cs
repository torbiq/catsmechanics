﻿using UnityEngine;
using System.Collections;

public class AdmobManager :MonoBehaviour
{
    public static AdmobManager Instance;

    public string BannerIdAndroid = "ca-app-pub-7022579750037037/7887001309";
    public string InterstitialIdAndroid = "ca-app-pub-7022579750037037/1840467709";
    public string BannerIdIOS = "";
    public string InterstitialIdIOS = "";
    public string BannerIdWP = "";
    public string InterstitialIdWP = "";

    public AdmobAd.BannerAdType BannerAdType;
    public AdmobAd.AdLayout BannerAdPosition;

    public bool debug = false;

    private bool _isInterstitialReady = false;
    private bool _isInterstitialLoading = false;
    private bool _isInterstitialVisible = false;


    private bool _isBannerReady = false;
    private bool _isBannerLoading = false;
    private bool _isBannerVisible = false;
    private bool _showIfBannerReady = false;

    private int _isAdvertsDisabled = 0;

    private bool isInterstitialReady
    {
        get
        {
            return _isInterstitialReady;
        }
        set
        {
            _isInterstitialReady = value;
        }
    }

    private bool isInterstitialLoading
    {
        get
        {
            return _isInterstitialLoading;
        }
        set
        {
            _isInterstitialLoading = value;
        }
    }

    private bool isInterstitialVisible
    {
        get
        {
            return _isInterstitialVisible;
        }
        set
        {
            _isInterstitialVisible = value;
        }
    }



	private bool isBannerReady
    {
        get
        {
            return _isBannerReady;
        }
         set
        {
            _isBannerReady = value;
        }
    }

	private bool isBannerLoading
    {
        get
        {
            return _isBannerLoading;
        }
         set
        {
            _isBannerLoading = value;
        }
    }

	private bool isBannerVisible
    {
        get
        {
            return _isBannerVisible;
        }
         set
        {
            _isBannerVisible = value;
        }
    }

	private bool showIfBannerReady
    {
        get
        {
            return _showIfBannerReady;
        }
         set
        {
            _showIfBannerReady = value;
        }
    }

	private bool isAdvertsDisabled
    {
        get
        {
            return _isAdvertsDisabled == 1;
        }
         set
        {
            _isAdvertsDisabled = value ? 1 : 0;
        }
    }

    private static bool _instanceFound = false;

    void Awake ()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad ( this );
        }
        else
        {
            Debug.LogWarning ( "You have duplicate AdMob Managers in your scene!" );
			Destroy ( this.gameObject );
		}

        AdmobAdAgent.RetainGameObject ( ref _instanceFound, gameObject, null );
        AdmobAd.Instance ( );
    }

    void OnEnable ()
    {
        AdmobAdAgent.OnReceiveAd += OnReceiveAd;
        AdmobAdAgent.OnFailedToReceiveAd += OnFailedToReceiveAd;
        AdmobAdAgent.OnReceiveAdInterstitial += OnReceiveAdInterstitial;
        AdmobAdAgent.OnFailedToReceiveAdInterstitial += OnFailedToReceiveAdInterstitial;
        AdmobAdAgent.OnPresentScreenInterstitial += OnPresentScreenInterstitial;
        AdmobAdAgent.OnDismissScreenInterstitial += OnDismissScreenInterstitial;
        AdmobAdAgent.OnAdShown += OnAdShown;
        AdmobAdAgent.OnAdHidden += OnAdHidden;
        AdmobAdAgent.OnLeaveApplicationInterstitial += OnLeaveApplicationInterstitial;
        AdmobAdAgent.OnLeaveApplication += OnLeaveApplication;
    }

    void OnDisable ()
    {
        AdmobAdAgent.OnReceiveAd -= OnReceiveAd;
        AdmobAdAgent.OnFailedToReceiveAd -= OnFailedToReceiveAd;
        AdmobAdAgent.OnReceiveAdInterstitial -= OnReceiveAdInterstitial;
        AdmobAdAgent.OnFailedToReceiveAdInterstitial -= OnFailedToReceiveAdInterstitial;
        AdmobAdAgent.OnPresentScreenInterstitial -= OnPresentScreenInterstitial;
        AdmobAdAgent.OnDismissScreenInterstitial -= OnDismissScreenInterstitial;
        AdmobAdAgent.OnAdShown -= OnAdShown;
        AdmobAdAgent.OnAdHidden -= OnAdHidden;
        AdmobAdAgent.OnLeaveApplicationInterstitial -= OnLeaveApplicationInterstitial;
        AdmobAdAgent.OnLeaveApplication -= OnLeaveApplication;
    }

    public void Init (bool showBanner = false, bool showInterstitial = false)
    {
        if (PlayerPrefs.HasKey ( "AdmobDisabled" ))
        {
            _isAdvertsDisabled = PlayerPrefs.GetInt ( "AdmobDisabled" );
        }
        else
        {
            _isAdvertsDisabled = 0;
            PlayerPrefs.SetInt ( "AdmobDisabled", _isAdvertsDisabled );
        }

        if (!isAdvertsDisabled)
        {
            InitAdMob ( );
            LoadBanner ( showBanner );
            LoadInterstitial ( showInterstitial );
        }
    }

	private void InitAdMob ()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            AdmobAd.Instance ( ).Init ( BannerIdIOS, InterstitialIdIOS );
        }
        else if (Application.platform == RuntimePlatform.WP8Player)
        {
            AdmobAd.Instance ( ).Init ( BannerIdWP, InterstitialIdWP );
        }
        else
        {
            AdmobAd.Instance ( ).Init ( BannerIdAndroid, InterstitialIdAndroid );
        }
    }

	private void LoadInterstitial (bool showInterstitial = false)
    {
        if (!isInterstitialLoading && !isInterstitialReady)
        {
            AdmobAd.Instance ( ).LoadInterstitialAd ( true );
            isInterstitialLoading = true;
        }

        if (showInterstitial)
        {
            ShowInterstitial ( );
        }
    }

    public bool ShowInterstitial ()
    {
        if (isInterstitialReady)
        {
            HideBanner ( );
#if UNITY_EDITOR
            Debug.Log ( "Show Interstitial" );
#endif
            AdmobAd.Instance ( ).ShowInterstitialAd ( );
            return true;
        }
        else
        {
            return false;
        }
    }

	private void LoadBanner (bool showBanner = false)
    {
        if (!isBannerLoading && !isBannerReady)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                AdmobAd.Instance ( ).LoadBannerAd ( BannerAdType, BannerAdPosition, true );
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                Vector2 size = AdmobAd.Instance ( ).GetAdSizeInPixels ( BannerAdType );
                AdmobAd.Instance ( ).LoadBannerAd ( BannerAdType, (int) (Screen.height * 0.5f - size.y * 0.5f), (int) (Screen.width * 0.25f - size.x * 0.25f), true );
            }
            else if (Application.platform == RuntimePlatform.WP8Player)
            {
                AdmobAd.Instance ( ).LoadBannerAd ( BannerAdType, BannerAdPosition, true );
                //				AdmobAd.Instance ().LoadBannerAd (AdmobAd.BannerAdType.Universal_SmartBanner, AdmobAd.AdLayout.Top_Centered, true);
                //				AdmobAd.Instance ().LoadBannerAd (AdmobAd.BannerAdType.Universal_Banner_320x50, AdmobAd.AdLayout.Bottom_Centered);
            }
            isBannerLoading = true;
        }

        if (showBanner)
        {
            ShowBanner ( );
        }
    }

    public void ShowBanner ()
    {
#if UNITY_EDITOR
            Debug.Log ( "Show banner, isBannerReady=" + isBannerReady );
#endif
        if (isBannerReady)
        {
            AdmobAd.Instance ( ).ShowBannerAd ( );
        }
        else
        {
            showIfBannerReady = true;
        }
    }

	private void HideBanner ()
    {
#if UNITY_EDITOR
        Debug.Log ( "Hide banner" );
#endif
        isBannerVisible = false;
        showIfBannerReady = false;
        AdmobAd.Instance ( ).HideBannerAd ( );
    }

	private void RefreshBanner ()
    {
        AdmobAd.Instance ( ).RefreshBannerAd ( );
    }

	private void DestroyBanner ()
    {
        isBannerReady = false;
        AdmobAd.Instance ( ).DestroyBannerAd ( );
    }

	private void RepositionBanner (AdmobAd.AdLayout layout)
    {
        BannerAdPosition = layout;
        AdmobAd.Instance ( ).RepositionBannerAd ( layout );
    }

	private void DisableAllAds ()
    {
        AdmobAd.Instance ( ).DisableAd ( );
        isAdvertsDisabled = true;
        PlayerPrefs.SetInt ( "AdmobDisabled", _isAdvertsDisabled );
    }

	private void EnableAllAds ()
    {
        AdmobAd.Instance ( ).EnableAd ( );
        isAdvertsDisabled = false;
        PlayerPrefs.SetInt ( "AdmobDisabled", _isAdvertsDisabled );
    }


    /* ====== ADVERT EVENTS ====== */

    // Banner advert loaded and ready to be displayed
    void OnReceiveAd ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnReceiveAd() Fired." );

        isBannerReady = true;

        if (showIfBannerReady)
        {
            ShowBanner ( );
            showIfBannerReady = false;
        }
    }

    // Failed to receive banner advert
    void OnFailedToReceiveAd (string error)
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnFailedToReceiveAd() Fired. Error: " + error );

        isBannerLoading = false;

        if (!isAdvertsDisabled)
        {
            LoadBanner ( showIfBannerReady );
        }
    }

    // Banner advert is visible
    void OnAdShown ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnAdShown() Fired." );

        isBannerVisible = true;
    }

    // Banner advert is hidden
    void OnAdHidden ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnAdHidden() Fired." );

        isBannerVisible = false;
    }

    void OnLeaveApplication ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnLeaveApplication() Fired." );

        //GoogleAnalytics.Instance.LogEvent ( "AdMob Clicks", "Banner clicked" );
    }


    // Interstitial loaded and ready to be displayed
    void OnReceiveAdInterstitial ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnReceiveAdInterstitial() Fired." );

        isInterstitialReady = true;
        isInterstitialLoading = false;


        //if (showIfInterstitialReady) {
        //	ShowInterstitial ();
        //	showIfInterstitialReady = false;
        //}
    }

    // Failed to receive interstitial advert (e.g no internet connection)
    void OnFailedToReceiveAdInterstitial (string error)
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnFailedToReceiveAdInterstitial() Fired. Error: " + error );

        isInterstitialLoading = false;

        LoadInterstitial ( );
    }

    // When interstitial window opens
    void OnPresentScreenInterstitial ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnPresentScreenInterstitial() Fired." );

        isInterstitialReady = false;
        isInterstitialVisible = true;
    }

    // When interstitial window is closed (Via hardware back button or clicking the X)
    void OnDismissScreenInterstitial ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnDismissScreenInterstitial() Fired." );

        isInterstitialVisible = false;

        LoadInterstitial ( false );
    }

    // The player clicked an interstitial advert and the app has minimized
    void OnLeaveApplicationInterstitial ()
    {
        if (debug)
            print ( this.GetType ( ).ToString ( ) + " - OnLeaveApplicationInterstitial() Fired." );

        //GoogleAnalytics.Instance.LogEvent ( "AdMob Clicks", "Interstitial clicked" );
    }
}
