﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
using System.Security.Cryptography;
using UnityEngine.Windows;
using UnityEngine.WindowsPhone;
#else
using System.Security.Cryptography;
#endif

namespace PromoPlugin
{
    public static class WeightedRandom
    {
        public static float [] CalcLookups (float [] weights)
        {
            float total_weight = 0;
            for (int i = 0; i < weights.Length; i++)
            {
                total_weight += weights [i];
            }
            float [] lookups = new float [weights.Length];
            for (int i = 0; i < weights.Length; i++)
            {
                lookups [i] = (weights [i] / total_weight) + (i == 0 ? 0 : lookups [i - 1]);
            }
            return lookups;
        }

        private static int binary_search (float needle, float [] lookups)
        {
            int high = lookups.Length - 1;
            int low = 0;
            int probe = 0;
            if (lookups.Length < 2)
            {
                return 0;
            }
            while (low < high)
            {
                probe = (int) ((high + low) / 2);

                if (lookups [probe] < needle)
                {
                    low = probe + 1;
                }
                else if (lookups [probe] > needle)
                {
                    high = probe - 1;
                }
                else
                {
                    return probe;
                }
            }

            if (low != high)
            {
                return probe;
            }
            else
            {
                return (lookups [low] >= needle) ? low : low + 1;
            }
        }


        public static int RandomW (float [] weights)
        {
            weights = CalcLookups ( weights );

            if (weights.Length > 0)
                return binary_search ( Random.value, weights );
            else
                return -1;
        }

    }



    public static class Crypto
    {
#if UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
    public static string Md5Sum (string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding ( );
        byte [] bytes = ue.GetBytes ( strToEncrypt );

        byte [] hashBytes = Crypto.ComputeMD5Hash ( bytes );
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString ( hashBytes [i], 16 ).PadLeft ( 2, '0' );
        }

        return hashString.PadLeft ( 32, '0' );
    }
#else
        public static string Md5Sum (string strToEncrypt)
        {
            System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding ( );
            byte [] bytes = ue.GetBytes ( strToEncrypt );
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider ( );
            byte [] hashBytes = md5.ComputeHash ( bytes );
            string hashString = "";
            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += System.Convert.ToString ( hashBytes [i], 16 ).PadLeft ( 2, '0' );
            }
            return hashString.PadLeft ( 32, '0' );
        }
#endif
    }



    public static class ServiceUtils
    {
        private static string
            saved_locale_alias = null;


        public static bool IsAppInstalled (string bundleID)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaClass up = new AndroidJavaClass ( "com.unity3d.player.UnityPlayer" );
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject> ( "currentActivity" );
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject> ( "getPackageManager" );
        //Debug.Log ( " ********LaunchOtherApp " );
        AndroidJavaObject launchIntent = null;
        //if the app is installed, no errors. Else, doesn't get past next line
        try
        {
            launchIntent = packageManager.Call<AndroidJavaObject> ( "getLaunchIntentForPackage", bundleID );
            //        
            //        ca.Call("startActivity",launchIntent);
        }
        catch (System.Exception ex)
        {
            //Debug.Log ( "exception" + ex.Message );
        }

        if (PromoModule.debug)
        {
            Debug.Log ( bundleID + " installed = " + launchIntent != null );
        }
        return launchIntent != null;
#else
            return false;
#endif
        }


		public static bool IsMobilePlatform ()
		{
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
			return true;
#else
			return false;
#endif
		}

		public static bool IsAppAllowed (string bundleID)
        {
            bool res = !IsAppInstalled ( bundleID ) && !GetAppBundle ( ).Equals ( bundleID );
            if (PromoModule.debug)
            {
                Debug.Log ( "PromoModule: IsAppAllowed (" + bundleID + ") = " + res );
            }
            return res;
        }


        public static string GetCurrentLocale ()
        {
            if (saved_locale_alias == null)
            {
                int index = PrimarySettings.supported_languages.FindIndex ( X => X.Equals ( Application.systemLanguage ) );
                if (index >= 0 && locale_alias.ContainsKey ( PrimarySettings.supported_languages [index] ))
                {
                    saved_locale_alias = locale_alias [PrimarySettings.supported_languages [index]] [GetPlatform ( )];
                }
                else
                {
                    saved_locale_alias = locale_alias [SystemLanguage.English] [GetPlatform ( )];
                }
                if (PromoModule.debug)
                {
                    Debug.Log ( "PromoModule: Detected platform - " + GetPlatformName ( ) + "\nDetected locale - " + saved_locale_alias );
                }
            }

            return saved_locale_alias;
        }


        //uncomment necessary locales
        private static Dictionary<SystemLanguage, string []>
            locale_alias = new Dictionary<SystemLanguage, string []> ( )
            {
            { SystemLanguage.Arabic, new string[]{"ar", "bh", "ar-bh" } },
            //{SystemLanguage.Chinese, new string[]{"zh", "hk", "zh-hk" }},
            {SystemLanguage.Czech, new string[]{"cs", "cz", "cs-cz" } },
            {SystemLanguage.Danish, new string[]{"da", "dk", "da-dk" } },
            {SystemLanguage.Dutch, new string[]{"nl", "nl", "nl-nl" } },
            {SystemLanguage.English, new string[]{"en", "uk", "en-gb" } },
            {SystemLanguage.Finnish, new string[]{"fi", "fi", "fi-fi" } },
            {SystemLanguage.French, new string[]{"fr", "fr", "fr-fr" } },
            {SystemLanguage.German, new string[]{"de", "de", "de-de" } },
            {SystemLanguage.Greek, new string[]{"el", "gr", "el-gr" } },
            {SystemLanguage.Italian, new string[]{"it", "it", "it-it" } },
            //{SystemLanguage.Japanese, new string[]{"ja", "jp", "ja-jp" }},
            {SystemLanguage.Norwegian, new string[]{"no", "no", "no-no" } },
            {SystemLanguage.Polish, new string[]{"pl", "pl", "pl-pl" } },
            {SystemLanguage.Portuguese, new string[]{"pt", "pt", "pt-pt" } },
            {SystemLanguage.Romanian, new string[]{"ro", "ro", "ro-ro" } },
            {SystemLanguage.Russian, new string[]{"ru", "ru", "ru-ru" } },
            {SystemLanguage.Spanish, new string[]{"es", "es", "es-es" } },
            {SystemLanguage.Swedish, new string[]{"sv", "se", "sv-se" } },
            {SystemLanguage.Turkish, new string[]{"tr", "tr", "tr-tr" } },
            //{SystemLanguage.Korean, new string[]{"ko", "kr", "ko-kr" }},
            //{SystemLanguage.Thai, new string[]{"th", "th", "th-th", }},
            {SystemLanguage.Ukrainian, new string[]{"ru", "ru", "ru-ru" } }
            };


        private static int
            platform_override = -1;

        public static void OverridePlatform (int target = -1)
        {
            platform_override = target;
            saved_locale_alias = null;
        }

        public static int GetPlatform ()
        {
            int res = 0;
#if UNITY_ANDROID
            res = 0;
#elif UNITY_IPHONE
        res = 1;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
        res = 2;
#endif

            return platform_override < 0 ? res : platform_override;
        }


        public static string GetPlatformName (int p = -1)
        {
            string res = "";
			int _p = p < 0 ? GetPlatform ( ) : p; //get name of defined platform or define current platform
			switch (_p)
            {
                case 0:
                    {
                        res = "Android";
                        break;
                    }
                case 1:
                    {
                        res = "IOS";
                        break;
                    }
                case 2:
                    {
                        res = "WP";
                        break;
                    }
            }
            return res;
        }


        public static string GetAppBundle ()
        {
            //return custom_app_bundle; 
            return Application.bundleIdentifier;
        }





        public static Sprite TextureToSprite (Texture2D texture)
        {
            if (texture != null)
                return Sprite.Create ( texture, new Rect ( 0, 0, texture.width, texture.height ), new Vector2 ( 0.5f, 0.5f ) );
            else
                return null;
        }

    }
}