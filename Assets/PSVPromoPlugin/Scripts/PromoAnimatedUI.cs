﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using PSV_Prototype;
using System;

namespace PromoPlugin
{
	public class PromoAnimatedUI :MonoBehaviour, IExternalLink
	{
		private const string promo_items_limit_param = "PROMO_ITEMS_LIMIT";

		//params
		[Header ( "Buttons settings" )]
		public int
			max_buttons_to_show = 10;
		public float
			button_interval = 10f,
			weight_scale = 0.25f;

		[Header ( "Scroll settings" )]
		public float
			scroll_inertia = 0.3f;
		public float
			swipe_threshold = 20,
			max_swipe_forse = 10,
			scroll_acceleration = 15f,
			rotation_speed_scale = 0.5f,
			scroll_speed = 50f;

		//variables
		private int
			current_item;
		private Vector2
			touch_pos = Vector2.zero,
			scroll_direction = Vector2.down;
		private bool
			focus = false;
		private float
			last_direction = 1,
			my_time_scale = 1;
		List<PackageData>
			items = new List<PackageData> ( );
		List<float>
			items_weight = new List<float> ( );
		List<int>
			items_order = new List<int> ( );

		//objects
		[Header ( "Objects" )]
		public RectTransform
			scroller;
		public PromoButton []
			btn;
		public RectTransform []
			chains;
		public RectTransform []
			rotate_obj;
		private RectTransform []
			btn_r;

		private CanvasGroup
			canvas_group;

		void Awake ()
		{
			btn_r = new RectTransform [btn.Length];
			for (int i = 0; i < btn.Length; i++)
			{
				btn_r [i] = btn [i].GetComponent<RectTransform> ( );
			}
			canvas_group = GetComponent<CanvasGroup> ( );
			if (canvas_group == null)
			{
				canvas_group = this.gameObject.AddComponent<CanvasGroup> ( );
			}

			Canvas [] c = GetComponentsInParent<Canvas> ( );
			for (int i = 0; i < c.Length; i++)
			{
				c [i].pixelPerfect = false;
			}
		}




		void OnEnable ()
		{
#if UNITY_STANDALONE
			gameObject.SetActive ( false );
#endif
			int _max_buttons_to_show = 0;
			if (int.TryParse ( FirebaseManager.GetRemoteParam ( promo_items_limit_param ), out _max_buttons_to_show ))
			{
				max_buttons_to_show = _max_buttons_to_show;
			}
			
			if (ServiceUtils.IsMobilePlatform ( ))
			{
				StartCoroutine ( InitButtons ( ) );
				CreateLink ( );
				Subscribe ( );
			}
			else
			{
				gameObject.SetActive ( false );
			}
		}

		void OnDisable ()
		{
			if (ServiceUtils.IsMobilePlatform ( ))
			{
				DestroyLink ( );
			}
		}

		private void Subscribe ()
		{
			PromoModule.OnPackageReceived += GenerateButton;
		}

		private void Unsubscribe ()
		{
			PromoModule.OnPackageReceived -= GenerateButton;
		}


		IEnumerator InitButtons ()
		{
			yield return new WaitForEndOfFrame ( );
			Init ( );
		}



		void HidePromo (bool hide)
		{
			canvas_group.alpha = hide ? 0 : 1;
		}

		void Update ()
		{
			if (!focus && my_time_scale != 1)
			{
				my_time_scale = Mathf.Lerp ( my_time_scale, Mathf.Sign ( my_time_scale ), Time.deltaTime * scroll_inertia );
				if (Mathf.Abs ( my_time_scale ) < 1.01f)
					my_time_scale = Mathf.Sign ( my_time_scale );
			}
			Animate ( );
		}

		public void OnPointerDown (BaseEventData e)
		{
			focus = true;
			PointerEventData pointerData = e as PointerEventData;
			Vector2 point;
			RectTransformUtility.ScreenPointToLocalPointInRectangle ( scroller, pointerData.position, Camera.main, out point );
			//for touch controll
			//my_time_scale = -Mathf.Clamp ( point.y, scroller.rect.yMin, scroller.rect.yMax ) / scroller.rect.yMax * scroll_acceleration;

			//for swipe
			touch_pos = point;
		}


		public void OnDrag (BaseEventData e)
		{
			if (focus)
			{
				PointerEventData pointerData = e as PointerEventData;
				Vector2 point;
				RectTransformUtility.ScreenPointToLocalPointInRectangle ( scroller, pointerData.position, Camera.main, out point );
				//for touch controll
				//my_time_scale = -Mathf.Clamp ( point.y, scroller.rect.yMin, scroller.rect.yMax ) / scroller.rect.yMax * scroll_acceleration;

				//for swipe
				ProcessSwipe ( point );
			}
		}


		void ProcessSwipe (Vector2 point)
		{
			float swipe_dist = Vector2.Distance ( point, touch_pos );
			if (swipe_dist > swipe_threshold)
			{
				focus = false;
				my_time_scale = Mathf.Clamp ( -Mathf.Clamp ( (scroll_acceleration * 0.1f * (swipe_dist / swipe_threshold)), 0, scroll_acceleration ) * Mathf.Sign ( point.y - touch_pos.y ) + my_time_scale, -max_swipe_forse, max_swipe_forse );
			}
		}


		public void OnPointerUp (BaseEventData e)
		{
			if (focus)
			{
				PointerEventData pointerData = e as PointerEventData;
				Vector2 point;
				RectTransformUtility.ScreenPointToLocalPointInRectangle ( scroller, pointerData.position, Camera.main, out point );
				ProcessSwipe ( point );
				focus = false;
			}
		}


		void Init ()
		{
			//Debug.Log ( "Init" );
			items.Clear ( );
			if (PromoModule.Instance)
			{
				items.AddRange ( PromoModule.Instance.GetPromoItems ( max_buttons_to_show ) );
			}
			else
			{
				Debug.LogError ( "PromoModule: PromoModule is not present on the scene" );
			}
			items_weight.Clear ( );
			for (int i = 0; i < items.Count; i++)
			{
				items_weight.Add ( PromoModule.Instance.GetAppWeight ( items [i].app_bundle ) );
			}
			ReorderItems ( );
			current_item = 0;
			for (int i = 0; i < btn_r.Length; i++)
			{
				btn_r [i].anchoredPosition = scroll_direction * (((btn_r [i].rect.height + button_interval) * -i));
				if (items_order.Count > 0)
				{
					if (i == 0)
					{
						btn [i].Init ( GetItem ( current_item ) );
					}
					else
					{
						SwitchButton ( i );
					}
				}
			}

			if (PromoModule.debug)
			{
				PrintPromoSet ( items );
			}

		}


		private PackageData GetItem (int index)
		{
			return items [items_order [index]];
		}


		void ReorderItems ()
		{
			items_order.Clear ( );
			float max_weight = float.MinValue;
			float min_weight = float.MaxValue;
			List<int> repeatings = new List<int> ( );
			for (int i = 0; i < items_weight.Count; i++)
			{
				if (items_weight [i] > max_weight)
					max_weight = items_weight [i];
				if (items_weight [i] < min_weight)
					min_weight = items_weight [i];
			}
			int max_repeat = Mathf.CeilToInt ( items_weight.Count * weight_scale );
			for (int i = 0; i < items_weight.Count; i++)
			{
				int q = Mathf.CeilToInt ( Mathf.Clamp ( (items_weight [i]) / max_weight * max_repeat, 1, max_repeat ) );
				repeatings.Add ( q );
			}
			for (int i = 0; i < items.Count; i++)
			{
				items_order.Add ( i );
			}
			for (int i = 0; i < repeatings.Count; i++)
			{
				if (repeatings [i] > 1)
				{
					int dist = Mathf.FloorToInt ( items_order.Count / repeatings [i] );
					int index = items_order.FindIndex ( X => X == i );
					if (index >= 0)
					{
						for (int j = 0; j < repeatings [i] - 1; j++)
						{
							int next_index = (index + ((j + 1) * dist) + j) % items_order.Count;
							items_order.Insert ( next_index, i );
						}
					}
					else
					{
						Debug.LogError ( "PromoModule: " + items [i].app_bundle + " not found in repeatings" );
					}
				}
			}
			if (PromoModule.debug)
			{
				string dbg = "Repeatings:";
				for (int i = 0; i < items_order.Count; i++)
				{
					dbg += "\n" + GetItem ( i ).app_bundle;
				}
				Debug.Log ( "PromoModule: " + dbg );
			}
		}



		void PrintPromoSet (List<PackageData> set)
		{
			string res = "PromoUI GetItems(), received items " + items.Count + "\n";
			for (int i = 0; i < set.Count; i++)
			{
				res += "| " + set [i].app_bundle + " | " + set [i].app_alias + " | " + set [i].app_name + "|\n";
			}
			Debug.Log ( "PromoModule: " + res );
		}


		void Animate ()
		{
			Vector2 offset = scroll_direction * scroll_speed * my_time_scale * Time.deltaTime;
			for (int i = 0; i < btn_r.Length; i++)
			{
				if (ApplyOffset ( i, offset ))
				{
					//change icon
					SwitchButton ( i );
					for (int j = 1; j < btn_r.Length - 1; j++)
					{
						int next_ind = (int) Mathf.Repeat ( i + j * (int) Mathf.Sign ( my_time_scale ), btn.Length );
						RepositionButton ( next_ind );
						SwitchButton ( next_ind );
					}
				}
			}
			for (int i = 0; i < rotate_obj.Length; i++)
			{
				ApplyRotation ( rotate_obj [i], offset );
			}
		}


		void ApplyRotation (RectTransform obj, Vector2 offset)
		{
			obj.Rotate ( 0, 0, offset.y * rotation_speed_scale );
		}

		bool ApplyOffset (int index, Vector2 offset)
		{
			bool res = false;
			Vector2 pos = btn_r [index].anchoredPosition + offset;
			btn_r [index].anchoredPosition = pos;
			MoveChain ( index );
			if (Mathf.Abs ( pos.y ) > (btn_r [index].rect.height + button_interval) * (btn_r.Length - 1))
			{
				res = true;
				RepositionButton ( index );
			}
			return res;
		}


		void MoveChain (int index)
		{
			if (chains.Length > index)
			{
				chains [index].anchoredPosition = btn_r [index].anchoredPosition - Vector2.down * btn_r [index].rect.height;
			}
		}

		void RepositionButton (int index)
		{
			Vector2 pos = btn_r [index].anchoredPosition;
			pos.y += -Mathf.Sign ( pos.y ) * ((btn_r [index].rect.height + button_interval) * btn_r.Length);
			btn_r [index].anchoredPosition = pos;
			MoveChain ( index );
		}

		void GenerateButton (PackageData data)
		{
			if (!items.Contains ( data ))
			{
				items.Add ( data );
				items_weight.Add ( PromoModule.Instance.GetAppWeight ( data.app_bundle ) );
				ReorderItems ( );
			}
		}




		public void SwitchButton (int index)
		{
			if (items_order.Count > 0)
			{
				current_item = GetNextItem ( );

				if (last_direction != Mathf.Sign ( my_time_scale ))
				{
					for (int i = 1; i < btn_r.Length; i++)
					{
						current_item = GetNextItem ( );
					}
				}
				last_direction = Mathf.Sign ( my_time_scale );
				btn [index].Init ( GetItem ( current_item ) );
			}
		}

		int GetNextItem ()
		{
			if (items_order.Count > 0)
				return (int) Mathf.Repeat ( current_item + (-Mathf.Sign ( scroll_direction.y * my_time_scale )), items_order.Count );
			else
				return -1;
		}

		#region External link implementatition
		public bool IsStatic
		{
			get
			{
				return true;
			}
		}

		public void Show (bool param)
		{
			HidePromo ( !param );
		}

		public void CreateLink ()
		{
			ExternalLinksManager.Instance.AddLink ( this );
		}

		public void DestroyLink ()
		{
			ExternalLinksManager.Instance.DeleteLink ( this );
		}
		#endregion
	}
}