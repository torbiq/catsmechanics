﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PromoPlugin
{
    public class MarketBridge :MonoBehaviour
    {
        public static MarketBridge Instance;

        public delegate void Action (string app_bundle, PackageData info);
        public delegate void EditorAction (PackageData info);
        public static event Action OnDataReceived;

        private float
            mmin_texture_height = 100f;

        private int
            in_progress = 0;

        private const string
            protocol_alias = "http:",
            android_app_icon_tag = "<img class=\"cover-image\" src=\"",
            android_app_icon_tag_close = "\"",
            ios_app_icon_header = "class=\"lockup product application",
            ios_app_icon_tag = "src-swap-high-dpi=\"",
            ios_app_icon_tag_close = "\"",
            wp_app_icon_tag_1 = "<img class=\"cli_image m-b-lg srv_screenshot srv_microdata\" itemprop=\"screenshot\" src=\"",
            wp_app_icon_tag_close_1 = "?w=100&amp;h=100&amp;q=60",
            wp_app_icon_tag_2 = "<meta property=\"og:image\" content=\"",
            wp_app_icon_tag_close_2 = "?w=150&amp;h=150&amp;q=60",
            android_app_name_tag = "<div class=\"id-app-title\" tabindex=\"0\">",
            android_app_name_tag_close = "</div>",
            ios_app_name_tag = "<h1 itemprop=\"name\">",
            ios_app_name_tag_close = "</h1>",
            wp_app_name_tag = "<h1 id=\"page-title\" class=\"header-small m-v-n srv_title srv_microdata\" itemprop=\"name\">",
            wp_app_name_tag_close = "</h1>",
            android_market_url = "https://play.google.com/store/apps/details?id=",
            //ios_market_url = "https://itunes.apple.com/en/app/",
            ios_market_url_1 = "https://itunes.apple.com/",
            ios_market_url_2 = "app/",
            //wp_market_url = "http://windowsphone.com/s?appid=",
            wp_market_url_1 = "https://www.microsoft.com/",
            wp_market_url_2 = "store/phoneappid/",
            android_makret_app_url = "market://details?id=",
            ios_market_app_url = "itms-apps://itunes.apple.com/app/",
            wp_market_app_url = "ms-windows-store:navigate?appid=";


        void Awake ()
        {
            if (!Instance)
            {
                Instance = this;
            }
        }




        public void EnqueItems (PackageInfo [] package_info, float delay = 0, bool ovverride_locale = false)
        {
            if (in_progress <= 0)
            {
                for (int i = 0; i < package_info.Length; i++)
                {
                    StartCoroutine ( GetPackageDataOneShot ( package_info [i], ovverride_locale: ovverride_locale, delay: i * 0.1f ) );
                }
            }
            else
            {
                if (PromoModule.debug)
                    Debug.Log ( "PromoModule: There still " + in_progress + " downloads in progress" );
            }
        }

        public static string GetMarketURL (string app_alias, bool locale_override)
        {
            string res = "";
            switch (ServiceUtils.GetPlatform ( ))
            {
                case 0:
                    {
                        res = android_market_url + app_alias + (locale_override ? "&hl=" + ServiceUtils.GetCurrentLocale ( ) : "");
                        break;
                    }
                case 1:
                    {
                        res = ios_market_url_1 + (locale_override ? ServiceUtils.GetCurrentLocale ( ) + "/" : "") + ios_market_url_2 + app_alias;
                        break;
                    }
                case 2:
                    {
                        res = wp_market_url_1 + (locale_override ? ServiceUtils.GetCurrentLocale ( ) + "/" : "") + wp_market_url_2 + app_alias;
                        break;
                    }
            }
            return res;

            //#if UNITY_ANDROID
            //        return android_market_url + app_alias + (locale_override ? "&hl=" + ServiceUtils.GetCurrentLocale ( ) : "");
            //#elif UNITY_IPHONE
            //        return ios_market_url_1 + (locale_override ? ServiceUtils.GetCurrentLocale ( ) + "/" : "") + ios_market_url_2 + app_alias;
            //#elif UNITY_WP8 || UNITY_WP8_1
            //        return wp_market_url_1 + (locale_override ? ServiceUtils.GetCurrentLocale ( ) + "/" : "") + wp_market_url_2 + app_alias;
            //#endif
        }

        public static string GetAppMarketURL (string app_alias)
        {
            string res = "";
            switch (ServiceUtils.GetPlatform ( ))
            {
                case 0:
                    {
                        res = android_makret_app_url + app_alias;
                        break;
                    }
                case 1:
                    {
                        res = ios_market_app_url + app_alias;
                        break;
                    }
                case 2:
                    {
                        res = wp_market_app_url + app_alias;
                        break;
                    }
            }
            return res;


            //#if UNITY_ANDROID
            //        return android_makret_app_url + app_alias;
            //#elif UNITY_IPHONE
            //        return ios_market_app_url + app_alias;
            //#elif UNITY_WP8 || UNITY_WP8_1
            //        return wp_market_app_url + app_alias;
            //#endif
        }



        public static string FormatAppAlias (string app_alias)
        {
            return app_alias;
        }

        public static string GetIconTag (bool close)
        {
            string res = "";
            switch (ServiceUtils.GetPlatform ( ))
            {
                case 0:
                    {
                        res = close ? android_app_icon_tag_close : android_app_icon_tag;
                        break;
                    }
                case 1:
                    {
                        res = close ? ios_app_icon_tag_close : ios_app_icon_tag;
                        break;
                    }
                case 2:
                    {
                        res = close ? wp_app_icon_tag_close_2 : wp_app_icon_tag_2;
                        break;
                    }
            }
            return res;

            //#if UNITY_ANDROID
            //        return close ? android_app_icon_tag_close : android_app_icon_tag;
            //#elif UNITY_IPHONE
            //        return close ? ios_app_icon_tag_close : ios_app_icon_tag;
            //#elif UNITY_WP8 || UNITY_WP8_1
            //        return close ? wp_app_icon_tag_close : wp_app_icon_tag;
            //#endif
        }

        public static string GetNameTag (bool close)
        {
            string res = "";
            switch (ServiceUtils.GetPlatform ( ))
            {
                case 0:
                    {
                        res = close ? android_app_name_tag_close : android_app_name_tag;
                        break;
                    }
                case 1:
                    {
                        res = close ? ios_app_name_tag_close : ios_app_name_tag;
                        break;
                    }
                case 2:
                    {
                        res = close ? wp_app_name_tag_close : wp_app_name_tag;
                        break;
                    }
            }
            return res;

            //#if UNITY_ANDROID
            //        return close ? android_app_name_tag_close : android_app_name_tag;
            //#elif UNITY_IPHONE
            //        return close ? ios_app_name_tag_close : ios_app_name_tag;
            //#elif UNITY_WP8 || UNITY_WP8_1
            //        return close ? wp_app_name_tag_close : wp_app_name_tag;
            //#endif
        }

        public IEnumerator GetPackageDataOneShot (PackageInfo package_info, EditorAction call = null, bool announce = true, bool ovverride_locale = false, float delay = 0)
        {
            if (delay > 0)
            {
                yield return new WaitForSeconds ( delay );
            }
            string URL = GetMarketURL ( package_info.app_alias, ovverride_locale );
            WWW www = new WWW ( URL );
            string app_name = "";
            Texture2D icon = null;
            yield return www;
            if (!string.IsNullOrEmpty ( www.error ))
            {
                //  something went wrong
                if (PromoModule.debug)
                    Debug.LogWarning ( "PromoModule: Request error=" + www.error + "\nURL=" + URL );
            }
            else
            {
                if (www.text.Length > 0)
                {
                    //find app name
                    if (PrimarySettings.receive_app_name)
                    {
                        app_name = GetApplicationName ( www.text );
                    }

                    //load icon
                    string icon_url = GetIconURL ( www.text );
                    www = new WWW ( icon_url );
                    yield return www;
                    if (!string.IsNullOrEmpty ( www.error ))
                    {
                        if (PromoModule.debug)
                            Debug.LogWarning ( "PromoModule: Request error URL=" + icon_url + " error = " + www.error );
                    }
                    else
                    {
                        if (www.texture.width >= mmin_texture_height)
                            icon = www.texture;
                    }
                }
            }
            if (announce && OnDataReceived != null)
            {
                OnDataReceived ( package_info.app_bundle, new PackageData ( package_info.app_bundle, package_info.app_alias, app_name, icon ) );
            }
            if (call != null)
            {
                call ( new PackageData ( package_info.app_bundle, package_info.app_alias, app_name, icon ) );
            }
        }


        static string GetApplicationName (string txt)
        {
            //load app name
            string tmp = GetNameTag ( false );
            int start = txt.IndexOf ( tmp ) + tmp.Length;
            if (start >= 0)
            {
                int end = txt.IndexOf ( GetNameTag ( true ), start );

                if (end > start)
                {
                    return txt.Substring ( start, end - start );
                }
            }
            return "";
        }


        static string GetIconURL (string txt)
        {
            string icon_url = "";

            string tmp = GetIconTag ( false );
            int from = ServiceUtils.GetPlatform ( ) == 1 ? txt.IndexOf ( ios_app_icon_header ) : 0;

            int start = txt.IndexOf ( tmp, from ) + tmp.Length;

            if (start >= 0)
            {
                int end = txt.IndexOf ( GetIconTag ( true ), start );
                if (end > start)
                {
                    icon_url = FormatIconURL ( txt.Substring ( start, end - start ) );
                    if (ServiceUtils.GetPlatform ( ) == 0) //to get png file on Andoid PLayMarket should remove ending -rw
                    {
                        icon_url = icon_url.Replace ( "=w300-rw", "=w300" );
                    }
                }
            }
            return icon_url;
        }


        public static string FormatIconURL (string found_url)
        {
            //#if UNITY_ANDROID || UNITY_WP8 || UNITY_WP8_1 
            if (ServiceUtils.GetPlatform ( ) != 1)
            {
                found_url = protocol_alias + found_url;
            }
            //#endif
            return found_url;
        }

    }
}