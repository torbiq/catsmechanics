﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace PromoPlugin
{

    [RequireComponent ( typeof ( RectTransform ) )]
    public class PannelResizer :MonoBehaviour
    {

        public Vector2
            max_size = new Vector2 ( 65, 315 );

        private RectTransform
            pannel,
            scroller,
            scroll_content;

        private VerticalLayoutGroup
            vert_layout;

        private HorizontalLayoutGroup
            hor_layout;

        private ScrollRect
            scroll_rect;


        void Awake ()
        {
            scroll_rect = GetComponentInChildren<ScrollRect> ( );
            if (scroll_rect == null)
            {
                Debug.LogError ( "Component ScrollRect is absent in child" );
            }
            else
            {
                pannel = GetComponent<RectTransform> ( );
                scroller = scroll_rect.GetComponent<RectTransform> ( );
                scroll_content = scroll_rect.content;
                vert_layout = GetComponentInChildren<VerticalLayoutGroup> ( );
                hor_layout = GetComponentInChildren<HorizontalLayoutGroup> ( );
            }
        }

        void Start ()
        {
            ResizePannel ( );
        }

        public void ResizePannel ()
        {
            StartCoroutine ( WaitFrameAndResize ( ) );
        }

        void SetPannelSize ()
        {
            Vector2 new_size = pannel.sizeDelta;
            Vector2 borders = new Vector2 ( Mathf.Abs ( scroller.offsetMax.x ) + Mathf.Abs ( scroller.offsetMin.x ), Mathf.Abs ( scroller.offsetMax.y ) + Mathf.Abs ( scroller.offsetMin.y ) );

            Vector2 paddings = new Vector2 ( (hor_layout != null ? hor_layout.padding.left + hor_layout.padding.right : 0), (vert_layout != null ? vert_layout.padding.top + vert_layout.padding.bottom : 0) );

            float content_width = scroll_content.sizeDelta.x - paddings.x > 0 ? (scroll_content.sizeDelta.x + borders.x) : 0;
            float content_height = scroll_content.sizeDelta.y - paddings.y > 0 ? (scroll_content.sizeDelta.y + borders.y) : 0;

            new_size.x = max_size.x == 0 ? new_size.x : (max_size.x > content_width ? content_width : max_size.x);
            new_size.y = max_size.y == 0 ? new_size.y : (max_size.y > content_height ? content_height : max_size.y);
            pannel.sizeDelta = new_size;
            if (pannel.localPosition.y != 0)
            {
                Vector3 pos = pannel.localPosition;
                pos.y = Mathf.Sign ( pannel.localPosition.y ) * pannel.sizeDelta.y;
                pannel.localPosition = pos;
            }
        }


        IEnumerator WaitFrameAndResize ()
        {
            yield return new WaitForEndOfFrame ( );
            SetPannelSize ( );
        }

    }
}