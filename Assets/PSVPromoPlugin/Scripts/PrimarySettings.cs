﻿using UnityEngine;
using System.Collections.Generic;
using PSV_Prototype;

namespace PromoPlugin
{
	public static class PrimarySettings
	{
		public static bool
			show_banner_on_disable = false, //determines if small banner will be shown after bromo scene will be hidden (by default small banner should be hidden on promo scene)
			announce_local = true, //Loading resources will take some time and if promo scene will go first promo set won't be ready. This option will fix that
			receive_app_name = false, //determines whether script will search for app_name on application's page from market (app_name is optional variable)
			use_locale_override = false; //determines wether to use locale that is detected by market (usually by device location) or ovverride it in accordence to device language and supported_languages

		//this string is ussed to open publishers account by pressing more games button
		public static string
			default_acc = "Hippo+Kids+Games";

		//this list is used to display set of applications in promo by default (when no connection was performed yet)
		public static List<PackageInfo>
			primary_set = new List<PackageInfo> ( ) {
#if UNITY_ANDROID
				{ new PackageInfo("com.kidgame.supermarket",                "com.kidgame.supermarket",                          "supermarket") },
				{ new PackageInfo("com.psvn.traumatologist",                "com.psvn.traumatologist",                          "traumatologist") },
				{ new PackageInfo("com.PSVGamestudio.HippoSchoolBus",       "com.PSVGamestudio.HippoSchoolBus",                 "HippoSchoolBus") },
				{ new PackageInfo("com.PSVStudio.HippoMinigames",           "com.PSVStudio.HippoMinigames",                     "HippoMinigames") },
				{ new PackageInfo("com.PSV.Baby_Cooking_School",            "com.PSV.Baby_Cooking_School",                      "Baby_Cooking_School") },

#elif UNITY_IPHONE

				{ new PackageInfo("com.blueskywater.supermarket",			"id1031486293",										"supermarket") },
				{ new PackageInfo("com.PSV.BabyCookingSchool",				"id1161119568",										"BabyCookingSchool") },
				{ new PackageInfo("com.PSVStudio.KidsMiniGames",			"id1033482266",										"KidsMiniGames") },
				{ new PackageInfo("com.PSVGamestudio.BeachFamilyBusiness",	"id1164932277",										"BeachFamilyBusiness") },
				{ new PackageInfo("com.PSV.RedRidingHood",					"id1167957824",										"RedRidingHood") },
#endif
			};

		//this list determines what device languages will be processed by promo tho load add_name in this locale (other languages will be recognised as English)
		public static List<SystemLanguage>
			supported_languages = new List<SystemLanguage> ( )
			{
				SystemLanguage.English, //always leave this (used by default)
				SystemLanguage.Russian,
				SystemLanguage.Ukrainian,
			};

	}
}