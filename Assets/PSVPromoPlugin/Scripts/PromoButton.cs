﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace PromoPlugin
{

    [RequireComponent ( typeof ( Image ) )]
    public class PromoButton :MonoBehaviour
    {
        private string
            app_bundle = "",
            app_alias = "",
            app_name = "";

        private Text
            label;

        private Image
            img;


        void Awake ()
        {
            img = GetComponent<Image> ( );
            label = GetComponentInChildren<Text> ( );
        }

        public void Init (PackageData data)
        {
            if (!img || !label)
            {
                Awake ( );
            }

            app_bundle = data.app_bundle;
            app_alias = data.app_alias;
            app_name = data.app_name;
            if (img != null)
            {
                if (data.app_icon != null)
                    img.sprite = ServiceUtils.TextureToSprite ( data.app_icon );
                else
                    Debug.Log ( "PromoModule: " + app_bundle + " has null icon" );
            }
            else
            {
                Debug.Log ( "PromoModule: " + name + " img is null" );
            }
            if (label != null)
            {
                label.text = app_name;
            }
        }


        public void Action ()
        {
            if (!string.IsNullOrEmpty ( app_alias ))
            {
                string url = MarketBridge.GetAppMarketURL ( app_alias );
                if (!string.IsNullOrEmpty ( url ))
                {
                    if (PromoModule.debug)
                        Debug.Log ( "PromoModule: " + url );
                    Application.OpenURL ( url );
                    PSV_Prototype.AnalyticsManager.LogEvent ( PSV_Prototype.AnalyticsEvents.OpenPromo, app_bundle );
                }
            }
            else
            {
                if (PromoModule.debug)
                {
                    Debug.LogError ( "PromoModule: Action error: package_name is empty" );
                }
            }
        }

        public string GetAppBundle ()
        {
            return app_bundle;
        }
    }
}