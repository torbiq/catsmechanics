﻿//#define FIREBASE_PRESENT
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using PSV_Prototype;

/*
version 3 
    - implemented better plafotrm override for testing capabilities 
    - implemented market page licale override according to language presets
    - implemented settings for disabling app name search
    - implemented version tracking
version 4
    - implemented announce for local data (needed for games with menu_scene(with ui_promo) going first (without splash screen))
    - added option to enable local announce
version 5
    - moved some settings to external class for better usability
    - added animated moreGames button for hippo games
    - due to need to scale buttons prefabs were et to bigger size
version 6
    - added standalone UI module with stationary set of buttons that can be placed anywhere
version 7
    - added promo box prefab with scrolling promo buttons
    - added label "ADS" at the top of wooden frame
version 8
    - changed algorithm of ReceivePromoSet: if there is no loaded data only one primary_set icon will be shown by PromoUI prefab
version 9
    - added compatibility for old prefabs
version 10
    - added compatibility for old prefabs
    - added label "ADS" at the top of wooden frame
version 12
    - updated for WSA compatibility
    - implemented namespace PromoModule
version 13
    - updated analytics manager to support firebase
version 14
	- implemented firebase mode and psv_server mode as switchable param through remoteconfig
	- implemented receiveing promoset from firebase database using db_url got from remoteconfig
	- implemented receiving publisher name from remoteconfig
	- implemented promo_items_limit_param to get from remoteconfig (PromoAnimatedUI.cs)
	- splitted spritesheets into separate sprites and tagged them for SpritePacker (SettingsPanel, QuitDialogue, LangPannel)
	- updated push plugin - canvas only news prefab
	- push sprites are splitted in separate files
*/


namespace PromoPlugin
{

	public class PromoModule :MonoBehaviour
	{
		public static PromoModule Instance;

		public delegate void BannerTimeUpdate (float val);
		public static event BannerTimeUpdate OnBannerTimeUpdate;


		public delegate void Action (PackageData data);
		public static event Action OnPackageReceived;

		public delegate void Callback (string acc_alias);
		public static event Callback OnAccAliasReceived;


		public const int promo_version = 14;

		private static string
			promo_db_param = "PROMO_DB",
			current_publisher_param = "CURRENT_PUBLISHER",
			operating_mode_param = "PROMO_OPERATING_MODE";

		//params
		public static bool
			firebase_mode = false, //psv_server mode by default
			debug = false;
		private bool
			use_new_primary_set_logic = true;   //actual from animated UI (according to new rules anly one icon will be returned if no data received). If you want to get mroe than one icon set this to false

		const string
			default_promo_db = "https://promobase-e46c6.firebaseio.com/",
			promo_set_root_param = "PROMO_SET",
			promo_set_file = "promo_set.json",
			dat_file = "data_set.json";

		//variables
		private bool
			blocking_access = false;

		private float
			acc_alias_update_interval = 5,
			promo_set_update_interval = 1,
			retry_delay = 60f,
			min_weight = 0.01f,
			banner_time = -1;

		private int
			max_retry_efforts = 5,
			retry_efforts = 0,
			faulty_downloads = 0,
			downloads_in_progress = 0;


		private string
			publisher = "";

		//variables
		private List<PromoItem>
			promo_set = new List<PromoItem> ( );


		List<PackageData>
			available_set;

		Coroutine
			delayed_download = null;

		//methods

		void OnEnable ()
		{
			MarketBridge.OnDataReceived += ProcessData;
		}


		void OnDisable ()
		{
			MarketBridge.OnDataReceived -= ProcessData;
		}


		void Awake ()
		{
			if (!Instance)
			{
				Instance = this;
				DontDestroyOnLoad ( this );
				CheckVersion ( );
			}
			else
			{
				Debug.LogWarning ( "Not allowed multiple instances of PromoModule" );
				Destroy ( this.gameObject );
			}
		}

		public float GetBannerTime ()
		{
			if (banner_time < 0)
			{
				banner_time = PlayerPrefs.GetFloat ( "PromoBannerTime", 30f );
			}
			return banner_time;
		}


		void ProcessNewBannerTime (string val)
		{
			float new_time;
			if (float.TryParse ( val, out new_time ))
			{
				banner_time = new_time;
				PlayerPrefs.SetFloat ( "PromoBannerTime", banner_time );
				PlayerPrefs.Save ( );
				if (OnBannerTimeUpdate != null)
				{
					OnBannerTimeUpdate ( banner_time );
				}

				if (debug)
				{
					Debug.Log ( "PromoModule: banner_time=" + val );
				}
			}
		}


		public float GetAppWeight (string app_bundle)
		{
			float res = min_weight;
			int index = promo_set.FindIndex ( X => X.app_bundle.Equals ( app_bundle ) );
			if (index >= 0)
			{
				res = (float) promo_set [index].app_weight;
			}
			return res;
		}

		void CheckVersion ()
		{
			int curent_version = PlayerPrefs.GetInt ( "PromoVersion", -1 ); //due to all previos versions didn't save this value we use -1 by default for all versions bellow 3
																			//fixes local data for versions below 3
			if (curent_version < promo_version)
			{
				//make some stuff to fix differences in saved data between versions


				//to use new implementation of market locale override we have to remove all saved app aliases
				if (curent_version < 3 && PrimarySettings.receive_app_name && PrimarySettings.use_locale_override)
				{
					for (int i = 0; i < PrimarySettings.primary_set.Count; i++)
					{
						PlayerPrefs.DeleteKey ( PrimarySettings.primary_set [i].app_bundle );
					}
					//removing old app names received in wrong locale
					ClearLocalData ( );
				}

				//save version of saved data
				PlayerPrefs.SetInt ( "PromoVersion", promo_version );
				PlayerPrefs.Save ( );
			}

		}


		void ClearLocalData ()
		{
			string dat_path = GetExternalPath ( ) + dat_file;
			if (File.Exists ( dat_path ))
			{
				File.Delete ( dat_path );
			}
		}


		void Start ()
		{
			Init ( );
		}


		public void Init ()
		{
			string operating_mode_val = PSV_Prototype.FirebaseManager.GetRemoteParam ( operating_mode_param );
			//will change current mode only if it takes one of two allowed values
			if (!string.IsNullOrEmpty ( operating_mode_val ) || operating_mode_val == "firebase" || operating_mode_val == "psv")
			{
				firebase_mode = operating_mode_val == "firebase";
			}


			UpdatePrimarySetNames ( );

			if (firebase_mode)
			{
				//receiving publisher id from firebase remoteconfig server
				publisher = PSV_Prototype.FirebaseManager.GetRemoteParam ( current_publisher_param );
				if (string.IsNullOrEmpty ( publisher ))
				{
					publisher = PrimarySettings.default_acc;
				}
			}
			else
			{
				//getting publisher id stored locally (updated when answer from psv server is received)
				publisher = PlayerPrefs.GetString ( "Publisher", PrimarySettings.default_acc );
			}

			if (!Directory.Exists ( GetExternalPath ( ) ))
			{
				Directory.CreateDirectory ( GetExternalPath ( ) );
			}

			//performing init before there will be requests to Instance
			LoadLocalResources ( );

			LoadPromoSet ( );

			//LoadAccAlias ( ); //deprecated
		}


		void UpdatePrimarySetNames ()
		{
			if (PrimarySettings.use_locale_override)
			{
				//List<PackageInfo> queue = new List<PackageInfo> ( );
				for (int i = 0; i < PrimarySettings.primary_set.Count; i++)
				{
					string app_name = PlayerPrefs.GetString ( PrimarySettings.primary_set [i].app_bundle, "" );
					if (app_name != "")
					{
						PrimarySettings.primary_set [i] = new PackageInfo ( PrimarySettings.primary_set [i].app_bundle, PrimarySettings.primary_set [i].app_alias, app_name );
					}
					else
					{
						//queue.Add ( PrimarySettings.primary_set [i] );
						StartCoroutine ( MarketBridge.Instance.GetPackageDataOneShot ( PrimarySettings.primary_set [i], ProcessPrimarySetData, false, PrimarySettings.use_locale_override ) );
					}
				}
			}
		}


		void ProcessPrimarySetData (PackageData data)
		{
			if (!string.IsNullOrEmpty ( data.app_name ))
			{
				PlayerPrefs.SetString ( data.app_bundle, data.app_name );
				PlayerPrefs.Save ( );
				int index = PrimarySettings.primary_set.FindIndex ( X => X.app_bundle.Equals ( data.app_bundle ) );
				if (index >= 0)
				{
					if (debug)
					{
						Debug.Log ( "PromoModule: Appname has changed from " + PrimarySettings.primary_set [index].app_name + " to " + data.app_name );
					}
					PrimarySettings.primary_set [index] = new PackageInfo ( data.app_bundle, data.app_alias, data.app_name );
				}

				index = available_set.FindIndex ( X => X.app_bundle.Equals ( data.app_bundle ) );
				if (index >= 0)
				{
					if (debug)
					{
						Debug.Log ( "PromoModule: Available set app name has changed from " + available_set [index].app_name + " to " + data.app_name );
					}
					available_set [index].app_name = data.app_name;
				}
			}
		}


		public void LoadPrimarySetIcons ()
		{
#if UNITY_EDITOR
			for (int i = 0; i < PrimarySettings.primary_set.Count; i++)
			{
				StartCoroutine ( MarketBridge.Instance.GetPackageDataOneShot ( PrimarySettings.primary_set [i], ProcessEditorData, false, false ) );
			}
#endif
		}


		void ProcessEditorData (PackageData data)
		{
			string path = Application.dataPath + "/Resources/PSVPromo/";
			if (!Directory.Exists ( path ))
			{
				Directory.CreateDirectory ( path );
			}
			if (data != null && !string.IsNullOrEmpty ( data.app_bundle ) && data.app_icon != null)
			{
				File.WriteAllBytes ( path + data.app_bundle + ".png", data.app_icon.EncodeToPNG ( ) );
			}
		}

		public string GetPublisher ()
		{
			return publisher;
		}


		void SetPublisher (string val)
		{
			publisher = val;
			if (debug)
				Debug.Log ( "PromoModule: publisher=" + val );
			PlayerPrefs.SetString ( "Publisher", val );
			PlayerPrefs.Save ( );
			if (OnAccAliasReceived != null)
			{
				OnAccAliasReceived ( val );
			}
		}


		public void LoadPromoSet (string bundle = "", int platform = -1, bool ignore_timer = false)
		{
#if FIREBASE_PRESENT
			if (firebase_mode)
			{
				ReceiveFirebasePromoSet ( bundle, platform );
			}
			else
#endif
			{
				System.DateTime next_promo_update;
				System.DateTime.TryParse ( PlayerPrefs.GetString ( "LastPromoUpd", System.DateTime.Now.ToString ( ) ), out next_promo_update );
				if (next_promo_update <= System.DateTime.Now || ignore_timer)
				{
					StartCoroutine ( ReceivePromoSet ( bundle, platform ) );
				}
			}
		}

		//public void LoadAccAlias (string bundle = "", string platform = "", bool ignore_timer = false) // deprecated
		//{
		//	System.DateTime next_acc_alias_update;
		//	System.DateTime.TryParse ( PlayerPrefs.GetString ( "LastAccAliasUpd", System.DateTime.Now.ToString ( ) ), out next_acc_alias_update );

		//	if (next_acc_alias_update <= System.DateTime.Now || ignore_timer)
		//	{
		//		if (!firebase_mode)
		//		{
		//			StartCoroutine ( ReceiveParams ( bundle, platform ) );
		//		}
		//	}
		//}

		//void SetNextParamsUpdate ()
		//{
		//	PlayerPrefs.SetString ( "LastAccAliasUpd", System.DateTime.Now.AddDays ( acc_alias_update_interval ).AddHours ( Random.Range ( 0, 23 ) ).ToString ( ) );
		//	PlayerPrefs.Save ( );
		//}

		void SetNextPromoUpdate ()
		{
			PlayerPrefs.SetString ( "LastPromoUpd", System.DateTime.Now.AddDays ( promo_set_update_interval ).AddHours ( Random.Range ( 0, 23 ) ).ToString ( ) );
			PlayerPrefs.Save ( );
		}


		void BlockAccess (bool param)
		{
			blocking_access = param;
		}







		void LoadLocalResources ()
		{
			available_set = new List<PackageData> ( );

			//load data from cache (app_names)
			string dat_path = GetExternalPath ( ) + dat_file;
			if (File.Exists ( dat_path ))
			{
				List<PackageInfo> buffer = LitJson.JsonMapper.ToObject<List<PackageInfo>> ( File.ReadAllText ( dat_path ) );
				for (int i = 0; i < buffer.Count; i++)
				{
					string icon_path = GetExternalPath ( ) + buffer [i].app_bundle + ".png";
					if (File.Exists ( icon_path ))
					{
						Texture2D icon = new Texture2D ( 300, 300 );
						icon.LoadImage ( File.ReadAllBytes ( icon_path ) );
						available_set.Add ( new PackageData ( buffer [i], icon ) );
					}
					else
					{
						if (debug)
							Debug.Log ( "PromoModule: File " + icon_path + " does not exist" );
					}
				}
			}

			//load primary data from Assets/Resources/PSVPromo/
			Texture2D [] res = Resources.LoadAll<Texture2D> ( "PSVPromo/" );
			int matched = 0;
			for (int i = 0; i < res.Length; i++)
			{
				int index = PrimarySettings.primary_set.FindIndex ( X => X.app_bundle == res [i].name );
				if (index >= 0)
				{
					available_set.Add ( new PackageData ( PrimarySettings.primary_set [index], res [i] ) );
					matched++;
				}
			}

			if (matched < PrimarySettings.primary_set.Count)
			{
				Debug.LogWarning ( "PromoModule: No precached icons for promary set, caching" );
				LoadPrimarySetIcons ( );
			}

			//load promo_set from local file
			List<PromoItem> set = new List<PromoItem> ( );
			dat_path = GetExternalPath ( ) + promo_set_file;
			if (File.Exists ( dat_path ))
			{
				string _dat = File.ReadAllText ( dat_path );

				//converting string to object
				set = ServiceUtils.GetPlatform ( ) == 2 ?
					//WP has different comma seperator - using own parse method
					WP_JSON_Parse ( _dat ) :
					//if not WP using generic method
					LitJson.JsonMapper.ToObject<List<PromoItem>> ( _dat );
			}

			InitPromoSet ( set, PrimarySettings.announce_local );
		}


		//string convertor for WP platform
		List<PromoItem> WP_JSON_Parse (string src)
		{
			List<PromoItem> res = new List<PromoItem> ( );
			SimpleJSON.JSONArray data = SimpleJSON.JSONNode.Parse ( src ).AsArray;

			for (int i = 0; i < data.Count; i++)
			{
				string app_bundle = data [i] ["app_bundle"];
				string app_alias = data [i] ["app_alias"];
				string app_name = data [i] ["app_name"];
				double app_weight = data [i] ["app_weight"].AsDouble;
				//for WP debug
				//Debug.Log ( app_weight );

				bool is_local = data [i] ["is_local"].AsBool;
				res.Add ( new PromoItem ( app_bundle, app_alias, (float) app_weight, app_name, is_local ) );
			}
			return res;
		}

		//debug method to show availables set contents
		void PrintAvailableSet ()
		{
			string res = "";
			for (int i = 0; i < available_set.Count; i++)
			{
				res += available_set [i].app_bundle + " " + available_set [i].app_name + "\n";
			}
			Debug.Log ( "PromoModule: " + res );
		}


		/// <summary>
		/// Initialise promoSet using local data or data received from DB (can be in pending state (receiving icons))
		/// </summary>
		/// <param name="set">Array of items to check if allowed</param>
		/// <param name="local">Param that tells us if items were taken from local storage or were received from DB, if UI module starts with PromoModule simultaneously use announce local to make changes visible after UI started</param>
		void InitPromoSet (List<PromoItem> set, bool local = false)
		{
			//resulting  set that will contain
			List<PromoItem> res = new List<PromoItem> ( );

			//build set of unused local promo items
			List<int> local_set = new List<int> ( );
			for (int j = 0; j < PrimarySettings.primary_set.Count; j++)
			{
				local_set.Add ( j );
			}


			for (int i = 0; i < set.Count; i++)
			{
				//check item weight and if items package is installed on device and not same as this app
				//find local items
				int index = local_set.FindIndex ( X => PrimarySettings.primary_set [X].app_bundle.Equals ( set [i].app_bundle ) );

				bool is_local = index >= 0;

				//if (is_local)
				//    local_set.RemoveAt ( index );

				//such complicated data asignment is due to LitJSON convertor does not handle classes
				set [i] = new PromoItem ( set [i].app_bundle, set [i].app_alias, set [i].GetPromoWeight ( ), set [i].app_name, is_local );
				if (set [i].GetPromoWeight ( ) > 0 && ServiceUtils.IsAppAllowed ( set [i].app_bundle ))
				{
					res.Add ( set [i] );
				}
			}

			//add to promo set items left unused inside local_set to let us show at least something if loading will fail or non zero weighted apps will be unavailable
			for (int i = 0; i < local_set.Count; i++)
			{
				PackageInfo pkg_info = PrimarySettings.primary_set [local_set [i]];
				if (ServiceUtils.IsAppAllowed ( pkg_info.app_bundle ))
				{
					res.Add ( new PromoItem ( PrimarySettings.primary_set [local_set [i]], min_weight, true ) );
				}
			}

			//add random item from primarySettings to have at least one item present in UI module if due to previous conditions none of available apps can be shown
			//if (res.Count == 0 && PrimarySettings.primary_set.Count > 0)
			//{
			//    res.Add ( new PromoItem ( PrimarySettings.primary_set [Random.Range(0, PrimarySettings.primary_set .Count)], min_weight, true ) );
			//}

			StartCoroutine ( SelectPromoItems ( res, local ) );
		}



		/// <summary>
		/// Receiving params from DB depending on Bundle and Platform
		/// </summary>
		/// <param name="b">bundle id as param for DB request</param>
		/// <param name="p">platform as param for DB request</param>
		/// <returns></returns>
//IEnumerator ReceiveParams (string b, string p) //deprecated - moved to FirebaseRemoteConfig
//{

//	string bundle_id = b == "" ? ServiceUtils.GetAppBundle ( ) : b;
//	string platform = p == "" ? ServiceUtils.GetPlatform ( ).ToString ( ) : p;
//	int type = 1;
//	string key = "PrOmOhAsH";

//	string hash = Crypto.Md5Sum ( type.ToString ( ) + bundle_id + platform + key );

//	string url = "http://psvpromo.psvgamestudio.com/Scr/getpromoset.php?type=" + type + "&bundle=" + WWW.EscapeURL ( bundle_id ) + "&platform=" + WWW.EscapeURL ( platform ) + "&hash=" + hash;
//	if (debug)
//		Debug.Log ( "PromoModule: ReceiveAccounntAlias " + url );
//	int attempts = 0;
//	WWW www = new WWW ( url );
//	do
//	{
//		attempts++;
//		yield return www;
//	} while (!string.IsNullOrEmpty ( www.error ) && attempts < max_retry_efforts);
//	if (!string.IsNullOrEmpty ( www.error ))
//	{
//		if (debug)
//			Debug.LogWarning ( "PromoModule: Receive params from PSV server failed: " + www.error );
//	}
//	else
//	{
//		//look for publicher id param
//		int i = 0;
//		int j = www.text.IndexOf ( ';', i );
//		if (j > i)
//		{
//			SetPublisher ( www.text.Substring ( i, j - i ) );
//			SetNextParamsUpdate ( );
//			//look for banner time param
//			i = j + 1;
//			j = www.text.IndexOf ( ';', i );
//			if (j > i)
//			{
//				ProcessNewBannerTime ( www.text.Substring ( i, j - i ) );
//			}
//		}
//	}
//}


#if FIREBASE_PRESENT
		/// <summary>
		/// Receiving Promo set from Firebase depending on bundleID and platform
		/// </summary>
		/// <param name="b">bundle id as param for DB request</param>
		/// <param name="p">platform as param for DB request</param>
		/// <returns></returns>
		void ReceiveFirebasePromoSet (string b, int p)
		{

			string bundle_id = b == "" ? ServiceUtils.GetAppBundle ( ) : b;
			string platform = ServiceUtils.GetPlatformName ( p );

			// Set up the Editor before calling into the realtime database.
			string promo_db = FirebaseManager.GetRemoteParam ( promo_db_param );
			if (string.IsNullOrEmpty ( promo_db ))
			{
			}
			promo_db = default_promo_db;
			FirebaseManager.GetSnapshotFromDB ( promo_db, promo_set_root_param + "/" + platform, DatabaseValueChangeHandler );
			//FirebaseManager.GetSnapshotFromDB ( promo_db, promo_set_root_param + "/" + platform, ProcessReceivedSnapshot );
		}

		void DatabaseValueChangeHandler (object sender, Firebase.Database.ValueChangedEventArgs e)
		{
			if (e.DatabaseError != null)
			{
				Debug.Log ( "PromoModule: " + e.DatabaseError.Message );
				return;
			}

			ProcessReceivedSnapshot ( e.Snapshot );
		}


		void ProcessReceivedSnapshot (Firebase.Database.DataSnapshot snapshot)
		{
			List<PromoItem> set = new List<PromoItem> ( );

			foreach (var child in snapshot.Children)
			{
				string bundle = child.Child ( "bundle" ).GetValue ( true ).ToString ( );
				string alias = child.Child ( "alias" ).GetValue ( true ).ToString ( );
				string weight = child.Child ( "weight" ).GetValue ( true ).ToString ( );
				Debug.LogError ( "snaphot row: " + bundle + " " + alias + " " + weight );

				if (!string.IsNullOrEmpty ( bundle ) && !string.IsNullOrEmpty ( alias ) && !string.IsNullOrEmpty ( weight ))
				{
					float w;
					float.TryParse ( weight, out w );
					set.Add ( new PromoItem ( bundle, alias, w ) );
					if (debug)
						Debug.Log ( "PromoModule: bundle = " + bundle + "\nalias = " + alias + "\nweight = " + w );
				}
				else
				{
					break;
				}
			}
			SavePromoSet ( set );
		}
#endif
		/// <summary>
		/// Receiving Promo set from DB depending on bundleID and platform
		/// </summary>
		/// <param name="b">bundle id as param for DB request</param>
		/// <param name="p">platform as param for DB request</param>
		/// <returns></returns>
		IEnumerator ReceivePromoSet (string b, int p)
		{

			string bundle_id = b == "" ? ServiceUtils.GetAppBundle ( ) : b;
			string platform = p < 0 ? ServiceUtils.GetPlatform ( ).ToString ( ) : p.ToString ( );
			int type = 0;
			string key = "PrOmOhAsH";

			string hash = Crypto.Md5Sum ( type.ToString ( ) + bundle_id + platform + key );

			//string url = "http://psvpromo.psvgamestudio.com/Scr/webgl/getpromoset.php?type=" + type + "&bundle=" + WWW.EscapeURL ( bundle_id ) + "&platform=" + WWW.EscapeURL ( platform ) + "&hash=" + hash;
			string url = "http://psvpromo.psvgamestudio.com/Scr/getpromoset.php?type=" + type + "&bundle=" + WWW.EscapeURL ( bundle_id ) + "&platform=" + WWW.EscapeURL ( platform ) + "&hash=" + hash;

			if (debug)
				Debug.Log ( "PromoModule: ReceivePromoSet " + url );

			int attempts = 0;
			WWW www = new WWW ( url );
			do
			{
				attempts++;
				yield return www;

			} while (!string.IsNullOrEmpty ( www.error ) && attempts < max_retry_efforts);
			if (!string.IsNullOrEmpty ( www.error ))
			{
				if (debug)
					Debug.LogWarning ( "PromoModule: Receive PromoSet from PSV server failed: " + www.error );
			}
			else
			{
				List<PromoItem> set = new List<PromoItem> ( );

				int i = 0;
				while (i < www.text.Length)
				{
					string bundle = "";
					string alias = "";
					string weight = "";


					int j = www.text.IndexOf ( ';', i );
					if (j > i)
					{
						bundle = www.text.Substring ( i, j - i );
						i = j + 1;
						j = www.text.IndexOf ( ';', i );
						if (j > i)
						{
							alias = www.text.Substring ( i, j - i );
							i = j + 1;
							j = www.text.IndexOf ( '\n', i );
							if (j > i)
							{
								weight = www.text.Substring ( i, j - i );
								i = j + 1;
							}
						}
					}

					if (!string.IsNullOrEmpty ( bundle ) && !string.IsNullOrEmpty ( alias ) && !string.IsNullOrEmpty ( weight ))
					{
						float w;
						float.TryParse ( weight, out w );
						set.Add ( new PromoItem ( bundle, alias, w ) );
						if (debug)
							Debug.Log ( "PromoModule: bundle = " + bundle + "\nalias = " + alias + "\nweight = " + w );
					}
					else
					{
						break;
					}
				}

				SavePromoSet ( set );
			}
		}



		void SavePromoSet (List<PromoItem> set)
		{
			//save promo_set
			if (set.Count > 0)
			{
				if (!Directory.Exists ( GetExternalPath ( ) ))
				{
					Directory.CreateDirectory ( GetExternalPath ( ) );
				}
				File.WriteAllText ( GetExternalPath ( ) + promo_set_file, LitJson.JsonMapper.ToJson ( set ) );
				InitPromoSet ( set );
				SetNextPromoUpdate ( );
			}
		}

		/// <summary>
		/// create list of items selected in accordance to their weights
		/// </summary>     
		/// /// <param name="set">List of items to select for further processing</param>
		/// <param name="local">if we process local data and UI module starts with promo module we need to announce new data available to see icons after start (load of resources will take some time)</param>
		/// <returns></returns>
		IEnumerator SelectPromoItems (List<PromoItem> set, bool local)
		{
			List<PromoItem> p_selection = new List<PromoItem> ( );
			List<float> weights = new List<float> ( );
			List<int> w_ind = new List<int> ( );
			for (int i = 0; i < set.Count; i++)
			{
				weights.Add ( set [i].GetPromoWeight ( ) );
				w_ind.Add ( i );
			}
			for (int i = 0; i < set.Count; i++)
			{
				if (weights.Count > 0)
				{
					int selection = WeightedRandom.RandomW ( weights.ToArray ( ) );
					p_selection.Add ( set [w_ind [selection]] );
					weights.RemoveAt ( selection );
					w_ind.RemoveAt ( selection );
				}
			}
			while (blocking_access)
			{
				yield return null;
			}
			BlockAccess ( true );
			promo_set = p_selection;
			if (local)
				AnnounceAvailableItems ( );
			EnqueDownloads ( true );
			BlockAccess ( false );
		}

		/// <summary>
		/// Tells subscriptors that new data is available
		/// </summary>
		void AnnounceAvailableItems ()
		{
			for (int i = 0; i < promo_set.Count; i++)
			{
				int index = available_set.FindIndex ( X => X.app_bundle.Equals ( promo_set [i].app_bundle ) );
				if (index >= 0)
				{
					if (OnPackageReceived != null)
					{
						OnPackageReceived ( available_set [index] );
					}
				}
			}
		}


		/// <summary>
		/// Debug method to show contents of PromoSet
		/// </summary>
		void PrintPromoSet ()
		{
			string res = "";
			for (int i = 0; i < promo_set.Count; i++)
			{
				res += "| " + promo_set [i].app_bundle + " | " + promo_set [i].app_alias + " | " + promo_set [i].app_name + " | " + promo_set [i].app_weight + "|\n";
			}
			Debug.Log ( "PromoModule: " + res );
		}


		/// <summary>
		/// Creates queue of items to load from promoSet if icon is 
		/// </summary>
		/// <param name="new_data"></param>
		void EnqueDownloads (bool new_data = false)
		{
			List<PackageInfo> queue = new List<PackageInfo> ( );
			for (int i = 0; i < promo_set.Count; i++)
			{
				if (!promo_set [i].IsLocal ( ))
				{
					if (available_set.FindIndex ( X => X.app_bundle.Equals ( promo_set [i].app_bundle ) ) < 0)
					{
						queue.Add ( new PackageInfo ( promo_set [i].app_bundle, promo_set [i].app_alias ) );
					}
				}
			}
			MarketBridge.Instance.EnqueItems ( queue.ToArray ( ), ovverride_locale: PrimarySettings.use_locale_override );
			downloads_in_progress += queue.Count;
			//we reset efforts with every external call
			if (new_data && delayed_download != null)
			{
				StopCoroutine ( delayed_download );
			}

			retry_efforts = !new_data ? retry_efforts + 1 : 0;

			if (downloads_in_progress > 0 && retry_efforts < max_retry_efforts)
			{
				delayed_download = StartCoroutine ( DelayedRetry ( ) );
			}
		}


		IEnumerator DelayedRetry ()
		{
			yield return new WaitForSeconds ( retry_delay );
			while (downloads_in_progress > 0)
			{
				yield return new WaitForSeconds ( retry_delay );
			}
			if (faulty_downloads > 0)
				EnqueDownloads ( );
		}


		public PackageData [] GetPromoItems (int required_items)
		{
			List<PackageData> res = new List<PackageData> ( );
			if (!blocking_access)
			{
				BlockAccess ( true );
				for (int i = 0; i < promo_set.Count; i++)
				{
					PackageData item = GetPromoItem ( promo_set [i].app_bundle );
					if (item != null && item.app_icon != null)
					{
						res.Add ( item );
					}
					//limit items count to required
					if (res.Count == required_items)
						break;
				}
				BlockAccess ( false );
			}

			if (res.Count == 0)
			{
				if (PromoModule.debug)
					Debug.LogWarning ( "PromoModule: There are no available items from promo_set" );
				if (use_new_primary_set_logic)
				{
					PackageData item = GetRandomPrimarySetItem ( );
					if (item != null)
					{
						res.Add ( item );
					}
					else
					{
						Debug.LogError ( "PromoModule: Can't get random primary set item" );
					}
				}
				else
				{
					//old style behaviour for sliding UI
					//this code give us first available items
					for (int i = 0; i < available_set.Count; i++)
					{
						if (available_set [i].app_icon != null)
						{
							res.Add ( available_set [i] );
						}
						//limit items count to required
						if (res.Count == required_items)
							break;
					}
				}
			}

			return res.ToArray ( );
		}


		PackageData GetRandomPrimarySetItem ()
		{
			PackageData res = null;
			if (PrimarySettings.primary_set.Count > 0)
			{
				res = GetPromoItem ( PrimarySettings.primary_set [Random.Range ( 0, PrimarySettings.primary_set.Count )].app_bundle );
			}
			return res;
		}


		PackageData GetPromoItem (string app_bundle)
		{
			int index = available_set.FindIndex ( X => X.app_bundle.Equals ( app_bundle ) );
			if (index >= 0)
				return available_set [index];
			else
				return null;
		}



		void SaveData ()
		{
			List<PackageInfo> buffer = new List<PackageInfo> ( );
			for (int i = 0; i < available_set.Count; i++)
			{
				if (PrimarySettings.primary_set.FindIndex ( X => X.app_bundle.Equals ( available_set [i].app_bundle ) ) < 0)
				{
					buffer.Add ( new PackageInfo ( available_set [i].app_bundle, available_set [i].app_alias, available_set [i].app_name ) );
				}
			}
			File.WriteAllText ( GetExternalPath ( ) + dat_file, LitJson.JsonMapper.ToJson ( buffer ) );
		}


		void SaveIcon (Texture2D icon, string app_bundle)
		{
			File.WriteAllBytes ( GetExternalPath ( ) + app_bundle + ".png", icon.EncodeToPNG ( ) );
		}

		void ProcessData (string app_bundle, PackageData data)
		{
			if (data != null && data.app_icon != null)
			{
				//if (ServiceUtils.IsAppAllowed ( data.app_bundle ))
				//{
				available_set.Add ( data );
				if (OnPackageReceived != null)
				{
					OnPackageReceived ( data );
				}
				//}
				SaveIcon ( data.app_icon, app_bundle );
				SaveData ( );
			}
			else
			{
				faulty_downloads++;
			}

			downloads_in_progress--;

			if (debug)
				Debug.Log ( "PromoModule: " + app_bundle + " request ended" + (data == null ? "with fail" : "") );
		}



		private string GetExternalPath ()
		{
			return Application.persistentDataPath + "/PSVPromo/";
		}

	}
}
