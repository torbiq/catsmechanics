﻿using UnityEngine;
using System;
using System.Collections;

public class DeviceStateMonitor :MonoBehaviour
{
	public static event Action<Vector2> OnResolutionChange;
	public static event Action<DeviceOrientation> OnDeviceOrientationChange;
	public static event Action<ScreenOrientation> OnScreenOrientationChange;
	public static float CheckDelay = 0.5f;          // How long to wait until we check again.

	static Vector2 resolution;                      // Current Resolution
	static DeviceOrientation d_orientation;         // Current Device Orientation
	static ScreenOrientation s_orientation;         // Current Screen Orientation
	static bool isAlive = true;                     // Keep this script running?

	public bool
		check_resolution = false,
		check_device_orientation = false,
		check_screen_orientation = true;


	void Start ()
	{
		isAlive = true;
		StartCoroutine ( CheckForChange ( ) );
	}

	IEnumerator CheckForChange ()
	{
		d_orientation = Input.deviceOrientation;
		resolution = new Vector2 ( Screen.width, Screen.height );


		while (isAlive)
		{
			if (check_resolution)
			{
				// Check for a Resolution Change
				if (resolution.x != Screen.width || resolution.y != Screen.height)
				{
					resolution = new Vector2 ( Screen.width, Screen.height );
					if (OnResolutionChange != null)
						OnResolutionChange ( resolution );
				}
			}


			if (check_device_orientation)
			{
				// Check for an Orientation Change
				switch (Input.deviceOrientation)
				{
					case DeviceOrientation.Unknown:            // Ignore
					case DeviceOrientation.FaceUp:            // Ignore
					case DeviceOrientation.FaceDown:        // Ignore
						break;
					default:
						if (d_orientation != Input.deviceOrientation)
						{
							d_orientation = Input.deviceOrientation;
							if (OnDeviceOrientationChange != null)
								OnDeviceOrientationChange ( d_orientation );
						}
						break;
				}
			}

			if (check_screen_orientation)
			{
				if (Screen.orientation != s_orientation)
				{
					s_orientation = Screen.orientation;
					if (OnScreenOrientationChange != null)
						OnScreenOrientationChange ( s_orientation );
				}
			}

			yield return new WaitForSeconds ( CheckDelay );
		}
	}

	[ContextMenu ( "OrientPortrait" )]
	void OrientPortrait ()
	{
		if (OnScreenOrientationChange != null)
			OnScreenOrientationChange ( ScreenOrientation.Portrait );
	}

	[ContextMenu ( "OrientPLandscape" )]
	void OrientPLandscape ()
	{
		if (OnScreenOrientationChange != null)
			OnScreenOrientationChange ( ScreenOrientation.Landscape );
	}

	void OnDestroy ()
	{
		isAlive = false;
	}


}
