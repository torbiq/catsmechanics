﻿#define BRAND_NEW_PROTOTYPE

using UnityEngine;
using System.Collections;
using PSV_Prototype;

public class PN_CanvasCompatibility :MonoBehaviour
{
#if BRAND_NEW_PROTOTYPE
	public Scenes
		menu_scene = Scenes.MainMenu;
#endif


	void OnEnable ()
	{
#if !UNITY_ANDROID
		this.gameObject.SetActive ( false );
#endif
	}


	public void NewsBtn ()
	{
		if (PsvPushService.Instance.PSShowNews ( true ))
		{
#if BRAND_NEW_PROTOTYPE
			SceneLoader.Instance.SwitchToScene ( Scenes.Push );
#endif
		}
	}


	public void ActionBtn ()
	{
		Debug.Log ( "ActionBtn" );
		PsvPushService.Instance.PSRunAction ( );
	}

	public void MenuButton ()
	{
#if BRAND_NEW_PROTOTYPE
		SceneLoader.Instance.SwitchToScene ( menu_scene );
#else
		PsvPushService.Instance.PSShowNews ( false );
#endif
	}
}
