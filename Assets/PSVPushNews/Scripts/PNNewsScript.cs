﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class PNNewsScript :MonoBehaviour
{

	public ScreenOrientation
		orientation;

	public Text
		title,
		body;
	public Image
		icon;

	private CanvasGroup
		cg;

	void Awake ()
	{
		cg = gameObject.AddComponent<CanvasGroup> ( );
	}

	// Use this for initialization
	void OnEnable ()
	{
		Init ( );
		CheckOrientation ( Screen.orientation );
	}

	void Start ()
	{
		DeviceStateMonitor.OnScreenOrientationChange += CheckOrientation;
		PsvPushService.OnDataUpdated += OnDataUpdated;
	}

	void OnDestroy ()
	{
		DeviceStateMonitor.OnScreenOrientationChange -= CheckOrientation;
		PsvPushService.OnDataUpdated -= OnDataUpdated;
	}

	void CheckOrientation (ScreenOrientation curr_orient)
	{
		NormalizeOrientation ( ref curr_orient );
		cg.alpha = curr_orient == orientation ? 1 : 0;
	}

	private void OnDataUpdated (Sprite _icon, string _title, string _description)
	{
		title.text = _title;
		body.text = _description;
		icon.sprite = _icon;
	}

	void NormalizeOrientation (ref ScreenOrientation orient)
	{
		switch (orient)
		{
			case ScreenOrientation.LandscapeLeft:
			case ScreenOrientation.LandscapeRight:
				orient = ScreenOrientation.Landscape;
				break;
			case ScreenOrientation.PortraitUpsideDown:
				orient = ScreenOrientation.Portrait;
				break;
		}
	}

	void Init ()
	{
		OnDataUpdated ( PsvPushService.Instance.pssprite, PsvPushService.Instance.pstitle, PsvPushService.Instance.psdescr );
	}




}
