﻿#define USE_GOOGLE_ANALYTICS

using UnityEngine;
using System.Collections;

public class PsvPushService :MonoBehaviour
{
	public static PsvPushService Instance;

	public delegate void NewsDataCallback (Sprite icon, string title, string description);
	public static event NewsDataCallback OnDataUpdated;

	public GameObject
		PNMainScene,
		PNNewsScene;

	private GameObject
		currentnews_scene;

	public Sprite
		pssprite = null;

	public string
		psisnews = "",
		pstitle = "",
		psimgurl = "",
		psdescr = "",
		psflag = "",
		pstargeturl = "";

	private bool NewsOpened = false;

	public float PushNewsScale = 1f;



	/***modified***/

	private bool show_news = false;


	public bool AreNewsAvailable ()
	{
		return show_news;
	}

	bool SceneSystem ()
	{
		return PNMainScene == null && PNNewsScene == null;
	}

	void Awake ()
	{
		if (Instance == null)
		{
			Instance = this;
			if (SceneSystem ( ))
			{
				PSInit ( "icon_app", analytics_id: PSV_Prototype.ProjectSettingsManager.settings.android_analytics_id, client_id: PSV_Prototype.Utils.GetUID ( ) );
			}
		}
		else
		{
			Debug.LogWarning ( "PsvPushService: you have multiple instances of module in your scene" );
			Destroy ( this.gameObject );
		}
	}

	public bool PSShowNews (bool setscene)
	{
#if !UNITY_ANDROID
		return false;
#endif
		if (!(psisnews.Equals ( "-1" )))
		{
			if (SceneSystem ( ) && setscene)
			{
				show_news = true;
				Debug.Log ( "PsvPushService: have to show news" );
			}
			else
			{
				PNMainScene.SetActive ( !setscene );
				if (setscene)
				{
					if (currentnews_scene == null)
					{
						currentnews_scene = Instantiate ( PNNewsScene );
					}
					else
					{
						currentnews_scene.SetActive ( true );
					}
					NewsOpen ( setscene );
				}
				else
				{
					NewsOpen ( setscene );
					if (currentnews_scene != null)
					{
						currentnews_scene.SetActive ( false );
					}
				}
			}
		}
		return show_news;
	}
	/****modified****/






	public bool isNewsOpened ()
	{
		return NewsOpened;
	}

	public void NewsOpen (bool isopen)
	{
		NewsOpened = isopen;
	}



	public void PSStart ()
	{
		string [] psstr = PSGetNews ( );
		psisnews = psstr [0];
		pstitle = psstr [1];
		psimgurl = psstr [2];
		psdescr = psstr [3];
		psflag = psstr [4];

		Debug.Log ( "PsvPushService: " + ConvertToString ( psstr ) );

		pstargeturl = "market://details?id=" + psstr [5];
		if (psimgurl.Length > 0)
		{
			try
			{
				if (!(psimgurl.Substring ( 0, 7 ).Equals ( "http://" ) || psimgurl.Substring ( 0, 8 ).Equals ( "https://" )))
				{
					psimgurl = "http://" + psimgurl;
					StartCoroutine ( PNLoadImg ( psimgurl ) );
				}
			}
			catch (UnityException error)
			{
				print ( "UnityException:" + error );
			}
		}

		StartCoroutine ( PNLoadImg ( psimgurl ) );
		if (psisnews.Equals ( "1" ))
		{
			PSShowNews ( true );
			PSSetIsNews ( );
		}
		Notify ( );
	}

	public string PSInit (string s_icon, string blank_title = "New Message!", string uid = "", string analytics_id = "UA-64271377-75", string client_id = "")
	{
		//s_icon - назва іконки для сповіщення, blank_title - Титулка яка відображається при порожній титулці uid не повинен бути менше 30 символів
		string myuid = "";
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
			myuid = toast.CallStatic<string>("PSStart", app, s_icon, blank_title, uid
#if USE_GOOGLE_ANALYTICS
		, analytics_id, client_id
#endif
		);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif

		PSStart ( );
		return myuid;
	}

	public string [] PSGetNews ()
	{
		//s_icon - назва іконки для сповіщення, blank_title - Титулка яка відображається при порожній титулці uid не повинен бути менше 30 символів
		string [] str = new string [6] { "", "", "", "", "", "" };
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
			str = toast.CallStatic<string[]>("GetPSNews", app);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
		return str;
	}

	public int PSTimeZone ()
	{   //+возвращаем временное смещение в часах(у нас +3, и т.д.)
		int timeoffset = 0;
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
			timeoffset = toast.CallStatic<int>("gcmtimezone", app);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
		return timeoffset;
	}

	public string PSCurrentDateTime ()
	{   //+возвращаем текущее время в формате 2014-10-19 15:20:48
		string currentdate = "";
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
			currentdate = toast.CallStatic<string>("currentdatetime", app);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
		return currentdate;
	}

	public string PSRandomString (int UIDlength, bool useSpecial, bool useUpper)
	{   //возвращает случайную строку с указанным количеством символов
		string randomstr = "";
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
			randomstr = toast.CallStatic<string>("RandomString", UIDlength, useSpecial, useUpper);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
		return randomstr;
	}

	public void PSShowNotif (long delay, string title, string text, bool sound = false, bool vibrate = true, string small_icon_name = "", string big_icon_name = "")
	{
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSReceiver");
			toast.CallStatic("SetNotification", app, delay * 1000L, title, text, "", sound ? 1 : 0, vibrate ? 1 : 0, 0, small_icon_name, big_icon_name, 11167436);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
	}

	public void PSNotifRepeating (long delay, long timeout, string title, string text, bool sound = false, bool vibrate = true, string small_icon_name = "", string big_icon_name = "")
	{
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSReceiver");
			toast.CallStatic("SetRepeatingNotification", app, delay * 1000L, title, text, "", timeout * 1000L, sound ? 1 : 0, vibrate ? 1 : 0, 0, small_icon_name, big_icon_name, 11167436);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
	}

	public void PSCancelNotif ()
	{
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSReceiver");
			toast.CallStatic("CancelNotification", app);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
	}

	public void PSRunAction ()
	{
		if (psflag.Equals ( "1" ))
		{
			Application.OpenURL ( pstargeturl );
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
			try {
				AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
				AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
				AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
				toast.CallStatic("PSSendClick", app, pstargeturl);
			} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
		}
	}

	public void PSSetIsNews ()
	{
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
		try {
			AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject app = activity.Call<AndroidJavaObject>("getApplicationContext");
			AndroidJavaClass toast = new AndroidJavaClass("com.psvstudio.pushservice.PSInit");
			toast.CallStatic("PSSetIsNews", app);
		} catch(System.Exception e) { Debug.LogError ( "PSInit exception:" + e.Message ); };
#endif
	}


	IEnumerator PNLoadImg (string psimgurl)
	{
		if (!string.IsNullOrEmpty ( psimgurl ))
		{
			WWW wwwimg = new WWW ( psimgurl );
			yield return wwwimg;
			Texture2D tex = wwwimg.texture;
			Rect rec = new Rect ( 0, 0, tex.width, tex.height );
			Vector2 pivot = new Vector2 ( 0.5f, 0.5f );
			pssprite = Sprite.Create ( tex, rec, pivot );
			Notify ( );
		}
	}


	void Notify ()
	{
		if (OnDataUpdated != null)
		{
			OnDataUpdated ( pssprite, pstitle, psdescr );
		}
	}

	string ConvertToString (string [] arr)
	{
		string res = "Length = " + arr.Length + "\n";
		for (int i = 0; i < arr.Length; i++)
		{
			res += i + ":\t" + arr [i] + "\n";
		}
		return res;
	}

}

