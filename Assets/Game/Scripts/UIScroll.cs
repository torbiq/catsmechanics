﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class UIScroll : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler {
    private UIItem itemSelected;
    private float _distanceForInstantiate = 1.5f;
    private bool _onBeginDragCalled = false;

    public Vector3 startMousePos { get; private set; }

    public void OnPointerDown(PointerEventData eventData) {
        if (itemSelected == null) {
            eventData.hovered.Find(objectHovered => {
                var component = objectHovered.GetComponent<UIItem>();
                if (component) {
                    itemSelected = component;
                    startMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    itemSelected.OnPointerDown(eventData);
                    _onBeginDragCalled = false;
                    return true;
                }
                itemSelected = null;
                return false;
            });
        }
    }
    public void OnBeginDrag(PointerEventData eventData) {
    }
    public void OnDrag(PointerEventData eventData) {
        if (itemSelected) {
            var currentMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var deltaMousePositions = startMousePos - currentMousePos;
            if (_onBeginDragCalled) {
                itemSelected.OnDrag(eventData);
            }
            else if (deltaMousePositions.y > _distanceForInstantiate) {
                _onBeginDragCalled = true;
                itemSelected.OnBeginDrag(eventData);
                GarageController.Instance.scrollRect.horizontal = false;
                itemSelected.transform.localScale = Vector3.one;
            }
            else {
                itemSelected.transform.localScale = (0.5f + 0.5f * ((_distanceForInstantiate - Mathf.Max(deltaMousePositions.y, 0)) / _distanceForInstantiate)) * Vector3.one;
            }
        }
    }
    public void OnEndDrag(PointerEventData eventData) {
        if (itemSelected) {
            if (_onBeginDragCalled) {
                itemSelected.OnEndDrag(eventData);
                GarageController.Instance.scrollRect.horizontal = true;
            }
            else {
                itemSelected.transform.localScale = Vector3.one;
            }
            itemSelected = null;
        }
    }
    private void OnApplicationPause(bool pause) {
        if (pause) {
            OnEndDrag(null);
        }
    }
}
