﻿using System;
using System.Collections.Generic;
using System.Linq;

public class FloatRange : IRange<float> {
    public float min { get; private set; }
    public float max { get; private set; }
    public FloatRange(float min, float max) {
        if (max < min) {
            throw new Exception("Max < Min");
        }
        this.min = min;
        this.max = max;
    }
    public bool OutOfRange(float val) {
        return val <= min || val >= max;
    }
    public float GetRandom() {
        return UnityEngine.Random.Range(min, max);
    }
    public float GetDistanceTo(float value) {
        if (value < min) {
            return min - value;
        }
        if (value > max) {
            return value - max;
        }
        return 0f;
    }
    public float GetLength() {
        return max - min;
    }
    public override string ToString() {
        return "Min: " + min.ToString("0.0") + " Max: " + max.ToString("0.0") + "";
    }
    public IRange<float>[] Seperate(float at) {
        if (OutOfRange(at)) {
            return new FloatRange[1] { this };
        }
        return new FloatRange[2] {
            new FloatRange(min, at),
            new FloatRange(at, max)
        };
    }
    public IRange<float> Clarify(float distance) {
        if (distance * 2 >= max - min) {
            throw new Exception("Not enough space to clarify borders.");
        }
        return new FloatRange(min + distance, max - distance);
    }
    public IRange<float>[] Remove(IRange<float> range) {
        if (range.min == range.max) {
            return new FloatRange[1] {
                this,
            };
        }
        if (range.min > min) {
            if (range.max < max) {
                return new FloatRange[2] {
                    new FloatRange(min, range.min),
                    new FloatRange(range.max, max),
                };
            }
            return new FloatRange[1] {
                new FloatRange(min, range.min),
            };
        }
        if (range.max < max) {
            return new FloatRange[1] {
                new FloatRange(range.max, max),
            };
        }
        if (range.min <= min && range.max >= max) {
            return new FloatRange[0];
        }
        return new FloatRange[1] {
            this,
        };
    }
    public IRange<float>[] Remove(IRange<float>[] ranges) {
        throw new NotImplementedException();
    }
    //public IRange<float> Combine(IRange<float> range) {
    //    if (range.min <= max && min <= range.max) {
    //        return new FloatRange(range.min < min ? range.min : min, range.max < max ? max : range.max);
    //    }
    //    if (min <= range.max && range.min <= max) {
    //        return new FloatRange(min < range.min ? min : range.min, max < range.max ? range.max : max);
    //    }
    //    return new FloatRange(min, max);
    //}
    //public IRange<float> Combine(IRange<float>[] range) {
    //    FloatRange combined = new FloatRange(min, max);
    //    for (int i = 0; i < range.Length; i++) {
    //        combined = (FloatRange)combined.Combine(range[i]);
    //    }
    //    return combined;
    //}
    //public IRange<float>[] Sum(IRange<float> range) {
    //    if (range.min > max) {
    //        return new FloatRange[2] {
    //            new FloatRange(min, max),
    //            new FloatRange(range.min, range.max),
    //        };
    //    }
    //    if (range.max < min) {
    //        return new FloatRange[2] {
    //            new FloatRange(range.min, range.max),
    //            new FloatRange(min, max),
    //        };
    //    }
    //    return new FloatRange[1] {
    //        new FloatRange(range.min < min ? range.min : min, range.max < max ? max : range.max),
    //    };
    //}
    //public IRange<float>[] Sum(IRange<float>[] ranges) {
    //    List<IRange<float>> rangeSum = new List<IRange<float>> { this };
    //    for (int i_ranges = 0; i_ranges < ranges.Length; i_ranges++) {
    //        var takenFromArg = ranges[i_ranges];
    //        for (int i_summed = 0; i_summed < rangeSum.Count; i_summed++) {
    //            var localRange = rangeSum[i_summed];
    //        }
    //    }
    //}

}
