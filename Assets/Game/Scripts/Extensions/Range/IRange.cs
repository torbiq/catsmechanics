﻿public interface IRange<T> {
    T min { get; }
    T max { get; }
    T GetRandom();
    bool OutOfRange(T val);
    IRange<T> Clarify(T distance);
    IRange<T>[] Seperate(T val);
    //IRange<T> Combine(IRange<T> range);
    //IRange<T> Combine(IRange<T>[] range);
    IRange<T>[] Remove(IRange<T> range);
    IRange<T>[] Remove(IRange<T>[] ranges);
    T GetDistanceTo(T val);
    T GetLength();
}