﻿using System;
using System.Linq;

public struct IntRange : IRange<int> {
    public int min { get; private set; }
    public int max { get; private set; }
    public IntRange(int min, int max) {
        if (max < min) {
            throw new Exception("Max < Min");
        }
        this.min = min;
        this.max = max;
    }
    public int GetRandom() {
        return UnityEngine.Random.Range(min, max);
    }
    public bool OutOfRange(int val) {
        return val <= min || val >= max;
    }
    public IRange<int>[] Seperate(int at) {
        if (OutOfRange(at)) {
            return new IntRange[1] { this }.Cast<IRange<int>>().ToArray();
        }
        return new IntRange[2] {
            new IntRange(min, at),
            new IntRange(at, max)
        }.Cast<IRange<int>>().ToArray();
    }
    public IRange<int> Clarify(int distance) {
        if (distance * 2 >= max - min) {
            throw new Exception("Not enough space to clarify borders.");
        }
        return new IntRange(min + distance, max - distance);
    }
    public int GetLength() {
        return max - min;
    }
    public int GetDistanceTo(int value) {
        if (value < min) {
            return min - value;
        }
        if (value > max) {
            return value - max;
        }
        return 0;
    }
    public IRange<int>[] Remove(IRange<int> range) {
        throw new NotImplementedException();
    }
    public IRange<int>[] Remove(IRange<int>[] ranges) {
        throw new NotImplementedException();
    }
}
