﻿using UnityEngine;
public class RectArea {
    public Vector2 min;
    public Vector2 max;
    public Vector2 GetRand() {
        return new Vector2(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y));
    }
    public RectArea(Vector2 min, Vector2 max) {
        this.min = min;
        this.max = max;
    }
}
