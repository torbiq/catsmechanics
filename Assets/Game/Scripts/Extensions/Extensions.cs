﻿using UnityEngine;
using DG.Tweening;

namespace Extensions
{
    public static class Extensions {
        /// <summary>
        /// Returns count of elements in enum.
        /// </summary>
        /// <typeparam name="EnumType">Enumerator type.</typeparam>
        /// <returns>Count of elements in enum.</returns>
        public static int GetEnumSize<EnumType>()
        {
            return System.Enum.GetNames(typeof(EnumType)).Length;
        }
        /// <summary>
        /// Returns count of elements in enum.
        /// </summary>
        /// <typeparam name="EnumType">Enumerator type.</typeparam>
        /// <returns>Count of elements in enum.</returns>
        public static int GetEnumSize(this System.Enum enumValue)
        {
            return System.Enum.GetNames(enumValue.GetType()).Length;
        }
        /// <summary>
        /// Kills tween if it's playing.
        /// </summary>
        public static void KillIfPlaying(this Tween tween, bool complete = false) {
            if (tween != null) {
                if (tween.IsPlaying()) {
                    tween.Kill(complete);
                }
            }
        }
        /// <summary>
        /// Get length of current clip name.
        /// </summary>
        public static float GetClipLength(this Animator animator, string clipName) {
            int length = animator.runtimeAnimatorController.animationClips.Length;
            for (int i = 0; i < length; ++i) {
                var clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name == clipName) {
                    return clip.length;
                }
            }
            Log.Error("Clip " + clipName + " in Animator called " + animator.name + " not found.");
            return 0f;
        }
    }
}