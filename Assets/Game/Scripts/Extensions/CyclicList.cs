﻿#define SAFE_MODE

using System.Collections.Generic;

namespace Extensions {
    /// <summary>
    /// Randomizable list.
    /// </summary>
    public class CyclicList<T> {
        #region Private members
        /// <summary>
        /// All elements in list.
        /// </summary>
        private List<T> _elemAll;
        /// <summary>
        /// Left elements.
        /// </summary>
        private List<T> _elemLeft;
        #endregion

        #region Public members
        /// <summary>
        /// Creates a cyclic list.
        /// </summary>
        /// <param name="elementsList">Randomizable list of elements.</param>
        public CyclicList(List<T> elementsList) {
#if SAFE_MODE
            // Copy received list to prevent not-incapsulated access.
            _elemAll = new List<T>();
            if (elementsList.Count <= 1) {
                throw new System.Exception("Received list contains one or none elements.");
            }
            _elemAll.AddRange(elementsList);
#else
        _elemAll = elementsList;
#endif
            _elemLeft = new List<T>();
        }
        /// <summary>
        /// Creates a cyclic list.
        /// </summary>
        /// <param name="elementsList">Array of randomizable elements.</param>
        public CyclicList(T[] elementsArray) {
#if SAFE_MODE
            // Copy received list to prevent not-incapsulated access.
            _elemAll = new List<T>();
            if (elementsArray.Length <= 1) {
                throw new System.Exception("Received list contains one or none elements.");
            }
            _elemAll.AddRange(elementsArray);
#else
        _elemAll = elementsArray.ToList();
#endif
            _elemLeft = new List<T>();
        }
        /// <summary>
        /// Get a random value from this range (elements are never repeating).
        /// </summary>
        /// <returns>Random value from this range (never repeats).</returns>
        public T Get() {
            // If no elements left.
            if (_elemLeft.Count == 0) {
                // Readding elements for random.
                _elemLeft.AddRange(_elemAll);
            }
            // Index of element to be returned.
            int indexTaken = _elemLeft.Count - 1;
            // Saving returned element before removing.
            T returned = _elemLeft[indexTaken];
            // Removing this element from list.
            _elemLeft.RemoveAt(indexTaken);
            return returned;
        }
        /// <summary>
        /// Adds an element to this list.
        /// </summary>
        /// <param name="element">Element to add.</param>
        public void Add(T element) {
            _elemAll.Add(element);
            _elemLeft.Add(element);
        }
        /// <summary>
        /// Removes an element only from all randomizable.
        /// </summary>
        /// <param name="element">Element to remove.</param>
        public bool RemoveFromAll(T element) {
            int index = -1;
            index = _elemAll.FindIndex(arg => EqualityComparer<T>.Default.Equals(arg, element));
            if (index != -1) {
#if SAFE_MODE
                if (_elemAll.Count <= 2) {
                    Log.Error("Can't remove an element because list will contain one item or no items at all.");
                    return false;
                }
#endif
                _elemAll.RemoveAt(index);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Remove an element from left to get before next randomization.
        /// </summary>
        public bool RemoveLeft(T element) {
            return _elemLeft.Remove(element);
        }
        /// <summary>
        /// Removes an element in this list.
        /// </summary>
        /// <param name="element">Element to remove.</param>
        public bool Remove(T element) {
            RemoveLeft(element);
            return RemoveFromAll(element);
        }
        /// <summary>
        /// Gets count of all elements in randomizable list.
        /// </summary>
        /// <returns>Count of all elements in list.</returns>
        public int Count() {
            return _elemAll.Count;
        }
        /// <summary>
        /// Gets count of element left to get before next randomization.
        /// </summary>
        public int CountLeft() {
            return _elemLeft.Count;
        }
        #endregion
    }
}
