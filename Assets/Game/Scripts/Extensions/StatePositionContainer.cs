﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Extensions {
    [Serializable]
    public class StateObjContainer<TListHelper, TState, TObj> where TListHelper : StateObj<TState, TObj> where TObj : UnityEngine.Object {
        [SerializeField]
        private List<TListHelper> _statePositions;
        public List<TListHelper> statePositions { get { return _statePositions; } set { _statePositions = value; } }
        public TListHelper GetByState(TState state) {
            return _statePositions.Find(sttPos => sttPos.state.Equals(state));
        }
    }
}
