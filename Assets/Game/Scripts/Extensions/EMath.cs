﻿using UnityEngine;

namespace Extensions {
    /// <summary>
    /// Handles mathematical extensions.
    /// </summary>
    public static class EMath {
        /// <summary>
        /// Returns travel time with distance/speed.
        /// </summary>
        public static float GetTimeBySpeed(Vector3 start, Vector3 end, float speed) {
            return (end - start).magnitude / speed;
        }
        public static float Normalize(float value, float min = 0.0f, float max = 1.0f) {
            return Mathf.Min(Mathf.Max(value, min), max);
        }
        public static int Normalize(int value, int min, int max) {
            return Mathf.Min(Mathf.Max(value, min), max);
        }
    }
}
