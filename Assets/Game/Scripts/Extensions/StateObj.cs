﻿using UnityEngine;
using System;

namespace Extensions {
    [Serializable]
    public class StateObj<TState, TObj> where TObj : UnityEngine.Object {
        [SerializeField]
        private TState _state;
        [SerializeField]
        private TObj _obj;
        public TState state { get { return _state; } set { _state = value; } }
        public TObj obj { get { return _obj; } set { _obj = value; } }
    }
}