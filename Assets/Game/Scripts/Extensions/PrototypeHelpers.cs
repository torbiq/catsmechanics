﻿using System;
using UnityEngine.SceneManagement;
using PSV_Prototype;

public static class PrototypeHelpers {
    #region Sound settings
    public static bool IsSoundEnabled() {
        return GameSettings.IsSoundsEnabled();
    }
    public static void SetSoundEnabled(bool val) {
        GameSettings.EnableSounds(val);
    }
    public static bool IsMusicEnabled() {
        return GameSettings.IsMusicEnabled();
    }
    public static void SetMusicEnabled(bool val) {
        GameSettings.EnableMusic(val);
    }
    public static float SoundVolume() {
        return GameSettings.GetSoundsVol();
    }
    public static float MusicVolume() {
        return GameSettings.GetMusicVol() * 0.1f;
    }
    #endregion

    public static bool IsAdsEnabled() {
        return ManagerGoogle.Instance.IsAdmobEnabled();
    }
    public static bool IsVibrationEnabled() {
        return GameSettings.IsMusicEnabled();
    }

    #region Scene management
    public static Scenes GetCurrentScene() {
        if (SceneLoader.Instance) {
            return SceneLoader.Instance.GetCurrentScene();
        }
        else {
            return (Scenes)Enum.Parse(typeof(Scenes), SceneManager.GetActiveScene().name);
        }
    }
    public static void ChangeLevel(Scenes scene) {
        if (SceneLoader.Instance) {
            AudioPlayer.Instance.Stop(SceneLoader.Instance.splash_duration / 2f);
            AudioPlayer.Instance.StopSpeech(SceneLoader.Instance.transition_duration / 2f);
            SceneLoader.Instance.SwitchToScene(scene);
        }
        else {
            AudioPlayer.Instance.Stop(0);
            AudioPlayer.Instance.StopSpeech(0);
            SceneManager.LoadScene(scene.ToString());
        }
    }
    #endregion
}
