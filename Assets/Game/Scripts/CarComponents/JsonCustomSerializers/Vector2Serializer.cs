﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Vector2Serializer : JsonConverter {
    public Vector2Serializer() {
    }
    public override bool CanConvert(Type objectType) {
        return objectType == typeof(Vector3);
    }
    public override bool CanRead {
        get { return true; }
    }
    public override bool CanWrite {
        get { return true; }
    }
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
        JObject jObject = JObject.Load(reader);
        var deserialized = new Vector2((float)jObject["x"], (float)jObject["y"]);
        return deserialized;
    }
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
        JToken t = JToken.FromObject(value);
        Vector2 valueV2 = (Vector2)value;
        if (t.Type != JTokenType.Object)
        {
            t.WriteTo(writer);
        }
        else
        {
            JObject o = (JObject)t;
            IList<string> propertyNames = o.Properties().Select(p => p.Name).ToList();
            o.AddFirst(new JProperty("x", valueV2.x));
            o.AddFirst(new JProperty("y", valueV2.y));
            o.WriteTo(writer);
        }
    }
}
