﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Vector3Serializer : JsonConverter {
    public Vector3Serializer() {
    }
    public override bool CanConvert(Type objectType) {
        return objectType == typeof(Vector3);
    }
    public override bool CanRead {
        get { return true; }
    }
    public override bool CanWrite {
        get { return true; }
    }
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
        JObject jObject = JObject.Load(reader);
        var deserialized = new Vector3((float)jObject["x"], (float)jObject["y"], (float)jObject["z"]);
        return deserialized;
    }
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
        //JToken t = JToken.;// JToken.FromObject(value, serializer);
        Vector3 valueV3 = (Vector3)value;
        //if (t.Type != JTokenType.Object)
        //{
        //    t.WriteTo(writer);
        //}
        //else
        //{
        JObject o = new JObject();
        //IList<string> propertyNames = o.Properties().Select(p => p.Name).ToList();
        o.AddFirst(new JProperty("z", valueV3.z));
        o.AddFirst(new JProperty("y", valueV3.y));
        o.AddFirst(new JProperty("x", valueV3.x));
        o.WriteTo(writer);
        //}
    }
}
