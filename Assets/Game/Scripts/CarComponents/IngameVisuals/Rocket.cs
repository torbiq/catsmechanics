﻿using UnityEngine;
using System;
using DG.Tweening;

public class Rocket : TActivatablePart<AWeapon> {
    public FlyingRocket rocketPrefab;
    public float freq;
    public float aliveTime;
    public float speed;
    public Tween tween { get; set; }

    public void Fire() {
        var rocket = ((GameObject)GameObject.Instantiate(rocketPrefab.gameObject, null)).GetComponent<FlyingRocket>();
        rocket.damage = part.chartics.damage;
        rocket.transform.rotation = rocketPrefab.transform.rotation;
        rocket.transform.position = rocketPrefab.transform.position;
        rocket.transform.localScale = rocketPrefab.transform.lossyScale;
        rocket.Launch(this, Vector2.right);
    }
    public void StartTween() {
        tween = DOVirtual.DelayedCall(freq, () => {
            Fire();
            StartTween();
        }, false);
    }
    public override void Enable() {
        base.Enable();
        StartTween();
    }
    public override void Disable() {
        base.Disable();
        tween.Kill(false);
    }
    public override void SetDirection(bool left) { }
}