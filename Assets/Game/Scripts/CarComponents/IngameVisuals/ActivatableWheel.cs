﻿using UnityEngine;

public class ActivatableWheel : TActivatablePart<AWheel> {
    public WheelJoint2D wheelJoint;

    public override void ActivateColliders(bool val) {
        base.ActivateColliders(val);
        wheelJoint.connectedBody = part.car.body.rigidbody2D;
        wheelJoint.anchor = Vector3.zero;
        wheelJoint.connectedAnchor = part.point.localPosition;
    }
    public override void Enable() {
        base.Enable();
        wheelJoint.enabled = true;
    }
    public override void Disable() {
        base.Disable();
        wheelJoint.enabled = false;
    }
    public override void SetDirection(bool val) {
        var motor = wheelJoint.motor;
        motor.motorSpeed = Mathf.Abs(motor.motorSpeed) * (val ? 1 : -1);
        wheelJoint.motor = motor;
    }
}