﻿using UnityEngine;

public class ActivatableBody : TActivatablePart<ABody> {
    public override void ActivateColliders(bool val) {
        base.ActivateColliders(val);
    }
    public override void Enable() {
        base.Enable();
        part.rigidbody2D.gravityScale = 1f;
    }
    public override void Disable() {
        base.Disable();
        part.rigidbody2D.gravityScale = 0f;
        part.rigidbody2D.velocity = Vector2.zero;
    }
    public override void SetDirection(bool val) {

    }
}