﻿using UnityEngine;
using System;
using DG.Tweening;

public class FlyingRocket : MonoBehaviour {
    public CollisionListener2D collisionListener;
    public float speed;
    public float explosionDelay;
    [NonSerialized]
    public int damage;
    public Vector3 direction;
    public Action OnFixedUpdate = null;
    public Tween killingTween { get; private set; }
    public Rocket rocket { get; private set; }

    public void Launch(Rocket rocket, Vector2 direction) {
        this.rocket = rocket;
        OnFixedUpdate = () => {
            transform.Translate(speed * direction * Time.fixedDeltaTime);
        };
        killingTween = DOVirtual.DelayedCall(explosionDelay, () => {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }, false);
        collisionListener.OnTriggerEnter += OnTrigEnter;
    }
    private void OnTrigEnter(Collider2D col) {
        var asPart = col.gameObject.GetComponent<Part>();
        if (asPart) {
            //if (asPart.hitBoxCollider2D == col && asPart.car != rocket.weapon.car) {
            //    collisionListener.OnTriggerEnter -= OnTrigEnter;
            //    asPart.car.TakeDamage(damage);
            //    gameObject.SetActive(false);
            //    killingTween.Kill(true);
            //}
        }
    }

    public void FixedUpdate() {
        if (OnFixedUpdate != null) {
            OnFixedUpdate();
        }
    }
}
