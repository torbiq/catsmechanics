﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class Blade : TActivatablePart<Weapon> {
    public CollisionListener2D listener;
    public Transform pillarTransform;
    public Transform fixtureTransform;
    public float pillarFreq;
    public float fixtureFreq;
    public Tween pillarTween { get; set; }
    public Tween fixtureTween { get; set; }

    private void OnDamageTriggerEnter2D(Collider2D col) {
        var asPart = col.GetComponent<Part>();
        if (asPart) {
            if (asPart.WasTouched(col) && asPart.car != part.car) {
                asPart.car.TakeDamage(part.chartics.damage);
            }
        }
    }
    public override void SetDirection(bool left) {
        pillarTween.Kill(false);
        fixtureTween.Kill(false);
        var attachP = part.attachmentPosition;
        float nextRot = (left ? -1 : 1) * 360f;
        pillarTween = pillarTransform.DORotate(new Vector3(0f, 0f, nextRot), 1.0f / pillarFreq, RotateMode.WorldAxisAdd).SetEase(Ease.Linear).SetLoops(-1);
        fixtureTween = attachP.DORotate(new Vector3(0f, 0f, nextRot), 1.0f / fixtureFreq, RotateMode.WorldAxisAdd).SetEase(Ease.Linear).SetLoops(-1);
    }
    public override void Enable() {
        base.Enable();
        var attachP = part.attachmentPosition;
        var lPos = attachP.localPosition;
        attachP.parent = part.transform.parent;
        part.transform.parent = attachP;
        part.transform.localPosition = -lPos;
        listener.OnTriggerEnter += OnDamageTriggerEnter2D;
    }
    public override void Disable() {
        base.Disable();
        var attachP = part.attachmentPosition;
        part.transform.parent = attachP.parent;
        attachP.parent = part.transform.parent;

        listener.OnTriggerEnter -= OnDamageTriggerEnter2D;

        pillarTween.Kill(false);
        fixtureTween.Kill(false);
    }
}
