﻿public class ItemPoint : TypedAttachmentPoint<AItem, ItemType, SItem> {
    public override PointType GetPointType() {
        return PointType.Item;
    }
    public override void LoadData(Body bodyAttached, STypedAttachmentPoint<AItem, ItemType, SItem> point) {
        base.LoadData(bodyAttached, point);
        body.itemPoints.Add(this);
        if (point.part != null) {
            Attach(PartLoader.LoadItem(point.part));
        }
    }
}
