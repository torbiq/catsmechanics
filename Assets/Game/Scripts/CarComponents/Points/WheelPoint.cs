﻿public class WheelPoint : TypedAttachmentPoint<AWheel, WheelType, SWheel> {
    public override PointType GetPointType() {
        return PointType.Wheel;
    }
    public override void LoadData(Body body, STypedAttachmentPoint<AWheel, WheelType, SWheel> point) {
        base.LoadData(body, point);
        body.wheelPoints.Add(this);
        if (point.part != null) {
            Attach(PartLoader.LoadWheel(point.part));
        }
    }
}
