﻿public class WeaponPoint : TypedAttachmentPoint<AWeapon, WeaponType, SWeapon> {
    public override PointType GetPointType() {
        return PointType.Weapon;
    }
    public override void LoadData(Body bodyAttached, STypedAttachmentPoint<AWeapon, WeaponType, SWeapon> point) {
        base.LoadData(bodyAttached, point);
        body.weaponPoints.Add(this);
        if (point.part != null) {
            Attach(PartLoader.LoadWeapon(point.part));
        }
    }
}
