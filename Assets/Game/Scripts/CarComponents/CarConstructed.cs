﻿using UnityEngine;
using System;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class CarConstructed : MonoBehaviour {
    public bool isLeft { get; set; }
    public Body body { get; set; }

    public int hp { get; set; }
    public int overallHp { get; set; }
    public int damage { get; set; }

    public Collider2D bodyAttachingCollider { get; private set; }

    public event Action<CarConstructed> OnDamageTaken;
    public event Action<CarConstructed> OnDestroyed;

    #region Game helpers
    public void LoadStats() {
        hp = GetHp();
        overallHp = hp;
        damage = GetDamage();
    }
    public int GetHp() {
        int hp = 0;
        if (body) {
            hp += body.chartics.hp;
            foreach (var wheel in body.GetWheelsAttached()) {
                hp += wheel.chartics.hp;
            }
            foreach (var weapon in body.GetWeaponsAttached()) {
                hp += weapon.chartics.hp;
            }
            foreach (var item in body.GetItemsAttached()) {
                hp += item.chartics.hp;
            }
        }
        return hp;
    }
    public int GetDamage() {
        int damage = 0;
        if (body) {
            damage += body.chartics.damage;
            foreach (var wheel in body.GetWheelsAttached()) {
                damage += wheel.chartics.damage;
            }
            foreach (var weapon in body.GetWeaponsAttached()) {
                damage += weapon.chartics.damage;
            }
            foreach (var item in body.GetItemsAttached()) {
                damage += item.chartics.damage;
            }
        }
        return damage;
    }
    public void UnwearBodyOnCarConstructed() {
        foreach (var wheelPoint in body.wheelPoints) {
            wheelPoint.Remove();
        }
        foreach (var weaponPoint in body.weaponPoints) {
            weaponPoint.Remove();
        }
        foreach (var itemPoint in body.itemPoints) {
            itemPoint.Remove();
        }
    }
    public void SetActiveAttachmentsOnCarConstructed(bool val) {
        if (body) {
            foreach (var wheelPoint in body.wheelPoints) {
                if (wheelPoint.part) {
                    wheelPoint.part.gameObject.SetActive(val);
                }
            }
            foreach (var weaponPoint in body.weaponPoints) {
                if (weaponPoint.part) {
                    weaponPoint.part.gameObject.SetActive(val);
                }
            }
            foreach (var itemPoint in body.itemPoints) {
                if (itemPoint.part) {
                    itemPoint.part.gameObject.SetActive(val);
                }
            }
        }
    }
    public void TakeDamage(int damage) {
        hp -= damage;
        if (OnDamageTaken != null) {
            OnDamageTaken(this);
        }
        if (hp <= 0) {
            hp = 0;
            if (OnDestroyed != null) {
                OnDestroyed(this);
            }
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
    public void Kill() {
        hp = 0;
        if (OnDamageTaken != null) {
            OnDamageTaken(this);
        }
        if (OnDestroyed != null) {
            OnDestroyed(this);
        }
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
    public void ActivateColliders(bool val) {
        if (body) {
            body.activatablePart.ActivateColliders(true);
            foreach (var point in body.wheelPoints) {
                if (point.part) {
                    point.part.activatablePart.ActivateColliders(true);
                }
            }
            foreach (var point in body.weaponPoints) {
                if (point.part) {
                    point.part.activatablePart.ActivateColliders(true);
                }
            }
            foreach (var point in body.itemPoints) {
                if (point.part) {
                    point.part.activatablePart.ActivateColliders(true);
                }
            }

        }
    }
    #endregion

    #region Constructing helpers
    public event Action<ABody> OnBodyAttached;
    public event Action<ABody> OnBodyDetached;

    public void Load(SCarConstructed carConstructed) {
        if (carConstructed != null) {
            if (carConstructed.body != null) {
                Attach(PartLoader.LoadBody(this, carConstructed.body));
            }
        }
    }
    public void LoadAttachingCollider() {
        bodyAttachingCollider = GetComponent<Collider2D>();
    }
    public void Attach(Body body) {
        this.body = body;
        this.body.car = this;
        foreach (var wheelPoint in this.body.wheelPoints) {
            wheelPoint.body = this.body;
        }
        foreach (var weaponPoint in this.body.weaponPoints) {
            weaponPoint.body = this.body;
        }
        foreach (var itemPoint in this.body.itemPoints) {
            itemPoint.body = this.body;
        }
        this.body.transform.parent = transform;
        this.body.transform.localPosition = Vector2.zero;
        if (OnBodyAttached != null) {
            OnBodyAttached(body);
        }
    }
    public ABody Detach() {
        var detachedBody = body;
        detachedBody.car = null;
        detachedBody.transform.parent = null;
        body = null;
        if (OnBodyDetached != null) {
            OnBodyDetached(detachedBody);
        }
        return detachedBody;
    }
    public void Replace(Body body) {
        Remove();
        Attach(body);
    }
    public void Remove() {
        if (body) {
            UnwearBodyOnCarConstructed();
            GarageController.Instance.AddUIItemPart(Detach());
        }
    }
    #endregion
}