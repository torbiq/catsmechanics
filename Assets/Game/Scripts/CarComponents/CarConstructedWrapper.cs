﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class CarConstructedWrapper {
    [JsonProperty(PropertyName = "car")]
    public CarConstructed car { get; set; }
}