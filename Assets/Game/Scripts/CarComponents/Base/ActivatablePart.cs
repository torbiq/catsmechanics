﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivatablePart : MonoBehaviour {
    public List<Collider2D> hitBoxColliders2D;
    public bool collidersEnabled { get; private set; }

    public event Action OnEnabled;
    public event Action OnDisabled;
    public event Action<bool> OnCollidersActivated;

    public abstract void SetDirection(bool left);

    protected virtual void Initialize() { }
    public virtual void Enable() {
        ActivateColliders(true);
        if (OnEnabled != null) {
            OnEnabled();
        }
    }
    public virtual void Disable() {
        ActivateColliders(false);
        if (OnDisabled != null) {
            OnDisabled();
        }
    }
    public virtual void ActivateColliders(bool val) {
        collidersEnabled = val;
        foreach (var collider in hitBoxColliders2D) {
            collider.enabled = val;
        }
        if (OnCollidersActivated != null) {
            OnCollidersActivated(val);
        }
    }

    protected virtual void Awake() { }
    protected virtual void Start() { }
    protected virtual void Update() { }
}

public abstract class TActivatablePart<T> : ActivatablePart where T : Part {
    public T part;
}