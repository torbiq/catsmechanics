﻿using UnityEngine;

public abstract class Part : MonoBehaviour {
    public Strength strength { get; set; }
    public Rarity rarity { get; set; }
    public Level level;
    public Chartics chartics;
    public int stars { get; set; }

    public CarConstructed car { get; set; }

    public Collider2D touchableCollider;
    public Transform attachmentPosition;

    public abstract string GetSubTypeName();
    public abstract PartType GetPartType();

    protected virtual void LoadPartData(SPart part) {
        level = new Level(part.level);
        strength = part.strength;
        rarity = part.rarity;
        chartics = new Chartics(part.chartics);
        stars = part.stars;
    }
    public virtual bool WasTouched(Collider2D collider) {
        return false;
    }
    protected virtual void Awake() { }
    protected virtual void Start() { }
    protected virtual void Update() { }
}