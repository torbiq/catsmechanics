﻿using System;
using UnityEngine;
using Newtonsoft.Json;

public abstract class AttachmentPoint : MonoBehaviour {
    [JsonProperty(PropertyName = "local_position", ReferenceLoopHandling = ReferenceLoopHandling.Ignore)]
    [JsonConverter(typeof(Vector3Serializer))]
    public Vector3 localPosition { get; set; }
    
    public Body body { get; set; }

    public abstract PointType GetPointType();

    protected void LoadBaseData(Body body, SAttachmentPoint point) {
        this.body = body;
        localPosition = point.localPosition;
        transform.SetParent(this.body.transform);
        transform.localPosition = localPosition;
        transform.localScale = Vector3.one;
        // In inherited class we need to extend method and add load part code.
    }
    
    protected virtual void Awake() {
        localPosition = transform.localPosition;
        if (body == null) {
            var parent = transform.parent;
            if (parent) {
                var grandParent = parent.parent;
                if (grandParent) {
                    body = grandParent.GetComponent<Body>();
                }
            }
        }
    }
    protected virtual void Start() { }
    protected virtual void Update() { }
}
