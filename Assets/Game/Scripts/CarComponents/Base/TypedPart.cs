﻿using Newtonsoft.Json;
using System;
using UnityEngine;

public abstract class TypedPart<TPartClass, TPartType, TPartSerialized> : Part 
        where TPartClass : TypedPart<TPartClass, TPartType, TPartSerialized>
        where TPartType : struct, IConvertible
        where TPartSerialized : STypedPart<TPartClass, TPartType, TPartSerialized> {
    public TPartType subtype;

    public ActivatablePart activatablePart;

    public TypedAttachmentPoint<TPartClass, TPartType, TPartSerialized> point { get; set; }

    public override string GetSubTypeName() {
        return subtype.ToString();
    }
    public virtual void LoadData(TPartSerialized part) {
        base.LoadPartData(part);
        subtype = part.subtype;
    }
    public override bool WasTouched(Collider2D collider) {
        if (activatablePart) {
            return activatablePart.hitBoxColliders2D.Contains(collider);
        }
        return false;
    }
}
