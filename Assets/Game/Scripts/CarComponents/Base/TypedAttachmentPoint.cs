﻿using System;
using UnityEngine;
using Newtonsoft.Json;

public abstract class TypedAttachmentPoint<TPartClass, TPartType, TPartSerialized> : AttachmentPoint 
        where TPartClass : TypedPart<TPartClass, TPartType, TPartSerialized>
        where TPartType : struct, IConvertible
        where TPartSerialized : STypedPart<TPartClass, TPartType, TPartSerialized> {
    public TPartClass part { get; set; }

    public event Action<TPartClass> OnPartAttached;
    public event Action<TPartClass> OnPartDetached;
    
    public virtual void LoadData(Body body, STypedAttachmentPoint<TPartClass, TPartType, TPartSerialized> point) {
        base.LoadBaseData(body, point);
        // In inherited class we need to extend method and add load part code.
    }

    public virtual void Attach(TPartClass item) {
        part = item;
        part.point = this;
        part.car = body.car;
        part.transform.SetParent(transform);
        part.transform.localScale = Vector3.one;
        part.transform.localPosition = -part.attachmentPosition.localPosition;
        if (OnPartAttached != null) {
            OnPartAttached(item);
        }
    }
    public virtual TPartClass Detach() {
        var detached = part;
        detached.point = null;
        detached.car = null;
        detached.transform.parent = null;
        part = null;
        if (OnPartDetached != null) {
            OnPartDetached(detached);
        }
        return detached;
    }
    public virtual void Replace(TPartClass item) {
        Remove();
        Attach(item);
    }
    public virtual void Remove() {
        if (part) {
            GarageController.Instance.AddUIItemPart(Detach());
        }
    }
}
