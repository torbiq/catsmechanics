﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using Newtonsoft.Json;

public class Body : ABody {
    public override void LoadData(SBody part) {
        base.LoadData(part);
        transform.localScale = Vector3.one * 0.58f;
        wheelPoints = new List<WheelPoint>();
        weaponPoints = new List<WeaponPoint>();
        itemPoints = new List<ItemPoint>();
        foreach (var point in part.wheelPoints) {
            var loadedPoint = PartLoader.LoadWheelPoint(this, point);
        }
        foreach (var point in part.weaponPoints) {
            var loadedPoint = PartLoader.LoadWeaponPoint(this, point);
        }
        foreach (var point in part.itemPoints) {
            var loadedPoint = PartLoader.LoadItemPoint(this, point);
        }
    }
}

public class ABody : TypedPart<ABody, BodyType, SBody> {
    public Rigidbody2D rigidbody2D;
    public Transform pointsContainer;

    public Anchor itemsRandomAnchor;
    public float itemsMinDistance;
    public Anchor wheelsRandomAnchor;
    public float wheelsMinDistance;
    
    [JsonProperty(PropertyName = "wheel_points")]
    public List<WheelPoint> wheelPoints = new List<WheelPoint>();
    [JsonProperty(PropertyName = "weapon_points")]
    public List<WeaponPoint> weaponPoints = new List<WeaponPoint>();
    [JsonProperty(PropertyName = "item_points")]
    public List<ItemPoint> itemPoints = new List<ItemPoint>();
    
    public List<AWheel> GetWheelsAttached() {
        var wheels = new List<AWheel>();
        wheelPoints.ForEach(wheel => {
            if (wheel.part) {
                wheels.Add(wheel.part);
            }
        });
        return wheels;
    }
    public List<AWeapon> GetWeaponsAttached() {
        var weapons = new List<AWeapon>();
        weaponPoints.ForEach(weapon => {
            if (weapon.part) {
                weapons.Add(weapon.part);
            }
        });
        return weapons;
    }
    public List<AItem> GetItemsAttached() {
        var items = new List<AItem>();
        itemPoints.ForEach(item => {
            if (item.part) {
                items.Add(item.part);
            }
        });
        return items;
    }

    public List<Part> GetPartsAttached() {
        return new List<Part>().Concat(GetWheelsAttached().Cast<Part>()).Concat(GetWeaponsAttached().Cast<Part>()).Concat(GetItemsAttached().Cast<Part>()).ToList();
    }

    public override PartType GetPartType() {
        return PartType.Body;
    }
}

[Serializable]
public class Anchor {
    public Transform min;
    public Transform max;
}