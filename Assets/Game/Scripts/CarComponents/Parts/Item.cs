﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class Item : AItem { }

[JsonObject(MemberSerialization.OptIn)]
public class AItem : TypedPart<AItem, ItemType, SItem> {
    public override PartType GetPartType() {
        return PartType.Item;
    }
}