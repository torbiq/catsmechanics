﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class Weapon : AWeapon { }

[JsonObject(MemberSerialization.OptIn)]
public class AWeapon : TypedPart<AWeapon, WeaponType, SWeapon> {
    public override PartType GetPartType() {
        return PartType.Weapon;
    }
}