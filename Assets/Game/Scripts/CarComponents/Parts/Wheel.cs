﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class Wheel : AWheel { }

[JsonObject(MemberSerialization.OptIn)]
public class AWheel : TypedPart<AWheel, WheelType, SWheel> {
    public override PartType GetPartType() {
        return PartType.Wheel;
    }
}