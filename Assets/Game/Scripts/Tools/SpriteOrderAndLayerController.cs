﻿using UnityEngine;
using System.Collections.Generic;
using Extensions;
using System.Linq;
using System;

[ExecuteInEditMode]
public class SpriteOrderAndLayerController : MonoBehaviour {

    #region Controlling variables
    /// <summary>
    /// Current sorting min order.
    /// </summary>
    private int _currentMinSpriteOrder;
    /// <summary>
    /// Current sorting max order.
    /// </summary>
    private int _currentMaxSpriteOrder;
    /// <summary>
    /// Current layer in sorting orders.
    /// </summary>
    private int _currentLayer;
    /// <summary>
    /// In-editor sorting layer of characters.
    /// </summary>
    private int _currentEditorLayer;
    /// <summary>
    /// Delta in sorting orders, recorded on awake.
    /// </summary>
    private int _deltaOrders;
    #endregion

    #region Inspector's serialized variables
    /// <summary>
    /// Ignored parts when calculating delta between min and max in children.
    /// </summary>
    [SerializeField]
    private List<Transform> _ignoredParts;
    /// <summary>
    /// Min sprite order (recorded on awake).
    /// </summary>
    [SerializeField]
    private int _minSpriteOrder;
    /// <summary>
    /// Min sprite order (recorded on awake).
    /// </summary>
    [SerializeField]
    private int _maxSpriteOrder;
    /// <summary>
    /// Current layer.
    /// </summary>
    [SerializeField]
    private int _layer = 0;
    /// <summary>
    /// Sorting layer in editor.
    /// </summary>
    [SerializeField]
    private int _editorLayer;
    [SerializeField]
    private List<Transform> _allChildren;
    [SerializeField]
    private List<SortingOrderElement> _spriteOrders;
    #endregion

    [Serializable]
    public class SortingOrderElement {
        [SerializeField]
        private SpriteRenderer _spriteRenderer;
        [SerializeField]
        private int _sortingOrder;

        public SpriteRenderer spriteRenderer  {
            get {
                return _spriteRenderer;
            }
            set {
                _spriteRenderer = value;
            }
        }
        public int sortingOrder {
            get { return _sortingOrder; }
            set {
                _sortingOrder = value;
                Update();
            }
        }
        public void Update() {
            _spriteRenderer.sortingOrder = _sortingOrder;
        }
        public SortingOrderElement(SpriteRenderer spriteRenderer, int sortingOrder) {
            _spriteRenderer = spriteRenderer;
            _sortingOrder = sortingOrder;
            Update();
        }
    } 

    #region Accessors
    /// <summary>
    /// Layer of object.
    /// </summary>
    public int layer {
        get {
            return _layer;
        }
        set {
            _layer = value;
            CheckLayer();
        }
    }
    /// <summary>
    /// Accessing inspector's min sprite order.
    /// </summary>
    public int minSpriteOrder {
        get {
            return _minSpriteOrder;
        }
        set {
            _minSpriteOrder = value;
            CheckMinSpriteOrder();
        }
    }
    /// <summary>
    /// Accessing inspector's max sprite order.
    /// </summary>
    public int maxSpriteOrder {
        get {
            return _maxSpriteOrder;
        }
        set {
            _maxSpriteOrder = value;
            CheckMaxSpriteOrder();
        }
    }
    /// <summary>
    /// Layer in editor.
    /// </summary>
    public int editorLayer {
        get {
            return _editorLayer;
        }
        set {
            _editorLayer = value;
            CheckEditorLayer();
        }
    }
    /// <summary>
    /// List of ingored parts.
    /// </summary>
    public List<Transform> ignoredParts {
        get {
            return _ignoredParts;
        }
        set {
            _ignoredParts = value;
        }
    }
    /// <summary>
    /// Delta in orders.
    /// </summary>
    public int deltaOrders {
        get {
            return _deltaOrders;
        }
        private set {
            _deltaOrders = value;
        }
    }
    public List<SortingOrderElement> spriteOrders {
        get {
            return _spriteOrders;
        }
        set {
            _spriteOrders = value;
        }
    }
    #endregion

    #region Private methods
    /// <summary>
    /// Changes layer and returns delta.
    /// </summary>
    private int ChangeLayerAndGetDelta(int newLayer) {
        int delta = _deltaOrders * (newLayer - _currentLayer);
        _currentLayer = newLayer;
        _currentMinSpriteOrder += delta;
        _currentMaxSpriteOrder += delta;
        maxSpriteOrder = _currentMaxSpriteOrder;
        minSpriteOrder = _currentMinSpriteOrder;
        return delta;
    }
    /// <summary>
    /// Changes min sprite order and returns delta.
    /// </summary>
    private int ChangeMinOrderAndGetDelta(int newMinOrder) {
        int delta = newMinOrder - _currentMinSpriteOrder;
        _currentMinSpriteOrder = newMinOrder;
        _currentMaxSpriteOrder += delta;
        maxSpriteOrder = _currentMaxSpriteOrder;
        return delta;
    }
    /// <summary>
    /// Changes max sprite order and returns delta.
    /// </summary>
    private int ChangeMaxOrderAndGetDelta(int newMaxOrder) {
        int delta = newMaxOrder - _currentMaxSpriteOrder;
        _currentMaxSpriteOrder = newMaxOrder;
        _currentMinSpriteOrder += delta;
        minSpriteOrder = _currentMinSpriteOrder;
        return delta;
    }
    /// <summary>
    /// Returns transform's orders range.
    /// </summary>
    private int GetTransformOrdersRange(Transform parentTransform, out int min, out int max) {
        var allSprites = new Stack<SpriteRenderer>();
        var sr = parentTransform.GetComponent<SpriteRenderer>();
        if (sr) {
            allSprites.Push(sr);
        }
        parentTransform.GetComponentsInChildrenRecursively(ref allSprites);

        min = allSprites.Pop().sortingOrder;
        max = min;

        while (allSprites.Count != 0) {
            var sprite = allSprites.Pop();

            if (_ignoredParts.Contains(sprite.transform)) {
                continue;
            }

            int sortingOrder = sprite.sortingOrder;

            if (sortingOrder < min) {
                min = sortingOrder;
            }
            else if (sortingOrder > max) {
                max = sortingOrder;
            }
        }
        return max - min;
    }
    /// <summary>
    /// Changes editor layer on all sprites recoursevily.
    /// </summary>
    private void ChangeEditorLayerRecoursevly(Transform parentTransform, int newEditorLayer) {
        var srStack = new Stack<SpriteRenderer>();
        var sr = parentTransform.GetComponent<SpriteRenderer>();
        if (sr) {
            srStack.Push(sr);
        }
        parentTransform.GetComponentsInChildrenRecursively(ref srStack);
        while (srStack.Count != 0) {
            var spriteRenderer = srStack.Pop();
            if (ignoredParts.Contains(spriteRenderer.transform)) {
                continue;
            }
            spriteRenderer.sortingLayerID = newEditorLayer;
        }
        _currentEditorLayer = newEditorLayer;
    }

    private void RefreshLists() {
        Stack<Transform> childrenStack = new Stack<Transform>();
        transform.GetComponentsInChildrenRecursively(ref childrenStack);
        childrenStack.Push(transform);
        if (_allChildren == null) {
            _allChildren = new List<Transform>();
        }
        _ignoredParts.ForEach(x => _allChildren.Remove(x));
        if (_spriteOrders == null) {
            _spriteOrders = new List<SortingOrderElement>();
        }
        if (_allChildren.Count != childrenStack.Count) {
            var realChildrenList = childrenStack.ToArray().Reverse().ToList();
            _allChildren.ForEach(x => realChildrenList.Remove(x));
            realChildrenList.ForEach(x => _allChildren.Add(x));
        }
        List<SpriteRenderer> realSpriteRenderers = new List<SpriteRenderer>();
        _allChildren.ForEach(x => {
            var sr = x.GetComponent<SpriteRenderer>();
            if (sr) {
                realSpriteRenderers.Add(sr);
            }
        });
        if (_spriteOrders.Count != realSpriteRenderers.Count) {
            _spriteOrders.ForEach(x => realSpriteRenderers.Remove(x.spriteRenderer));
            realSpriteRenderers.ForEach(x => _spriteOrders.Add(new SortingOrderElement(x, x.sortingOrder)));
        }
    }
    #endregion

    #region Public for editor script
    public void LoadCurrentLayer() {
        _currentLayer = layer;
    }
    public void RefreshDelta() {
        _deltaOrders = GetTransformOrdersRange(transform, out _currentMinSpriteOrder, out _currentMaxSpriteOrder);
        _minSpriteOrder = _currentMinSpriteOrder;
        _maxSpriteOrder = _currentMaxSpriteOrder;
    }
    public void CheckMinSpriteOrder() {
        if (minSpriteOrder != _currentMinSpriteOrder) {
            transform.ChangeOrderRecursively(ChangeMinOrderAndGetDelta(minSpriteOrder), ref _ignoredParts);
        }
    }
    public void CheckMaxSpriteOrder() {
        if (maxSpriteOrder != _currentMaxSpriteOrder) {
            transform.ChangeOrderRecursively(ChangeMaxOrderAndGetDelta(maxSpriteOrder), ref _ignoredParts);
        }
    }
    public void CheckLayer() {
        if (layer != _currentLayer) {
            transform.ChangeOrderRecursively(ChangeLayerAndGetDelta(layer), ref _ignoredParts);
        }
    }
    public void CheckEditorLayer() {
        if (editorLayer != _currentEditorLayer) {
            ChangeEditorLayerRecoursevly(transform, editorLayer);
        }
    }
    #endregion

    #region Mono
    private void Awake() {
        if (ignoredParts == null) {
            ignoredParts = new List<Transform>();
        }
        //RefreshLists();
        RefreshDelta();
        LoadCurrentLayer();
    }
    private void Update() {
        //RefreshLists();
        //RefreshDelta();
        //_spriteOrders.ForEach(x => x.Update());
    }
    #endregion
}
