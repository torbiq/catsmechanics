﻿using UnityEngine;
using System.Collections.Generic;
using Extensions;

public class TransformPool {
    private List<Transform> _inPool;
    private List<Transform> _onScene;

    public Transform exampleObject { get; private set; }
    public SpriteRenderer exampleSpriteRenderer { get; private set; }

    public bool spawnLacking { get; private set; }

    public TransformPool(Transform exampleObject, int count, bool spawnLacking) {
        _inPool = new List<Transform>();
        _onScene = new List<Transform>();
        
        this.exampleObject = exampleObject;
        this.spawnLacking = spawnLacking;
        exampleSpriteRenderer = exampleObject.GetComponent<SpriteRenderer>();

        for (int i = 0; i < count; i++) {
            var instance = GameObject.Instantiate(exampleObject.gameObject);
            instance.name = exampleObject.name;
            Return(instance.transform);
        }

        this.exampleObject.gameObject.SetActive(false);
    }

    public int CountAll {
        get {
            return _inPool.Count + _onScene.Count;
        }
    }

    public int CountLeft {
        get {
            return _inPool.Count;
        }
    }

    public int CountScene {
        get {
            return _onScene.Count;
        }
    }

    public void SetCorrectTransform(Transform objectTransform) {
        objectTransform.SetParent(exampleObject.transform.parent);
        objectTransform.localPosition = exampleObject.localPosition;
        objectTransform.localRotation = exampleObject.localRotation;
        objectTransform.localScale = exampleObject.localScale;
    }

    public Transform Get() {
        var returned = _inPool.GetLastAndRemove();

        if (!returned && spawnLacking) {
            returned = GameObject.Instantiate(exampleObject);
        }

        if (returned) {
            var sr = returned.GetComponent<SpriteRenderer>();
            if (sr && exampleSpriteRenderer) {
                sr.sortingOrder = exampleSpriteRenderer.sortingOrder;
            }
            returned.gameObject.SetActive(true);
            _onScene.Add(returned);
            _inPool.Remove(returned);
        }
        return returned;
    }

    public void Return(Transform transform) {
        transform.gameObject.SetActive(false);
        _onScene.Remove(transform);
        _inPool.Add(transform);
        SetCorrectTransform(transform);
    }
}
