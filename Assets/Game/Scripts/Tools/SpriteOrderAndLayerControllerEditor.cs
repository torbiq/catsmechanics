﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System;

[CustomEditor(typeof(SpriteOrderAndLayerController))]
[CanEditMultipleObjects]
public class SpriteOrderAndLayerControllerEditor : Editor {
    SerializedProperty ignoredToChangeOrder;
    SerializedProperty spriteOrders;

    private string[] _sortingNames;
    private int[] _sortingIds;

    void OnEnable() {
        var characterLayer = target as SpriteOrderAndLayerController;

        ignoredToChangeOrder = serializedObject.FindProperty("_ignoredParts");
        spriteOrders = serializedObject.FindProperty("_spriteOrders");

        if (characterLayer.ignoredParts == null) characterLayer.ignoredParts = new List<Transform>();

        RefreshSortingInfo();
        characterLayer.RefreshDelta();
        characterLayer.LoadCurrentLayer();
    }

    private void RefreshSortingInfo() {
        Stack<string> layerNames = new Stack<string>();
        Stack<int> layerOrders = new Stack<int>();

        Array.ForEach(SortingLayer.layers, elem => {
            layerNames.Push(elem.name);
            layerOrders.Push(elem.id);
        });

        _sortingNames = layerNames.ToArray();
        _sortingIds = layerOrders.ToArray();
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();

        var layerController = target as SpriteOrderAndLayerController;
        layerController.RefreshDelta();

        var ignoredList = layerController.ignoredParts;
        EditorGUILayout.LabelField("Ignored parts");
        for (int i = 0; i < ignoredList.Count; i++) {
            EditorGUILayout.PropertyField(ignoredToChangeOrder.GetArrayElementAtIndex(i));
        }
        if (GUILayout.Button("Add")) {
            ignoredList.Add(null);
        }
        if (GUILayout.Button("Remove") && ignoredList.Count > 0) {
            ignoredList.RemoveAt(ignoredList.Count - 1);
        }
        
        layerController.layer = EditorGUILayout.IntField("Layer ", layerController.layer);
        EditorGUILayout.LabelField("Note: it depends on min/max order and delta between these two values");
        EditorGUILayout.LabelField("Current delta between min and max order " + layerController.deltaOrders);
        layerController.minSpriteOrder = EditorGUILayout.IntField("Minimal order ", layerController.minSpriteOrder);
        layerController.maxSpriteOrder = EditorGUILayout.IntField("Maximal order ", layerController.maxSpriteOrder);
        layerController.editorLayer = EditorGUILayout.IntPopup("Sorting layer in editor ",
            layerController.editorLayer,
            _sortingNames,
            _sortingIds
            );

        //EditorGUILayout.LabelField("Sprites contained: ");
        //for (int i = 0; i < layerController.spriteOrders.Count; i++) {
        //    layerController.spriteOrders[i].sortingOrder = EditorGUILayout.IntField(layerController.spriteOrders[i].spriteRenderer.name, layerController.spriteOrders[i].sortingOrder);
        //}
        //characterLayer.spriteOrders.ForEach(x => {
        //    x.sortingOrder = EditorGUILayout.IntField(x.spriteRenderer.name, x.sortingOrder);
        //});
        serializedObject.ApplyModifiedProperties();
    }
}
#endif