﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class PaintingAlpha : MonoBehaviour {
    [SerializeField] [Range(1, 100)] private int _paintingRadius = 30;
    [SerializeField] [Range(0.01f, 1)] private float _alphaPercentForFinish = 0.4f;
    [SerializeField] [Range(0, 1)] private float _startImageAlpha = 0.0f;

    public int paintingRadius {
        get {
            return _paintingRadius;
        }
        set {
            _paintingRadius = value;
        }
    }
    public float alphaPercentForFinish {
        get {
            return _alphaPercentForFinish;
        }
        set {
            _alphaPercentForFinish = value;
        }
    }
    public float startImageAlpha {
        get {
            return _startImageAlpha;
        }
        set {
            _startImageAlpha = value;
        }
    }

    private SpriteRenderer _spriteRenderer;
    private Texture2D _texture;
    private Vector2 _currentMouseClickPosition,
                    _previousMouseClickPosition,
                    _size;

    private float _width,
                _height,
                _left,
                _right,
                _top,
                _bottom;

    private int _startX,
                _startY,
                _finishX,
                _finishY,
                _currentCountPixels,
                _countPixels;

    public PaintingAlpha() {

    }

    public PaintingAlpha(int paintingRadius, float alphaPercentToFinish, float startImageAlpha) {
        _paintingRadius = paintingRadius;
        _alphaPercentForFinish = alphaPercentToFinish;
        _startImageAlpha = startImageAlpha;
    }

    private int _angle;

    public delegate void OnElementsFound();

    public event OnElementsFound OnPaintingFinishedEvent;

    void Start() {
        _spriteRenderer = GetComponent<SpriteRenderer>();

        CreateNewTexture();
        SpriteSize();
    }

    private void Update() {
        if (GetComponent<Collider2D>().enabled) {
            if (Input.GetMouseButtonDown(0)) {
                SpriteSize();
                _previousMouseClickPosition = GetMousePositionOnTexture();

                ChangeTexture();
            }
            else if (Input.GetMouseButton(0)) {
                _previousMouseClickPosition = _currentMouseClickPosition;

                ChangeTexture();
            }
            else if (Input.GetMouseButtonUp(0)) {
                _previousMouseClickPosition = new Vector2(0, 0);
            }
        }
    }

    private void CreateNewTexture() {
        Sprite sprite = _spriteRenderer.sprite;
        Texture2D texture = _spriteRenderer.sprite.texture;

        _texture = new Texture2D(texture.width, texture.height);
        _texture.SetPixels(texture.GetPixels());

        Vector2 pivot = new Vector2(sprite.pivot.x / texture.width, sprite.pivot.y / texture.height);

        _spriteRenderer.sprite = Sprite.Create(
            _texture,
            new Rect(Vector2.zero, new Vector2(texture.width, texture.height)),
            new Vector2(pivot.x, pivot.y)
        );

        _countPixels = 0;

        for (int i = 0; i < _texture.width; i++) {
            for (int j = 0; j < _texture.height; j++) {
                var currentPixel = _texture.GetPixel(i, j);
                _texture.SetPixel(i, j, new Color(currentPixel.r, currentPixel.g, currentPixel.b, _startImageAlpha));
                _countPixels++;
            }
        }

        _texture.Apply();
    }

    private void SpriteSize() {
        var sprite = _spriteRenderer.sprite;

        _left = transform.position.x - sprite.pivot.x / sprite.pixelsPerUnit * transform.lossyScale.x;
        _right = transform.position.x + (sprite.rect.width - sprite.pivot.x) / sprite.pixelsPerUnit * transform.lossyScale.x;
        _top = transform.position.y + (sprite.rect.height - sprite.pivot.y) / sprite.pixelsPerUnit * transform.lossyScale.y;
        _bottom = transform.position.y - sprite.pivot.y / sprite.pixelsPerUnit * transform.lossyScale.y;
    }

    private Vector2 RotatePoint(Vector2 pointOfRotation, Vector2 pointPosition, float angle) {
        var xOffset = pointOfRotation.x;
        var yOffset = pointOfRotation.y;

        pointOfRotation = Vector2.zero;
        pointPosition = new Vector2(pointPosition.x - xOffset, pointPosition.y - yOffset);

        var x = pointOfRotation.x + (pointPosition.x - pointOfRotation.x) * Mathf.Cos(angle * 3.14f / 180f) + (pointOfRotation.y - pointPosition.y) * Mathf.Sin(angle * 3.14f / 180f);
        var y = pointOfRotation.x + (pointPosition.x - pointOfRotation.x) * Mathf.Sin(angle * 3.14f / 180f) + (pointPosition.y - pointOfRotation.y) * Mathf.Cos(angle * 3.14f / 180f);

        x += xOffset;
        y += yOffset;

        return new Vector2(x, y);
    }

    public void FillAllTexture() {
        for (int i = 0; i < _texture.width; i++) {
            for (int j = 0; j < _texture.height; j++) {
                var currentPixel = _texture.GetPixel(i, j);
                if (currentPixel.a < 1) {
                    _texture.SetPixel(i, j, new Color(currentPixel.r, currentPixel.g, currentPixel.b, 1));
                }
            }
        }
        _texture.Apply();
    }

    private Vector2 GetMousePositionOnTexture() {
        var inputMousePosition = Input.mousePosition;

        var _mouseClickPosition = RotatePoint(transform.position, Camera.main.ScreenToWorldPoint(inputMousePosition), -transform.eulerAngles.z);

        int x = Mathf.RoundToInt(_texture.width / (_right - _left) * (_mouseClickPosition.x - _left)) - 1;
        int y = Mathf.RoundToInt(_texture.height / (_top - _bottom) * (_mouseClickPosition.y - _bottom)) - 1;

        return new Vector2(x, y);
    }

    private void ChangeTexture() {
        _currentMouseClickPosition = GetMousePositionOnTexture();

        if (_currentMouseClickPosition.x < 0 - _paintingRadius)
            return;
        else if (_currentMouseClickPosition.y < 0 - _paintingRadius)
            return;
        else if (_currentMouseClickPosition.x > _texture.width + _paintingRadius)
            return;
        else if (_currentMouseClickPosition.y > _texture.height + _paintingRadius)
            return;

        FillCompoundPoints(_previousMouseClickPosition, new Vector2(_currentMouseClickPosition.x, _currentMouseClickPosition.y));
    }

    private void DrawPointOutline(Vector2 point, int radius) {
        int x = 0;
        int y = radius;
        int delta = 1 - 2 * radius;
        int error = 0;
        while (y >= 0) {

            var currentNegativePointX = (int)point.x - x;
            var currentPositivePointX = (int)point.x + x;
            var currentNegativePointY = (int)point.y - y;
            var currentPositivePointY = (int)point.y + y;

            if (_texture.GetPixel(currentPositivePointX, currentPositivePointY).a < 1) {
                if (currentPositivePointX < _texture.width && currentPositivePointY < _texture.height && currentPositivePointX >= 0 && currentPositivePointY >= 0) {
                    var currentPixel = _texture.GetPixel(currentPositivePointX, currentPositivePointY);
                    var opaqueCurrentPixel = new Color(currentPixel.r, currentPixel.g, currentPixel.b, 1);

                    _texture.SetPixel(currentPositivePointX, currentPositivePointY, opaqueCurrentPixel);
                    _currentCountPixels++;
                }
            }

            if (_texture.GetPixel(currentPositivePointX, currentNegativePointY).a < 1) {
                if (currentPositivePointX < _texture.width && currentNegativePointY < _texture.height && currentPositivePointX >= 0 && currentNegativePointY >= 0) {
                    var currentPixel = _texture.GetPixel(currentPositivePointX, currentNegativePointY);
                    var opaqueCurrentPixel = new Color(currentPixel.r, currentPixel.g, currentPixel.b, 1);

                    _texture.SetPixel(currentPositivePointX, currentNegativePointY, opaqueCurrentPixel);
                    _currentCountPixels++;
                }
            }


            if (_texture.GetPixel(currentNegativePointX, currentPositivePointY).a < 1) {
                if (currentNegativePointX < _texture.width && currentPositivePointY < _texture.height && currentNegativePointX >= 0 && currentPositivePointY >= 0) {
                    var currentPixel = _texture.GetPixel(currentNegativePointX, currentPositivePointY);
                    var opaqueCurrentPixel = new Color(currentPixel.r, currentPixel.g, currentPixel.b, 1);

                    _texture.SetPixel(currentNegativePointX, currentPositivePointY, opaqueCurrentPixel);
                    _currentCountPixels++;
                }
            }

            if (_texture.GetPixel(currentNegativePointX, currentNegativePointY).a < 1) {
                if (currentNegativePointX < _texture.width && currentNegativePointY < _texture.height && currentNegativePointX >= 0 && currentNegativePointY >= 0) {
                    var currentPixel = _texture.GetPixel(currentNegativePointX, currentNegativePointY);
                    var opaqueCurrentPixel = new Color(currentPixel.r, currentPixel.g, currentPixel.b, 1);

                    _texture.SetPixel(currentNegativePointX, currentNegativePointY, opaqueCurrentPixel);
                    _currentCountPixels++;
                }
            }

            error = 2 * (delta + y) - 1;

            if (delta < 0 && error <= 0) {
                x++;
                delta += 2 * x + 1;
                continue;
            }

            error = 2 * (delta - x) - 1;

            if (delta > 0 && error > 0) {
                y--;
                delta += 1 - 2 * y;
                continue;
            }

            x++;
            delta += 2 * (x - y);
            y--;


        }
    }

    private void DrawPoint(Vector2 point, int radius) {
        float a = radius * 1f;
        float b = radius * 1f;

        for (int i = (int)point.x - radius; i < point.x + radius; i++) {
            if (i > _texture.width - 1 || i < 0)
                continue;

            for (int j = (int)point.y - radius; j < point.y + radius; j++) {
                if (j > _texture.height - 1 || j < 0)
                    continue;

                if (((i - point.x) * (i - point.x)) / (a * a) + ((j - point.y) * (j - point.y)) / (b * b) <= 1) {
                    var currentPixel = _texture.GetPixel(i, j);
                    if (currentPixel.a < 1) {
                        var opaqueCurrentPixel = new Color(currentPixel.r, currentPixel.g, currentPixel.b, 1);
                        _texture.SetPixel(i, j, opaqueCurrentPixel);
                        _currentCountPixels++;
                    }
                }
            }
        }
        _texture.Apply();
    }

    private void FillCompoundPoints(Vector2 from, Vector2 to) {
        if (from == new Vector2(0, 0)) {
            return;
        }

        int deltaX = (int)Mathf.Abs(to.x - from.x);
        int deltaY = (int)Mathf.Abs(to.y - from.y);

        int signX = from.x < to.x ? 1 : -1;
        int signY = from.y < to.y ? 1 : -1;

        int error = deltaX - deltaY;

        DrawPoint(from, _paintingRadius);
        if (from != to) {
            while ((signX > 0 ? from.x <= to.x : from.x > to.x) || (signY > 0 ? from.y <= to.y : from.y > to.y)) {
                if (_currentCountPixels > (_countPixels * _alphaPercentForFinish)) {

                    var collider = GetComponent<BoxCollider2D>();
                    if (collider) {
                        collider.enabled = false;
                    }

                    if (OnPaintingFinishedEvent != null) {
                        OnPaintingFinishedEvent();
                    }

                    return;
                }

                DrawPointOutline(from, _paintingRadius);

                int error2 = error * 2;

                if (error2 > -deltaY) {
                    error -= deltaY;
                    from.x += signX;
                }
                else if (error2 < deltaX) {
                    error += deltaX;
                    from.y += signY;
                }
            }

            _texture.Apply();
        }
    }
}