﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public static class JSONHelpers {
    public static T[] FromJson<T>(string json) {
        ArrayWrapper<T> wrapper = JsonUtility.FromJson<ArrayWrapper<T>>(json);
        return wrapper.Array;
    }
    public static string ToJson<T>(List<T> list) {
        ArrayWrapper<T> wrapper = new ArrayWrapper<T>();
        wrapper.Array = list.ToArray();
        return JsonUtility.ToJson(wrapper);
    }
    public static string ToJson<T>(List<T> list, bool prettyPrint) {
        ArrayWrapper<T> wrapper = new ArrayWrapper<T>();
        wrapper.Array = list.ToArray();
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    public static string ToJson<T>(T[] array) {
        ArrayWrapper<T> wrapper = new ArrayWrapper<T>();
        wrapper.Array = array;
        return JsonUtility.ToJson(wrapper);
    }
    public static string ToJson<T>(T[] array, bool prettyPrint) {
        ArrayWrapper<T> wrapper = new ArrayWrapper<T>();
        wrapper.Array = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class ArrayWrapper<T> {
        public T[] Array;
    }
}
