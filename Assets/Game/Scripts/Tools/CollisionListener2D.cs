﻿using UnityEngine;
using System;

public class CollisionListener2D : MonoBehaviour {
    public event Action<Collider2D> OnTriggerEnter;
    public event Action<Collider2D> OnTriggerStay;
    public event Action<Collider2D> OnTriggerExit;
    public event Action<Collision2D> OnCollisionEnter;
    public event Action<Collision2D> OnCollisionStay;
    public event Action<Collision2D> OnCollisionExit;

    public void OnTriggerEnter2D(Collider2D collider) {
        if (OnTriggerEnter != null) {
            OnTriggerEnter(collider);
        }
    }
    public void OnTriggerStay2D(Collider2D collider) {
        if (OnTriggerStay != null) {
            OnTriggerStay(collider);
        }
    }
    public void OnTriggerExit2D(Collider2D collider) {
        if (OnTriggerExit != null) {
            OnTriggerExit(collider);
        }
    }
    public void OnCollisionEnter2D(Collision2D collision) {
        if (OnCollisionEnter != null) {
            OnCollisionEnter(collision);
        }
    }
    public void OnCollisionStay2D(Collision2D collision) {
        if (OnCollisionStay != null) {
            OnCollisionStay(collision);
        }
    }
    public void OnCollisionExit2D(Collision2D collision) {
        if (OnCollisionExit != null) {
            OnCollisionExit(collision);
        }
    }
}
