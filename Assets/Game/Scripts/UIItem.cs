﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using System.Linq;

public class UIItem : MonoBehaviour//, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
    public Transform partParent { get; private set; }
    public Part partContained { get; private set; }
    public Transform starsParent { get; private set; }

    public float instanceScale { get; private set; }
    public float itemScale { get; private set; }

    public Part draggablePartInstance { get; private set; }

    public Vector2 prevMousePos { get; private set; }
    public Vector2 currMousePos { get; private set; }

    public void LoadPart(Part part) {
        partParent = transform.Find("Container_PartGameObject");
        partContained = partParent.GetComponentInChildren<Part>();
        starsParent = transform.Find("Grid_Stars");

        instanceScale = .58f;
        itemScale = 1f;

        draggablePartInstance = null;

        prevMousePos = Vector2.zero;
        currMousePos = Vector2.zero;

        gameObject.SetActive(true);
        SetPartContained(part);
    }
    public void SetPartContained(Part part) {
        if (partContained) {
            partContained.transform.parent = null;
            DestroyImmediate(partContained);
        }
        partContained = part;
        part.gameObject.SetActive(true);
        part.transform.SetParent(partParent);
        part.touchableCollider.enabled = false;
        part.transform.localPosition = Vector3.zero;
        part.transform.localScale = Vector3.one * itemScale;
        LoadStars(part.stars);
    }
    private void LoadStars(int starsCount) {
        int i = 0;
        for (; i < starsCount; i++) {
            starsParent.GetChild(i).gameObject.SetActive(true);
        }
        for (; i < starsParent.childCount; i++) {
            starsParent.GetChild(i).gameObject.SetActive(false);
        }
    }
    private void InstantiatePart(Vector3 position, Vector3 scale, out GameObject partGOInstance, out Part partInstance) {
        partGOInstance = (GameObject)GameObject.Instantiate(partContained.gameObject);
        partGOInstance.transform.localScale = scale;
        partGOInstance.transform.position = position;
        partInstance = partGOInstance.GetComponent<Part>();
        partInstance.touchableCollider.enabled = true;
    }

    #region Input events
    public void OnPointerDown(PointerEventData eventData) {
        AudioPlayer.Instance.Play("part_pick");
        if (partContained) {
            GarageController.Instance.blankDisplayController.SelectPart(partContained);
        }
    }
    public void OnBeginDrag(PointerEventData eventData) {
        AudioPlayer.Instance.Play("part_drop");
        currMousePos = Input.mousePosition;

        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(currMousePos);
        worldPosition.z = 0;

        GameObject draggableInstance;
        Part draggablePartInstance;
        InstantiatePart(worldPosition, Vector3.one * instanceScale, out draggableInstance, out draggablePartInstance);
        this.draggablePartInstance = draggablePartInstance;
    }
    public void OnDrag(PointerEventData eventData) {
        if (draggablePartInstance) {
            prevMousePos = currMousePos;
            currMousePos = Input.mousePosition;

            var deltaPos = Camera.main.ScreenToWorldPoint(currMousePos) - Camera.main.ScreenToWorldPoint(prevMousePos);
            draggablePartInstance.transform.position += deltaPos;
        }
    }
    public void OnEndDrag(PointerEventData eventData) {
        if (draggablePartInstance) {
            var asBody = draggablePartInstance as Body;
            bool wasAttached = false;
            if (asBody) {
                bool carColliderTouched =
                    (GarageController.Instance.currentCarConstructed.bodyAttachingCollider.transform.position - draggablePartInstance.attachmentPosition.position).sqrMagnitude
                    < GarageController.Instance.minSqrDistanceToAttachBody;
                wasAttached = carColliderTouched;
                if (carColliderTouched) {
                    AudioPlayer.Instance.Play("part_attach");
                    GarageController.Instance.currentCarConstructed.Replace(asBody);
                }
            }
            if (GarageController.Instance.currentCarConstructed.body) {
                var asWheel = draggablePartInstance as Wheel;
                var asWeapon = draggablePartInstance as Weapon;
                var asItem = draggablePartInstance as Item;
                if (asWheel) {
                    WheelPoint wheelPointTouched =
                        GarageController.Instance.currentCarConstructed.body.wheelPoints.Find(
                            x => (x.transform.position - draggablePartInstance.attachmentPosition.position).sqrMagnitude < GarageController.Instance.minSqrDistanceToAttachAttachments);
                    wasAttached = wheelPointTouched;
                    if (wheelPointTouched) {
                        wheelPointTouched.Replace(asWheel);
                    }
                }
                else if (asWeapon) {
                    WeaponPoint weaponPointTouched =
                        GarageController.Instance.currentCarConstructed.body.weaponPoints.Find(
                            x => (x.transform.position - draggablePartInstance.attachmentPosition.position).sqrMagnitude < GarageController.Instance.minSqrDistanceToAttachAttachments);
                    wasAttached = weaponPointTouched;
                    if (weaponPointTouched) {
                        weaponPointTouched.Replace(asWeapon);
                    }
                }
                else if (asItem) {
                    ItemPoint itemPointTouched =
                        GarageController.Instance.currentCarConstructed.body.itemPoints.Find(
                            x => (x.transform.position - draggablePartInstance.attachmentPosition.position).sqrMagnitude < GarageController.Instance.minSqrDistanceToAttachAttachments);
                    wasAttached = itemPointTouched;
                    if (itemPointTouched) {
                        itemPointTouched.Replace(asItem);
                    }
                }
                if (wasAttached) {
                    AudioPlayer.Instance.Play("part_attach");
                    DestroyImmediate(gameObject);
                    return;
                }
            }
            DestroyImmediate(draggablePartInstance.gameObject);
            draggablePartInstance = null;
        }
    }
    #endregion
}
