﻿using UnityEngine;
using System.Collections;
using SocketIO;
using System;
using System.Text.RegularExpressions;

public class SocketIOHelper : Singletone<SocketIOHelper> {
    public event Action<GameResponse> OnGameResponceReceived;

    protected override void Init() {
        SocketIOComponent.Instance.On(ServerAPIEvents.game_response.ToString(), GameResponceHandler);
        Login();
    }
    private void Login() {
        SocketIOComponent.Instance.Emit(ClientAPIEvents.login.ToString(), new JSONObject(JsonUtility.ToJson(new DeviceInfo())));
    }
    public void GameResponceHandler(SocketIOEvent socketIOevent) {
        if (OnGameResponceReceived != null) {
            Debug.Log(Regex.Unescape(socketIOevent.data.Print(true)));
            OnGameResponceReceived(JsonUtility.FromJson<GameResponse>(Regex.Unescape(socketIOevent.data.Print())));
        }
    }
    public void SendGameRequest(GameRequest request) {
        SocketIOComponent.Instance.Emit(ClientAPIEvents.game_request.ToString(), new JSONObject(JsonUtility.ToJson(request)));
    }
    public void SendGameRequest(bool isRandom, string opponent = "") {
        SocketIOComponent.Instance.Emit(ClientAPIEvents.game_request.ToString(), new JSONObject(JsonUtility.ToJson(new GameRequest(isRandom, opponent))));
    }
}
