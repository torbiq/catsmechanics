﻿using UnityEngine;
using System;
using Newtonsoft.Json;

[Serializable]
[JsonObject(MemberSerialization.OptIn)]
public class Level {
    [JsonProperty(PropertyName = "level")]
    public int level { get; private set; }
    [JsonProperty(PropertyName = "progress")]
    public int progress { get; private set; }
    [JsonProperty(PropertyName = "hp_per_level")]
    public int hpPerLevel { get; private set; }
    [JsonProperty(PropertyName = "hp_bonus")]
    public int hpBonus { get; private set; }
    [JsonProperty(PropertyName = "damage_per_level")]
    public int damagePerLevel { get; private set; }
    [JsonProperty(PropertyName = "damage_bonus")]
    public int damageBonus { get; private set; }
    [JsonProperty(PropertyName = "max")]
    public int max { get; private set; }

    public float ProgressPercentage { get { return ((float)progress / (float)max); } }

    public event Action<int> OnProgressChangedEvent;
    public event Action<int> OnLevelsChangedEvent;
    public event Action<int> OnHpBonusChangedEvent;
    public event Action<int> OnDamageBonusChangedEvent;

    private void AddLevels(int levels) {
        if (levels > 0) {
            level += levels;
            if (OnLevelsChangedEvent != null) {
                OnLevelsChangedEvent(levels);
            }
            int hpAdded = hpPerLevel * levels;
            if (hpAdded > 0) {
                hpBonus += hpAdded;
                if (OnHpBonusChangedEvent != null) {
                    OnHpBonusChangedEvent(hpAdded);
                }
            }
            int damageAdded = damagePerLevel * levels;
            if (damageAdded > 0) {
                damageBonus += damageAdded;
                if (OnDamageBonusChangedEvent != null) {
                    OnDamageBonusChangedEvent(hpAdded);
                }
            }
        }
    }
    public void AddProgress(int progress) {
        if (progress > 0) {
            this.progress += progress;
            AddLevels(this.progress / max);
            this.progress %= max;
            if (OnProgressChangedEvent != null) {
                OnProgressChangedEvent(this.progress);
            }
        }
    }
    public Level() {
        level = 0;
        max = 100;
        progress = 0;
        hpPerLevel = 0;
        hpBonus = 0;
        damagePerLevel = 0;
        damageBonus = 0;
    }
    public Level(
        int level,
        int progress,
        int hpPerLevel = 0, 
        int hpBonus = 0, 
        int damagePerLevel = 0, 
        int damageBonus = 0,
        int max = 100
        ) {
        this.level = level;
        this.max = max;
        this.progress = progress;
        this.hpPerLevel = hpPerLevel;
        this.hpBonus = hpBonus;
        this.damagePerLevel = damagePerLevel;
        this.damageBonus = damageBonus;
    }
    public Level(Level itemLevel) {
        this.level = itemLevel.level;
        this.max = itemLevel.max;
        this.progress = itemLevel.progress;
        this.hpPerLevel = itemLevel.hpPerLevel;
        this.hpBonus = itemLevel.hpBonus;
        this.damagePerLevel = itemLevel.damagePerLevel;
        this.damageBonus = itemLevel.damageBonus;
    }
}