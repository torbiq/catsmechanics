﻿using Newtonsoft.Json;
using System;

[Serializable]
[JsonObject(MemberSerialization.OptIn)]
public class Chartics {
    // JSON SERIALIZED PROPERTIES
    [JsonProperty(PropertyName = "hp")]
    public int hp { get; private set; }
    [JsonProperty(PropertyName = "damage")]
    public int damage { get; private set; }
    [JsonProperty(PropertyName = "energy")]
    public int energy { get; private set; }

    [JsonProperty(PropertyName = "toolbox_hp")]
    public int toolboxHp { get; private set; }
    [JsonProperty(PropertyName = "toolbox_damage")]
    public int toolboxDamage { get; private set; }
    [JsonProperty(PropertyName = "toolbox_energy")]
    public int toolboxEnergy { get; private set; }
    // JSON SERIALIZED PROPERTIES

    public event Action<int> OnHpChanged;
    public event Action<int> OnDamageChanged;
    public event Action<int> OnEnergyChanged;

    public event Action<int> OnToolboxHpChanged;
    public event Action<int> OnToolboxDamageChanged;
    public event Action<int> OnToolboxEnergyChanged;

    public void SetHp(int hp) {
        this.hp = hp;
        if (OnHpChanged != null) {
            OnHpChanged(hp);
        }
    }
    public void SetDamage(int damage) {
        this.damage = damage;
        if (OnDamageChanged != null) {
            OnDamageChanged(damage);
        }
    }
    public void SetEnergy(int energy) {
        this.energy = energy;
        if (OnEnergyChanged != null) {
            OnEnergyChanged(energy);
        }
    }
    public void SetToolboxHp(int hp) {
        this.toolboxHp = hp;
        if (OnToolboxHpChanged != null) {
            OnToolboxHpChanged(hp);
        }
    }
    public void SetToolboxDamage(int damage) {
        this.toolboxDamage = damage;
        if (OnToolboxDamageChanged != null) {
            OnToolboxDamageChanged(damage);
        }
    }
    public void SetToolboxEnergy(int energy) {
        this.toolboxEnergy = energy;
        if (OnToolboxEnergyChanged != null) {
            OnToolboxEnergyChanged(energy);
        }
    }

    public Chartics() {
        hp = 0;
        damage = 0;
        energy = 0;
        toolboxHp = 0;
        toolboxDamage = 0;
        toolboxEnergy = 0;
    }

    public Chartics(
        int hp, 
        int damage, 
        int energy, 
        int toolboxHp = 0,
        int toolboxDamage = 0, 
        int toolboxEnergy = 0
        ) {
        this.hp = hp;
        this.damage = damage;
        this.energy = energy;
        this.toolboxHp = toolboxHp;
        this.toolboxDamage = toolboxDamage;
        this.toolboxEnergy = toolboxEnergy;
    }
    public Chartics(Chartics chartics) {
        this.hp = chartics.hp;
        this.damage = chartics.damage;
        this.energy = chartics.energy;
        this.toolboxHp = chartics.toolboxHp;
        this.toolboxDamage = chartics.toolboxDamage;
        this.toolboxEnergy = chartics.toolboxEnergy;
    }
}