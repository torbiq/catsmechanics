﻿public enum PointType : byte {
    Wheel = 0,
    Weapon = 1,
    Item = 2,
}
public enum PartType : byte {
    Body = 0,
    Wheel = 1,
    Weapon = 2,
    Item = 3,
    Toolbox = 4,
}
public enum BonusType : byte {
    Body = 0,
    Item = 1,
    Weapon = 2,
    Wheel = 3,
}
public enum Strength : byte {
    Wooden = 0,
    Metal = 1,
    Military = 2,
    Golden = 3,
    Carbon = 4,
}
public enum League : byte {
    Wooden = 0,
    Metal = 1,
    Military = 2,
    Golden = 3,
    Carbon = 4,
}
public enum Subleague : byte {
    SUBLEAGUE_1 = 0,
    SUBLEAGUE_2 = 1,
    SUBLEAGUE_3 = 2,
    SUBLEAGUE_4 = 3,
    SUBLEAGUE_5 = 4,
}
public enum BodyType : byte {
    Classic = 0,
    Titan = 1,
    Surfer = 2,
    Sneaky = 3,
    Boulder = 4,
}
public enum WeaponType : byte {
    Rocket = 0,
    Blade = 1,
    Laser = 2,
    Chainsaw = 3,
    Stinger = 4,
    Drill = 5,
}
public enum WheelType : byte {
    Bigfoot = 0,
    Tire = 1,
    Scooter = 2,
    Roller = 3,
    Knob = 4,
}
public enum StickerType : byte {
    Girl = 0,
    Brush = 1,
    Tony = 2,
    Droid = 3,
    Clef = 4,
    Robot = 5,
}
public enum ToolboxType : byte {
    Health = 0,
    Damage = 1,
    Energy = 2,
}
public enum ItemType : byte {
    Reverser = 0,
}
public enum Rarity : byte {
    Common = 0,
    Rare = 1,
    Mythtic = 2,
    Legendary = 3,
}
public enum ClientAPIEvents : byte {
    connection = 0,
    login = 1,
    car_serialized = 2,
    game_request = 3,
    game_finished = 4,
    disconnect = 5,
}
public enum ServerAPIEvents : byte {
    game_response = 0,
}