﻿using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.AdminModels;
//using SocketIO;

public class PlayfabController : MonoBehaviour {
    public static bool isLogged = false;
    public static string playfabID = "";
    public static PlayFab.ClientModels.UserAccountInfo accountInfo;

    private void Awake() {
        if (!isLogged) {
            TryLogin();
        }
    }
    private void TryLogin() {
        var req = CreateLoginRequest();
    }
    private LoginWithAndroidDeviceIDRequest CreateLoginRequest() {
        LoginWithAndroidDeviceIDRequest req = new LoginWithAndroidDeviceIDRequest();
        req.TitleId = PlayFabSettings.TitleId;
        req.CreateAccount = true;
        req.AndroidDevice = SystemInfo.deviceModel;
        req.AndroidDeviceId = SystemInfo.deviceUniqueIdentifier;
        req.OS = SystemInfo.operatingSystem;
        return req;
    }
    private GetAccountInfoRequest CreateGetAccountRequest() {
        GetAccountInfoRequest accRequest = new GetAccountInfoRequest();
        accRequest.PlayFabId = playfabID;
        return accRequest;
    }
    //private static CarConstructedSerialized GetSerializedCar() {
    //    PlayFabAdminAPI.GetPlayerSegments
    //    return null;
    //}
}
