﻿using UnityEngine;
using System.Collections;

public class GridVisualiser : MonoBehaviour {
    public Transform min;
    public Transform max;
    public float step;
    public Color color;
    public bool enabled = false;
    private void OnDrawGizmos() {
        if (enabled) {
            if (min && max) {
                Vector2 scale = max.position - min.position;
                Vector2 upLeft = new Vector2(min.position.x, max.position.y);
                int xSteps = Mathf.RoundToInt((max.position.x - upLeft.x) / step);
                int ySteps = Mathf.RoundToInt((upLeft.y - min.position.y) / step);
                Gizmos.color = color;
                for (int x = 0; x < xSteps; x++) {
                    for (int y = 0; y < ySteps; y++) {
                        Gizmos.DrawSphere(new Vector2(upLeft.x + x * step, upLeft.y - y * step), 0.1f);
                    }
                    Gizmos.DrawSphere(new Vector2(upLeft.x + x * step, min.position.y), 0.1f);
                }
            }
        }
    }
}
