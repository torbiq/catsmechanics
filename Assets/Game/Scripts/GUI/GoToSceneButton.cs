﻿using UnityEngine;
using UnityEngine.UI;
using PSV_Prototype;

[RequireComponent(typeof(Button))]
public class GoToSceneButton : ButtonClickHandler
{
	public Scenes target;

    protected override void OnButtonClick ()
    {
        SceneLoader.Instance.SwitchToScene ( target );
    }

}
