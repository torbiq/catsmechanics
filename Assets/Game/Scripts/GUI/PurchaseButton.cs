﻿using UnityEngine;
using System.Collections;


public class PurchaseButton : ButtonClickHandler {
    /// ChangeLog
    /// v2.0
    /// added support fro any product (just select enum item)


    public BillingManager.Products product = BillingManager.Products.SKU_ADMOB;


    // Use this for initialization
    void Start() {
#if UNITY_ANDROID || UNITY_IPHONE
        if (product == BillingManager.Products.SKU_ADMOB) {
            this.gameObject.SetActive(ManagerGoogle.Instance.IsAdmobEnabled());
        }
#endif
        BillingManager.OnPurchaseSucceded += OnPurchaseSucceded;
        BillingManager.OnPurchaseCancelled += OnPurchaseCancelled;
    }

    void OnDestroy() {
        BillingManager.OnPurchaseSucceded -= OnPurchaseSucceded;
        BillingManager.OnPurchaseCancelled -= OnPurchaseCancelled;
    }


    void Action() {
#if UNITY_ANDROID || UNITY_IPHONE
        BillingManager.Instance.Purchase(product);
#endif
    }


    private void OnPurchaseSucceded(BillingManager.Products _product, bool consumable) {
        if (_product == product && !consumable) {
            this.gameObject.SetActive(false);
        }
    }

    private void OnPurchaseCancelled(BillingManager.Products _product, bool consumable) {
        if (_product == product && !consumable) {
            this.gameObject.SetActive(true);
        }
    }

    protected override void OnButtonClick() {
        Action();
    }
}