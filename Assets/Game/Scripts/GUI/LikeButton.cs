﻿using UnityEngine;
using System.Collections;

public class LikeButton :MonoBehaviour
{

    private string

        ios_app_bundle = "",
        wp_app_bundle = "",
        android_makret_app_url = "market://details?id=",
        ios_market_app_url = "itms-apps://itunes.apple.com/app/",
        wp_market_app_url = "ms-windows-store:navigate?appid=";


    public void Action ()
    {

        Application.OpenURL ( GetAppMarketURL ( GetAppBundle() ) );
    }


    public string GetAppBundle ()
    {
        string res = "";
        switch (GetPlatform ( ))
        {
            case 0:
                {
                    res = Application.bundleIdentifier;
                    break;
                }
            case 1:
                {
                    res = ios_app_bundle;
                    break;
                }
            case 2:
                {
                    res = wp_app_bundle;
                    break;
                }
        }
        return res;
    }

    public string GetAppMarketURL (string app_alias)
    {
        string res = "";
        switch (GetPlatform ( ))
        {
            case 0:
                {
                    res = android_makret_app_url + app_alias;
                    break;
                }
            case 1:
                {
                    res = ios_market_app_url + app_alias;
                    break;
                }
            case 2:
                {
                    res = wp_market_app_url + app_alias;
                    break;
                }
        }
        return res;
    }


    private int GetPlatform ()
    {
        int res = 0;
#if UNITY_ANDROID
        res = 0;
#elif UNITY_IPHONE
        res = 1;
#elif UNITY_WP8 || UNITY_WP8_1
        res = 2;
#endif
        return res;
    }




}
