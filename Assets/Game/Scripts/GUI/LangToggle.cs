﻿#define BRAND_NEW_PROTOTYPE
public class LangToggle :ToggleButton
{

	public Languages.Language
		lang;

	private bool
		initialized = false;

	protected override void OnValueChanged (bool param)
	{
		if (initialized && param)
		{
			SetLanguage ( );
		}
	}

#if BRAND_NEW_PROTOTYPE
	protected override void Awake ()
	{
		if (!Languages.languages.ContainsKey ( lang ))
		{
			gameObject.SetActive ( false );
		}
		else
		{
			base.Awake ( );
		}
	}
#endif

	void SetLanguage ()
	{
		Languages.SetLanguage ( lang );
	}


	void Start ()
	{
		if (Languages.GetLanguage ( ) == lang)
		{
			SetToggle ( true );
		}
		initialized = true;
	}

}
