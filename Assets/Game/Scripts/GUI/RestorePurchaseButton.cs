﻿using UnityEngine;
using System.Collections;


public class RestorePurchaseButton :ButtonClickHandler
{

	// Use this for initialization
	void Start ()
	{
#if !UNITY_IPHONE
        this.gameObject.SetActive ( false );
#endif
	}


	void Action ()
	{
#if UNITY_IPHONE
		BillingManager.Instance.RestoreProducts(); 
#endif
	}

	protected override void OnButtonClick ()
	{
		Action ( );
	}
}