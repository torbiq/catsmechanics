﻿using PSV_Prototype;
using UnityEngine;

public class PausePanel :DialoguePanel
{
	public bool show_on_esc = true;

	private bool ignore_application_pause = true;




	protected override void OnEnable ()
	{
		//Debug.Log ( "PausePanel. OnEnable" );

		base.OnEnable ( );

		Show ( PauseManager.Instance.IsPaused ( ) );

		PauseManager.OnPause += OnPause;
		if (show_on_esc)
		{
			KeyListener.OnKeyPressed += KeyListener_OnKeyPressed;
		}
	}


	protected override void OnDisable ()
	{
		base.OnDisable ( );
		PauseManager.OnPause -= OnPause;
		if (PauseManager.Instance.IsPaused ( ))
		{
			PauseManager.Instance.SetPause ( false );
		}
		if (show_on_esc)
		{
			KeyListener.OnKeyPressed -= KeyListener_OnKeyPressed;
		}
	}


	private void KeyListener_OnKeyPressed (KeyCode key)
	{
		if (key == KeyCode.Escape)
		{
			PauseManager.Instance.TogglePause ( );
		}
	}


	private void OnPause (bool pause)
	{
		//Debug.Log ( "PausePanel. OnPause " + pause );
		ShowPannel ( pause );
	}


	protected override void OnBlockerClick ()
	{
		//base.OnBlockerClick ( );
		PauseManager.Instance.SetPause ( false );
	}


	private void OnApplicationPause (bool pause)
	{
		if (!ignore_application_pause && pause)
		{
			PauseManager.Instance.SetPause ( true );
		}
	}

	public void IgnoreApplicationPause (bool param)
	{
		ignore_application_pause = param;
	}

}
