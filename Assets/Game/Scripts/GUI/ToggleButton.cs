﻿using UnityEngine;
using UnityEngine.UI;


public class ToggleButton : MonoBehaviour
{

    protected ButtonClickSound
        clicker;
    protected Toggle
        toggle;
	public bool
		center_pivot = true;

	virtual protected void Awake ()
    {
        toggle = GetComponent<Toggle> ( );
        toggle.onValueChanged.AddListener ( OnValueChanged );
        clicker = GetComponent<ButtonClickSound> ( );
		Animator anim = GetComponent<Animator> ( );
		if (anim != null)
		{
			anim.updateMode = AnimatorUpdateMode.UnscaledTime;
		}
		if (center_pivot)
		{
			RectTransform rt = GetComponent<RectTransform> ( );
			rt.pivot = Vector2.one * 0.5f;
		}
	}

    virtual protected void OnValueChanged (bool val)
    {

    }

    protected void SetToggle (bool param)
    {
        EnableClicker ( false );
        toggle.isOn = param;
        EnableClicker ( true );
    }

    void EnableClicker (bool param)
    {
        if (clicker != null)
        {
            clicker.enabled = param;
        }
    }
}
