﻿using UnityEngine;
using UnityEngine.UI;
using PSV_Prototype;

[RequireComponent ( typeof ( Image ) )]
public class LoadingIndicator :MonoBehaviour
{

	private Image
		indicator;

	void Awake ()
	{
		indicator = GetComponent<Image> ( );
	}

	// Use this for initialization
	void UpdateIndicator (float progress)
	{
		//Debug.Log ( "UpdateIndicator " + progress );
		if (indicator != null)
		{
			indicator.fillAmount = progress;
		}
	}

	void OnEnable ()
	{
		UpdateIndicator ( 0f );
		SceneLoader.OnLoadProgressChange += UpdateIndicator;
	}

	void OnDisable ()
	{
		SceneLoader.OnLoadProgressChange -= UpdateIndicator;
	}
}
