﻿
namespace PSV_Prototype
{

	public class PauseToggleButton :ButtonClickHandler
	{

		protected override void OnButtonClick ()
		{
			PauseManager.Instance.TogglePause ( );
		}
	}
}
