﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class QuitMenu :MonoBehaviour
{
    private bool
        isMoving = false,
        menuShown = false;
    private Vector2
        MenuShift = new Vector2 ( 0, 1000 );
    private float
        TargetAlpha = 200f / 250f,
        AnimationDuration = 0.25f;
    public GraphicRaycaster
        PanelRaycaster;
    public GameObject
        BackObject;
    public RectTransform
        QuitMenuPanel;
    public Image
        BackPanel;
    private Sequence
        seq = null;

    public void ProcessKey (KeyCode key)
    {
        if (key == KeyCode.Escape)
        {
            ShowMenu ( );
        }
    }

    void Awake ()
    {
#if UNITY_WEBGL
        this.gameObject.SetActive ( false );
#endif
    }


    void Start ()
    {
        BackObject.SetActive ( false );
        PanelRaycaster.enabled = false;
        BackPanel.color = new Color ( 0, 0, 0, 0 );
        QuitMenuPanel.localPosition = MenuShift;
        isMoving = menuShown = false;
    }


    void OnEnable ()
    {
        KeyListener.OnKeyPressed += ProcessKey;
    }

    void OnDisable ()
    {
        KeyListener.OnKeyPressed -= ProcessKey;
    }

    public void OkPressed ()
    {
        Application.Quit ( );
    }


    public void CancelPressed ()
    {
        ShowMenu ( );
    }

    public void ShowMenu ()
    {
        if (seq != null && isMoving)
        {
            seq.Kill ( );
        }

        menuShown = !menuShown;
        Vector2 MenuPos = menuShown ? Vector2.zero : MenuShift;
        float PanelAlpha = menuShown ? TargetAlpha : 0;
        if (menuShown)
            BackObject.SetActive ( true );
        PanelRaycaster.enabled = menuShown;
        //BackObject.SetActive ( false );
        seq = DOTween.Sequence ( );
        isMoving = true;
        seq.Append ( QuitMenuPanel.DOLocalMove ( MenuPos, AnimationDuration, false ).SetEase ( Ease.OutElastic ) );
        seq.Join ( BackPanel.DOFade ( PanelAlpha, AnimationDuration * (menuShown ? 4 : 1) ) );
        seq.AppendCallback ( () =>
        {
            isMoving = false;
            if (!menuShown)
                BackObject.SetActive ( false );
        } );
    }

}
