﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// ChangeLog
/// - fixed method name OnButtonCLick > OnButtonClick
/// </summary>

[RequireComponent ( typeof ( Button ) )]
public class ButtonClickHandler :MonoBehaviour
{
	public bool
		center_pivot = true;

	virtual protected void Awake ()
	{
		GetComponent<Button> ( ).onClick.AddListener ( OnButtonClick );
		Animator anim = GetComponent<Animator> ( );
		if (anim != null)
		{
			anim.updateMode = AnimatorUpdateMode.UnscaledTime;
		}
		if (center_pivot)
		{
			RectTransform rt = GetComponent<RectTransform> ( );
			rt.pivot = Vector2.one * 0.5f;
		}
	}

	virtual protected void OnButtonClick ()
	{

	}

}
