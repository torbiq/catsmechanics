﻿#define BRAND_NEW_PROTOTYPE

using UnityEngine;
using System.Collections;
#if BRAND_NEW_PROTOTYPE
using PSV_Prototype;
#endif
public class BannerPanelController :MonoBehaviour
{

	private float margin = 0.2f;  //  10px/50px

	public float Margin
	{
		set
		{
			margin = value;
			UpdateBannerPanel ( );
		}
		get
		{
			return margin;
		}
	}


	private RectTransform
		panel;

	private void Awake ()
	{
		panel = GetComponent<RectTransform> ( );
	}



#if BRAND_NEW_PROTOTYPE
	private void OnEnable ()
	{
		UpdateBannerPanel ( );
		ManagerGoogle.OnAdsEnabled += (bool param) => UpdateBannerPanel ( );
		AdsInterop.OnHideBannerAd += UpdateBannerPanel;
		AdsInterop.OnShowBannerAd += (bool param) => UpdateBannerPanel ( );
		AdsInterop.OnRefreshBannerAd += (AdsInterop.AdPosition pos, AdsInterop.AdSize size) => UpdateBannerPanel ( );
	}

	private void OnDisable ()
	{
		ManagerGoogle.OnAdsEnabled -= (bool param) => UpdateBannerPanel ( );
		AdsInterop.OnHideBannerAd -= UpdateBannerPanel;
		AdsInterop.OnShowBannerAd -= (bool param) => UpdateBannerPanel ( );
		AdsInterop.OnRefreshBannerAd -= (AdsInterop.AdPosition pos, AdsInterop.AdSize size) => UpdateBannerPanel ( );
	}



	void Subscribe (bool param)
	{
		ManagerGoogle.OnAdsEnabled += (p) => UpdateBannerPanel ( );
		AdsInterop.OnHideBannerAd += UpdateBannerPanel;
		AdsInterop.OnShowBannerAd += (p) => UpdateBannerPanel ( );
		AdsInterop.OnRefreshBannerAd += (AdsInterop.AdPosition pos, AdsInterop.AdSize size) => UpdateBannerPanel ( );
	}

	private Vector2 GetAnchor (AdsInterop.AdPosition ad_position)
	{
		Vector2 res = Vector2.zero;
		switch (ad_position)
		{
			case AdsInterop.AdPosition.Bottom_Centered:
				res.y = 0;
				res.x = 0.5f;
				break;
			case AdsInterop.AdPosition.Top_Centered:
				res.y = 1;
				res.x = 0.5f;
				break;
			case AdsInterop.AdPosition.Bottom_Left:
				res.y = 0;
				res.x = 0;
				break;
			case AdsInterop.AdPosition.Bottom_Right:
				res.y = 0;
				res.x = 1;
				break;
			case AdsInterop.AdPosition.Top_Left:
				res.y = 1;
				res.x = 0;
				break;
			case AdsInterop.AdPosition.Top_Right:
				res.y = 1;
				res.x = 1;
				break;
			case AdsInterop.AdPosition.Middle_Centered:
				res.y = 0.5f;
				res.x = 0.5f;
				break;
			case AdsInterop.AdPosition.Middle_Left:
				res.y = 0.5f;
				res.x = 0;
				break;
			case AdsInterop.AdPosition.Middle_Right:
				res.y = 0.5f;
				res.x = 1;
				break;
		}
		return res;
	}

#else
	private void OnEnable ()
	{
		ManagerGoogle.OnAdsDisabled += UpdateBannerPanel;
		ManagerGoogle.OnRefreshBannerAd += UpdateBannerPanel;
	}

	private void OnDisable ()
	{
		ManagerGoogle.OnAdsDisabled -= UpdateBannerPanel;
		ManagerGoogle.OnRefreshBannerAd -= UpdateBannerPanel;
	}

	private Vector2 GetAnchor (AdmobAd.AdLayout ad_position)
	{
		Vector2 res = Vector2.zero;
		switch (ad_position)
		{
			case AdmobAd.AdLayout.Bottom_Centered:
				res.y = 0;
				res.x = 0.5f;
				break;
			case AdmobAd.AdLayout.Top_Centered:
				res.y = 1;
				res.x = 0.5f;
				break;
			case AdmobAd.AdLayout.Bottom_Left:
				res.y = 0;
				res.x = 0;
				break;
			case AdmobAd.AdLayout.Bottom_Right:
				res.y = 0;
				res.x = 1;
				break;
			case AdmobAd.AdLayout.Top_Left:
				res.y = 1;
				res.x = 0;
				break;
			case AdmobAd.AdLayout.Top_Right:
				res.y = 1;
				res.x = 1;
				break;
			case AdmobAd.AdLayout.Middle_Centered:
				res.y = 0.5f;
				res.x = 0.5f;
				break;
			case AdmobAd.AdLayout.Middle_Left:
				res.y = 0.5f;
				res.x = 0;
				break;
			case AdmobAd.AdLayout.Middle_Right:
				res.y = 0.5f;
				res.x = 1;
				break;
		}
		return res;
	}

	//insert in ManagerGoogle
	//public static event Action OnAdsDisabled;
	//public static event Action OnRefreshBannerAd;

	//public bool GetBannerVisible ()
	//{
	//	return banner_visible;
	//}


	//public Vector2 GetBannerSizeInPX ()
	//{

	//	Vector2 b_size = banner_size != null ? new Vector2 ( banner_size.x, banner_size.y ) : Vector2.zero;
	//	float coef = (Screen.dpi / 160f);
	//	return b_size * coef * (GetBannerVisible ( ) && !IsAdmobDisabled ( ) ? 1 : 0);
	//}



	//public AdmobAd.AdLayout GetAdPos ()
	//{
	//	return AdmobManager.Instance.BannerAdPosition;
	//}

#endif


	void UpdateBannerPanel ()
	{
		if (ManagerGoogle.Instance != null && panel != null)
		{
			Vector2 sz = ManagerGoogle.Instance.GetBannerSizeInPX ( );
			float m = sz.y * margin;
			sz.x += m;
			sz.y += m;
			panel.sizeDelta = sz;

			Vector2 anchor = GetAnchor ( ManagerGoogle.Instance.GetAdPos ( ) );
			panel.pivot = anchor;
			panel.anchorMax = anchor;
			panel.anchorMin = anchor;
			panel.anchoredPosition = GetPos ( m, anchor ); //Vector2.zero;
		}
	}

	Vector2 GetPos (float delta_size, Vector2 pivot)
	{
		delta_size *= 0.5f;
		return new Vector2(
			GetAxisOffset(delta_size,pivot.x), 
			GetAxisOffset ( delta_size, pivot.y )
			);
	}

	float GetAxisOffset (float offset, float pivot)
	{
		float res = 0;
		if (pivot == 0)
		{
			res -= offset;
		}
		if (pivot == 1)
		{
			res += offset;
		}
		return res;

	}

}
