﻿
namespace PSV_Prototype
{

	public class PauseButton :ButtonClickHandler
	{

		public bool pause = false;

		protected override void OnButtonClick ()
		{
			PauseManager.Instance.SetPause ( pause );
		}

	}
}
