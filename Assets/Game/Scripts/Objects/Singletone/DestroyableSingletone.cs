﻿using UnityEngine;
using System.Collections;

public abstract class DestroyableSingletone<T> : MonoBehaviour where T : DestroyableSingletone<T> {
    private static T _instance;
    public static T Instance {
        get {
            return _instance;
        }
    }
    protected void Awake() {
        if (_instance != null) {
            if (_instance != this) {
                Log.Warning("DestroyableSingletone of type " + typeof(T).ToString() + " already exists. Destroying gameObject.");
                Destroy(gameObject);
                return;
            }
        }
        _instance = (T)this;
        Init();
    }
    protected virtual void Init() {

    }
}