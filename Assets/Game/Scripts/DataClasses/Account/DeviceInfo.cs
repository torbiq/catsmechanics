﻿using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Rendering;

public class DeviceInfo {
    public enum Platform {
        Android = 0,
        iOS = 1,
        Standalone = 2,
    }
    [JsonProperty(PropertyName = "platform")]
    public Platform? platform { get; private set; }
    [JsonProperty(PropertyName = "id")]
    public string ID { get; private set; }
    [JsonProperty(PropertyName = "name")]
    public string name { get; private set; }
    [JsonProperty(PropertyName = "model")]
    public string model { get; private set; }
    [JsonProperty(PropertyName = "type")]
    public DeviceType type { get; private set; }
    [JsonProperty(PropertyName = "gpu_id")]
    public int? gpuID { get; private set; }
    [JsonProperty(PropertyName = "gpu_name")]
    public string gpuName { get; private set; }
    [JsonProperty(PropertyName = "gpu_type")]
    public GraphicsDeviceType gpuType { get; private set; }
    [JsonProperty(PropertyName = "os")]
    public string os { get; private set; }
    [JsonProperty(PropertyName = "cpu")]
    public string cpu { get; private set; }
    
    public DeviceInfo() {
        this.platform = platform;
        this.ID = SystemInfo.deviceUniqueIdentifier;
        this.name = SystemInfo.deviceName;
        this.model = SystemInfo.deviceModel;
        this.type = SystemInfo.deviceType;
        this.gpuID = SystemInfo.graphicsDeviceID;
        this.gpuName = SystemInfo.graphicsDeviceName;
        this.gpuType = SystemInfo.graphicsDeviceType;
        this.os = SystemInfo.operatingSystem;
        this.cpu = SystemInfo.processorType;
    }
}