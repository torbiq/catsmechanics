﻿using Newtonsoft.Json;
using System;

public class PlayerInfo {
    [JsonProperty(PropertyName = "player_id")]
    public Int64? playerID { get; private set; }
    [JsonProperty(PropertyName = "nick")]
    public string nick { get; private set; }
    [JsonProperty(PropertyName = "device_info")]
    public DeviceInfo deviceInfo { get; private set; }
    [JsonProperty(PropertyName = "league_info")]
    public LeagueInfo leagueInfo { get; private set; }

    public PlayerInfo(
        Int64? playerID = null,
        string nick = null,
        DeviceInfo deviceInfo = null,
        LeagueInfo leagueInfo = null
        ) {
        this.playerID = playerID;
        this.nick = nick;
        this.deviceInfo = deviceInfo;
        this.leagueInfo = leagueInfo;
    }
}