﻿using Newtonsoft.Json;

public class GameRequest {
    [JsonProperty(PropertyName = "is_random")]
    public bool? isRandom { get; private set; }
    [JsonProperty(PropertyName = "opponent")]
    public string opponent { get; private set; }
    [JsonProperty(PropertyName = "league_info")]
    public LeagueInfo leagueInfo { get; private set; }

    public GameRequest(bool? isRandom = null, string opponent = null, LeagueInfo leagueInfo = null) {
        this.isRandom = isRandom;
        this.opponent = opponent;
        this.leagueInfo = leagueInfo;
    }
}