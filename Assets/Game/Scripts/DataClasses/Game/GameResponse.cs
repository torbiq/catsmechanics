﻿using Newtonsoft.Json;

public class GameResponse {
    [JsonProperty(PropertyName = "car")]
    public SCarConstructed car { get; private set; }
    [JsonProperty(PropertyName = "nick")]
    public string nick { get; private set; }
    [JsonProperty(PropertyName = "league_info")]
    public LeagueInfo leagueInfo { get; private set; }

    public GameResponse(SCarConstructed car = null, string nick = null, LeagueInfo leagueInfo = null) {
        this.car = car;
        this.nick = nick;
        this.leagueInfo = leagueInfo;
    }
}