﻿using Newtonsoft.Json;

public class LeagueInfo {
    [JsonProperty("league")]
    public League? league { get; private set; }
    [JsonProperty("subleague")]
    public Subleague? subleague { get; private set; }

    public LeagueInfo(League? league = null, Subleague? subleague = null) {
        this.league = league;
        this.subleague = subleague;
    }
}