﻿using Newtonsoft.Json;
using System;

public class MatchResult {
    [JsonProperty(PropertyName = "winner_left")]
    public bool? winnerLeft { get; private set; }
    [JsonProperty(PropertyName = "league_info")]
    public LeagueInfo leagueInfo { get; private set; }
    [JsonProperty(PropertyName = "p1_id")]
    public Int64? p1ID { get; private set; }
    [JsonProperty(PropertyName = "p2_id")]
    public Int64? p2ID { get; private set; }

    public MatchResult(
        bool? winnerLeft = null,
        LeagueInfo leagueInfo = null,
        Int64? p1ID = null,
        Int64? p2ID = null
        ) {
        this.winnerLeft = winnerLeft;
        this.leagueInfo = leagueInfo;
        this.p1ID = p1ID;
        this.p2ID = p2ID;
    }
}