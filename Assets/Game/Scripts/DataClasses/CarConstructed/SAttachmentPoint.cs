﻿using Newtonsoft.Json;
using UnityEngine;

public abstract class SAttachmentPoint {
    [JsonProperty(PropertyName = "local_position", ReferenceLoopHandling = ReferenceLoopHandling.Ignore)]
    [JsonConverter(typeof(Vector3Serializer))]
    public Vector3 localPosition { get; set; }

    protected SAttachmentPoint(Vector3 localPosition) {
        this.localPosition = localPosition;
    }
}