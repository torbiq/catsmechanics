﻿using Newtonsoft.Json;

public class SCarConstructed {
    [JsonProperty(PropertyName = "left")]
    public bool isLeft { get; set; }
    [JsonProperty(PropertyName = "body")]
    public SBody body { get; set; }

    public SCarConstructed() {
        this.isLeft = false;
        this.body = null;
    }
    public SCarConstructed(bool isLeft, SBody body) {
        this.isLeft = isLeft;
        this.body = body;
    }
    public SCarConstructed(bool isLeft, ABody body) {
        this.isLeft = isLeft;
        if (body != null) {
            this.body = new SBody(body);
        }
        else {
            this.body = null;
        }
    }
    public SCarConstructed(CarConstructed carConstructed) {
        isLeft = carConstructed.isLeft;
        body = new SBody(carConstructed.body);
    }
}