﻿using Newtonsoft.Json;
using UnityEngine;

public class SWheelPoint : STypedAttachmentPoint<AWheel, WheelType, SWheel> {
    public SWheelPoint() : base(Vector3.zero, null) { }
    public SWheelPoint(Vector3 localPosition, AWheel part = null) : base(localPosition) {
        if (part != null) {
            this.part = new SWheel(part);
        }
        else {
            this.part = null;
        }
    }
    public SWheelPoint(Vector3 localPosition, SWheel part = null) : base(localPosition, part) {
    }
    public SWheelPoint(WheelPoint point) : base(point) {
        if (point.part != null) {
            part = new SWheel(point.part);
        }
        else {
            part = null;
        }
    }
    public SWheelPoint(SWheelPoint point) : base(point) { }
}