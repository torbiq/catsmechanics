﻿using Newtonsoft.Json;
using UnityEngine;

public class SItemPoint : STypedAttachmentPoint<AItem, ItemType, SItem> {
    public SItemPoint() : base(Vector3.zero, null) { }
    public SItemPoint(Vector3 localPosition, AItem part = null) : base(localPosition) {
        if (part != null) {
            this.part = new SItem(part);
        }
        else {
            this.part = null;
        }
    }
    public SItemPoint(Vector3 localPosition, SItem part = null) : base(localPosition, part) {
    }
    public SItemPoint(ItemPoint point) : base(point) {
        if (point.part != null) {
            part = new SItem(point.part);
        }
        else {
            part = null;
        }
    }
    public SItemPoint(SItemPoint point) : base(point) { }
}