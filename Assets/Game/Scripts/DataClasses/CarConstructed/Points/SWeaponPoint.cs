﻿using Newtonsoft.Json;
using UnityEngine;

public class SWeaponPoint : STypedAttachmentPoint<AWeapon, WeaponType, SWeapon> {
    public SWeaponPoint() : base(Vector3.zero, null) { }
    public SWeaponPoint(Vector3 localPosition, AWeapon part = null) : base(localPosition) {
        if (part != null) {
            this.part = new SWeapon(part);
        }
        else {
            this.part = null;
        }
    }
    public SWeaponPoint(Vector3 localPosition, SWeapon part = null) : base(localPosition, part) {
    }
    public SWeaponPoint(WeaponPoint point) : base(point) {
        if (point.part != null) {
            part = new SWeapon(point.part);
        }
        else {
            part = null;
        }
    }
    public SWeaponPoint(SWeaponPoint point) : base(point) { }
}