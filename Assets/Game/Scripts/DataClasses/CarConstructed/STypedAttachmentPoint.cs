﻿using System;
using Newtonsoft.Json;
using UnityEngine;

public abstract class STypedAttachmentPoint<TPartClass, TPartType, TPartSerialized> : SAttachmentPoint
        where TPartClass : TypedPart<TPartClass, TPartType, TPartSerialized>
        where TPartType : struct, IConvertible
        where TPartSerialized : STypedPart<TPartClass, TPartType, TPartSerialized> {
    [JsonProperty(PropertyName = "part")]
    public TPartSerialized part { get; set; }

    protected STypedAttachmentPoint(Vector3 localPosition) : base(localPosition) {
        // Part must be assigned in inherited class.
    }
    protected STypedAttachmentPoint(Vector3 localPosition, TPartSerialized part) : base(localPosition) {
        if (part != null) {
            this.part = part;
        }
        else {
            this.part = null;
        }
    }
    protected STypedAttachmentPoint(STypedAttachmentPoint<TPartClass, TPartType, TPartSerialized> point) : base(point.localPosition) {
        this.part = point.part;
    }
    protected STypedAttachmentPoint(TypedAttachmentPoint<TPartClass, TPartType, TPartSerialized> point) : base(point.localPosition) {
        // Part must be assigned in inherited class.
    }
}