﻿using System;
using Newtonsoft.Json;

public abstract class STypedPart<TPartClass, TPartType, TPartSerialized> : SPart
        where TPartClass : TypedPart<TPartClass, TPartType, TPartSerialized>
        where TPartType : struct, IConvertible
        where TPartSerialized : STypedPart<TPartClass, TPartType, TPartSerialized> {
    [JsonProperty(PropertyName = "subtype")]
    public TPartType subtype { get; set; }

    protected STypedPart(TPartType subtype = default(TPartType)) : base() {
    }
    protected STypedPart(Strength strength, Rarity rarity, Level level, Chartics chartics, int stars, TPartType subType = default(TPartType)) : base(strength, rarity, level, chartics, stars) {
    }
    protected STypedPart(TPartClass part) : base(part) {
        subtype = part.subtype;
    }
    protected STypedPart(TPartSerialized part) : base(part) {
        subtype = part.subtype;
    }
}