﻿using Newtonsoft.Json;

public abstract class SPart {
    [JsonProperty(PropertyName = "strength")]
    public Strength strength { get; set; }
    [JsonProperty(PropertyName = "rarity")]
    public Rarity rarity { get; set; }
    [JsonProperty(PropertyName = "level")]
    public Level level { get; set; }
    [JsonProperty(PropertyName = "chartics")]
    public Chartics chartics { get; set; }
    [JsonProperty(PropertyName = "stars")]
    public int stars { get; set; }
    
    protected SPart() {
        this.strength = Strength.Carbon;
        this.rarity = Rarity.Common;
        this.level = new Level();
        this.chartics = new Chartics();
        this.stars = 1;
    }
    protected SPart(Part part) {
        this.strength = part.strength;
        this.rarity = part.rarity;
        this.level = new Level(part.level);
        this.chartics = new Chartics(part.chartics);
        this.stars = part.stars;
    }
    protected SPart(SPart sPart) {
        this.chartics = sPart.chartics;
        this.rarity = sPart.rarity;
        this.level = sPart.level;
        this.chartics = sPart.chartics;
        this.stars = sPart.stars;
    }
    protected SPart(Strength strength, Rarity rarity, Level level, Chartics chartics, int stars) {
        this.strength = strength;
        this.rarity = rarity;
        this.level = level;
        this.chartics = chartics;
        this.stars = stars;
    }
}