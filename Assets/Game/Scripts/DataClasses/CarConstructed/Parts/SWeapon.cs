﻿using Newtonsoft.Json;

public class SWeapon : STypedPart<AWeapon, WeaponType, SWeapon> {
    public SWeapon() : base() {
    }
    public SWeapon(AWeapon weapon) : base(weapon) {
    }
    public SWeapon(SWeapon weapon) : base(weapon) {
    }
    public SWeapon(Strength strength, Rarity rarity, Level level, Chartics chartics, int stars, WeaponType weaponType) : base(strength, rarity, level, chartics, stars, weaponType) {
    }
}