﻿using Newtonsoft.Json;

public class SWheel : STypedPart<AWheel, WheelType, SWheel> {
    public SWheel() : base() {
    }
    public SWheel(AWheel wheel) : base(wheel) {
    }
    public SWheel(SWheel wheel) : base(wheel) {
    }
    public SWheel(Strength strength, Rarity rarity, Level level, Chartics chartics, int stars, WheelType wheelType) : base(strength, rarity, level, chartics, stars, wheelType) {
    }
}