﻿using Newtonsoft.Json;

public class SItem : STypedPart<AItem, ItemType, SItem> {
    public SItem() : base() {
    }
    public SItem(AItem item) : base(item) {
    }
    public SItem(SItem item) : base(item) {
    }
    public SItem(Strength strength, Rarity rarity, Level level, Chartics chartics, int stars, ItemType itemType) : base(strength, rarity, level, chartics, stars, itemType) {
    }
}