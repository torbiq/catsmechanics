﻿using Newtonsoft.Json;
using System.Collections.Generic;

public class SBody : STypedPart<ABody, BodyType, SBody> {
    [JsonProperty(PropertyName = "wheel_points")]
    public List<SWheelPoint> wheelPoints { get; set; }
    [JsonProperty(PropertyName = "weapon_points")]
    public List<SWeaponPoint> weaponPoints { get; set; }
    [JsonProperty(PropertyName = "item_points")]
    public List<SItemPoint> itemPoints { get; set; }
    
    public SBody() : base() {
        wheelPoints = new List<SWheelPoint>();
        weaponPoints = new List<SWeaponPoint>();
        itemPoints = new List<SItemPoint>();
    }
    public SBody(ABody body) : base(body) {
        wheelPoints = new List<SWheelPoint>();
        foreach (var wheelP in body.wheelPoints) {
            wheelPoints.Add(new SWheelPoint(wheelP));
        }
        weaponPoints = new List<SWeaponPoint>();
        foreach (var weaponP in body.weaponPoints) {
            weaponPoints.Add(new SWeaponPoint(weaponP));
        }
        itemPoints = new List<SItemPoint>();
        foreach (var itemP in body.itemPoints) {
            itemPoints.Add(new SItemPoint(itemP));
        }
    }
    public SBody(SBody body) : base(body) {
        wheelPoints = body.wheelPoints;
        weaponPoints = body.weaponPoints;
        itemPoints = body.itemPoints;
    }
    public SBody(Strength strength, Rarity rarity, Level level, Chartics chartics, int stars, BodyType bodyType, List<SWheelPoint> wheelPoints = null, List<SWeaponPoint> weaponPoints = null, List<SItemPoint> itemPoints = null) : base(strength, rarity, level, chartics, stars, bodyType) {
        this.wheelPoints = wheelPoints ?? new List<SWheelPoint>();
        this.weaponPoints = weaponPoints ?? new List<SWeaponPoint>();
        this.itemPoints = itemPoints ?? new List<SItemPoint>();
    }
}