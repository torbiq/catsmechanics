﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class BlankDisplayController : MonoBehaviour {
    #region Serialized
    public RectTransform showRectTransform;
    public RectTransform hideRectTransform;

    public Text levelText;
    public Text levelCountText;
    public Text healthText;
    public Text damageText;
    public Text energyText;
    public Text itemTypeText;
    public Image progressBarImage;
    public Transform itemContainer;
    public RectTransform starsParent;
    public RectTransform groupHealth;
    public RectTransform groupDamage;
    public RectTransform groupEnergy;
    #endregion

    public event Action<Part> OnPartSelected;
    public event Action OnShown;
    public event Action OnHidden;

    public bool isActive { get; private set; }

    private GameObject _selectedPartInstance;
    private Part _selectedPart;

    private void Awake() {
        Hide(0f);
    }
    public void SelectPart(Part part) {
        if (!isActive) {
            Show();
        }
        if (_selectedPartInstance) {
            DestroyPart();
        }
        levelCountText.text = part.level.level.ToString();
        progressBarImage.fillAmount = part.level.ProgressPercentage;
        UpdateTextValueGroup(groupHealth, healthText, part.chartics.hp);
        UpdateTextValueGroup(groupDamage, damageText, part.chartics.damage);
        UpdateTextValueGroup(groupEnergy, energyText, part.chartics.energy);
        UpdateStars(part.stars);
        itemTypeText.text = part.GetSubTypeName();
        InstantiatePart(part);
        if (OnPartSelected != null) {
            OnPartSelected(part);
        }
    }
    public Tween Show(float delay = 0.5f) {
        isActive = true;
        GetComponent<RectTransform>().DOKill();
        if (OnShown != null) {
            OnShown();
        }
        return GetComponent<RectTransform>().DOAnchorPos(showRectTransform.anchoredPosition, delay).SetEase(Ease.OutElastic);
    }
    public Tween Hide(float delay = 0.5f) {
        isActive = false;
        GetComponent<RectTransform>().DOKill();
        if (OnHidden != null) {
            OnHidden();
        }
        return GetComponent<RectTransform>().DOAnchorPos(hideRectTransform.anchoredPosition, delay).SetEase(Ease.InCubic);
    }

    private void InstantiatePart(Part part) {
        _selectedPartInstance = (GameObject)GameObject.Instantiate(part.gameObject, itemContainer);
        _selectedPart = _selectedPartInstance.GetComponent<Part>();
        var asBody = _selectedPart as Body;
        if (asBody) {
            foreach (var wheel in asBody.wheelPoints) {
                if (wheel.part) {
                    wheel.part.gameObject.SetActive(false);
                }
            }
            foreach (var item in asBody.itemPoints) {
                if (item.part) {
                    item.part.gameObject.SetActive(false);
                }
            }
            foreach (var weapon in asBody.weaponPoints) {
                if (weapon.part) {
                    weapon.part.gameObject.SetActive(false);
                }
            }
        }
        _selectedPartInstance.transform.localPosition = Vector3.zero;
        _selectedPartInstance.transform.localScale = Vector3.one;
    }
    private void DestroyPart() {
        DestroyImmediate(_selectedPartInstance);
        _selectedPartInstance = null;
        _selectedPart = null;
    }
    private void UpdateTextValueGroup(RectTransform group, Text text, int value) {
        bool isEnabled = value != 0;
        if (isEnabled) {
            text.text = value.ToString();
        }
        group.gameObject.SetActive(isEnabled);
    }
    private void UpdateStars(int starsCount) {
        int i = 0;
        for (; i < starsCount; i++) {
            starsParent.GetChild(i).gameObject.SetActive(true);
        }
        for (; i < starsParent.childCount; i++) {
            starsParent.GetChild(i).gameObject.SetActive(false);
        }
    }
}
