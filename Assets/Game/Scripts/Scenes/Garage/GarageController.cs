﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;
using Extensions;
using SocketIO;
using Newtonsoft.Json;
//Н

public class GarageController : BaseSceneController<GarageController> {
    [Serializable]
    public class ButonCarPaired {
        public Button button;
        public CarConstructed carConstructed;
        public Color selectedColor = EColor.ToColor("FF878760");
        public Color deselectedColor = EColor.ToColor("85858560");
        public event Action OnSelected;
        public event Action OnDeselected;
        public void Select() {
            button.targetGraphic.color = selectedColor;
            carConstructed.gameObject.SetActive(true);
            if (OnSelected != null) {
                OnSelected();
            }
        }
        public void Deselect() {
            button.targetGraphic.color = deselectedColor;
            carConstructed.gameObject.SetActive(false);
            if (OnDeselected != null) {
                OnDeselected();
            }
        }
    }

    public static string carChosenIndexKey = "CAR_CHOSEN_INDEX";
    public UIItem itemPrefab;
    public List<Part> partsToInstantiate;
    public BlankDisplayController blankDisplayController;
    public ScrollRect scrollRect;
    public List<ButonCarPaired> carPointsControls;


    public float minSqrDistanceToAttachAttachments { get; private set; }
    public float minSqrDistanceToAttachBody { get; private set; }

    public Part partDragged { get; private set; }
    public CarConstructed currentCarConstructed { get; private set; }
    
    public static SCarConstructed GetCarChosen() {
        var carLoaded = DeserializeCar(PlayerPrefs.GetString("CAR_SAVED_" + PlayerPrefs.GetInt(carChosenIndexKey, 0)));
        return carLoaded;
    }
    public static SCarConstructed DeserializeCar(string json) {
        var settings = new JsonSerializerSettings {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };
        return JsonConvert.DeserializeObject<SCarConstructed>(json, settings);
    }
    public static string SerializeCar(CarConstructed car) {
        var wrapper = new SCarConstructed() {
            body = car.body == null ? null : new SBody(car.body),
            isLeft = car.isLeft
        };
        return JsonConvert.SerializeObject(wrapper, Formatting.Indented, new JsonSerializerSettings() {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
    }
    public static string SerializeCar(SCarConstructed car) {
        return JsonConvert.SerializeObject(car, Formatting.Indented, new JsonSerializerSettings() {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
    }

    protected override void SAwake() {
        minSqrDistanceToAttachAttachments = 1f;
        minSqrDistanceToAttachBody = 5f;
        Log.Msg("Index chosen: " + PlayerPrefs.GetInt(carChosenIndexKey, 0));
        for (int i = 0; i < carPointsControls.Count; i++) {
            int selectionIndexCopied = i;
            carPointsControls[i].button.onClick.AddListener(()=> {
                SelectCarPoint(selectionIndexCopied);
                PlayerPrefs.SetInt(carChosenIndexKey, selectionIndexCopied);
            });
            carPointsControls[i].Deselect();
            carPointsControls[i].carConstructed.LoadAttachingCollider();
            string carSerialized = PlayerPrefs.GetString("CAR_SAVED_" + i, "");
            var carLoaded = DeserializeCar(carSerialized);
            Log.Msg("Car serialized " + i + ": " + carSerialized);
            carPointsControls[i].carConstructed.Load(carLoaded);
        }
        currentCarConstructed = carPointsControls[0].carConstructed;

        SelectCarPoint(PlayerPrefs.GetInt(carChosenIndexKey, 0));
        itemPrefab.gameObject.SetActive(false);
        var partsInstantiated = new List<Part>();
        for(int i = 0; i < 10; i++) {
            var body = new SBody();
            body.subtype = BodyType.Classic;
            body.level = new Level(1, 100, 0, 4, 0, 0, 0);
            body.strength = Strength.Wooden;
            body.rarity = Rarity.Common;
            body.chartics = new Chartics(UnityEngine.Random.Range(100, 301), 0, UnityEngine.Random.Range(10, 16), 0, 0, 0);
            body.stars = UnityEngine.Random.Range(1, 6);

            partsToInstantiate.Add(PartLoader.GenerateRandomWheelPoints(PartLoader.GenerateRandomItemPoints(PartLoader.LoadBody(body), UnityEngine.Random.Range(1, 4), UnityEngine.Random.Range(1, 3)), UnityEngine.Random.Range(2, 4)));
        }
        for (int i = 0; i < 10; i++) {
            var weapon = new SWeapon();
            weapon.level = new Level(1, 100, 0, 0, 0, 4, 0);
            weapon.subtype = WeaponType.Rocket;
            weapon.strength = Strength.Wooden;
            weapon.rarity = Rarity.Common;
            weapon.chartics = new Chartics(0, UnityEngine.Random.Range(10, 51), UnityEngine.Random.Range(1, 6), 0, 0, 0);
            weapon.stars = UnityEngine.Random.Range(1, 6);

            partsToInstantiate.Add(PartLoader.LoadWeapon(weapon));
        }
        for (int i = 0; i < 10; i++) {
            var weapon = new SWeapon();
            weapon.level = new Level(1, 100, 0, 0, 0, 4, 0);
            weapon.subtype = WeaponType.Blade;
            weapon.strength = Strength.Wooden;
            weapon.rarity = Rarity.Common;
            weapon.chartics = new Chartics(0, UnityEngine.Random.Range(10, 51), UnityEngine.Random.Range(1, 6), 0, 0, 0);
            weapon.stars = UnityEngine.Random.Range(1, 6);

            partsToInstantiate.Add(PartLoader.LoadWeapon(weapon));
        }
        for (int i = 0; i < 10; i++) {
            var wheel = new SWheel();
            wheel.level = new Level(1, 100, 0, 0, 0, 4, 0);
            wheel.subtype = WheelType.Bigfoot;
            wheel.strength = Strength.Wooden;
            wheel.rarity = Rarity.Common;
            wheel.chartics = new Chartics(0, UnityEngine.Random.Range(10, 51), UnityEngine.Random.Range(1, 6), 0, 0, 0);
            wheel.stars = UnityEngine.Random.Range(1, 6);

            partsToInstantiate.Add(PartLoader.LoadWheel(wheel));
        }

        ////foreach (var part in partsToInstantiate) {
        ////    partsInstantiated.Add(GameObject.Instantiate(part).GetComponent<APart>());
        ////}
        partsToInstantiate.ForEach(x => AddUIItemPart(x));
    }
    public void AddUIItemPart(Part part) {
        GameObject instanceItem = (GameObject)GameObject.Instantiate(itemPrefab.gameObject, itemPrefab.transform.parent);
        var UIItemComponent = instanceItem.GetComponent<UIItem>();
        UIItemComponent.LoadPart(part);
    }
    public void SelectCarPoint(int index) {
        if (index < 0 || index >= carPointsControls.Count) {
            throw new System.Exception("Car Point out of range");
        }
        carPointsControls.Find(x => x.carConstructed == currentCarConstructed).Deselect();
        var toSelect = carPointsControls[index];
        toSelect.Select();
        currentCarConstructed = toSelect.carConstructed;
    }
    protected override void SUpdate() {
        if (Input.GetKeyDown(KeyCode.C)) {
            Log.Msg("car_serialized" + SerializeCar(currentCarConstructed));
            SocketIOComponent.Instance.Emit("car_serialized", new JSONObject(SerializeCar(currentCarConstructed)));
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            OnExit();
        }
        if (mouseUp) {
            MouseUp();
        }
        if (mouseDown) {
            MouseDown();
        }
        if (mouseDrag) {
            MouseDrag();
        }
        if (mouseUp) {
            MouseUp();
        }
    }
    private void OnApplicationPause(bool pause) {
        if (pause) {
            MouseUp();
            SaveCars();
        }
    }
    private void OnApplicationQuit() {
        OnExit();
    }
    private void OnExit() {
        SaveCars();
    }
    private void SaveCars() {
        Log.Msg("Cars saved.");
        for (int i = 0; i < carPointsControls.Count; i++) {
            PlayerPrefs.SetString("CAR_SAVED_" + i, SerializeCar(carPointsControls[i].carConstructed));
        }
        PlayerPrefs.SetInt(carChosenIndexKey, carPointsControls.FindIndex(x => x.carConstructed == currentCarConstructed));
        PlayerPrefs.Save();
        SocketIOComponent.Instance.Emit(
            ClientAPIEvents.car_serialized.ToString(),
            new JSONObject(SerializeCar(GetCarChosen()))
            );
    }
    private void MouseDown() {
        Collider2D[] touchedColliders = TouchedCollidersWorld();
        var colliderContainsPartComponent = touchedColliders.FirstOrDefault(x => x.GetComponent<Wheel>());
        if (!colliderContainsPartComponent) {
            colliderContainsPartComponent = touchedColliders.FirstOrDefault(x => x.GetComponent<Weapon>());
        }
        if (!colliderContainsPartComponent) {
            colliderContainsPartComponent = touchedColliders.FirstOrDefault(x => x.GetComponent<Item>());
        }
        if (!colliderContainsPartComponent) {
            colliderContainsPartComponent = touchedColliders.FirstOrDefault(x => x.GetComponent<Body>());
        }
        partDragged = colliderContainsPartComponent ? colliderContainsPartComponent.GetComponent<Part>() : null;
        if (partDragged) {
            AudioPlayer.Instance.Play("part_pick");
            blankDisplayController.SelectPart(partDragged);
            if (partDragged is Body) {
                currentCarConstructed.SetActiveAttachmentsOnCarConstructed(false);
            }
            if (partDragged is Wheel) {

            }
            if (partDragged is Item) {

            }
            if (partDragged is Weapon) {

            }
        }
    }
    private void MouseDrag() {
        if (partDragged) {
            partDragged.transform.position += (Vector3)GetMouseDeltaWorld();
        }
    }
    private void MouseUp() {
        var asBody = partDragged as Body;
        var asWheel = partDragged as Wheel;
        var asWeapon = partDragged as Weapon;
        var asItem = partDragged as Item;

        if (partDragged) {
            Collider2D[] touchedColliders = TouchedCollidersWorld();
            var scrollRectCollider = scrollRect.GetComponent<Collider2D>();
            bool scrollTouched = touchedColliders.FirstOrDefault(x => x.GetComponent<Collider2D>() == scrollRectCollider);

            // If we decide to remove an item at all by dragging it to scroll
            if (scrollTouched) {
                if (asBody) {
                    currentCarConstructed.Remove();
                }
                if (asWheel) {
                    partDragged.transform.parent.GetComponent<WheelPoint>().Remove();
                }
                if (asWeapon) {
                    partDragged.transform.parent.GetComponent<WeaponPoint>().Remove();
                }
                if (asItem) {
                    partDragged.transform.parent.GetComponent<ItemPoint>().Remove();
                }
                AudioPlayer.Instance.Play("part_drop");
                partDragged = null;
                return;
            }
            // If we decide to reposition or attach an item
            else {
                bool wasAttached = false;
                if (asBody) {
                    currentCarConstructed.SetActiveAttachmentsOnCarConstructed(true);
                }
                else if (currentCarConstructed.body) {
                    if (asWheel) {
                        WheelPoint closestPoint = currentCarConstructed.body.wheelPoints.FirstOrDefault();
                        float prevMagnitude = (closestPoint.transform.position - partDragged.attachmentPosition.position).sqrMagnitude;
                        currentCarConstructed.body.wheelPoints.ForEach(x => {
                            float newMagnitude = (x.transform.position - partDragged.attachmentPosition.position).sqrMagnitude;
                            if (newMagnitude < prevMagnitude) {
                                closestPoint = x;
                                prevMagnitude = newMagnitude;
                            }
                        });
                        if ((closestPoint.transform.position - partDragged.attachmentPosition.position).sqrMagnitude > minSqrDistanceToAttachAttachments) {
                            closestPoint = null;
                        }
                        wasAttached = closestPoint;
                        if (closestPoint) {
                            closestPoint.Replace(partDragged.transform.parent.GetComponent<WheelPoint>().Detach());
                        }
                    }
                    else if (asWeapon) {
                        WeaponPoint closestPoint = currentCarConstructed.body.weaponPoints.FirstOrDefault();
                        float prevMagnitude = (closestPoint.transform.position - partDragged.attachmentPosition.position).sqrMagnitude;
                        currentCarConstructed.body.weaponPoints.ForEach(x => {
                            float newMagnitude = (x.transform.position - partDragged.attachmentPosition.position).sqrMagnitude;
                            if (newMagnitude < prevMagnitude) {
                                closestPoint = x;
                                prevMagnitude = newMagnitude;
                            }
                        });
                        if ((closestPoint.transform.position - partDragged.attachmentPosition.position).sqrMagnitude > minSqrDistanceToAttachAttachments) {
                            closestPoint = null;
                        }
                        wasAttached = closestPoint;
                        if (closestPoint) {
                            closestPoint.Replace(partDragged.transform.parent.GetComponent<WeaponPoint>().Detach());
                        }
                    }
                    else if (asItem) {
                        ItemPoint closestPoint = currentCarConstructed.body.itemPoints.FirstOrDefault();
                        float prevMagnitude = (closestPoint.transform.position - partDragged.attachmentPosition.position).sqrMagnitude;
                        currentCarConstructed.body.itemPoints.ForEach(x => {
                            float newMagnitude = (x.transform.position - partDragged.attachmentPosition.position).sqrMagnitude;
                            if (newMagnitude < prevMagnitude) {
                                closestPoint = x;
                                prevMagnitude = newMagnitude;
                            }
                        });
                        if ((closestPoint.transform.position - partDragged.attachmentPosition.position).sqrMagnitude > minSqrDistanceToAttachAttachments) {
                            closestPoint = null;
                        }
                        wasAttached = closestPoint;
                        if (closestPoint) {
                            closestPoint.Replace(partDragged.transform.parent.GetComponent<ItemPoint>().Detach());
                        }
                    }
                    if (wasAttached) {
                        AudioPlayer.Instance.Play("part_attach");
                        partDragged = null;
                        return;
                    }
                }
            }
            // If we missed any position and didn't drag it to scroll
            AudioPlayer.Instance.Play("part_drop");
            partDragged.transform.localPosition = -partDragged.attachmentPosition.localPosition;
            partDragged = null;
        }
    }
}
