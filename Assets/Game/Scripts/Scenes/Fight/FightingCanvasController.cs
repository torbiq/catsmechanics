﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class FightingCanvasController : MonoBehaviour {
    public PlayerStats left;
    public PlayerStats right;

    private void Awake() {
        //left.Init();
        //right.Init();
    }

    [ContextMenu("Test left damage")]
    private void TestLeft() {
        float fillAmount = UnityEngine.Random.Range(0f, 1f);
        left.UpdateFill(fillAmount);
    }
    [ContextMenu("Test right damage")]
    private void TestRight() {
        float fillAmount = UnityEngine.Random.Range(0f, 1f);
        right.UpdateFill(fillAmount);
    }
}

[Serializable]
public class PlayerStats {
    public Image healthBarBgImage;
    public Image healthBarImage;
    public Text damageText;
    public Text healthText;
    public Text nicknameText;
    public float fillAmount = 1f;
    public float takingDamageDelay;
    public float takingDamageDuration;
    public Tween hbBackFillTween;
    public Tween hbDelayTween;

    public void Init(CarConstructed car) {
        damageText.text = car.damage.ToString();
        healthText.text = car.hp.ToString();

        healthBarBgImage.fillAmount = fillAmount;
        healthBarImage.fillAmount = fillAmount;
    }

    public void UpdateFill(float fillAmount) {
        this.fillAmount = fillAmount;
        healthBarImage.fillAmount = fillAmount;

        if (hbBackFillTween != null) {
            hbBackFillTween.Kill(false);
        }
        if (hbDelayTween != null) {
            hbDelayTween.Kill(false);
        }

        hbDelayTween = DOVirtual.DelayedCall(takingDamageDelay, () => {
            hbBackFillTween = healthBarBgImage.DOFillAmount(fillAmount, takingDamageDuration);
        }, false);
    }
}