﻿using UnityEngine;
using System.Collections.Generic;
//using SocketIO;
using System.Linq;
using DG.Tweening;

public class FightSceneController : MonoBehaviour {
    public float killersMoveDistance = 9f;
    public float killersDelay = 2f;
    public CollisionListener2D killerTop;
    public CollisionListener2D killerLeft;
    public CollisionListener2D killerRight;

    public Body testBody;

    public Transform leftCarPoint;
    public Transform rightCarPoint;

    public FightingCanvasController fightingCanvasController;

    private void SetupCameraSize() {
        float aspectRatio = Screen.width / (float)Screen.height; // 1.78 = 16:9 // 1.33 = 4:3
        Camera.main.orthographicSize /= (aspectRatio / 1.78f);   // Scaling camera with 
    }

    private void ActivateKillers() {
        killerTop.OnCollisionEnter += OnKillerCollisionHandler;
        killerLeft.OnCollisionEnter += OnKillerCollisionHandler;
        killerRight.OnCollisionEnter += OnKillerCollisionHandler;
    }
    private void DeactivateKillers() {
        killerTop.OnCollisionEnter -= OnKillerCollisionHandler;
        killerLeft.OnCollisionEnter -= OnKillerCollisionHandler;
        killerRight.OnCollisionEnter -= OnKillerCollisionHandler;
    }
    private void OnKillerCollisionHandler(Collision2D col) {
        var asPart = col.gameObject.GetComponent<Part>();
        if (asPart) {
            //if (asPart.hitBoxCollider2D == col.collider) {
            //    asPart.car.Kill();
            //    StopKillers();
            //    DeactivateKillers();
            //}
        }
    }

    private void StartKillers() {
        killerLeft.transform.DOMoveX(killerLeft.transform.position.x + killersMoveDistance, 15f);
        killerRight.transform.DOMoveX(killerRight.transform.position.x - killersMoveDistance, 15f);
    }
    private void StopKillers() {
        killerLeft.transform.DOKill(false);
        killerRight.transform.DOKill(false);
    }

    private void SetupKillers() {
        ActivateKillers();
        DOVirtual.DelayedCall(killersDelay, () => {
            StartKillers();
        }, false);
    }

    private void Awake() {
        SetupCameraSize();
        //SocketIOHelper.Instance.OnGameResponceReceived += LoadFight;
        //SocketIOHelper.Instance.SendGameRequest(isRandom: true);

        LoadFight(new GameResponse(GarageController.GetCarChosen(), "Player 2", new LeagueInfo(League.Carbon, Subleague.SUBLEAGUE_1)));
    }

    private void OnDestroy() {
        SocketIOHelper.Instance.OnGameResponceReceived -= LoadFight;
    }

    private void LoadFight(GameResponse gameResponse) {
        //SetupKillers();
        var left = LoadCar(leftCarPoint, GarageController.GetCarChosen());
        var right = LoadCar(rightCarPoint, gameResponse.car);
        fightingCanvasController.left.nicknameText.text = "YOU";
        fightingCanvasController.right.nicknameText.text = gameResponse.nick;
    }

    private CarConstructed LoadCar(Transform point, SCarConstructed carConstructed) {
        var instance = new GameObject("Car").AddComponent<CarConstructed>();
        instance.Load(carConstructed);
        instance.LoadStats();
        instance.transform.position = point.position;
        var body = instance.body;
        var lScale = instance.transform.localScale;

        body.activatablePart.Enable();

        var wheels = body.GetWheelsAttached();
        wheels.ForEach(x => x.activatablePart.Enable());
        var weapons = body.GetWeaponsAttached();
        weapons.ForEach(x => x.activatablePart.Enable());
        var items = body.GetItemsAttached();
        items.ForEach(x => x.activatablePart.Enable());

        if (point.position.x > 0) {
            instance.transform.localScale = new Vector3(-1 * lScale.x, lScale.y, lScale.z);
            instance.body.GetWheelsAttached().ForEach(x => x.activatablePart.SetDirection(false));
            instance.isLeft = false;
            instance.OnDamageTaken += car => {
                fightingCanvasController.right.UpdateFill(((float)instance.hp) / instance.overallHp);
            };
            fightingCanvasController.right.Init(instance);
            fightingCanvasController.right.UpdateFill(1f);
            weapons.ForEach(x => x.activatablePart.SetDirection(false));
        }
        else {
            instance.transform.localScale = new Vector3(lScale.x, lScale.y, lScale.z);
            instance.isLeft = true;
            instance.OnDamageTaken += car => {
                fightingCanvasController.left.UpdateFill(((float)instance.hp) / instance.overallHp);
            };
            fightingCanvasController.left.Init(instance);
            fightingCanvasController.left.UpdateFill(1f);
            weapons.ForEach(x => x.activatablePart.SetDirection(true));
        }
        instance.transform.localScale *= 0.7f;
        instance.ActivateColliders(true);
        return instance;
    }
}
