﻿using UnityEngine;
using Extensions;
using System.Collections.Generic;
using System.Linq;

public static class PartLoader {
    public static Body LoadBody(SBody body) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Bodies/" + body.subtype);
        var instance = GameObject.Instantiate(prefab).GetComponent<Body>();
        instance.LoadData(body);
        return instance;
    }
    public static Body LoadBody(CarConstructed carConstructed, SBody body) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Bodies/" + body.subtype);
        var instance = GameObject.Instantiate(prefab).GetComponent<Body>();
        instance.car = carConstructed;
        instance.LoadData(body);
        return instance;
    }
    public static AWheel LoadWheel(SWheel wheel) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Wheels/" + wheel.subtype);
        var instance = GameObject.Instantiate(prefab).GetComponent<Wheel>();
        instance.LoadData(wheel);
        return instance;
    }
    public static AWeapon LoadWeapon(SWeapon weapon) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Weapons/" + weapon.subtype);
        var instance = GameObject.Instantiate(prefab).GetComponent<Weapon>();
        instance.LoadData(weapon);
        return instance;
    }
    public static AItem LoadItem(SItem item) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Items/" + item.subtype);
        var instance = GameObject.Instantiate(prefab).GetComponent<Item>();
        instance.LoadData(item);
        return instance;
    }

    public static WheelPoint LoadWheelPoint(Body body, SWheelPoint point) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Points/Wheel");
        var instance = GameObject.Instantiate(prefab).GetComponent<WheelPoint>();
        instance.LoadData(body, point);
        return instance;
    }
    public static WeaponPoint LoadWeaponPoint(Body body, SWeaponPoint point) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Points/Weapon");
        var instance = GameObject.Instantiate(prefab).GetComponent<WeaponPoint>();
        instance.LoadData(body, point);
        return instance;
    }
    public static ItemPoint LoadItemPoint(Body body, SItemPoint point) {
        var prefab = Resources.Load<GameObject>("Prefabs/Dummies/Points/Item");
        var instance = GameObject.Instantiate(prefab).GetComponent<ItemPoint>();
        instance.LoadData(body, point);
        return instance;
    }
    public static Body GenerateRandomWheelPoints(Body body, int count) {
        // Calculating same y for all wheel points.
        float yRand = Random.Range(body.wheelsRandomAnchor.min.localPosition.y, body.wheelsRandomAnchor.max.localPosition.y);

        if ((count + 1) * body.wheelsMinDistance >= body.wheelsRandomAnchor.max.localPosition.x - body.wheelsRandomAnchor.min.localPosition.x) {
            throw new System.Exception("Min point distance is too big or uncorrectly small anchored area.");
        }

        // Calculating pos by x for each wheel position
        List<float> xTaken = new List<float>();
        List<FloatRange> rangesLeft = new List<FloatRange>() { new FloatRange(body.wheelsRandomAnchor.min.localPosition.x, body.wheelsRandomAnchor.max.localPosition.x) };

        for (int i_count = 0; i_count < count && rangesLeft.Count > 0; i_count++) {
            int chosenRange = Random.Range(0, rangesLeft.Count);
            float xGot = rangesLeft[chosenRange].GetRandom();
            xTaken.Add(xGot);
            IRange<float>[] subRanges = rangesLeft[chosenRange].Remove(new FloatRange(xGot - body.wheelsMinDistance, xGot + body.wheelsMinDistance));
            rangesLeft.RemoveAt(chosenRange);
            for (int i_sub = 0; i_sub < subRanges.Length; i_sub++) {
                rangesLeft.Insert(chosenRange++, (FloatRange)subRanges[i_sub]);
            }
        }
        for (int i = 0, countTaken = xTaken.Count; i < countTaken; i++) {
            var tempWP = new SWheelPoint();
            tempWP.localPosition = new Vector3(xTaken[i], yRand);
            tempWP.part = null;
            LoadWheelPoint(body, tempWP);
            //new Vector3(xTaken[i], yRand), null));
        }
        //for (int i_count = 0; i_count < count; i_count++) {
        //    int chosenRange = Random.Range(0, rangesLeft.Count);
        //    Log.Msg("Chosen range: [" + rangesLeft[chosenRange].min + ":" + rangesLeft[chosenRange].max + "]");
        //    float xGot = rangesLeft[chosenRange].Clarify(body.minDistance).GetRandom();
        //    Log.Msg("x got: " + xGot);
        //    xTaken.Add(xGot);
        //    List<FloatRange> rangesSeperated = rangesLeft[chosenRange].Seperate(xGot).Cast<FloatRange>().ToList();
        //    rangesLeft.RemoveAt(chosenRange);
        //    for (int i_range = 0; i_range < rangesSeperated.Count; i_range++) {
        //        for (int i_taken = 0, takenCount = xTaken.Count; i_taken < takenCount; i_taken++) {
        //            if (rangesLeft.Get)
        //        }
        //        rangesLeft.Insert(chosenRange++, rangesSeperated[i_range]);
        //    }
        //}
        return body;
    }
    public static Body GenerateRandomItemPoints(Body body, int weaponsCount, int itemsCount) {
        List<Vector2> dotsTaken = new List<Vector2>();
        int sumCount = weaponsCount + itemsCount;
        Vector2 size = body.itemsRandomAnchor.max.localPosition - body.itemsRandomAnchor.min.localPosition;
        int countX = Mathf.RoundToInt((size.x / body.itemsMinDistance) / 2);
        int countY = Mathf.RoundToInt((size.y / body.itemsMinDistance) / 2);
        int rectsCount = countX * countY;
        if (rectsCount < sumCount) {
            throw new System.Exception("Not enough points.");
        }
        float halfDistance = body.itemsMinDistance;
        Vector2 leftUpCornerOnBodyInLocal = new Vector2(body.itemsRandomAnchor.min.localPosition.x, body.itemsRandomAnchor.max.localPosition.y);
        List<RectArea> rects = new List<RectArea>();
        for (int y_i = 0; y_i < countY; y_i++) {
            int y_real = y_i * 2;
            for (int x_i = 0; x_i < countX; x_i++) {
                int x_real = x_i * 2;
                float realFXPos = leftUpCornerOnBodyInLocal.x + x_real * body.itemsMinDistance;
                float realFYPos = leftUpCornerOnBodyInLocal.y - y_real * body.itemsMinDistance;
                rects.Add(new RectArea(new Vector2(realFXPos, realFYPos - body.itemsMinDistance),
                    new Vector2(realFXPos + body.itemsMinDistance, realFYPos)));
            }
        }
        if (rects.Count < sumCount) {
            throw new System.Exception("Not enough rects");
        }
        var randList = new RandomList<RectArea>(rects);

        var weaponPoints = new List<Vector2>();
        for (int i = 0; i < weaponsCount; i++) {
            weaponPoints.Add(randList.Get().GetRand());
        }
        var itemPoints = new List<Vector2>();
        for (int i = 0; i < itemsCount; i++) {
            itemPoints.Add(randList.Get().GetRand());
        }

        for (int i = 0; i < weaponPoints.Count; i++) {
            var tempWP = new SWeaponPoint();
            tempWP.localPosition = weaponPoints[i];
            tempWP.part = null;
            LoadWeaponPoint(body, tempWP);
        }
        for (int i = 0; i < itemPoints.Count; i++) {
            var tempIP = new SItemPoint();
            tempIP.localPosition = itemPoints[i];
            tempIP.part = null;
            LoadItemPoint(body, tempIP);
        }
        return body;
    }
}