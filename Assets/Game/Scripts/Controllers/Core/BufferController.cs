﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Buffer for exchanging info between scenes.
/// </summary>
public static class BufferController {
    /// <summary>
    /// Buffer keys.
    /// </summary>
    public enum BufferKey {
        // Main.
        IntroPlayed,
        YouTubePageFirstOpenedVideoType,
        VideosOpenedDuringGameplay,

        // Temporary.
        AchievementWasUnlocked,
        YouTubeEnabled,
        LastRecipeSelectionState,
    }

    /// <summary>
    /// Temporary info that's clearing after any scene was loaded.
    /// </summary>
    private static Dictionary<BufferKey, object> _temporaryInfo;

    /// <summary>
    /// Clearing only after app was closed.
    /// </summary>
    private static Dictionary<BufferKey, object> _mainInfo;

    /// <summary>
    /// Called on first buffer call.
    /// </summary>
    static BufferController() {
        _temporaryInfo = new Dictionary<BufferKey, object>();
        _mainInfo = new Dictionary<BufferKey, object>();

        SceneManager.sceneLoaded += OnSceneChangedHandler;
    }

    /// <summary>
    /// Called when scene was changed.
    /// </summary>
    private static void OnSceneChangedHandler(Scene scene, LoadSceneMode loadSceneMode) {
        _temporaryInfo.Clear();
    }

    /// <summary>
    /// Returns main data.
    /// </summary>
    public static object GetMainDataByKey(BufferKey key) {
        object returnedInfo = null;
        _mainInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
        }
        return returnedInfo;
    }

    /// <summary>
    /// Returns temporary data (WORKS ONLY ON AWAKE/START).
    /// </summary>
    public static object GetTemporaryDataByKey(BufferKey key) {
        object returnedInfo = null;
        _temporaryInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in temporary data of BufferController");
        }
        return returnedInfo;
    }

    /// <summary>
    /// Returns main data by type (returns type's default value if null).
    /// </summary>
    public static T GetMainDataByKey<T>(BufferKey key) {
        object returnedInfo = null;
        _mainInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
            return default(T);
        }
        return (T)returnedInfo;
    }

    /// <summary>
    /// Returns main data by type (returns type's default value if null).
    /// </summary>
    public static T GetMainDataByKey<T>(BufferKey key, T defaultValue) {
        object returnedInfo = null;
        _mainInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
            return defaultValue;
        }
        return (T)returnedInfo;
    }

    /// <summary>
    /// Returns temporary data by type (returns type's default value if null).
    /// </summary>
    public static T GetTemporaryDataByKey<T>(BufferKey key) {
        object returnedInfo = null;
        _temporaryInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in temporary data of BufferController");
            return default(T);
        }
        return (T)returnedInfo;
    }


    /// <summary>
    /// Returns temporary data by type (returns type's default value if null).
    /// </summary>
    public static T GetTemporaryDataByKey<T>(BufferKey key, T defaultValue) {
        object returnedInfo = null;
        _temporaryInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
            return defaultValue;
        }
        return (T)returnedInfo;
    }

    /// <summary>
    /// Add main data by key.
    /// </summary>
    public static void SetMainData(BufferKey key, object value) {
        if (_mainInfo.ContainsKey(key)) {
            _mainInfo[key] = value;
            Log.Warning("Key [" + key + "] info will be replaced in main data of BufferController");
            return;
        }
        _mainInfo.Add(key, value);
    }

    /// <summary>
    /// Add temporary data by key.
    /// </summary>
    public static void SetTemporaryData(BufferKey key, object value) {
        if (_temporaryInfo.ContainsKey(key)) {
            _temporaryInfo[key] = value;
            Log.Warning("Key [" + key + "] info will be replaced in temporary data of BufferController");
            return;
        }
        _temporaryInfo.Add(key, value);
    }

    /// <summary>
    /// Remove main data by key.
    /// </summary>
    public static bool RemoveMainData(BufferKey key) {
        return _mainInfo.Remove(key);
    }

    /// <summary>
    /// Remove temporary data by key.
    /// </summary>
    public static bool RemoveTemporaryData(BufferKey key) {
        return _temporaryInfo.Remove(key);
    }
}
