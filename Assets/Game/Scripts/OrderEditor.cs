﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
[RequireComponent(typeof(Renderer))]
public class OrderEditor : MonoBehaviour {
    private Renderer _renderer;
    [SerializeField]
    [HideInInspector]
    private int _sortingOrder;
    public int sortingOrder;

    private void Awake() {
        _renderer = GetComponent<Renderer>();
        _renderer.sortingOrder = _sortingOrder;
        sortingOrder = _sortingOrder;
    }
	void Update () {
#if UNITY_EDITOR
        if (sortingOrder != _sortingOrder) {
            _sortingOrder = sortingOrder;
        }
        _renderer.sortingOrder = _sortingOrder;
#endif
    }
}
