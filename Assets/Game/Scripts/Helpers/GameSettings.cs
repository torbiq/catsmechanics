﻿using UnityEngine;
using System;

public static class GameSettings
{
	public static Action<float>
		OnMusicVolumeChanged,
		OnSoundsVolumeChanged;
	public static Action<bool>
		OnMusicEnabled,
		OnSoundsEnabled,
		OnVibroEnabled;


	private static bool actual = false;

	private static bool vibro_on = true;
	//private static bool music_on = true;
	private static bool sounds_on = true;
	private static float music_vol = 1;
	private static float sounds_vol = 1;
	private static float music_vol_override = 1;
	private static int current_lang = -1;



	[RuntimeInitializeOnLoadMethod ( RuntimeInitializeLoadType.BeforeSceneLoad )]
	public static void UpdateSettings ()
	{
		if (!actual)
		{
			Debug.Log ( "GameSettings: UpdateSettings" );
			vibro_on = PlayerPrefs.GetInt ( "vibro_on", 1 ) == 0 ? false : true;
			sounds_on = PlayerPrefs.GetInt ( "sounds_on", 1 ) == 0 ? false : true;
			//music_on = PlayerPrefs.GetInt ( "music_on", 1 ) == 0 ? false : true;

			sounds_vol = PlayerPrefs.GetFloat ( "sounds_vol", 1 );
			music_vol = PlayerPrefs.GetFloat ( "music_vol", 1 );

			current_lang = PlayerPrefs.GetInt ( "CurrentLanguage", -1 );
			actual = true;
		}
	}


	public static bool IsVibroEnabled ()
	{
		return vibro_on;
	}

	public static void EnableVibro (bool param)
	{
		vibro_on = param;
		PlayerPrefs.SetInt ( "vibro_on", vibro_on == false ? 0 : 1 );
		SaveSettings ( );
		if (OnVibroEnabled != null)
		{
			OnVibroEnabled ( vibro_on );
		}
	}


	public static bool IsMusicEnabled ()
	{
		//return music_on;
		return IsSoundsEnabled ( );
	}

	public static void EnableMusic (bool param)
	{
		//      music_on = param;
		//      AudioController.EnableMusic ( music_on );
		//      PlayerPrefs.SetInt ( "music_on", (music_on == false ? 0 : 1) );
		//      SaveSettings ( );
		//if (OnMusicEnabled != null)
		//{
		//	OnMusicEnabled ( sounds_on );
		//}
		EnableSounds ( param );
	}


	public static bool IsSoundsEnabled ()
	{
		return sounds_on;
	}

	public static void EnableSounds (bool param)
	{
		sounds_on = param;
		AudioController.EnableSounds ( sounds_on );
		AudioController.EnableMusic ( sounds_on );
		PlayerPrefs.SetInt ( "sounds_on", (sounds_on == false ? 0 : 1) );
		SaveSettings ( );
		if (OnSoundsEnabled != null)
		{
			OnSoundsEnabled ( sounds_on );
		}
		if (OnMusicEnabled != null)
		{
			OnMusicEnabled ( sounds_on );
		}
	}

	public static float GetMusicVol ()
	{
		return music_vol * music_vol_override;
	}

	public static void SetMusicVolOverride (float param)
	{
		music_vol_override = param;
		RefreshVolume ( );
	}

	public static void SetMusicVol (float param)
	{
		music_vol = param;
		AudioController.SetMusicVolume ( music_vol );
		PlayerPrefs.SetFloat ( "music_vol", music_vol );
		SaveSettings ( );
	}


	public static float GetSoundsVol ()
	{
		return sounds_vol;
	}


	private static void RefreshVolume ()
	{
		float s_vol = GetSoundsVol ( );
		float m_vol = GetMusicVol ( );
		AudioController.SetSoundsVolume ( s_vol );
		if (OnSoundsVolumeChanged != null)
		{
			OnSoundsVolumeChanged ( s_vol );
		}
		AudioController.SetMusicVolume ( m_vol );
		if (OnMusicVolumeChanged != null)
		{
			OnMusicVolumeChanged ( m_vol );
		}
	}

	public static void SetSoundsVol (float param)
	{
		sounds_vol = param;
		PlayerPrefs.SetFloat ( "sounds_vol", sounds_vol );
		SaveSettings ( );
		RefreshVolume ( );

	}


	public static int GetCurrentLang ()
	{
		return current_lang;
	}


	public static void SetCurrentLang (int lang)
	{
		current_lang = lang;
		PlayerPrefs.SetInt ( "CurrentLanguage", current_lang );
		SaveSettings ( );
	}


	public static void SaveSettings ()
	{
		PlayerPrefs.Save ( );
	}
}
