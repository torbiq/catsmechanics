using UnityEngine;
using System.Collections;

public static class Vibration
{

#if UNITY_ANDROID && !UNITY_EDITOR
    public static AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    public static AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
    public static AndroidJavaObject vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");	
#endif



	public static void Vibrate (long milliseconds)
	{
		if (GameSettings.IsVibroEnabled ( ))
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			vibrator.Call("vibrate", milliseconds);
#endif
		}
	}

	public static void Cancel ()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		vibrator.Call("cancel");
#endif
	}

	private static bool isAndroid ()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
	return true;
#else
		return false;
#endif
	}
}
