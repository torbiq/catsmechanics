﻿using System.Collections.Generic;
namespace PSV_Prototype
{

	public enum Scenes  //List here all scene names included in the build   (permanent scenes are InitScene, PSV_Splash (or other), Push)
	{
		InitScene,
		PSV_Splash,
		//Melnitsa_Splash,
		Push,
		MainMenu,
		NewAdsDemo,
		RateMeScene,
        Garage,
        Fight,
    }

    public static class SceneLoaderSettings
	{

		public static bool
			transition_after_ad = true;

		public static AdsInterop.AdPosition
			small_banner_default_pos = AdsInterop.AdPosition.Bottom_Centered,
			native_default_pos = AdsInterop.AdPosition.Bottom_Centered;

		public static AdsInterop.AdSize
			small_banner_default_size = AdsInterop.AdSize.Standard_banner_320x50,
			native_default_size = new AdsInterop.AdSize ( 320, 80 );


		public static Dictionary<Scenes, AdsInterop.AdPosition>
			small_banner_position_override = new Dictionary<Scenes, AdsInterop.AdPosition> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
            },
			native_position_override = new Dictionary<Scenes, AdsInterop.AdPosition> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
				//{ Scenes.Sample, AdsInterop.AdPosition.Bottom_Centered},
			};


		public static Dictionary<Scenes, AdsInterop.AdSize>
			small_banner_size_override = new Dictionary<Scenes, AdsInterop.AdSize> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
				//{ Scenes.MainMenu, AdsInterop.AdSize.Smart_banner},
				//{ Scenes.Sample, AdsInterop.AdSize.Standard_banner_320x50},
			},
			native_size_override = new Dictionary<Scenes, AdsInterop.AdSize> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
				//{ Scenes.MainMenu, AdsInterop.AdSize.Smart_banner},
				//{ Scenes.Sample, AdsInterop.AdSize.Standard_banner_320x50},
			};


		public static List<Scenes>
			not_allowed_small_banner = new List<Scenes> ( ) //list here scenes that shouldn't show ad 
            {
				Scenes.PSV_Splash,
				Scenes.Push,
				Scenes.MainMenu,
			};

		public static List<Scenes>
			not_allowed_interstitial = new List<Scenes> ( ) //list here scenes which wouldn't show big banner if we will leave them
            {
				Scenes.InitScene,
				Scenes.PSV_Splash,
				Scenes.Push,
				Scenes.RateMeScene,
			};

		public static List<Scenes>
			native_allowed = new List<Scenes> ( )
			{
				//Scenes.Sample,
			};

		public static List<Scenes>
			rate_me_after_end = new List<Scenes> ( ) //list here scenes which will call rate_me dialogue on end
            {
			};

		public static List<Scenes>
			home_ads_after_end = new List<Scenes> ( ) //list here scenes which will call rate_me dialogue on end
            {
				Scenes.NewAdsDemo,
			};
	}
}
