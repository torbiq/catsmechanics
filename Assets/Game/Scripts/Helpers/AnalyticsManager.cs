﻿//#define FIREBASE_PRESENT

#if FIREBASE_PRESENT
using Firebase.Analytics;
using UnityEngine;
#endif

using System.Collections.Generic;

#if UNITY_ANALYTICS
using UnityEngine.Analytics;
#endif

namespace PSV_Prototype
{
	public enum AnalyticsEvents    //used to log events from one place
	{
		StartApplication,
		CloseApplication,
		LogScreen,
		BannerClicked,
		InterstitialClicked,
		OpenPub,
		OpenPromo,
		Custom,
		NeatPlugError,
		HomeAdsError,
	}


	public static class AnalyticsManager
	{
		private const int event_name_limit = 40;

		public static void LogEvent (AnalyticsEvents _event, string _message = "")
		{
			if (_event == AnalyticsEvents.LogScreen)
			{
				_message += GetGameMode ( );
			}

			if (ProjectSettingsManager.settings.analytics_manager_debug)  //comment if missing this manager
			{
				UnityEngine.Debug.Log ( "AnalyticsManager.LogEvent ( " + _event + ", " + _message + " )" );
			}
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
			LogGoogleAnalyticsEvent ( _event, _message );
			LogFirebaseEvent ( _event, _message );
			LogUnityEvent ( _event, _message );
#endif
		}

		private static void LogGoogleAnalyticsEvent (AnalyticsEvents _event, string _message)
		{
			if (GoogleAnalytics.Instance != null)
			{
				string
					titleCat = null,
					titleAction = null,
					titleLabel = "";
				int value = 0;

				switch (_event)
				{
					case AnalyticsEvents.LogScreen:
						GoogleAnalytics.Instance.LogScreen ( _message );
						break;
					case AnalyticsEvents.Custom:
						titleCat = _event.ToString ( );
						titleAction = _message;
						break;
					case AnalyticsEvents.CloseApplication:
					case AnalyticsEvents.StartApplication:
						titleCat = _event.ToString ( );
						titleAction = _event.ToString ( );
						break;
					case AnalyticsEvents.BannerClicked:
						titleCat = "AdMob Clicks";
						titleAction = "Banner clicked";
						break;
					case AnalyticsEvents.InterstitialClicked:
						titleCat = "AdMob Clicks";
						titleAction = "Interstitial clicked";
						break;
					case AnalyticsEvents.OpenPub:
						titleCat = "Promo";
						titleAction = "OpenURL " + PromoPlugin.ServiceUtils.GetPlatformName ( ) + ": MoreGames";
						break;
					case AnalyticsEvents.OpenPromo:
						titleCat = "Promo";
						titleAction = "OpenURL " + PromoPlugin.ServiceUtils.GetPlatformName ( ) + ": " + _message;
						break;
					case AnalyticsEvents.NeatPlugError:
						titleCat = "Ads";
						titleAction = "NeatPlugError: " + _message;
						break;
					case AnalyticsEvents.HomeAdsError:
						titleCat = "Ads";
						titleAction = "HomeAdsError: " + _message;
						break;
				}

				if (!string.IsNullOrEmpty ( titleCat ) && !string.IsNullOrEmpty ( titleAction ))
				{
					GoogleAnalytics.Instance.LogEvent ( titleCat, titleAction, titleLabel, value );

				}
			}
		}

		private static void LogFirebaseEvent (AnalyticsEvents _event, string _message)
		{
#if FIREBASE_PRESENT

			switch (_event)
			{
				case AnalyticsEvents.CloseApplication:
				case AnalyticsEvents.StartApplication:
					//case AnalyticsEvents.BannerClicked:
					//case AnalyticsEvents.InterstitialClicked:
					FirebaseAnalytics.LogEvent ( _event.ToString ( ) );
					break;
				case AnalyticsEvents.OpenPub:
				case AnalyticsEvents.OpenPromo:
				case AnalyticsEvents.NeatPlugError:
				case AnalyticsEvents.HomeAdsError:
				case AnalyticsEvents.Custom:
					string m = "C_" + _message;
					FirebaseAnalytics.LogEvent ( m.Substring ( 0, UnityEngine.Mathf.Min ( m.Length, event_name_limit ) ) );
					break;
				case AnalyticsEvents.LogScreen:
					FirebaseAnalytics.LogEvent ( FirebaseAnalytics.EventSelectContent, new Parameter [] {
						new Parameter ( FirebaseAnalytics.ParameterContentType, _message),
						new Parameter ( FirebaseAnalytics.ParameterItemCategory, "GameMode" )
					} );
					break;
			}
#endif
		}



		private static void LogUnityEvent (AnalyticsEvents _event, string _message)
		{
#if UNITY_ANALYTICS
			Dictionary<string, object> parameters = new Dictionary<string, object> ( );
			string eventName = null;
			switch (_event)
			{
				case AnalyticsEvents.LogScreen:
					eventName = _event.ToString ( );
					parameters.Add ( "screen", _message );
					break;
				case AnalyticsEvents.Custom:
					eventName = _event.ToString ( );
					parameters.Add ( "message", _message );
					break;
				case AnalyticsEvents.CloseApplication:
				case AnalyticsEvents.StartApplication:
				case AnalyticsEvents.BannerClicked:
				case AnalyticsEvents.InterstitialClicked:
					eventName = _event.ToString ( );
					break;
				case AnalyticsEvents.OpenPub:
					eventName = "Promo";
					parameters.Add ( "MoreGames " + PromoPlugin.ServiceUtils.GetPlatformName ( ), _message );
					break;
				case AnalyticsEvents.OpenPromo:
					eventName = "Promo";
					parameters.Add ( "OpenURL " + PromoPlugin.ServiceUtils.GetPlatformName ( ), _message );
					break;
				case AnalyticsEvents.NeatPlugError:
					eventName = "Ads";
					parameters.Add ( "NeatPlugError", _message );
					break;
				case AnalyticsEvents.HomeAdsError:
					eventName = "Ads";
					parameters.Add ( "HomeAdsError", _message );
					break;
			}


			Analytics.CustomEvent ( eventName, parameters );


#endif
		}




		public static string GetGameMode ()
		{
			string res = "";
			//put here processing of current selected game_mode for scenes that support it
			int current_game_mode = 0; //{2+, 5+, 0}

			//switch (AgeController.GetAge ( ))
			//{
			//	case Age.AGE_FIVE_PLUS:
			//		current_game_mode = 5;
			//		break;
			//	case Age.AGE_TWO_PLUS:
			//		current_game_mode = 2;
			//		break;
			//}


			if (current_game_mode > 0)
			{
				res = "_" + current_game_mode;
			}
			return res;
		}



	}
}
