﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif


namespace PSV_Prototype
{
	public static class ProjectSettingsManager
	{
		private static ProjectSettingsContainer container = null;

		public static ProjectSettingsContainer settings
		{
			get
			{
				if (container == null)
				{
					LoadSettings ( );
				}
				return container;
			}
		}

		private static void LoadSettings ()
		{
			container = Resources.Load<ProjectSettingsContainer> ( "ProjectSettings" );
			if (container == null)
			{
				Debug.LogError ( "ProjectSettingsManager: to configure your project create ProjectSettingsContainer and save it at Assets/Resources/ProjectSettings.asset" );
				container = ScriptableObject.CreateInstance<ProjectSettingsContainer> ( );
			}
		}
	}

	[CreateAssetMenu]
	public class ProjectSettingsContainer :ScriptableObject
	{
		[System.Serializable]
		public class ProviderSettings
		{
			public ProviderParams param;
			public string value;

			public ProviderSettings (ProviderParams _param, string _value)
			{
				param = _param;
				value = _value;
			}
		}

		[System.Serializable]
		public class PrimarySet
		{
			public string
				bundle,
				alias,
				name;

			public PrimarySet (string _bundle, string _alias, string _name)
			{
				bundle = _bundle;
				alias = _alias;
				name = _name;
			}
		}


		[Header ( "IAP purchase key" )]
		public string
			public_key;

		[Header ( "ADS and Analytics" )]
		public bool
			ads_manager_debug = false;
		public bool
			manager_google_debug = false;
		public bool
			firebase_manager_debug = false;
		public bool
			billing_manager_debug = false;
		public bool
			analytics_manager_debug = false;

		[Space ( 10 )]

		public string
			android_analytics_id;
		public string
			ios_analytics_id;
		public string
			windows_analytics_id;

		public ProviderSettings []
			android_provider_settings = new ProviderSettings [] { new ProviderSettings ( ProviderParams.NEATPLUG_BANNER_ID, "" ), new ProviderSettings ( ProviderParams.NEATPLUG_INTERSTITIAL_ID, "" ) },
			ios_provider_settings = new ProviderSettings [] { new ProviderSettings ( ProviderParams.GOOGLEADS_BANNER_ID, "" ), new ProviderSettings ( ProviderParams.GOOGLEADS_INTERSTITIAL_ID, "" ) };

		[HideInInspector]
		public AdNetwork []
	providers_order
		{
			get
			{
#if UNITY_ANDROID
				return android_providers_order;
#elif UNITY_IPHONE
				return ios_providers_order;
#else
				return new AdNetwork [0];
#endif
			}
		}


		[Header ( "Add GoogleAds if using rewarded or native (avoid dublicates)" )]
		public AdNetwork []
			android_providers_order = new AdNetwork []
			{
				AdNetwork.NeatPlug,
				AdNetwork.HomeAds,
			};
		public AdNetwork []
			ios_providers_order = new AdNetwork []
			{
				AdNetwork.GoogleAds,
			};
	}


#if UNITY_EDITOR


	// IngredientDrawer
	[CustomPropertyDrawer ( typeof ( ProjectSettingsContainer.ProviderSettings ) )]
	public class ProviderSettingsDrawer :PropertyDrawer
	{
		// Draw the property inside the given rect
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty ( position, label, property );

			// Draw label
			position = EditorGUI.PrefixLabel ( position, GUIUtility.GetControlID ( FocusType.Passive ), label );

			// Don't make child fields be indented
			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			// Calculate rects
			float param_w = Mathf.Clamp ( position.width * 0.4f, 30f, 100f );
			var paramRect = new Rect ( position.x, position.y, param_w, position.height );
			var valueRect = new Rect ( position.x + param_w, position.y, position.width - param_w, position.height );

			// Draw fields - passs GUIContent.none to each so they are drawn without labels
			EditorGUI.PropertyField ( paramRect, property.FindPropertyRelative ( "param" ), GUIContent.none );
			EditorGUI.PropertyField ( valueRect, property.FindPropertyRelative ( "value" ), GUIContent.none );

			// Set indent back to what it was
			EditorGUI.indentLevel = indent;

			EditorGUI.EndProperty ( );
		}
	}

#endif

}
