﻿#if UNITY_ANALYTICS
#define USE_UNITY_IAP
#endif
//#define USE_OPEN_IAB


using UnityEngine;
using System.Collections.Generic;
using PSV_Prototype;

#if USE_OPEN_IAB
using OnePF;
#endif


#if USE_UNITY_IAP
using UnityEngine.Purchasing;
#endif
/// ChangeLog
/// v2.0
/// - added ProjectSettings interaction (asset which holds settings for current project)
/// - added ruchase restore
/// v2.1
/// - removed unsupported platforms from SDK
/// v3.0
/// - changed MapSKU method, now it process' array of productIdentifiers
/// - to add new SKU declare it as const and add it to array "productIdentifiers"
/// - implemented EasyStoreKit interaction to support IAP on IOS
/// - implemented array of consumable purchases for Android
/// - using deubg trigger from ProjectSettingsContainer
/// v 4.0
/// - fixed restoring for android (process only received items)
/// - implemented sku as dictionary value for Products (can be assigned to different value under other platforms
/// - added purchasees events with Producs enum argument
/// v 5.0
/// - added UnityPurchase support (IOS)
/// - EasyStore deprecated
/// v5.1
/// - fixed GetProduct (if (item.Value.sku.Equals ( sku_alias ))



///Depends On:
///- ProjectSettingsContainer
///- ManagerGoogle
///- StoreKit (XCode framework)


public class BillingManager :MonoBehaviour
#if USE_UNITY_IAP
, IStoreListener
#endif

{
#if !USE_UNITY_IAP
	public enum ProductType
	{
		Consumable,
		NonConsumable,
		Subscription,
	}
#endif


	public struct ProductProperties
	{
		public string sku;
		public ProductType type;

		public ProductProperties (string sku, ProductType type)
		{
			this.sku = sku;
			this.type = type;
		}

		override public string ToString ()
		{
			return sku + " [" + type + "]";
		}
	}

	public enum Products
	{
		Empty, //Leave this (service item)
		SKU_ADMOB,
		SKU_CONSUMABLE,
		SKU_PREMIUM_SUBSCRIPTION,
	}


	static public System.Action<Products, bool>
		OnPurchaseSucceded,
		OnPurchaseCancelled;
	static public System.Action
		OnPurchaseFailure;

	private static Dictionary<Products, ProductProperties> Products_SKU = new Dictionary<Products, ProductProperties> ( )
	{

#if UNITY_ANDROID
		//list here as constantns all your products, registered in GooglePlay console
		//no capital letters
		{ Products.SKU_ADMOB,                   new ProductProperties ("admob",       ProductType.NonConsumable) },
		//{ Products.SKU_CONSUMABLE,              new ProductProperties ("cons_id",     ProductType.Consumable) },
		//{ Products.SKU_PREMIUM_SUBSCRIPTION,    new ProductProperties ("premium",     ProductType.Subscription) },


#elif UNITY_IPHONE
	//IOS SKUs are unique per account, so we have to add app's bundle to common product name
	//use your projects' SKU
		{ Products.SKU_ADMOB,					new SkuItem ("com.hippo.cafe2_admob",				ProductType.NonConsumable) },
		//{ Products.SKU_CONSUMABLE,				new SkuItem ("com.hippo.cafe2_coins10",				ProductType.Consumable) },
		//{ Products.SKU_PREMIUM_SUBSCRIPTION,	new SkuItem ("com.hippo.cafe2_subscription",		ProductType.Subscription) },

#endif

	};


	static string PUBLIC_KEY = "";

	bool debug_mode = false;
	bool _isInitialized = false;

#if USE_OPEN_IAB
	Inventory _inventory = null;
#endif

	public bool IsInitialized
	{
		get
		{
#if USE_UNITY_IAP
			// Only say we are initialized if both the Purchasing references are set.
			return m_StoreController != null && m_StoreExtensionProvider != null;
#elif USE_OPEN_IAB
			return _isInitialized;
#else
			return false;
#endif
		}
	}

	Products GetProduct (string sku_alias)
	{
		foreach (KeyValuePair<Products, ProductProperties> item in Products_SKU)
		{
			if (item.Value.sku.Equals ( sku_alias ))
			{
				return item.Key;
			}
		}
		return Products.Empty;
	}


	ProductProperties [] GetProductIdentifiers ()
	{

		ProductProperties [] identifiers = new ProductProperties [Products_SKU.Count];
		int index = 0;
		foreach (var item in Products_SKU)
		{
			identifiers [index++] = item.Value;
		}
		return identifiers;
	}

	private void DebugLog (string s, bool error = false)
	{
		if (debug_mode || error)
		{
			if (error)
			{
				Debug.LogError ( "BillingManager: " + s );
			}
			else
			{
				Debug.Log ( "BillingManager: " + s );
			}
		}
	}



	#region Singelton

	private static BillingManager instance;

	public static BillingManager Instance
	{
		get
		{
			if (instance == null)
			{
				//check if manager exists in scene
				BillingManager manager = FindObjectOfType<BillingManager> ( );
				if (manager == null)
				{
					//create new manager
					GameObject obj = new GameObject ( "BillingManager" );
					manager = obj.AddComponent<BillingManager> ( );
				}
				SetInstance ( manager );
			}
			return instance;
		}
	}


	private static void SetInstance (BillingManager manager)
	{
		instance = manager;
		DontDestroyOnLoad ( instance.gameObject );
	}

	void Awake ()
	{
		SetInstance ( this );
		if (instance == this)
		{
#if USE_OPEN_IAB
			if (FindObjectOfType<OpenIABEventManager> ( ) == null)
			{
				GameObject e_man = new GameObject ( "OpenIABEventManager" );
				e_man.AddComponent<OpenIABEventManager> ( );
			}
#endif
		}
		else
		{
			DebugLog ( "BillingManager: not allowed multiple instances", true );
		}
	}

	#endregion


	private void OnEnable ()
	{
#if USE_OPEN_IAB
		if (instance == this)
		{
			// Listen to all events for illustration purposes
			OpenIABEventManager.billingSupportedEvent += billingSupportedEvent;
			OpenIABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
			OpenIABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
			OpenIABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
			OpenIABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
			OpenIABEventManager.purchaseFailedEvent += purchaseFailedEvent;
			OpenIABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
			OpenIABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
		}
#endif
	}

	private void OnDisable ()
	{
#if USE_OPEN_IAB
		if (instance == this)
		{
			// Remove all event handlers
			OpenIABEventManager.billingSupportedEvent -= billingSupportedEvent;
			OpenIABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
			OpenIABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
			OpenIABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
			OpenIABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
			OpenIABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
			OpenIABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
			OpenIABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
		}
#endif
	}

	public void Init ()
	{
		debug_mode = ProjectSettingsManager.settings.billing_manager_debug; //comment if missing this manager

#if USE_OPEN_IAB
		string key = ProjectSettingsManager.settings.public_key;
		if (!string.IsNullOrEmpty ( key ))
		{
			PUBLIC_KEY = key;
		}
		if (string.IsNullOrEmpty ( PUBLIC_KEY ))
		{
			Debug.LogError ( "BillingManager: Set proper PUBLIC_KEY for your project" );
		}
		else
		{
			MapSKU ( );
			var options = new Options ( );
			options.checkInventoryTimeoutMs = Options.INVENTORY_CHECK_TIMEOUT_MS * 2;
			options.discoveryTimeoutMs = Options.DISCOVER_TIMEOUT_MS * 2;
			options.checkInventory = false;
			options.verifyMode = OptionsVerifyMode.VERIFY_SKIP;
			options.storeKeys = new Dictionary<string, string> { { OpenIAB_Android.STORE_GOOGLE, PUBLIC_KEY } };
			options.storeSearchStrategy = SearchStrategy.INSTALLER_THEN_BEST_FIT;

			OpenIAB.init ( options );
		}
#elif USE_UNITY_IAP
		InitializePurchasing ( );
#endif
	}

	public void Purchase (Products product)
	{
		DebugLog ( "Trying to purchase " + product );
		if (IsInitialized)
		{
			ProductProperties prop;
			if (Products_SKU.TryGetValue ( product, out prop ))
			{
#if USE_OPEN_IAB
				if (prop.type == ProductType.Subscription)
				{
					OpenIAB.purchaseSubscription ( prop.sku );
				}
				else
				{
					OpenIAB.purchaseProduct ( prop.sku );
				}
#elif USE_UNITY_IAP
				BuyProductID ( prop.sku );
#endif
			}
			else
			{
				DebugLog ( "Purchase " + product + " failed - no identifier found", true );
			}

			DebugLog ( "Purchase " + prop.ToString ( ) );

		}
		else
		{
			DebugLog ( product + " purchase failed, not initialized", true );
			Init ( );
		}
	}

	[System.Obsolete ( "Use Purchase(Products product) instead." )]
	public void PurchaseSubscription (Products product)
	{
		Purchase ( product );
	}


	private void PurchaseSucceded (string sku)
	{
		DebugLog ( "purchaseSuccededEvent: '" + sku + "'" );

		Products prod = GetProduct ( sku );

		switch (prod)
		{
			case Products.SKU_ADMOB:
				{
					ManagerGoogle.Instance.DisableAdmob ( );
					break;
				}
		}
		if (OnPurchaseSucceded != null)
		{
			OnPurchaseSucceded ( prod, GetProductType ( prod ) == ProductType.Consumable );
		}
	}

	private void purchaseCancelled (string sku)
	{
		DebugLog ( "purchaseCanceledEvent: '" + sku + "'" );

		Products prod = GetProduct ( sku );
		switch (prod)
		{
			case Products.SKU_ADMOB:
				{
					ManagerGoogle.Instance.EnableAdmob ( );
					break;
				}
		}
		if (OnPurchaseCancelled != null)
		{
			OnPurchaseCancelled ( prod, GetProductType ( prod ) == ProductType.Consumable );
		}
	}

	private void PurchaseFailed ()
	{
		if (OnPurchaseFailure != null)
		{
			OnPurchaseFailure ( );
		}
	}

	public ProductType GetProductType (string sku)
	{
		Products prod = GetProduct ( sku );
		return GetProductType ( prod );
	}

	public ProductType GetProductType (Products prod)
	{
		ProductType res = ProductType.NonConsumable;
		ProductProperties sku_item;
		if (Products_SKU.TryGetValue ( prod, out sku_item ))
		{
			res = sku_item.type;
		}
		return res;
	}

#if USE_OPEN_IAB

	private void MapSKU ()
	{
		foreach (var item in Products_SKU)
		{
			string sku = item.Value.sku;
			OpenIAB.mapSku ( sku, OpenIAB_Android.STORE_GOOGLE, sku );
		}
	}

	private void billingSupportedEvent ()
	{
		_isInitialized = true;
		OpenIAB.queryInventory ( );
		DebugLog ( "billingSupportedEvent" );
	}

	private void billingNotSupportedEvent (string error)
	{
		DebugLog ( "billingNotSupportedEvent: " + error, true );
	}

	private void queryInventorySucceededEvent (Inventory inventory)
	{
		DebugLog ( "queryInventorySucceededEvent: " + inventory );
		ProcessInventory ( inventory );
	}

	//restoring purchases
	private void ProcessInventory (Inventory inventory)
	{
		if (inventory != null)
		{
			_inventory = inventory;
			//check all purchases
			List<Purchase> purchases = inventory.GetAllPurchases ( );
			for (int i = 0; i < purchases.Count; i++)
			{
				Purchase p = purchases [i];
				if (p.PurchaseState == 0)
				{
					purchaseSucceededEvent ( p );
				}
				else
				{
					purchaseCancelled ( p.Sku );
				}
			}
		}
	}

	private void queryInventoryFailedEvent (string error)
	{
		DebugLog ( "queryInventoryFailedEvent: " + error, true );
	}

	private void purchaseSucceededEvent (Purchase purchase)
	{
		DebugLog ( "purchaseSucceededEvent: " + purchase );
		PurchaseSucceded ( purchase.Sku );

		ProductType type = GetProductType ( purchase.Sku );
		if (type == ProductType.Consumable)
		{
			OpenIAB.consumeProduct ( purchase );
		}
	}

	private void purchaseFailedEvent (int errorCode, string errorMessage)
	{
		DebugLog ( "purchaseFailedEvent: " + errorMessage, true );
		PurchaseFailed ( );
	}

	private void consumePurchaseSucceededEvent (Purchase purchase)
	{
		DebugLog ( "consumePurchaseSucceededEvent: " + purchase );
	}

	private void consumePurchaseFailedEvent (string error)
	{
		DebugLog ( "consumePurchaseFailedEvent: " + error, true );
	}


#elif USE_UNITY_IAP

	private void InitializePurchasing ()
	{

		if (!IsInitialized)
		{
			var builder = ConfigurationBuilder.Instance ( StandardPurchasingModule.Instance ( ) );

			ProductProperties [] properties = GetProductIdentifiers ( );
			DebugLog ( "Products enqued: " + properties.ConvertToString ( ) );

			for (int i = 0; i < properties.Length; i++)
			{
				string sku = properties [i].sku;
				ProductType prod_type = properties [i].type;
				DebugLog ( "Adding product " + sku + " as " + prod_type );
				builder.AddProduct ( sku, prod_type );
			}

			// Expect a response either in OnInitialized or OnInitializeFailed.
			UnityPurchasing.Initialize ( this, builder );
		}

	}

	void BuyProductID (string productId)
	{
		// If Purchasing has been initialized ...
		if (IsInitialized)
		{
			// ... look up the Product reference with the general product identifier and the Purchasing 
			// system's products collection.
			Product product = m_StoreController.products.WithID ( productId );

			// If the look up found a product for this device's store and that product is ready to be sold ... 
			if (product != null && product.availableToPurchase)
			{
				DebugLog ( string.Format ( "Purchasing product asychronously: '{0}'", product.definition.id ) );
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.
				m_StoreController.InitiatePurchase ( product );
			}
			// Otherwise ...
			else
			{
				// ... report the product look-up failure situation  
				DebugLog ( "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase", true );
			}
		}
		// Otherwise ...
		else
		{
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			DebugLog ( "BuyProductID FAIL. Not initialized.", true );
		}
	}


	public void RestoreProducts ()
	{
		// If Purchasing has not yet been set up ...
		if (!IsInitialized)
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			DebugLog ( "RestorePurchases FAIL. Not initialized.", true );
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer ||
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			DebugLog ( "RestorePurchases started ..." );

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ( );
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions ( (result) =>
			{
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				DebugLog ( "RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore." );
			} );
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			DebugLog ( "RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform, true );
		}
	}


	//  
	// --- IStoreListener
	//

	private static IStoreController m_StoreController;          // The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		DebugLog ( "OnInitialized: PASS" );

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;
	}


	public void OnInitializeFailed (InitializationFailureReason error)
	{
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		DebugLog ( "OnInitializeFailed InitializationFailureReason:" + error, true );
	}


	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
	{
		PurchaseSucceded ( args.purchasedProduct.definition.id );

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		DebugLog ( string.Format ( "OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason ), true );
		PurchaseFailed ( );
	}

#endif

}
