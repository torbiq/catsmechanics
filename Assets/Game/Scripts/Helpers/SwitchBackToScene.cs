﻿using UnityEngine;
using System.Collections;
using PSV_Prototype;
public class SwitchBackToScene : MonoBehaviour
{


	public Scenes 
		target_scene;



	void OnEnable()
	{
		KeyListener.OnKeyPressed += ProcessKey;
	}

	void OnDisable()
	{
		KeyListener.OnKeyPressed -= ProcessKey;
	}


	public void ProcessKey(KeyCode key) {
        // --- CUSTOM PROTOTYPE EDIT --- //
		if ((key == KeyCode.Escape))
			{
				Action();
			}
        // --- CUSTOM PROTOTYPE EDIT --- //
    }

    public void Action()
	{
		SceneLoader.Instance.SwitchToScene(target_scene);
	}
}
