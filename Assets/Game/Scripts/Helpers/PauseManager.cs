﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using System;

namespace PSV_Prototype
{

	public class PauseManager :MonoBehaviour
	{
		public static Action<bool> OnPause;

		private static bool
			debug_mode = true,
			game_paused = false,
			paused = false;

		private List<Tween>
			paused_tweens = null;

		private static PauseManager
			instance;

		public static PauseManager
			Instance
		{
			get
			{
				if (instance == null)
				{
					GameObject p_man = new GameObject ( "PauseManager" );

					SetInstance ( p_man.AddComponent<PauseManager> ( ) );
				}
				return instance;
			}
		}


		private static void SetInstance (PauseManager _instance)
		{
			if (instance == null)
			{
				instance = _instance;
				DontDestroyOnLoad ( instance.gameObject );
			}
		}

		private void InterstitialShown ()
		{
			if (!game_paused)
			{
				SetPause ( true, false );
			}
		}

		private void InterstitialClosed ()
		{
			if (!game_paused)
			{
				SetPause ( false, false );
			}
		}

		private static void DebugLog (string message)
		{
			if (debug_mode)
			{
				Debug.Log ( "PauseManager: " + message );
			}
		}

		private void Awake ()
		{
			SetInstance ( this );
			if (Instance.Equals ( this ))
			{
				ManagerGoogle.OnInterstitialShown += InterstitialShown;
				ManagerGoogle.OnRewardedShown += InterstitialShown;
				ManagerGoogle.OnInterstitialClosed += InterstitialClosed;
				ManagerGoogle.OnRewardedClosed += InterstitialClosed;
			}
		}


		private void FixedUpdate ()
		{
			if (paused && Time.timeScale != 0)
			{
				Time.timeScale = 0;
			}
		}



		private void OnApplicationPause (bool pause)
		{
			Debug.Log ( "PauseManager: OnApplicationPause " + pause );

			if (!game_paused)
			{
				SetPause ( pause, false );
			}
		}

		public bool IsPaused ()
		{
			return paused;
		}


		public void TogglePause ()
		{
			SetPause ( !IsPaused ( ) );
		}

		public void SetPause (bool pause, bool save_state = true)
		{
			if (pause || (!pause && paused))
			{
				paused = pause;

				if (save_state)
				{
					game_paused = paused;
				}

				Time.timeScale = paused ? 0 : 1;

				DebugLog ( "setPause " + paused + " timescale == " + Time.timeScale );

				AudioController.Pause ( paused );

				if (paused)
				{
					DOTween.PauseAll ( );
					paused_tweens = DOTween.PausedTweens ( );
				}
				else
				{
					for (int i = 0; paused_tweens != null && i < paused_tweens.Count; i++)
					{
						paused_tweens [i].Play ( );
					}
				}

				if (OnPause != null)
				{
					OnPause ( paused );
				}
			}
		}


	}
}
