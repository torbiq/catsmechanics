﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BufferManager
{
	public enum Vars
	{
		Scores,
		HighScore,
	}

	private static Dictionary<Vars,object> buffer = new Dictionary<Vars, object>();


	public static object GetVar(Vars key)
	{
		if (buffer.ContainsKey(key))
			return buffer [key];
		else
			return null;
	}


	public static void SetVar(Vars key, object value)
	{
		if (buffer.ContainsKey(key))
			buffer [key] = value;
		else
			buffer.Add(key, value);
	}


}
