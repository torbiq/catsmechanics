﻿//#define FIREBASE_PRESENT
using System;
using System.Collections.Generic;
using UnityEngine;
#if FIREBASE_PRESENT
using System.Threading.Tasks;
using Firebase.Analytics;
using Firebase.RemoteConfig;
using Firebase.Database;
using Firebase.Unity.Editor;
#endif

namespace PSV_Prototype
{

	public static class FirebaseManager
	{

		public static event Action OnRemoteConfigUpdated;

#if FIREBASE_PRESENT
		static Firebase.DependencyStatus
			dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
#endif

		private static bool
			debug_mode = false;

		private const string
			db_connect_interval_param = "FIREBASE_DB_CONNECT_INTERVAL";

		private const int
			DEFAULT_CONNECT_LIMIT = 24 * 5;

		// When the app starts, check to make sure that we have
		// the required dependencies to use Firebase, and if not,
		// add them if possible.
		public static void Init ()
		{
#if FIREBASE_PRESENT

			debug_mode = ProjectSettingsManager.settings.firebase_manager_debug; //comment if missing this manager

			try
			{
				dependencyStatus = Firebase.FirebaseApp.CheckDependencies ( );
				if (dependencyStatus != Firebase.DependencyStatus.Available)
				{
					Firebase.FirebaseApp.FixDependenciesAsync ( ).ContinueWith ( task =>
					{
						dependencyStatus = Firebase.FirebaseApp.CheckDependencies ( );
						if (dependencyStatus == Firebase.DependencyStatus.Available)
						{
							InitializeFirebase ( );
						}
						else
						{
							// This should never happen if we're only using Firebase Analytics.
							// It does not rely on any external dependencies.
							Debug.LogError (
										"Could not resolve all Firebase dependencies: " + dependencyStatus );
						}
					} );
				}
				else
				{
					InitializeFirebase ( );
				}
			}
			catch (Exception e) { DebugLog ( e.Message ); }
#endif
		}

#if FIREBASE_PRESENT
		// Initialize remote config, and set the default values.
		public static void InitializeFirebase ()
		{
			try
			{
				DebugLog ( "Enabling data collection." );
				FirebaseAnalytics.SetAnalyticsCollectionEnabled ( true );
				SetFirebaseConfigDefaults ( );
			}
			catch (Exception e) { DebugLog ( e.Message ); }
		}


		private static void SetFirebaseConfigDefaults ()
		{
			Dictionary<string, object> defaults = new Dictionary<string, object> ( );

			// These are the values that are used if we haven't fetched data from the
			// server yet, or if we ask for values that the server doesn't have:
			defaults.Add ( "propertyname_string", "null string" );
			defaults.Add ( "propertyname_int", 0 );
			defaults.Add ( "propertyname_float", 0 );
			defaults.Add ( "propertyname_bool", false );
			FirebaseRemoteConfig.SetDefaults ( defaults );
		}
#endif

		#region RemoteConfig

		public static string GetRemoteParam (string key)
		{
			string val = "";
#if FIREBASE_PRESENT
			try
			{
				val = FirebaseRemoteConfig.GetValue ( key ).StringValue;
			}
			catch (Exception) { }
#endif
			DebugLog ( "Got value for " + key + ": \"" + val + "\"" );
			return val;
		}

		public static bool GetRemoteParamBool (string key, bool default_val)
		{
			string val = FirebaseManager.GetRemoteParam ( key );
			bool res;
			if (!bool.TryParse ( val, out res ))
			{
				res = default_val;
			}
			return res;
		}



		public static void FetchData ()
		{
#if FIREBASE_PRESENT
			try
			{

				DebugLog ( "Fetching data..." );
				Task fetchTask = FirebaseRemoteConfig.FetchAsync ( TimeSpan.Zero );
				fetchTask.ContinueWith ( FetchComplete );
			}
			catch (Exception e) { DebugLog ( e.Message ); }
#endif
		}


#if FIREBASE_PRESENT
		private static void FetchComplete (Task fetchTask)
		{
			if (fetchTask.IsCanceled)
			{
				DebugLog ( "Fetch canceled." );
			}
			else if (fetchTask.IsFaulted)
			{
				DebugLog ( "Fetch encountered an error." );
			}
			else if (fetchTask.IsCompleted)
			{
				DebugLog ( "Fetch completed successfully!" );
			}

			switch (FirebaseRemoteConfig.Info.LastFetchStatus)
			{
				case LastFetchStatus.Success:
					FirebaseRemoteConfig.ActivateFetched ( );
					DebugLog ( "Remote data loaded and ready." );
					if (OnRemoteConfigUpdated != null)
					{
						OnRemoteConfigUpdated ( );
					}
					break;
				case LastFetchStatus.Failure:
					switch (FirebaseRemoteConfig.Info.LastFetchFailureReason)
					{
						case FetchFailureReason.Error:
							DebugLog ( "Fetch failed for unknown reason" );
							break;
						case FetchFailureReason.Throttled:
							DebugLog ( "Fetch throttled until " +
									 FirebaseRemoteConfig.Info.ThrottledEndTime );
							break;
					}
					break;
				case LastFetchStatus.Pending:
					DebugLog ( "Latest Fetch call still pending." );
					break;
			}
		}


		private class SnapshotRequset
		{
			public string
				database_url,
				path;
			public EventHandler<ValueChangedEventArgs>
				value_changed_callback;
			public SnapshotRequset (string _database_url, string _path, EventHandler<ValueChangedEventArgs> _value_changed_callback)
			{
				database_url = _database_url;
				path = _path;
				value_changed_callback = _value_changed_callback;
			}
		}


		private static Dictionary<string, FirebaseDatabase> firebase_instances = new Dictionary<string, FirebaseDatabase> ( );

		public static void GetSnapshotFromDB (string database_url, string path, EventHandler<ValueChangedEventArgs> value_changed_callback)
		{
			FirebaseDatabase fb_db = GetFirebaseDatabaseByURL ( database_url );
			if (fb_db != null)
			{
				DebugLog ( "Attempting to load data from " + database_url + " on FirebaseApp '" + fb_db.App.Name + "' at path " + path );
				fb_db.GetReference ( path ).ValueChanged += value_changed_callback;
			}
		}

		public static void GetSnapshotFromDB (string database_url, string path, Action<DataSnapshot> snapshot_received_callback)
		{
			try
			{
				DebugLog ( "Attempting to load data from " + database_url );
				FirebaseDatabase fb_db = GetFirebaseDatabaseByURL ( database_url );
				if (fb_db != null)
				{
					DebugLog ( "Attempting to load data from " + database_url + " on FirebaseApp '" + fb_db.App.Name + "' at path " + path );
					fb_db.GetReference ( path )
						.GetValueAsync ( ).ContinueWith ( task =>
						{
							if (task.IsFaulted)
							{
							// Handle the error...
							DebugLog ( "Failed to load data from " + database_url + " on FirebaseApp '" + fb_db.App.Name + "' at path " + path );
							}
							else if (task.IsCompleted)
							{
							//DataSnapshot snapshot = task.Result;
							snapshot_received_callback ( task.Result );
							}
						} );
				}
			}
			catch (Exception e) { DebugLog ( e.Message ); }
		}


		private static int GetDBConnectInterval ()
		{
			string interval_val = GetRemoteParam ( db_connect_interval_param ); //will be updated after new data actualization
			int interval;
			if (!int.TryParse ( interval_val, out interval )) //parsing remote param
			{
				interval = DEFAULT_CONNECT_LIMIT;
			}
			return interval;
		}

		private static List<string> allowed_urls = new List<string> ( );

		private static bool IsAllowedToConnect (string database_url)
		{
			bool is_allowed = allowed_urls.Contains ( database_url );
			if (!is_allowed)
			{
				int db_connect_interval = GetDBConnectInterval ( );
				DateTime now = DateTime.UtcNow;
				DateTime last_connect = now.AddHours ( -db_connect_interval );
				string timestamp_val = PlayerPrefs.GetString ( database_url );
				DateTime.TryParse ( timestamp_val, out last_connect );
				double interval_h = (now - last_connect).Hours;
				DebugLog ( "Interval between connections to " + database_url + " = " + interval_h + ", allowed interval = " + db_connect_interval );
				if (interval_h >= db_connect_interval)
				{
					allowed_urls.Add ( database_url );
					int additional_interval = db_connect_interval > 0 ? UnityEngine.Random.Range ( 0, 5 ) : 0;
					PlayerPrefs.SetString ( database_url, now.AddHours ( (double) (db_connect_interval + additional_interval) ).ToString ( ) );
					PlayerPrefs.Save ( );
					is_allowed = true;
				}
			}
			return is_allowed;
		}

		private static FirebaseDatabase GetFirebaseDatabaseByURL (string database_url)
		{
			FirebaseDatabase fb_db = null;
			if (!firebase_instances.TryGetValue ( database_url, out fb_db ) && IsAllowedToConnect ( database_url ))
			{
				string instance_name = "instance_" + firebase_instances.Count;
				DebugLog ( "Attempting to connect to DB " + database_url + " on FirebaseApp '" + instance_name );
				Firebase.FirebaseApp.Create ( Firebase.FirebaseApp.DefaultInstance.Options, instance_name );
				Firebase.FirebaseApp fb_app = Firebase.FirebaseApp.GetInstance ( instance_name );
				fb_app.SetEditorDatabaseUrl ( database_url );
				fb_db = FirebaseDatabase.GetInstance ( fb_app );
				//fb_db.LogLevel = Firebase.LogLevel.Debug;
				firebase_instances.Add ( database_url, fb_db );
			}
			if (fb_db == null)
			{
				DebugLog ( "FirebaseManager: not allowed to connect to " + database_url );
			}
			return fb_db;
		}
#endif       

#endregion

		private static void DebugLog (string s)
		{
			if (debug_mode)
				Debug.Log ( "FirebaseManager: " + s );
		}
	}
}