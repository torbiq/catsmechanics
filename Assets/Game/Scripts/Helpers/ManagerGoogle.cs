﻿using System;
using UnityEngine;
using PSV_Prototype;


public class ManagerGoogle :MonoBehaviour
{
	public static Action<bool>
		OnAdsEnabled;
	public static Action
		OnInterstitialShown,
		OnInterstitialClosed,
		OnRewardedShown,
		OnRewardedClosed,
		OnRewardedComplete;


	static public ManagerGoogle Instance;

	public GameObject purchase_btn;

	private float
		last_interstitial_time = 0, //0
		interstitial_time = 30,  //30
		last_rewarded_time = 0,
		rewarded_time = 0;


	private bool
		debug_mode = false,
		children_tagged = true,
		for_families = false,
		single_ad_mode = true,
		banner_use_home_ads = false,
		native_use_home_ads = false,
		external_link_present = false,
		banner_visible = false,
		native_visible = false,
		interstitial_shown = false,
		rewarded_shown = false,
		ads_enabled = true;

	private AdsInterop.AdPosition
		banner_pos = AdsInterop.AdPosition.Bottom_Centered,
		native_pos = AdsInterop.AdPosition.Bottom_Centered;

	private AdsInterop.AdSize
		banner_size = AdsInterop.AdSize.Standard_banner_320x50,
		native_size = AdsInterop.AdSize.Native_banner_minimal_320x80;

	const string
		children_tagged_param = "CHILDREN_TAGGED",
		for_families_param = "FOR_FAMILIES",
		interstitial_interval_param = "INTERSTITIAL_DELAY",
		first_interstitial_param = "FIRST_INTERSTITIAL_DELAY",
		purchased_data_pref = "AdmobDisabled";

	private bool
		_initialised = false;


	[ContextMenu( "ResetInterstitilaTime" )]
	void ResetInterstitilaTime ()
	{
		interstitial_time = 0;
	}

	[ContextMenu ( "ShowFullscreenBanner" )]
	void ShowInterstitial ()
	{
		ShowFullscreenBanner ( );
	}


	void Awake ()
	{
		if (!Instance)
		{
			Instance = this;
			DontDestroyOnLoad ( this );
		}
	}

	void OnEnable ()
	{
		Subscribe ( );
	}


	void OnDisable ()
	{
		Unsubscribe ( );
	}

	void Start ()
	{
		UpdateInterstitialTime ( );
		Init ( );
	}

	void LogMessage (string message)
	{
		if (debug_mode)
		{
			Debug.Log ( this.GetType ( ).ToString ( ) + " " + message );
		}
	}


	#region EventHandlers	

	private void Subscribe ()
	{
		AdsInterop.OnInterstitialClosed += InterstitialClosed;
		AdsInterop.OnInterstitialShown += InterstitialShown;
		AdsInterop.OnRewardedShown += RewardedShown;
		AdsInterop.OnRewardedClosed += RewardedClosed;
		AdsInterop.OnRewardedCompleted += RewardedComplete;
		FirebaseManager.OnRemoteConfigUpdated += UpdateInterstitialTime;
		//PromoModule.OnBannerTimeUpdate += UpdateInterstitialTime; //deprecated
		ExternalLinksManager.OnExternalLinksVisible += ExternalLinkPresent;
	}


	private void Unsubscribe ()
	{
		AdsInterop.OnInterstitialClosed -= InterstitialClosed;
		AdsInterop.OnInterstitialShown -= InterstitialShown;
		AdsInterop.OnRewardedShown -= RewardedShown;
		AdsInterop.OnRewardedClosed -= RewardedClosed;
		AdsInterop.OnRewardedCompleted -= RewardedComplete;
		FirebaseManager.OnRemoteConfigUpdated -= UpdateInterstitialTime;
		//PromoModule.OnBannerTimeUpdate -= UpdateInterstitialTime; //deprecated
		ExternalLinksManager.OnExternalLinksVisible -= ExternalLinkPresent;
	}

	private void ExternalLinkPresent (bool param)
	{
		external_link_present = param;
		if (param)
		{
			//hide ads
			HideNativeBanner ( false );
			HideSmallBanner ( false );
		}
		else
		{
			//show ads if were any
			if (native_visible)
			{
				ShowNativeBanner ( native_use_home_ads );
			}
			else if (banner_visible)
			{
				ShowSmallBanner ( banner_use_home_ads );
			}
		}
	}

	private void InterstitialClosed ()
	{
		LogMessage ( "InterstitialClosed" );
		if (interstitial_shown)
		{
			ResetLastInterstitialTime ( );
			interstitial_shown = false;
		}
		if (OnInterstitialClosed != null)
		{
			OnInterstitialClosed ( );
		}
	}

	private void InterstitialShown ()
	{
		interstitial_shown = true;
		if (OnInterstitialShown != null)
		{
			OnInterstitialShown ( );
		}
	}

	private void RewardedComplete ()
	{
		LogMessage ( "RewardedVideoComplete" );
		if (OnRewardedComplete != null)
		{
			OnRewardedComplete ( );
		}
	}

	private void RewardedClosed ()
	{
		LogMessage ( "RewardedVideoClosed" );
		if (rewarded_shown)
		{
			ResetLastRewardedTime ( );
			rewarded_shown = false;
		}
		if (OnRewardedClosed != null)
		{
			OnRewardedClosed ( );
		}
	}

	private void RewardedShown ()
	{
		rewarded_shown = true;
		if (OnRewardedShown != null)
		{
			OnRewardedShown ( );
		}
	}
	#endregion

	#region General Methods

	bool GetTaggedForChildren ()
	{
		return FirebaseManager.GetRemoteParamBool ( children_tagged_param , children_tagged);
	}

	bool GetForFamilies ()
	{
		return FirebaseManager.GetRemoteParamBool ( for_families_param, for_families );
	}

	private void SetAdsEnabled (bool param, bool save = true, bool announce = true)
	{
		ads_enabled = param;
		if (save)
		{
			PlayerPrefs.SetInt ( purchased_data_pref, ads_enabled ? 0 : 1 );
			PlayerPrefs.Save ( );
		}
		if (_initialised && announce)
		{
			if (ads_enabled)
			{
				AdsInterop.EnableAds ( );
			}
			else
			{
				AdsInterop.DisableAds ( );
			}
			if (OnAdsEnabled != null)
			{
				OnAdsEnabled ( ads_enabled );
			}
		}
	}

	public bool GetBannerVisible ()
	{
		return banner_visible && !external_link_present;
	}


	public Vector2 GetBannerSizeInPX ()
	{

		Vector2 b_size = banner_size != null ? new Vector2 ( banner_size.w, banner_size.h ) : Vector2.zero;
		float coef = (Screen.dpi / 160f);
		return b_size * coef * (GetBannerVisible ( ) && IsAdmobEnabled ( ) ? 1 : 0);
	}

	public AdsInterop.AdSize GetAdSize ()
	{
		return banner_size;
	}

	public AdsInterop.AdPosition GetAdPos ()
	{
		return banner_pos;

	}

	void UpdateInterstitialTime ()
	{
		float _interstitial_time;
		if (float.TryParse ( FirebaseManager.GetRemoteParam ( interstitial_interval_param ), out _interstitial_time ))
		{
			interstitial_time = _interstitial_time;
			LogMessage ( "interstitial_interval updated to " + interstitial_time );
		}
		float _last_interstitial_time;

		if (float.TryParse ( FirebaseManager.GetRemoteParam ( first_interstitial_param ), out _last_interstitial_time ))
		{
			last_interstitial_time = _last_interstitial_time;
			LogMessage ( "last_interstitial_time updated to " + last_interstitial_time );
		}
	}

	void UpdateRewardedTime (float val)
	{
		rewarded_time = val;
	}

	public void ResetLastInterstitialTime ()
	{
		last_interstitial_time = Time.time;
	}

	public void ResetLastRewardedTime ()
	{
		last_rewarded_time = Time.time;
	}


	[ContextMenu ( "DisableAdmob" )]
	public void DisableAdmob ()
	{
		SetAdsEnabled ( false );
	}

	[ContextMenu ( "EnableAdmob" )]
	public void EnableAdmob ()
	{
		SetAdsEnabled ( true );
	}

	public bool IsAdmobEnabled ()
	{
		return ads_enabled;
	}

	private void Init ()
	{
		debug_mode = ProjectSettingsManager.settings.manager_google_debug; //comment if missing this manager

		LogMessage ( "Initialising" );

#if UNITY_ANDROID || UNITY_IPHONE
		//if using AdsInterop there can be no admob manager as it is about ad providers
		//so we will perform initialisation of purchased data here


		if (PlayerPrefs.HasKey ( purchased_data_pref ))
		{
			SetAdsEnabled ( PlayerPrefs.GetInt ( purchased_data_pref ) == 0, false );
		}
		else
		{
			SetAdsEnabled ( true );
		}

        BillingManager.Instance.Init();
#endif

        AdsInterop.InitialiseAds ( IsAdmobEnabled ( ), children_tagged, for_families ); //sending ads_enabled to leave active rewarded video

		if (purchase_btn != null)
		{
			purchase_btn.SetActive ( IsAdmobEnabled ( ) );
		}
		_initialised = true;
	}

	private bool GetSrcReady (AdEventsListener.EventSource src)
	{
		if (AdEventsListener.Instance != null)
		{
			return AdEventsListener.Instance.GetSrcReady ( src );
		}
		else
		{
			Debug.Log ( "AdEventsListener not present" );
			return false;
		}
	}
	#endregion

	#region Banner

	public bool IsBannerReady ()
	{
		return GetSrcReady ( AdEventsListener.EventSource.Banner );
	}

	public void HideSmallBanner (bool save_state = true)
	{
		LogMessage ( "Hide banner" );
		if (save_state)
		{
			banner_visible = false;
		}
		AdsInterop.HideBannerAd ( );
	}

	public void ShowSmallBanner (bool show_home_ads = false)
	{

		banner_use_home_ads = show_home_ads;
		if (single_ad_mode && native_visible)
		{
			LogMessage ( "Single Ad Mode: Hiding NativeBanner" );
			HideNativeBanner ( );
		}
		if (IsAdmobEnabled ( ))
		{
			banner_visible = true;
			if (!external_link_present)
			{
				LogMessage ( "Show banner, external_link_present = " + external_link_present );
				AdsInterop.ShowBannerAd ( show_home_ads );
			}
		}
	}

	public void RefreshSmallBanner (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
	{
		if (banner_pos != ad_pos || banner_size != ad_size)
		{
			banner_pos = ad_pos;
			banner_size = ad_size;

			//check if banner in needed position to avoid its unnecessary reload
			if (IsAdmobEnabled ( ))
			{
				AdsInterop.RefreshBannerAd ( banner_pos, banner_size );
			}
		}
	}

	#endregion

	#region Native
	public bool IsNativeReady ()
	{
		return GetSrcReady ( AdEventsListener.EventSource.Native );
	}

	public void HideNativeBanner (bool save_state = true)
	{
		LogMessage ( "Hide native" );
		if (save_state)
			native_visible = false;
		AdsInterop.HideNativeAd ( );
	}


	public void ShowNativeBanner (bool show_home_ads = false)
	{
		native_use_home_ads = show_home_ads;
		if (single_ad_mode && banner_visible)
		{
			LogMessage ( "Single Ad Mode: Hiding SmallBanner" );
			HideSmallBanner ( );
		}
		if (IsAdmobEnabled ( ))
		{
			native_visible = true;
			if (!external_link_present)
			{
				LogMessage ( "Show native" );
				AdsInterop.ShowNativeAd ( show_home_ads );

			}
		}
	}

	public void RefreshNativeBanner (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
	{
		if (native_pos != ad_pos || native_size != ad_size)
		{
			native_pos = ad_pos;
			native_size = ad_size;
			//check if banner in needed position to avoid its unnecessary reload
			if (IsAdmobEnabled ( ))
			{
				AdsInterop.RefreshNativeAd ( native_pos, native_size );
			}
		}
	}

	#endregion

	#region Interstitial

	public bool IsInterstitialReady ()
	{
		return GetSrcReady ( AdEventsListener.EventSource.Interstitial );
	}


	public void ShowFullscreenBanner (bool show_home_ads = false, bool force_show = false)
	{
		if (IsAdmobEnabled ( ))
		{
			float time = Time.time;
			if (time - last_interstitial_time >= interstitial_time || force_show)
			{
				LogMessage ( "Show interstitial show_home_ads=" + show_home_ads );
				AdsInterop.ShowInterstitialAd ( show_home_ads );
				return;
			}
			LogMessage ( "It not time to show interstitial, now=" + time + " last=" + last_interstitial_time );
		}
		InterstitialClosed ( );
	}

	#endregion

	#region Rewarded
	public bool IsRewardedReady ()
	{
		return GetSrcReady ( AdEventsListener.EventSource.Rewarded );
	}

	public void ShowRewardedVideoAd (bool show_home_ads = false)
	{
		float time = Time.time;
		if (time - last_rewarded_time >= rewarded_time)
		{
			AdsInterop.ShowRewarded ( show_home_ads );
		}
	}
	#endregion

}
