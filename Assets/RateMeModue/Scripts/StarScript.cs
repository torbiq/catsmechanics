﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace RateMePlugin
{
    [RequireComponent ( typeof ( Image ) )]
    public class StarScript :MonoBehaviour, IPointerDownHandler
    {

        public bool positive = false;

        private Image img;

        public delegate void Callback (bool positive, float x);
        public static event Callback OnStarClicked;

        public void Awake ()
        {
            img = GetComponent<Image> ( );
        }

        public void OnDisable ()
        {
            OnStarClicked -= CheckIfActive;
        }

        public void OnEnable ()
        {
            SetActive ( false );
            OnStarClicked += CheckIfActive;
        }


        void CheckIfActive (bool positive, float x)
        {
            SetActive ( x >= this.transform.position.x );
        }


        void SetActive (bool active)
        {
            Color col = img.color;
            col.a = active ? 1 : 100 / 255f;
            img.color = col;
        }


        public void OnPointerDown (PointerEventData eventData)
        {
            if (OnStarClicked != null)
            {
                OnStarClicked ( positive, this.transform.position.x );
            }
        }

    }
}