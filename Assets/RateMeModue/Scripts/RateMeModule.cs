﻿#define BRAND_NEW_PROTOTYPE

using UnityEngine;
//using System.Collections;
using System.Collections.Generic;
using PSV_Prototype;


/// <summary>
/// ChangeLog
/// v2
///     - fixed bug when variables were not saved on change
///     - fixed CanShow() - replaced || operator with &&
/// v3
///     - optimised canvas settings
///     - use_time_scale set to false by default
///     - PanelScript now uses Time.unscaledDeltaTime for fade animation
/// v4
///		- implemented scene mode as option
///		- use_time_scale deprecated - practice shows it was usless feature
///		- Panel script now implements IExternalLnk
///	v5
///		- Changed Bhavior of close button (now it also goes to market if rate positive)
///	v6
///		- 4-5 stars click leads directly to PlayMarket without submitting
/// </summary>


namespace RateMePlugin
{
	[RequireComponent ( typeof ( AudioSource ) )]
	public class RateMeModule :MonoBehaviour
	{
		public static RateMeModule Instance;

		private string
			closed_var = "RateMeModule:closed",
			negative_var = "RateMeModule:negative",
			positive_var = "RateMeModule:positive";

		private int
			negative_limit = 2,
			closed_limit = 5,
			closed_val,
			negative_val,
			positive_val;

#if BRAND_NEW_PROTOTYPE
		public const Scenes
			rate_scene = Scenes.RateMeScene;
		private Scenes
			target_scene;
#endif

		private const float
			time_interval = 60; //60
		private float
			last_time = -time_interval; //to show dialogue after first call - set to zero to avoid early activation

		private bool
			positive = false,
			is_shown = false;

#if BRAND_NEW_PROTOTYPE
		public bool
			scene_mode = false;
#else
		private bool
			scene_mode = false;
#endif

		private string
			txt_en = "If you like to play with me, please rate me 5 Stars!",
			txt_fr = "Si vous aimez jouer avec moi, se il vous plaît noter m'a 5 étoiles!",
			txt_pt = "Se você gosta de jogar comigo, por favor classifique-me com 5 estrelas!",
			txt_ru = "Если тебе понравилось со мной играть, пожалуйста поставь мне 5 звездочек!",
			txt_sp = "Si te gusta jugar conmigo, por favor puntúame con 5 Estrellas!";

		public AudioClip
			snd_en,
			snd_fr,
			snd_pt,
			snd_ru,
			snd_sp;

		private AudioSource src;

		public struct LocalisedData
		{
			public AudioClip audio;
			public string txt;
			public LocalisedData (AudioClip a, string t)
			{
				audio = a;
				txt = t;
			}
		}

		private Dictionary<Languages.Language, LocalisedData> localised_data;


		public GameObject
			rate_me_panel;


		void InitialiseLocalisedData ()
		{
			localised_data = new Dictionary<Languages.Language, LocalisedData>
			{
				{Languages.Language.English, new LocalisedData(snd_en,txt_en) },
				{Languages.Language.French, new LocalisedData(snd_fr,txt_fr) },
				{Languages.Language.Portuguese, new LocalisedData(snd_pt,txt_pt) },
				{Languages.Language.Russian, new LocalisedData(snd_ru,txt_ru) },
				{Languages.Language.Spanish, new LocalisedData(snd_sp,txt_sp) },
			};
		}

		public void Awake ()
		{
			if (Instance == null)
			{
				Instance = this;
				InitialiseLocalisedData ( );
				src = GetComponent<AudioSource> ( );
				if (src == null)
				{
					src = this.gameObject.AddComponent<AudioSource> ( );
				}
				DontDestroyOnLoad ( this.gameObject );
				GetVariables ( );
			}
			else
			{
				Debug.LogWarning ( "RateMeModule: you have multiple instances of module in your scene" );
				Destroy ( this.gameObject );
			}
		}


		Languages.Language GetLang ()
		{
#if BRAND_NEW_PROTOTYPE
			Languages.Language lang = Languages.GetLanguage ( );
#else
			Languages.Language lang = Languages.Instance.GetLanguage ( );
#endif
			//set overrides
			switch (lang)
			{
				case Languages.Language.Ukrainian:
					//case Languages.Language.Polish:
					lang = Languages.Language.Russian;
					break;
			}
			if (!localised_data.ContainsKey ( lang ))
			{
				lang = Languages.Language.English;
			}
			return lang;
		}



		public string GetLocalisedText ()
		{
			return localised_data [GetLang ( )].txt;
		}

		public AudioClip GetLocalisedAudio ()
		{
			return localised_data [GetLang ( )].audio;
		}


		void GetVariables ()
		{
			closed_val = GetVal ( closed_var );
			negative_val = GetVal ( negative_var );
			positive_val = GetVal ( positive_var );
		}

		int GetVal (string var)
		{
			return PlayerPrefs.GetInt ( var, 0 );
		}

		void SetVal (string var, int val)
		{
			PlayerPrefs.SetInt ( var, val );
			PlayerPrefs.Save ( );
		}


		public void OnDisable ()
		{
			RateButtonScript.OnRateClicked -= OnRateClicked;
			CloseButton.OnClose -= OnClosed;
			StarScript.OnStarClicked -= OnStarClicked;
		}

		public void OnEnable ()
		{
			RateButtonScript.OnRateClicked += OnRateClicked;
			CloseButton.OnClose += OnClosed;
			StarScript.OnStarClicked += OnStarClicked;
		}

		void OnStarClicked (bool _positive, float x)
		{
			Debug.Log ( "OnStarClicked " + _positive );
			positive = _positive;
			if (positive)
			{
				OnRateClicked ( );
			}
		}

		void OnClosed ()
		{
			if (positive)
			{
				OnRateClicked ( );
			}
			else
			{
				closed_val++;
				SetVal ( closed_var, closed_val );
				ShowDialogue ( false );
			}
		}


		void OnRateClicked ()
		{
			if (positive)
			{
				positive_val++;
				SetVal ( positive_var, positive_val );
				RateApplication ( );
			}
			else
			{
				negative_val++;
				SetVal ( negative_var, negative_val );
			}
			ShowDialogue ( false );
		}

#if BRAND_NEW_PROTOTYPE
		private void SaveTargetScene (Scenes scene)
		{
			target_scene = scene;
		}
#endif

		[ContextMenu("ForseShowDialogue")]
		void ForseShow ()
		{
			ShowDialogue ( true, true );
		}


		public bool ShowDialogue (bool show = true, bool forse = false)
		{
#if !UNITY_ANDROID
			return false;
#endif

			bool res = false;

			if (forse || !show || CanShow ( Time.time ))
			{
				if (show)
				{
					positive = false;
				}

				if (IsVisible ( ))
				{
					last_time = Time.time;
				}

				is_shown = show;
#if BRAND_NEW_PROTOTYPE
				if (scene_mode)
				{
					if (show)
					{
						Scenes _target_scene = SceneLoader.Instance.GetTargetScene ( );
						if (_target_scene != rate_scene)
						{
							//if rate_me can be shown we tell scene loader to change destignation and return false
							SaveTargetScene ( _target_scene );
							SceneLoader.Instance.SetTargetScene ( rate_scene );
							res = true;
						}
						else
						{
							Debug.LogError ( "RateMeModule: Do not call transition to RateMeScene directly! Use RateMeModule.Instance.ShowDialogue() before normal transition to the scene you need. RateMeModule will override transition order itself" );
						}
					}
					else
					{
						if (SceneLoader.Instance.GetCurrentScene ( ) == rate_scene)
						{
							SceneLoader.Instance.SwitchToScene ( target_scene );
							res = true;
						}
					}
				}
#endif
				if (!scene_mode)
				{
					ActivatePanel ( show );
					res = true;
				}

				if (!show)
				{
					PlaySound ( false );
				}
			}
			return res;
		}



		public void PlaySound (bool play)
		{
			if (play)
			{
				bool mute =
#if BRAND_NEW_PROTOTYPE
					!GameSettings.IsSoundsEnabled ( );
#else
					!AudioController.IsSoundsEnabled ( );
#endif
				src.clip = GetLocalisedAudio ( );
				src.mute = mute;
				src.Play ( );
			}
			else
			{
				src.Stop ( );
			}
		}


		float audio_pos = 0;
		public void PuauseAudio (bool pause)
		{
			//if (src != null && src.clip != null)
			//{
			//	if (pause)
			//	{
			//		audio_pos = src.time;
			//		src.Pause ( );
			//	}
			//	else
			//	{
			//		src.time = audio_pos;
			//		src.Play ( );
			//	}
			//}
		}


		private void ActivatePanel (bool show)
		{
			if (rate_me_panel != null)
			{
				//PlaySound ( show );
				rate_me_panel.SetActive ( show );
			}
			else
			{
				Debug.LogError ( "RateMeModule: rate_me_panel not set" );
			}
		}

		bool CanShow (float t)
		{
			return IsOnline ( ) && t - last_time >= time_interval && positive_val < 1 && negative_val < negative_limit && closed_val < closed_limit;
		}

		bool IsOnline ()
		{
			return Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork;
		}

		public bool IsVisible ()
		{
			return is_shown;
		}




		public string
			ios_bundle_id = "",
			wp_bundle_id = "";
		private string
			android_makret_app_url = "market://details?id=",
			ios_market_app_url = "itms-apps://itunes.apple.com/app/",
			wp_market_app_url = "ms-windows-store:navigate?appid=";



		public void RateApplication ()
		{
			string app_alias = GetAppAlias ( );
			if (!string.IsNullOrEmpty ( app_alias ))
			{
				string url = GetAppMarketURL ( app_alias );
				Debug.Log ( "RateMeDialogue: open URL=" + url );
				Application.OpenURL ( url );
			}
			else
			{
				Debug.LogError ( "RateMeDialogue: package_name is empty" );
			}
		}



		string GetAppAlias ()
		{
			string res = "";
#if UNITY_ANDROID
			res = Application.bundleIdentifier;
#elif UNITY_IPHONE
            res = ios_bundle_id;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
            res = wp_bundle_id;
#endif
			return res;
		}

		string GetAppMarketURL (string app_alias)
		{
			string res = "";
#if UNITY_ANDROID
			res = android_makret_app_url + app_alias;
#elif UNITY_IPHONE
            res = ios_market_app_url + app_alias;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
            res = wp_market_app_url + app_alias;
#endif
			return res;
		}
	}
}