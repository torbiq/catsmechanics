﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace RateMePlugin
{
    public class CloseButton :MonoBehaviour,IPointerClickHandler
    {
        public delegate void Callback ();
        public static event Callback OnClose;


        public void OnPointerClick (PointerEventData eventData)
        {
            if (OnClose != null)
            {
                OnClose();
            }
        }

    }
}
