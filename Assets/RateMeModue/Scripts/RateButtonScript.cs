﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using PromoPlugin;


namespace RateMePlugin
{
    [RequireComponent ( typeof ( Button ) )]
    public class RateButtonScript :MonoBehaviour, IPointerClickHandler
    {
        public static event System.Action OnRateClicked;

        

        private Button
            btn;

		


        public void Awake ()
        {
            btn = GetComponent<Button> ( );
        }

        public void OnEnable ()
        {
            btn.interactable = false;
            StarScript.OnStarClicked += OnStarClicked;
        }

        public void OnDisable ()
        {
            StarScript.OnStarClicked -= OnStarClicked;
        }

        void OnStarClicked (bool _positive, float x)
        {
            btn.interactable = true;
        }




        public void OnPointerClick (PointerEventData eventData)
        {
			if (btn.interactable)
			{
				if (OnRateClicked != null)
				{
					OnRateClicked ( );
				}
			}
		}

    }
}
