﻿namespace PSV_Prototype
{
	public interface IExternalLinkView
	{
		bool IsStatic
		{
			get;
		}

		void Show (bool param); //be carefull not to call here methods that implement IExternalLinkBuilder - it can loop the app
	}
}