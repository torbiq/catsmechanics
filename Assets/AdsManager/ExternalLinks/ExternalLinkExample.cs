﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PSV_Prototype
{

	public class ExternalLinkExample :MonoBehaviour, IExternalLink
	{
		private bool isStatic;

		public bool IsStatic
		{
			get
			{
				return isStatic;
			}
		}

		public void CreateLink ()
		{
			ExternalLinksManager.Instance.AddLink ( this );
		}

		public void DestroyLink ()
		{
			ExternalLinksManager.Instance.DeleteLink ( this );
		}

		public void Show (bool param)
		{
			//show or hide here the link 
			//be aware of making it the same as CreateLink or DestroyLink if it is not the way you want your class to act in that way
		}
	}
}