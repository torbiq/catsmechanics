﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace PSV_Prototype
{


	public class ExternalLinksManager :MonoBehaviour
	{
		#region Variables

		private List<IExternalLinkView> links_list = new List<IExternalLinkView> ( );

		private bool
			interstitial_visible = false,
			rewarded_visible = false;
		#endregion

		#region Events

		public static Action OnExternalLinks;
		public static Action<bool> OnExternalLinksVisible;
		#endregion

		#region Singelton

		private static ExternalLinksManager instance;

		public static ExternalLinksManager Instance
		{
			get
			{
				if (instance == null)
				{
					GameObject e_man = new GameObject ( "ExternalLinksManager" );

					SetInstance ( e_man.AddComponent<ExternalLinksManager> ( ) );
				}
				return instance;
			}
		}


		static void SetInstance (ExternalLinksManager _instance)
		{
			instance = _instance;
			DontDestroyOnLoad ( instance.gameObject );
		}

		void Awake ()
		{
			if (instance == null)
			{
				SetInstance ( this );
			}
			else
			{
				Debug.LogError ( "ExternalManager: not allowed multiple instances" );
				Destroy ( gameObject );
			}
		}

		void OnEnable ()
		{
			ManagerGoogle.OnInterstitialShown += InterstitialShown;
			ManagerGoogle.OnInterstitialClosed += InterstitialClosed;
			ManagerGoogle.OnRewardedShown += RewardedShown;
			ManagerGoogle.OnRewardedClosed += RewardedClosed;
		}


		void OnDisable ()
		{
			ManagerGoogle.OnInterstitialShown -= InterstitialShown;
			ManagerGoogle.OnInterstitialClosed -= InterstitialClosed;
			ManagerGoogle.OnRewardedShown -= RewardedShown;
			ManagerGoogle.OnRewardedClosed -= RewardedClosed;
		}

		#endregion

		private void InterstitialShown ()
		{
			//Debug.Log ( "INTERSTITIAL SHOWN" );
			interstitial_visible = true;
			UpdateLinks ( );
		}


		private void InterstitialClosed ()
		{
			//Debug.Log ( "INTERSTITIAL CLOSED" );
			interstitial_visible = false;
			UpdateLinks ( );
		}

		private void RewardedShown ()
		{
			rewarded_visible = true;
			UpdateLinks ( );
		}


		private void RewardedClosed ()
		{
			rewarded_visible = false;
			UpdateLinks ( );
		}


		public void AddLink (IExternalLinkView externalLink)
		{
			if (!links_list.Contains ( externalLink ))
			{
				links_list.Add ( externalLink );
				UpdateLinks ( );
			}
		}

		public void DeleteLink (IExternalLinkView externalLink)
		{
			if (links_list.Contains ( externalLink ))
			{
				links_list.Remove ( externalLink );
				UpdateLinks ( );
			}
		}

		private void UpdateLinks ()
		{
			int selected_link = -1;

			if (links_list.Count > 0)
			{
				if (!IsFullscreenAdVisible ( ))
				{
					for (int i = links_list.Count - 1; i >= 0; i--)
					{
						if (!links_list [i].IsStatic)
						{
							selected_link = i;
							break;
						}
					}
					if (selected_link < 0)
					{
						selected_link = links_list.Count - 1;
					}
				}
				ShowLink ( selected_link );
			}
			ExternalLinkVisible ( selected_link >= 0 || IsFullscreenAdVisible ( ) );
		}


		private bool IsFullscreenAdVisible ()
		{
			return interstitial_visible || rewarded_visible;
		}

		private void ShowLink (int link)
		{
			for (int i = 0; i < links_list.Count; i++)
			{
				links_list [i].Show ( link == i );
			}
		}


		#region Service

		private void ExternalLinkVisible (bool param)
		{
			if (OnExternalLinksVisible != null)
			{
				OnExternalLinksVisible ( param );
			}
		}

		#endregion
	}

}