﻿using UnityEngine;
using System.Collections;

namespace PSV_Prototype
{
	public class DelayedCallHandler :MonoBehaviour
	{
		public static DelayedCallHandler
			Instance;

		private static bool
			debug_mode = false;

		public static void LogMessage (string message, bool error = false)
		{

			if (error)
			{
				Debug.LogError ( "@ DelayedCallHandler @ " + message );
			}
			else if (debug_mode)
			{
				Debug.Log ( "@ DelayedCallHandler @ " + message );
			}

		}

		void Awake ()
		{
			if (Instance == null)
			{
				Instance = this;
				DontDestroyOnLoad ( this.gameObject );
			}
			else
			{
				LogMessage ( "Not allowed multiple instances of DelayedCallHandler", true );
				Destroy ( this.gameObject );
			}
		}


		public void DelayedCall (System.Action callback, float t)
		{
			LogMessage ( "Starting delayed call in " + t + " seconds");
			StartCoroutine ( WaitAndExecute ( callback, t ) );
		}


		IEnumerator WaitAndExecute (System.Action callback, float t)
		{
			t += Time.unscaledTime;
			while (Time.unscaledTime < t)
			{
				yield return null;
			}
			LogMessage ( "Delayed call started" );
			callback ( );
		}

	}
}
