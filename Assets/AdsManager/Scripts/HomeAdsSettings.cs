﻿//#define FIREBASE_PRESENT
using UnityEngine;
using System;
using System.Collections.Generic;
#if FIREBASE_PRESENT
using Firebase.Database;
#endif
namespace PSV_Prototype
{
	public static class HomeAdsSettings
	{
		public static Action<ProviderParams, string []> OnHomeAdsIDsUpdated;

		private const string
			home_ads_db_param = "HOME_ADS_DB",
			home_ids_root_param = "HOME_ADS";

		public enum IntervalParams
		{
			HOME_INTERSTITIAL_INTERVAL,
			HOME_BANNER_INTERVAL,
			HOME_NATIVE_INTERVAL,
			HOME_REWARDED_INTERVAL,
		};


		private const int
			default_ad_interval = 7;

		private static Dictionary<IntervalParams, int> ad_interval = new Dictionary<IntervalParams, int> ( ) {
			{ IntervalParams.HOME_INTERSTITIAL_INTERVAL, default_ad_interval },
			{ IntervalParams.HOME_BANNER_INTERVAL,       0 },
			{ IntervalParams.HOME_NATIVE_INTERVAL,       default_ad_interval },
			{ IntervalParams.HOME_REWARDED_INTERVAL,     0 },
		};



		public static ProviderParams [] home_ads_params = new ProviderParams []
		{
			ProviderParams.HOMEADS_BANNER_ID,
			ProviderParams.HOMEADS_NATIVE_ID,
			ProviderParams.HOMEADS_INTERSTITIAL_ID,
			ProviderParams.HOMEADS_REWARDED_ID,
		};

		private static string
			default_home_ads_db = "https://hippohomeads.firebaseio.com/";


		static Dictionary<string, string>
			home_ads_banners = new Dictionary<string, string> ( )
			{
			},
			home_ads_interstitials = new Dictionary<string, string> ( )
			{
			},
			home_ads_natives = new Dictionary<string, string> ( )
			{
			},
			home_ads_rewarded = new Dictionary<string, string> ( )
			{
			};


		public static void Init ()
		{
			LoadSavedParams ( );
#if FIREBASE_PRESENT
			FetchNewIds ( );
#endif
		}

		//exclude ids related to an app present on the device
		private static string [] FilterIds (Dictionary<string, string> ids)
		{
			List<string> res = new List<string> ( );
			foreach (var item in ids)
			{
				if (!string.IsNullOrEmpty ( item.Key ) && !string.IsNullOrEmpty ( item.Value ) && PromoPlugin.ServiceUtils.IsAppAllowed ( item.Key ))
				{
					res.Add ( item.Value );
				}
			}
			return res.ToArray ( );
		}


		public static void LoadSavedParams ()
		{
			LoadAdsParams ( );
		}

		private static void LogMessage (string message, bool error = false)
		{
			AdsManager.LogMessage ( "HomeAdsSettings: " + message, error );
		}

		#region Getters

		public static int GetAdInterval (IntervalParams param)
		{
			//getting remote param
			string val = FirebaseManager.GetRemoteParam ( param.ToString ( ) );
			int int_val;
			//trying to get param value
			if (!int.TryParse ( val, out int_val ))
			{
				//trying to get preset param value
				if (!ad_interval.TryGetValue ( param, out int_val ))
				{
					//using default interval
					int_val = default_ad_interval;
				}
			}
			return int_val;
		}

		public static string [] GetIDs (ProviderParams param)
		{
			string [] res = null;
			Dictionary<string, string> params_dic = GetParamDic ( param );

			//LogMessage ( "Got dictionary for " + param + ":\n" + params_dic.ConvertToString ( ) );

			if (params_dic != null)
			{
				res = FilterIds ( params_dic );
			}
			return res;
		}


		private static string GetLocalParam (string key)
		{
			//LogMessage ( "Getting local param " + key );
			return PlayerPrefs.GetString ( key, "" );
		}

		private static Dictionary<string, string> GetIdsFromString (string json)
		{
			Dictionary<string, string> res = new Dictionary<string, string> ( );
			SimpleJSON.JSONNode node = SimpleJSON.JSON.Parse ( json );

			string s = "GetIdsFromString:\n";
			foreach (var child in node.Childs)
			{
				string bundle = child ["bundle"];
				string value = child ["value"];
				if (!string.IsNullOrEmpty ( bundle ) && !string.IsNullOrEmpty ( value ))
				{
					s += value + " \t" + bundle + " \n";

					res.Add ( bundle, value );
				}
			}
			LogMessage ( s );
			return res;
		}


		private static Dictionary<string, string> GetParamDic (ProviderParams param)
		{
			Dictionary<string, string> res = null;

			switch (param)
			{
				case ProviderParams.HOMEADS_BANNER_ID:
					res = home_ads_banners;
					break;
				case ProviderParams.HOMEADS_INTERSTITIAL_ID:
					res = home_ads_interstitials;
					break;
				case ProviderParams.HOMEADS_NATIVE_ID:
					res = home_ads_natives;
					break;
				case ProviderParams.HOMEADS_REWARDED_ID:
					res = home_ads_rewarded;
					break;
				default:
					LogMessage ( "GetParamDic exception: No dictionary for " + param, true );
					break;
			}
			return res;
		}

		#endregion

		#region Setters

		private static bool SetIds (ProviderParams param, string json, ref Dictionary<string, string> dic)
		{
			//will change given dictionary only if parsed json will contain data and it will be the same
			if (json.Length > 0)
			{
				Dictionary<string, string> ids = GetIdsFromString ( json );
				LogMessage ( "Got IDs for " + param + " " + ids.ConvertToString ( ) );

				if (ids.Count > 0)
				{
					if (!AreDictionariesEqueal ( ids, dic ))
					{
						dic.Clear ( );
						foreach (KeyValuePair<string, string> id in ids)
						{
							dic.Add ( id.Key, id.Value );
						}
						return true;
					}
				}
			}
			return false;
		}

		private static bool AreDictionariesEqueal (Dictionary<string, string> dic1, Dictionary<string, string> dic2)
		{
			bool res = true;
			if (dic1.Count == dic2.Count)
			{
				if (dic1.Count > 0)
				{
					Dictionary<string, string>.Enumerator
						enum1 = dic1.GetEnumerator ( ),
						enum2 = dic2.GetEnumerator ( );
					do
					{
						if (enum1.Current.Key != enum2.Current.Key || enum1.Current.Value != enum2.Current.Value)
						{
							res = false;
							break;
						}
					} while (enum1.MoveNext ( ) && enum2.MoveNext ( ));
				}
			}
			else
			{
				//not equal count
				res = false;
			}
			return res;
		}

#if FIREBASE_PRESENT
		private static void SetIds (ProviderParams param, DataSnapshot e)
		{
			string s_param = param.ToString ( );
			if (e.HasChild ( s_param ))
			{
				DataSnapshot child = e.Child ( s_param );
				if (child.ChildrenCount > 0)
				{
					LogMessage ( "Received " + child.ChildrenCount + " IDs" );
					string json = child.GetRawJsonValue ( );
					Dictionary<string, string> dic = GetParamDic ( param );
					if (SetIds ( param, json, ref dic ))
					{
						SetLocalParam ( s_param, json );
						//tell abref params update
						if (OnHomeAdsIDsUpdated != null)
						{
							OnHomeAdsIDsUpdated ( param, GetIDs ( param ) );
						}
					}
				}
			}
		}
#endif
		private static void SetLocalParam (string key, string value)
		{
			LogMessage ( "Setting local param " + key + " = " + value );
			PlayerPrefs.SetString ( key, value );
			PlayerPrefs.Save ( );
		}

		#endregion

		#region Server interaction

		private static void LoadAdsParams ()
		{
			for (int i = 0; i < home_ads_params.Length; i++)
			{
				Dictionary<string, string> dic = GetParamDic ( home_ads_params [i] );
				if (dic != null)
				{
					LoadIds ( home_ads_params [i], ref dic );
				}
			}
		}

		private static void LoadIds (ProviderParams param, ref Dictionary<string, string> dic)
		{
			//load saved data
			string json = GetLocalParam ( param.ToString ( ) );
			SetIds ( param, json, ref dic );
		}

#if FIREBASE_PRESENT
		private static void FetchNewIds ()
		{
			// Set up the Editor before calling into the realtime database.
			string home_ads_db = FirebaseManager.GetRemoteParam ( home_ads_db_param );
			if (string.IsNullOrEmpty ( home_ads_db ))
			{
				home_ads_db = default_home_ads_db;
			}

			FirebaseManager.GetSnapshotFromDB ( home_ads_db, home_ids_root_param, ProcessNewIds );
		}
		private static void ProcessNewIds (DataSnapshot e)
		{
			LogMessage ( "Received data snapshot: < " + e.GetRawJsonValue ( ) + " >" );
			if (e != null && e.ChildrenCount > 0)
			{
				for (int i = 0; i < home_ads_params.Length; i++)
				{
					SetIds ( home_ads_params [i], e );
				}
			}
		}
#endif
		#endregion

	}
}