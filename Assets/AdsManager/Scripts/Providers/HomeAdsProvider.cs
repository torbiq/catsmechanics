﻿#define GOOGLE_ADS_PRESENT

#if GOOGLE_ADS_PRESENT
namespace PSV_Prototype
{
	public class HomeAdsProvider :GoogleAdsProvider, IHomeAdsProvider
	{

		private const ProviderParams
			banner_param = ProviderParams.HOMEADS_BANNER_ID,
			interstitial_param = ProviderParams.HOMEADS_INTERSTITIAL_ID,
			native_param = ProviderParams.HOMEADS_NATIVE_ID,
			rewarded_param = ProviderParams.HOMEADS_REWARDED_ID;

		private ProviderParams []
			necessary_params = new ProviderParams [] {
				banner_param,
				interstitial_param,
				native_param,
				rewarded_param,
			};

		private AdNetwork
			net = AdNetwork.HomeAds;


		private string []
			banner_ids,
			interstitial_ids,
			native_ids,
			rewarded_ids;

		private int
			banner_iterator = -1,
			interstitial_iterator = -1,
			native_iterator = -1,
			rewarded_iterator = -1;



#region Getters
		public override AdNetwork GetNetworkType ()
		{
			return net;
		}

		protected override ProviderParams GetBannerParam ()
		{
			return banner_param;
		}

		protected override ProviderParams GetInterstitialParam ()
		{
			return interstitial_param;
		}

		protected override ProviderParams GetNativeParam ()
		{
			return native_param;
		}

		protected override ProviderParams GetRewardedParam ()
		{
			return rewarded_param;
		}

		public override ProviderParams [] GetNecessaryParams ()
		{
			return necessary_params;
		}

		protected override string GetBannerID ()
		{
			string res = base.GetBannerID ( );
			GetNextID ( banner_ids, ref banner_iterator, ref res );
			LogMessage ( "GetNextBannerID [" + banner_iterator + "] = \"" + res + "\"" );
			return res;
		}

		protected override string GetInterstitialID ()
		{
			string res = base.GetInterstitialID ( );
			GetNextID ( interstitial_ids, ref interstitial_iterator, ref res );
			LogMessage ( "GetNextInterstitialID [" + interstitial_iterator + "] = \"" + res + "\"" );
			return res;
		}

		protected override string GetNativeID ()
		{
			string res = base.GetNativeID ( );
			GetNextID ( native_ids, ref native_iterator, ref res );
			LogMessage ( "GetNextNativeID [" + native_iterator + "] = \"" + res + "\"" );
			return res;
		}

		protected override string GetRewardedID ()
		{
			string res = base.GetRewardedID ( );
			GetNextID ( rewarded_ids, ref rewarded_iterator, ref res );
			LogMessage ( "GetNextRewardedID [" + rewarded_iterator + "] = \"" + res + "\"" );
			return res;
		}

		private void GetNextID (string [] ids, ref int iterator, ref string id)
		{
			//will set id to selected among available or live unchanged, define it before calling this method
			if (ids != null && ids.Length > 0)
			{
				iterator = (iterator + 1) % ids.Length;
				id = ids [iterator];
			}
		}

#endregion

#region Setters
		public void SetHomeAdsIDs (ProviderParams param, string [] ids)
		{
			switch (param)
			{
				case ProviderParams.HOMEADS_BANNER_ID:
					SetBannerIds ( ids );
					break;
				case ProviderParams.HOMEADS_NATIVE_ID:
					SetNativeIds ( ids );
					break;
				case ProviderParams.HOMEADS_INTERSTITIAL_ID:
					SetInterstitialIds ( ids );
					break;
				case ProviderParams.HOMEADS_REWARDED_ID:
					SetRewardedIds ( ids );
					break;
			}
		}

		private void SetBannerIds (string [] ids)
		{
			LogMessage ( "Banners set to: " + ids.ConvertToString ( ) );
			banner_ids = ids;
			banner_iterator = -1;
			//RequestBanner ( true );
		}

		private void SetInterstitialIds (string [] ids)
		{
			LogMessage ( "Interstitials set to: " + ids.ConvertToString ( ) );
			interstitial_ids = ids;
			interstitial_iterator = -1;
			//RequestInterstitial ( true );
		}

		private void SetNativeIds (string [] ids)
		{
			LogMessage ( "Natives set to: " + ids.ConvertToString ( ) );
			native_ids = ids;
			native_iterator = -1;
			//RequestNativeAd ( true );
		}

		private void SetRewardedIds (string [] ids)
		{
			LogMessage ( "Rewardeds set to: " + ids.ConvertToString ( ) );
			rewarded_ids = ids;
			rewarded_iterator = -1;
			//RequestRewardedAd ( true );
		}
#endregion

	}
}

#endif