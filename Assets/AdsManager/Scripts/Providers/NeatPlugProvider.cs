﻿//#define NO_GOOGLE_ADS

#if !UNITY_IPHONE
using UnityEngine;
using System;
using System.Collections.Generic;

namespace PSV_Prototype
{

	public class NeatPlugProvider :IAdProvider
	{
		private bool
			is_designed_for_families = false,
			tagged_for_children = true,
			ads_enabled = true,
			interstitial_ready = false,
			interstitial_loading = false,
			banner_ready = false,
			banner_loading = false;

		private float
			last_banner_request = 0,
			last_interstitial_request;

		protected AdsInterop.AdSize
			ad_size = null;
		protected AdsInterop.AdPosition
			ad_pos = AdsInterop.AdPosition.Undefined;


		protected AdmobAd.BannerAdType
			banner_type = AdmobAd.BannerAdType.Universal_Banner_320x50; //use this only due to higher fillrate
		protected AdmobAd.AdLayout
			banner_position = AdmobAd.AdLayout.Bottom_Centered; //default position

		public event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		public event Action<string>
			OnEventOccured;
		public event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		protected AdNetwork
			net = AdNetwork.NeatPlug;

		protected ProviderParams
			banner_param = ProviderParams.NEATPLUG_BANNER_ID,
			interstitial_param = ProviderParams.NEATPLUG_INTERSTITIAL_ID;

		private Dictionary<AdmobAd.BannerAdType, AdsInterop.AdSize> CorrespondingAdSize = new Dictionary<AdmobAd.BannerAdType, AdsInterop.AdSize> ( )
		{
			{AdmobAd.BannerAdType.Tablets_IAB_Banner_468x60,        AdsInterop.AdSize.IAB_fullsize_banner_468x60 },
			{AdmobAd.BannerAdType.Tablets_IAB_LeaderBoard_728x90,   AdsInterop.AdSize.IAB_leaderboard_728x90 },
			{AdmobAd.BannerAdType.Tablets_IAB_MRect_300x250,        AdsInterop.AdSize.IAB_medium_rectangle_300x250 },
			{AdmobAd.BannerAdType.Tablets_IAB_SkyScraper_160x600,   AdsInterop.AdSize.IAB_SkyScraper_160x600 },
			{AdmobAd.BannerAdType.Universal_Banner_320x50,          AdsInterop.AdSize.Standard_banner_320x50 },
			{AdmobAd.BannerAdType.Universal_SmartBanner,            AdsInterop.AdSize.Smart_banner },
		};

		private bool
			emulating_rewarded = false;

		#region NotSupported

		public void ShowNativeAd (bool show)
		{
		}

		public bool IsNativeAdAvailable ()
		{
			return false;
		}

		public void RefreshNativeAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
		{
		}


		#endregion

		#region rewarded_emulation
		public bool IsRewardedAvailable ()
		{
#if NO_GOOGLE_ADS
			return IsInterstitialAdAvailable ( );
#else
			return false;
#endif
		}

		public bool ShowRewardedAd ()
		{
#if NO_GOOGLE_ADS
			bool res = false;
			if (interstitial_ready)
			{
				LogMessage ( "Show Rewarded" );
				AdmobAd.Instance ( ).ShowInterstitialAd ( );
				emulating_rewarded = res = true;
			}
			return res;
#else
			return false;
#endif
		}
		#endregion


		#region General Methods

		public new string ToString ()
		{
			return GetNetworkType ( ).ToString ( );
		}

		private bool CanRequestAd (float last_time)
		{
			return Time.time - last_time >= AdsManager.REQUEST_INTERVAL;
		}

		protected void LogMessage (string message, bool error = false)
		{
			AdsManager.LogMessage ( this.GetType ( ).ToString ( ) + " " + message, error );
		}

		protected void LogEvent (string message)
		{
			string msg = (GetNetworkType ( ) + "_" + message).Replace ( ' ', '_' );
			if (OnEventOccured != null)
			{
				OnEventOccured ( msg );
			}
		}

		public AdNetwork GetNetworkType ()
		{
			return net;
		}

		public ProviderParams [] GetNecessaryParams ()
		{
			return new ProviderParams [] { banner_param, interstitial_param };
		}

		public void Init (bool ads_enabled, bool children_tagged, bool for_families, object settings)
		{
			is_designed_for_families = for_families;
			tagged_for_children = children_tagged;
			this.ads_enabled = ads_enabled;
			LogMessage ( "Initialising with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families );
			SubscribeListeners ( );
			InitAdMob ( settings );
			LoadBanner ( );
			LoadInterstitial ( );
		}

		protected void InitAdMob (object settings) //receiving array with banner and interstitial ids
		{
			Dictionary<ProviderParams, string> _settings = settings as Dictionary<ProviderParams, string>;
			LogMessage ( "Processing params: " + _settings.ConvertToString ( ) );
			string
				banner = "",
				interstitial = "";
			if (_settings != null &&
				_settings.TryGetValue ( banner_param, out banner ) &&
				_settings.TryGetValue ( interstitial_param, out interstitial ))
			{
				AdmobAd.Instance ( ).Init ( banner, interstitial );
			}
		}

		public void DisableAds ()
		{
			ads_enabled = false;
			HideBanner ( );
		}

		public void EnableAds ()
		{
			ads_enabled = true;
			LoadBanner ( );
			LoadInterstitial ( );
		}

		protected AdmobAd.AdLayout ConvertPosition (AdsInterop.AdPosition pos)
		{
			return Utils.ToEnum<AdmobAd.AdLayout> ( pos.ToString ( ) );
		}

		protected AdmobAd.BannerAdType ConvertSize (AdsInterop.AdSize ad_size)
		{

			AdmobAd.BannerAdType res = AdmobAd.BannerAdType.Universal_Banner_320x50;
			foreach (var item in CorrespondingAdSize)
			{
				if (item.Value == ad_size)
				{
					res = item.Key;
					break;
				}
			}
			return res;
		}

		protected void SubscribeListeners ()
		{
			AdmobAdAgent.OnReceiveAd += OnReceiveAd;
			AdmobAdAgent.OnFailedToReceiveAd += OnFailedToReceiveAd;
			AdmobAdAgent.OnReceiveAdInterstitial += OnReceiveAdInterstitial;
			AdmobAdAgent.OnFailedToReceiveAdInterstitial += OnFailedToReceiveAdInterstitial;
			AdmobAdAgent.OnPresentScreenInterstitial += OnPresentScreenInterstitial;
			AdmobAdAgent.OnDismissScreenInterstitial += OnDismissScreenInterstitial;
			AdmobAdAgent.OnAdShown += OnAdShown;
			AdmobAdAgent.OnAdHidden += OnAdHidden;
			AdmobAdAgent.OnLeaveApplicationInterstitial += OnLeaveApplicationInterstitial;
			AdmobAdAgent.OnLeaveApplication += OnLeaveApplication;
		}

		protected void UnsubscribeListeners ()
		{
			AdmobAdAgent.OnReceiveAd -= OnReceiveAd;
			AdmobAdAgent.OnFailedToReceiveAd -= OnFailedToReceiveAd;
			AdmobAdAgent.OnReceiveAdInterstitial -= OnReceiveAdInterstitial;
			AdmobAdAgent.OnFailedToReceiveAdInterstitial -= OnFailedToReceiveAdInterstitial;
			AdmobAdAgent.OnPresentScreenInterstitial -= OnPresentScreenInterstitial;
			AdmobAdAgent.OnDismissScreenInterstitial -= OnDismissScreenInterstitial;
			AdmobAdAgent.OnAdShown -= OnAdShown;
			AdmobAdAgent.OnAdHidden -= OnAdHidden;
			AdmobAdAgent.OnLeaveApplicationInterstitial -= OnLeaveApplicationInterstitial;
			AdmobAdAgent.OnLeaveApplication -= OnLeaveApplication;
		}

		#endregion

		#region Banner

		public bool IsBannerAdAvailable ()
		{
			return banner_ready;
		}

		protected void LoadBanner (bool force = false)
		{
			if (ads_enabled && (force || !banner_loading && !banner_ready))
			{
				LogMessage ( "Requesting banner" );
				banner_loading = true;
				last_banner_request = Time.time;

				Vector2 size = AdmobAd.Instance ( ).GetAdSizeInPixels ( banner_type );
				AdmobAd.TagForChildrenDirectedTreatment for_children = tagged_for_children ? AdmobAd.TagForChildrenDirectedTreatment.Yes : AdmobAd.TagForChildrenDirectedTreatment.Unset;
				Dictionary<string, string> extras = is_designed_for_families ? new Dictionary<string, string> ( ) { { "is_designed_for_families", "true" } } : null;

				try
				{
					if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WP8Player)
					{
						AdmobAd.Instance ( ).
							LoadBannerAd (
											banner_type,
											banner_position,
											Vector2.zero,
											true,
											extras,
											for_children
							);
					}
					else if (Application.platform == RuntimePlatform.IPhonePlayer)
					{
						AdmobAd.Instance ( ).LoadBannerAd (
							banner_type,
							(int) (Screen.height * 0.5f - size.y * 0.5f),
							(int) (Screen.width * 0.25f - size.x * 0.25f),
							true,
							extras,
							for_children
						);
					}
				}
				catch (Exception e)
				{
					LogMessage ( "LoadBanner exception: " + e.Message, true );
				}
			}
		}

		public void ShowBannerAd (bool show)
		{
			if (show)
			{
				ShowBanner ( );
			}
			else
			{
				HideBanner ( );
			}
		}

		public void RefreshBannerAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
		{
			if (this.ad_size != ad_size)
			{
				this.ad_size = ad_size;
				banner_type = ConvertSize ( ad_size );
				LoadBanner ( true );
			}
			if (this.ad_pos != ad_pos)
			{
				this.ad_pos = ad_pos;
				banner_position = ConvertPosition ( ad_pos );
				AdmobAd.Instance ( ).RepositionBannerAd ( banner_position );
			}
		}

		protected void ShowBanner ()
		{
			LogMessage ( "ShowBanner: banner_ready - " + banner_ready );
			if (banner_ready)
			{
				AdmobAd.Instance ( ).ShowBannerAd ( );
			}
		}

		protected void HideBanner ()
		{
			LogMessage ( "HideBanner" );
			AdmobAd.Instance ( ).HideBannerAd ( );
		}

		protected void BannerReadyChanged (bool ready)
		{
			bool last_state = banner_ready;
			banner_ready = ready;
			banner_loading = false;

			if (!banner_ready)
			{
				if (last_state == true || CanRequestAd ( last_banner_request )) //if ad was loaded or last request was made some time before
				{
					LoadBanner ( );
				}
				else
				{
					// lower ad request frequency
					DelayedCallHandler.Instance.DelayedCall ( () => LoadBanner ( ), AdsManager.REQUEST_INTERVAL );
				}
			}

			if (OnBannerEvent != null)
			{
				OnBannerEvent ( GetNetworkType ( ), ready );
			}

			if (OnBannerReadyChanged != null)
			{
				OnBannerReadyChanged ( );
			}
		}

		#endregion

		#region Interstitial

		public bool IsInterstitialAdAvailable () //IAdProvider
		{
			return interstitial_ready;
		}


		void LoadInterstitial ()
		{
			//LogMessage ( "Trying to request interstitial/nads_enabled=" + ads_enabled + "\ninterstitial_loading=" + interstitial_loading + "\ninterstitial_ready=" + interstitial_ready );

			if (/*ads_enabled && */!interstitial_loading && !interstitial_ready)
			{
				LogMessage ( "Requesting interstitial" );
				interstitial_loading = true;
				last_interstitial_request = Time.time;

				AdmobAd.TagForChildrenDirectedTreatment for_children = tagged_for_children ? AdmobAd.TagForChildrenDirectedTreatment.Yes : AdmobAd.TagForChildrenDirectedTreatment.Unset;
				Dictionary<string, string> extras = is_designed_for_families ? new Dictionary<string, string> ( ) { { "is_designed_for_families", "true" } } : null;
				try
				{
					AdmobAd.Instance ( ).LoadInterstitialAd (
						true,
						extras,
						for_children
					);
				}
				catch (Exception e)
				{
					LogMessage ( "LoadInterstitial exception: " + e.Message, true );
				}
			}
		}

		public bool ShowInterstitialAd () //IAdProvider
		{
			if (ads_enabled && interstitial_ready)
			{
				LogMessage ( "Show Interstitial" );
				AdmobAd.Instance ( ).ShowInterstitialAd ( );
				return true;
			}
			else
			{
				return false;
			}
		}

		protected void InterstitialReadyChanged (bool ready)
		{
			bool last_state = interstitial_ready;
			interstitial_loading = false;
			interstitial_ready = ready;
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_interstitial_request )) //if ad was loaded or last request was made some time before
				{
					LoadInterstitial ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.Instance.DelayedCall ( () => LoadInterstitial ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
			if (OnInterstitialEvent != null)
			{
				OnInterstitialEvent ( GetNetworkType ( ), ready );
			}
		}

		private void InterstitialShown ()
		{
			if (OnInterstitialShown != null)
			{
				OnInterstitialShown ( );
			}
		}

		private void InterstitialClosed ()
		{
			if (emulating_rewarded)
			{
				emulating_rewarded = false;
				if (OnRewardedClosed != null)
				{
					OnRewardedClosed ( );
				}
				if (OnRewardedCompleted != null)
				{
					OnRewardedCompleted ( );
				}
			}
			else
			{
				if (OnInterstitialClosed != null)
				{
					OnInterstitialClosed ( );
				}
			}
		}
		#endregion

		//Ad callbacks holders

		#region BANNER_EVENTS

		// Banner advert loaded and ready to be displayed
		void OnReceiveAd ()
		{
			LogMessage ( "OnReceiveAd() Fired." );
			BannerReadyChanged ( true );
		}

		// Failed to receive banner advert
		void OnFailedToReceiveAd (string error)
		{
			LogMessage ( "OnFailedToReceiveAd() Fired. Error: " + error );
			LogEvent ( "b_err " + error );
			BannerReadyChanged ( false );
		}

		// Banner advert is visible
		void OnAdShown ()
		{
			LogMessage ( "OnAdShown() Fired." );
		}

		// Banner advert is hidden
		void OnAdHidden ()
		{
			LogMessage ( "OnAdHidden() Fired." );
		}

		void OnLeaveApplication ()
		{
			LogMessage ( "OnLeaveApplication() Fired." );
			LogEvent ( "banner clicked" );
			AnalyticsManager.LogEvent ( AnalyticsEvents.BannerClicked );
		}

		#endregion

		#region INTERSTITIAL_EVENTS

		// Interstitial loaded and ready to be displayed
		void OnReceiveAdInterstitial ()
		{
			LogMessage ( "OnReceiveAdInterstitial() Fired." );
			InterstitialReadyChanged ( true );
		}

		// Failed to receive interstitial advert (e.g no internet connection)
		void OnFailedToReceiveAdInterstitial (string error)
		{
			LogMessage ( "OnFailedToReceiveAdInterstitial() Fired. Error: " + error );
			LogEvent ( "i_err " + error );
			InterstitialReadyChanged ( false );
		}

		// When interstitial window opens
		void OnPresentScreenInterstitial ()
		{
			LogMessage ( "OnPresentScreenInterstitial() Fired." );
			InterstitialShown ( );
		}

		// When interstitial window is closed (Via hardware back button or clicking the X)
		void OnDismissScreenInterstitial ()
		{
			LogMessage ( "OnDismissScreenInterstitial() Fired." );
			InterstitialClosed ( );
			InterstitialReadyChanged ( false );
		}

		// The player clicked an interstitial advert and the app has minimized
		void OnLeaveApplicationInterstitial ()
		{
			LogMessage ( "OnLeaveApplicationInterstitial() Fired." );
			LogEvent ( "interstitial clicked" );
		}

		#endregion

	}
}
#endif