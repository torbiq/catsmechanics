﻿#define GOOGLE_ADS_PRESENT

#if GOOGLE_ADS_PRESENT
using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

namespace PSV_Prototype
{

	public class GoogleAdsProvider :IAdProvider
	{
		protected BannerView
			bannerView;
		protected InterstitialAd
			interstitial;
		protected RewardBasedVideoAd
			rewardBasedVideo;
		protected NativeExpressAdView
			nativeView;


		protected AdSize
			banner_size = AdSize.Banner,
			native_size = new AdSize ( 320, 80 ); //minimum height = 80

		protected AdPosition
			banner_position = AdPosition.Bottom,
			native_position = AdPosition.Bottom;

		protected bool
			ads_enabled = true,
			banner_ready = false,
			interstitial_ready = false,
			rewarded_ready = false,
			native_ready = false;

		private bool
			is_designed_for_families = false,
			tagged_for_children = true,
			native_loading = false,
			rewarded_loading = false,
			banner_loading = false,
			interstitial_loading = false,
			banner_visible = false,
			native_visible = false;

		private string
			banner_id = "",
			interstitial_id = "",
			native_id = "",
			rewarded_id = "";

		private const ProviderParams
			banner_param = ProviderParams.GOOGLEADS_BANNER_ID,
			interstitial_param = ProviderParams.GOOGLEADS_INTERSTITIAL_ID,
			native_param = ProviderParams.GOOGLEADS_NATIVE_ID,
			rewarded_param = ProviderParams.GOOGLEADS_REWARDED_ID;

		private ProviderParams []
			necessary_params = new ProviderParams [] {
				banner_param,
				interstitial_param,
				native_param,
				rewarded_param,
			};

		public event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		public event Action<string>
			OnEventOccured;
		public event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		private AdNetwork
			net = AdNetwork.GoogleAds;

		private float
			last_banner_request = 0,
			last_interstitial_request,
			last_native_request = 0,
			last_rewarded_request = 0;

		#region General Methods

		public new string ToString ()
		{
			return GetNetworkType ( ).ToString ( );
		}


		private bool CanRequestAd (float last_time)
		{
			return Time.time - last_time >= AdsManager.REQUEST_INTERVAL;
		}


		protected void LogMessage (string message, bool error = false)
		{
			AdsManager.LogMessage ( this.GetType ( ).ToString ( ) + " " + message, error );
		}

		protected void LogEvent (string message)
		{
			string msg = (GetNetworkType ( ) + "_" + message).Replace ( ' ', '_' );
			if (OnEventOccured != null)
			{
				OnEventOccured ( msg );
			}
		}

		public void DisableAds ()
		{
			ads_enabled = false;
			DestroyBanner ( );
			DestroyNative ( );
			DestroyInterstitial ( );
			//rewarded stays active to keep user receiving bonuses for watching ad
		}

		public void EnableAds ()
		{
			ads_enabled = true;
			LoadBanner ( );
			LoadNativeAd ( );
			LoadInterstitial ( );
		}

		virtual public AdNetwork GetNetworkType ()
		{
			return net;
		}

		virtual protected ProviderParams GetBannerParam ()
		{
			return banner_param;
		}

		virtual protected ProviderParams GetInterstitialParam ()
		{
			return interstitial_param;
		}

		virtual protected ProviderParams GetNativeParam ()
		{
			return native_param;
		}

		virtual protected ProviderParams GetRewardedParam ()
		{
			return rewarded_param;
		}

		virtual public ProviderParams [] GetNecessaryParams ()
		{
			return necessary_params;
		}

		public void Init (bool ads_enabled, bool children_tagged, bool for_families, object settings)
		{
			tagged_for_children = children_tagged;
			is_designed_for_families = for_families;
			this.ads_enabled = ads_enabled;
			LogMessage ( "Initialising with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families );
			ProcessParams ( settings );
		}


		protected AdPosition ConvertAdPosition (AdsInterop.AdPosition pos)
		{
			AdPosition res = AdPosition.Bottom;

			switch (pos)
			{
				case AdsInterop.AdPosition.Bottom_Centered:
					res = AdPosition.Bottom;
					break;
				case AdsInterop.AdPosition.Bottom_Left:
					res = AdPosition.BottomLeft;
					break;
				case AdsInterop.AdPosition.Bottom_Right:
					res = AdPosition.BottomRight;
					break;
				case AdsInterop.AdPosition.Middle_Centered:
				case AdsInterop.AdPosition.Middle_Left:
				case AdsInterop.AdPosition.Middle_Right:
					res = AdPosition.Center;
					break;
				case AdsInterop.AdPosition.Top_Centered:
					res = AdPosition.Top;
					break;
				case AdsInterop.AdPosition.Top_Left:
					res = AdPosition.TopLeft;
					break;
				case AdsInterop.AdPosition.Top_Right:
					res = AdPosition.TopRight;
					break;
			}
			return res;
		}

		protected AdSize ConvertAdSize (AdsInterop.AdSize ad_size)
		{
			AdSize res = AdSize.Banner;
			if (ad_size == AdsInterop.AdSize.Smart_banner)
			{
				return AdSize.SmartBanner;
			}
			else
			{
				return new AdSize ( ad_size.w, ad_size.h );
			}
		}

		virtual protected void ProcessParams (object settings)
		{
			Dictionary<ProviderParams, string> _settings = settings as Dictionary<ProviderParams, string>;
			LogMessage ( "Processing params: " + _settings.ConvertToString ( ) );

			if (_settings != null)
			{
				if (_settings.TryGetValue ( GetBannerParam ( ), out banner_id ))
				{
					LogMessage ( "banner initialised" );
					LoadBanner ( true );
				}

				if (_settings.TryGetValue ( GetInterstitialParam ( ), out interstitial_id ))
				{
					LogMessage ( "interstitial initialised" );
					LoadInterstitial ( true );
				}

				//optional
				if (_settings.TryGetValue ( GetRewardedParam ( ), out rewarded_id ))
				{
					LogMessage ( "rewarded initialised" );
					LoadRewardedVideoAd ( true );
				}

				if (_settings.TryGetValue ( GetNativeParam ( ), out native_id ))
				{
					LogMessage ( "native_ad initialised" );
					LoadNativeAd ( true );
				}
			}
		}

		virtual protected string GetBannerID ()
		{
			return banner_id;
		}

		virtual protected string GetInterstitialID ()
		{
			return interstitial_id;
		}

		virtual protected string GetNativeID ()
		{
			return native_id;
		}


		virtual protected string GetRewardedID ()
		{
			return rewarded_id;
		}


		protected AdRequest createAdRequest ()
		{
			AdRequest.Builder res = new AdRequest.Builder ( )
				//.AddTestDevice (AdRequest.TestDeviceSimulator)
				//.AddTestDevice ("0123456789ABCDEF0123456789ABCDEF")
				//.AddKeyword ("game")
				//.SetGender (Gender.Male)
				//.SetBirthday (new DateTime (1985, 1, 1))
				.AddExtra ( "color_bg", "9B30FF" );

			if (tagged_for_children)
			{
				res = res.TagForChildDirectedTreatment ( true );
			}
			if (is_designed_for_families)
			{
				res = res.AddExtra ( "is_designed_for_families", "true" );
			}

			return res.Build ( );



			//	if (tagged_for_children)
			//{
			//	return new AdRequest.Builder ( )
			//			//.AddTestDevice (AdRequest.TestDeviceSimulator)
			//			//.AddTestDevice ("0123456789ABCDEF0123456789ABCDEF")
			//			//.AddKeyword ("game")
			//			//.SetGender (Gender.Male)
			//			//.SetBirthday (new DateTime (1985, 1, 1))
			//			.AddExtra ( "is_designed_for_families", "true" )
			//			.TagForChildDirectedTreatment ( true )
			//			.AddExtra ( "color_bg", "9B30FF" )
			//			.Build ( );
			//}
			//else
			//{
			//	return new AdRequest.Builder ( )
			//			//.AddTestDevice (AdRequest.TestDeviceSimulator)
			//			//.AddTestDevice ("0123456789ABCDEF0123456789ABCDEF")
			//			//.AddKeyword ("game")
			//			//.SetGender (Gender.Male)
			//			//.SetBirthday (new DateTime (1985, 1, 1))
			//			//.TagForChildDirectedTreatment ( true )
			//			.AddExtra ( "color_bg", "9B30FF" )
			//			.Build ( );
			//}
		}

		#endregion

		#region Banner

		protected void RequestBanner (bool forse)
		{
			if (!banner_loading || forse)
			{
				DestroyBanner ( );
				string id = GetBannerID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						LogMessage ( "Requesting banner" );
						banner_loading = true;
						bannerView = new BannerView ( id, banner_size, banner_position );
						// Register for ad events.
						bannerView.OnAdLoaded += HandleAdLoaded;
						bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
						bannerView.OnAdLoaded += HandleAdOpened;
						bannerView.OnAdClosed += HandleAdClosed;
						bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
						// Load a banner ad.
						try
						{
							bannerView.LoadAd ( createAdRequest ( ) );
						}
						catch (Exception e)
						{
							LogMessage ( "RequestBanner exception: " + e.Message, true );
						}
						last_banner_request = Time.time;
					}
					else
					{
						OnBannerRequestError ( id );
					}
				}
			}
		}

		virtual protected void OnBannerRequestError (string id)
		{
			LogMessage ( "BannerRequestError empty id", true );
		}

		protected void LoadBanner (bool forse = false)
		{
			RequestBanner ( forse );
			HideBanner ( );
		}

		public bool IsBannerAdAvailable ()
		{
			return banner_ready;
		}

		public void RefreshBannerAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
		{
			LogMessage ( "RepositionAd" );
			banner_position = ConvertAdPosition ( ad_pos );
			banner_size = ConvertAdSize ( ad_size );
			BannerReadyChanged ( false );
		}

		public void ShowBannerAd (bool show)
		{
			if (show)
			{
				ShowBanner ( );
			}
			else
			{
				HideBanner ( );
			}
		}

		protected void ShowBanner ()
		{
			banner_visible = true;
			if (bannerView != null)
			{
				bannerView.Show ( );
			}
		}

		protected void HideBanner ()
		{
			banner_visible = false;
			if (bannerView != null)
			{
				bannerView.Hide ( );
			}
		}

		protected void DestroyBanner ()
		{
			if (bannerView != null)
			{
				bannerView.Destroy ( );
			}
		}

		protected void BannerReadyChanged (bool ready)
		{
			bool last_state = banner_ready;
			banner_ready = ready;
			banner_loading = false;
			if (OnBannerEvent != null)
			{
				OnBannerEvent ( GetNetworkType ( ), ready );
			}
			if (OnBannerReadyChanged != null)
			{
				OnBannerReadyChanged ( );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_banner_request )) //if ad was loaded or last request was made some time before
				{
					LoadBanner ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.Instance.DelayedCall ( () => LoadBanner ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

		#endregion

		#region Interstitial

		public bool IsInterstitialAdAvailable ()
		{
			return interstitial != null && interstitial_ready; /*interstitial.IsLoaded ( );*/
		}

		protected void RequestInterstitial (bool forse)
		{
			if (!interstitial_loading || forse)
			{
				DestroyInterstitial ( );
				// Create an interstitial.
				string id = GetInterstitialID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						LogMessage ( "Requesting interstitial" );
						interstitial_loading = true;
						interstitial = new InterstitialAd ( id );
						// Register listeners for ad events.
						interstitial.OnAdLoaded += HandleInterstitialLoaded;
						interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
						interstitial.OnAdOpening += HandleInterstitialOpened;
						interstitial.OnAdClosed += HandleInterstitialClosed;
						interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;
						// Load an interstitial ad.
						try
						{
							interstitial.LoadAd ( createAdRequest ( ) );
						}
						catch (Exception e)
						{
							LogMessage ( "RequestInterstitial exception: " + e.Message, true );
						}
						last_interstitial_request = Time.time;
					}
					else
					{
						OnInterstitialRequestError ( id );
					}
				}
			}
		}

		virtual protected void OnInterstitialRequestError (string id)
		{
			LogMessage ( "InterstitialRequestError empty id", true );
		}

		protected void LoadInterstitial (bool forse = false)
		{
			if (forse || !IsInterstitialAdAvailable ( ))
			{
				RequestInterstitial ( forse );
			}
		}

		public bool ShowInterstitialAd ()
		{
			if (IsInterstitialAdAvailable ( ))
			{
				interstitial.Show ( );
				return true;
			}
			return false;
		}

		protected void DestroyInterstitial ()
		{
			if (interstitial != null)
			{
				interstitial.Destroy ( );
			}
		}

		protected void InterstitialReadyChanged (bool ready)
		{
			bool last_state = interstitial_ready;
			interstitial_ready = ready;
			interstitial_loading = false;
			if (OnInterstitialEvent != null)
			{
				OnInterstitialEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_interstitial_request )) //if ad was loaded or last request was made some time before
				{
					LoadInterstitial ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.Instance.DelayedCall ( () => LoadInterstitial ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

		private void InterstitialShown ()
		{
			if (OnInterstitialShown != null)
			{
				OnInterstitialShown ( );
			}
		}

		private void InterstitialClosed ()
		{
			if (OnInterstitialClosed != null)
			{
				OnInterstitialClosed ( );
			}
		}
		#endregion

		#region Rewarded

		protected void RequestRewardedAd (bool forse)
		{
			if (!rewarded_loading || forse)
			{
				if (rewardBasedVideo == null)
				{
					rewardBasedVideo = RewardBasedVideoAd.Instance;
					rewardBasedVideo.OnAdClosed += OnRewardedVideoAdClosed;
					rewardBasedVideo.OnAdFailedToLoad += OnRewardedVideoAdFailedToLoad;
					rewardBasedVideo.OnAdLoaded += OnRewardedVideoAdLoaded;
					rewardBasedVideo.OnAdLeavingApplication += OnRewardedVideoAdLeavingApplication;
					rewardBasedVideo.OnAdOpening += OnRewardedVideoAdOpening;
					rewardBasedVideo.OnAdRewarded += OnRewardedVideoAdRewarded;
					rewardBasedVideo.OnAdStarted += OnRewardedVideoAdStarted;
				}
				string id = GetRewardedID ( );
				if (!string.IsNullOrEmpty ( id ))
				{
					LogMessage ( "Requesting rewarded" );
					rewarded_loading = true;
					AdRequest request = new AdRequest.Builder ( ).Build ( );
					try
					{
						rewardBasedVideo.LoadAd ( request, id );
					}
					catch (Exception e)
					{
						LogMessage ( "RequestRewardedAd exception: " + e.Message, true );
					}
					last_rewarded_request = Time.time;
				}
				else
				{
					OnRewardedRequestError ( id );
				}
			}
		}

		virtual protected void OnRewardedRequestError (string id)
		{
			LogMessage ( "RewardedRequestError empty id", true );
		}

		protected void LoadRewardedVideoAd (bool forse = false)
		{
			RequestRewardedAd ( forse );
		}


		public bool ShowRewardedAd ()
		{
			if (rewardBasedVideo != null && rewarded_ready /*rewardBasedVideo.IsLoaded ( )*/)
			{
				rewardBasedVideo.Show ( );
				return true;
			}
			return false;
		}

		protected void RewardedReadyChanged (bool ready)
		{
			bool last_state = rewarded_ready;
			rewarded_ready = ready;
			rewarded_loading = false;

			if (OnRewardedEvent != null)
			{
				OnRewardedEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_rewarded_request )) //if ad was loaded or last request was made some time before
				{
					LoadRewardedVideoAd ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.Instance.DelayedCall ( () => LoadRewardedVideoAd ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

		private void RewardedShown ()
		{
			if (OnRewardedShown != null)
			{
				OnRewardedShown ( );
			}
		}
		private void RewardedClosed ()
		{
			if (OnRewardedClosed != null)
			{
				OnRewardedClosed ( );
			}
		}
		private void RewardedCompleted ()
		{
			if (OnRewardedCompleted != null)
			{
				OnRewardedCompleted ( );
			}
		}

		#endregion

		#region Native

		public bool IsNativeAdAvailable ()
		{
			return native_ready;
		}

		protected void RequestNativeAd (bool forse)
		{
			if (!native_loading || forse)
			{
				DestroyNative ( );

				string id = GetNativeID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						LogMessage ( "Requesting native ad" );
						native_loading = true;
						nativeView = new NativeExpressAdView ( id, native_size, native_position );
						nativeView.OnAdLoaded += OnNativeAdLoaded;
						nativeView.OnAdFailedToLoad += OnNativeAdFailedToLoad;
						nativeView.OnAdClosed += OnNativeAdClosed;
						nativeView.OnAdLeavingApplication += OnNativeAdLeavingApplication;
						nativeView.OnAdOpening += OnNativeAdOpening;
						try
						{
							nativeView.LoadAd ( createAdRequest ( ) );
						}
						catch (Exception e)
						{
							LogMessage ( "RequestNativeAd exception: " + e.Message, true );
						}
						last_native_request = Time.time;
					}
					else
					{
						OnNativeRequestError ( id );
					}
				}
			}
		}

		virtual protected void OnNativeRequestError (string id)
		{
			LogMessage ( "NativeRequestError empty id", true );
		}

		protected void LoadNativeAd (bool forse = false)
		{
			RequestNativeAd ( forse );
			HideNativeAd ( );
		}

		public void ShowNativeAd (bool show)
		{
			if (show)
			{
				ShowNativeAd ( );
			}
			else
			{
				HideNativeAd ( );
			}
		}
		public void RefreshNativeAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
		{
			native_position = ConvertAdPosition ( ad_pos );
			native_size = ConvertAdSize ( ad_size );
			NativeReadyChanged ( false );
			if (!native_visible)
				HideNativeAd ( );
		}


		protected void ShowNativeAd ()
		{
			native_visible = true;
			if (ads_enabled && nativeView != null)
			{
				nativeView.Show ( );
			}
		}

		protected void HideNativeAd ()
		{
			native_visible = false;
			if (nativeView != null)
			{
				nativeView.Hide ( );
			}
		}

		protected void RefreshNativeAd ()
		{
			if (nativeView != null)
			{
				nativeView.LoadAd ( createAdRequest ( ) );
			}
		}

		protected void DestroyNative ()
		{
			if (nativeView != null)
			{
				nativeView.Destroy ( );
			}
		}


		protected void NativeReadyChanged (bool ready)
		{
			bool last_state = native_ready;
			native_ready = ready;
			native_loading = false;
			if (OnNativeEvent != null)
			{
				OnNativeEvent ( GetNetworkType ( ), ready );
			}
			if (OnNativeReadyChanged != null)
			{
				OnNativeReadyChanged ( );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_native_request )) //if ad was loaded or last request was made some time before
				{
					LoadNativeAd ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.Instance.DelayedCall ( () => LoadNativeAd ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

		#endregion

		/* ====== ADVERT EVENTS ====== */

		#region Banner callback handlers

		protected void HandleAdLoaded (object sender, EventArgs args)
		{
			LogMessage ( "Banner Ad loaded" );
			BannerReadyChanged ( true );
		}

		protected void HandleAdFailedToLoad (object sender, AdFailedToLoadEventArgs args)
		{
			LogMessage ( "Failed to receive Banner Ad with message: " + args.Message );
			LogEvent ( "b_err " + args.Message );
			BannerReadyChanged ( false );
		}

		protected void HandleAdOpened (object sender, EventArgs args)
		{
			LogMessage ( "Banner Ad opened" );
		}

		protected void HandleAdClosing (object sender, EventArgs args)
		{
			LogMessage ( "Banner Ad closing" );
		}

		protected void HandleAdClosed (object sender, EventArgs args)
		{
			LogMessage ( "Banner Ad closed" );
		}

		protected void HandleAdLeftApplication (object sender, EventArgs args)
		{
			LogEvent ( "banner clicked" );
			LogMessage ( "Banner Ad left application" );
		}

		#endregion

		#region Native callbacks holder

		protected void OnNativeAdFailedToLoad (object sender, AdFailedToLoadEventArgs e)
		{
			LogMessage ( "Native Ad Failed To Load" );
			LogEvent ( "n_err " + e.Message );
			NativeReadyChanged ( false );
		}

		protected void OnNativeAdLoaded (object sender, EventArgs e)
		{
			LogMessage ( "Native Ad Loaded" );
			NativeReadyChanged ( true );
		}

		protected void OnNativeAdOpening (object sender, EventArgs e)
		{
			LogMessage ( "Native Ad Opening" );
		}

		protected void OnNativeAdLeavingApplication (object sender, EventArgs e)
		{
			LogEvent ( "native clicked" );
			LogMessage ( "Native Ad Leaving Application" );
		}

		protected void OnNativeAdClosed (object sender, EventArgs e)
		{
			LogMessage ( "Native Ad Closed" );
		}


		#endregion

		#region Interstitial callback handlers

		protected void HandleInterstitialClosed (object sender, EventArgs args)
		{
			LogMessage ( "Interstitial closed" );
			InterstitialClosed ( );
			InterstitialReadyChanged ( false );
		}


		protected void HandleInterstitialFailedToLoad (object sender, AdFailedToLoadEventArgs args)
		{
			LogMessage ( "Interstitial failed to load with message: " + args.Message );
			LogEvent ( "i_err " + args.Message );
			InterstitialReadyChanged ( false );
		}


		protected void HandleInterstitialLoaded (object sender, EventArgs args)
		{
			LogMessage ( "Interstitial loaded" );
			InterstitialReadyChanged ( true );
		}

		protected void HandleInterstitialOpened (object sender, EventArgs args)
		{
			LogMessage ( "Interstitial opened" );
			InterstitialShown ( );
		}

		protected void HandleInterstitialClosing (object sender, EventArgs args)
		{
			LogMessage ( "Interstitial Closing" );
		}



		protected void HandleInterstitialLeftApplication (object sender, EventArgs args)
		{
			LogMessage ( "Interstitial left Application" );
			LogEvent ( "interstitial clicked" );
		}

		#endregion

		#region Rewarded callback holders


		protected void OnRewardedVideoAdClosed (object sender, EventArgs e)
		{
			LogMessage ( "Rewarded Video Ad Closed" );
			RewardedClosed ( );
			LoadRewardedVideoAd ( );
		}

		protected void OnRewardedVideoAdRewarded (object sender, Reward e)
		{
			LogMessage ( "Rewarded Video Ad Rewarded" );
			RewardedCompleted ( );
		}

		protected void OnRewardedVideoAdFailedToLoad (object sender, AdFailedToLoadEventArgs e)
		{
			LogMessage ( "Rewarded Video Ad Failed To Load: " + e.Message );
			LogEvent ( "r_err " + e.Message );
			RewardedReadyChanged ( false );
		}

		protected void OnRewardedVideoAdLeavingApplication (object sender, EventArgs e)
		{
			LogMessage ( "Rewarded Video Ad Leaving Application" );
			LogEvent ( "rewarded clicked" );
		}

		protected void OnRewardedVideoAdLoaded (object sender, EventArgs e)
		{
			LogMessage ( "Rewarded Video Ad Loaded" );
			RewardedReadyChanged ( true );
		}

		protected void OnRewardedVideoAdOpening (object sender, EventArgs e)
		{
			LogMessage ( "Rewarded Video Ad Opening" );
			RewardedShown ( );
		}

		protected void OnRewardedVideoAdStarted (object sender, EventArgs e)
		{
			LogMessage ( "Rewarded Video Ad Started" );
		}

		#endregion

	}
}
#endif