﻿#if UNITY_EDITOR
using System;
using UnityEngine;

namespace PSV_Prototype
{
	public class DummyProvider :MonoBehaviour, IHomeAdsProvider
	{

		protected Vector2
			banner_size = new Vector2 ( 320, 50 ),
			native_size = new Vector2 ( 320, 80 ); //minimum height = 80

		protected AdsInterop.AdPosition
			banner_position = AdsInterop.AdPosition.Bottom_Centered,
			native_position = AdsInterop.AdPosition.Bottom_Centered;

		protected bool
			ads_enabled = true;



		private bool
			rewarded_visible = false,
			interstitial_visible = false,
			banner_visible = false,
			native_visible = false;


		private ProviderParams []
			necessary_params = new ProviderParams [] {
			};

		public event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		public event Action<string>
			OnEventOccured;
		public event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		private AdNetwork
			net = AdNetwork.HomeAds;


		#region Ads emulation

		private float
			refferenced_multiplier = 3;
		private Vector2
			refferenced_res = new Vector2 ( 1920, 1080 );


		Vector2 GetAdSize (Vector2 ad_size_in_units)
		{
			float coef = Mathf.Max ( Screen.width, Screen.height ) / Mathf.Max ( refferenced_res.x, refferenced_res.y );
			if (ad_size_in_units == Vector2.one)
			{
				return new Vector2 ( Screen.width, 40 * coef * refferenced_multiplier );
			}
			else
			{
				return ad_size_in_units * coef * refferenced_multiplier;
			}
		}


		public void OnGUI ()
		{
			if (banner_visible)
			{
				Vector2 banner_size_px = GetAdSize ( banner_size );
				GUI.color = Color.white;
				Vector2 pos = GetRectPos ( banner_size_px, banner_position );
				GUI.Button ( new Rect ( pos.x, pos.y, banner_size_px.x, banner_size_px.y ), "BANNER_AD " + banner_size.ToString ( ) );
			}


			if (native_visible)
			{
				Vector2 native_size_px = GetAdSize ( native_size );
				GUI.color = Color.white;
				Vector2 pos = GetRectPos ( native_size_px, native_position );
				GUI.Button ( new Rect ( pos.x, pos.y, native_size_px.x, native_size_px.y ), "NATIVE_AD " + native_size.ToString ( ) );
			}

			if (interstitial_visible)
			{
				if (GUI.Button ( new Rect ( 0, 0, Screen.width, Screen.height ), "Close\n\ninterstitial" ))
				{
					interstitial_visible = false;
					InterstitialClosed ( );
				}
			}

			if (rewarded_visible)
			{
				if (GUI.Button ( new Rect ( 0, 0, Screen.width, Screen.height * 0.5f ), "Close\n\nrewarded" ))
				{
					rewarded_visible = false;
					RewardedClosed ( );
				}
				if (GUI.Button ( new Rect ( 0, Screen.height * 0.5f, Screen.width, Screen.height * 0.5f ), "Complete\n\nrewarded" ))
				{
					rewarded_visible = false;
					RewardedComplete ( );
					RewardedClosed ( );
				}
			}
		}


		Vector2 GetRectPos (Vector2 ad_size, AdsInterop.AdPosition ad_position)
		{
			Vector2 res = Vector2.zero;
			switch (ad_position)
			{
				case AdsInterop.AdPosition.Bottom_Centered:
					res.y = Screen.height - ad_size.y;
					res.x = Screen.width * 0.5f - ad_size.x * 0.5f;
					break;
				case AdsInterop.AdPosition.Top_Centered:
					res.y = 0;
					res.x = Screen.width * 0.5f - ad_size.x * 0.5f;
					break;
				case AdsInterop.AdPosition.Bottom_Left:
					res.y = Screen.height - ad_size.y;
					res.x = 0;
					break;
				case AdsInterop.AdPosition.Bottom_Right:
					res.y = Screen.height - ad_size.y;
					res.x = Screen.width - ad_size.x;
					break;
				case AdsInterop.AdPosition.Top_Left:
					res.y = 0;
					res.x = 0;
					break;
				case AdsInterop.AdPosition.Top_Right:
					res.y = 0;
					res.x = Screen.width - ad_size.x;
					break;
				case AdsInterop.AdPosition.Middle_Centered:
					res.y = Screen.height * 0.5f - ad_size.y * 0.5f;
					res.x = Screen.width * 0.5f - ad_size.x * 0.5f;
					break;
				case AdsInterop.AdPosition.Middle_Left:
					res.y = Screen.height * 0.5f - ad_size.y * 0.5f;
					res.x = 0;
					break;
				case AdsInterop.AdPosition.Middle_Right:
					res.y = Screen.height * 0.5f - ad_size.y * 0.5f;
					res.x = Screen.width - ad_size.x;
					break;
			}
			return res;
		}


		private void InterstitialClosed ()
		{
			if (OnInterstitialClosed != null)
			{
				OnInterstitialClosed ( );
			}
		}
		private void RewardedComplete ()
		{
			if (OnRewardedCompleted != null)
			{
				OnRewardedCompleted ( );
			}
		}
		private void RewardedClosed ()
		{
			if (OnRewardedClosed != null)
			{
				OnRewardedClosed ( );
			}
		}

		#endregion

		#region General Methods

		public new string ToString ()
		{
			return GetNetworkType ( ).ToString ( );
		}

		protected void LogMessage (string message)
		{
			AdsManager.LogMessage ( this.GetType ( ).ToString ( ) + ": " + message );
		}

		public void DisableAds ()
		{
			banner_visible = native_visible = ads_enabled = false;
			//rewarded stays to keep user receiving bonuses for watching ad
		}

		public void EnableAds ()
		{
			ads_enabled = true;
			if (OnBannerReadyChanged != null)
			{
				OnBannerReadyChanged ( );
			}
			if (OnNativeReadyChanged != null)
			{
				OnNativeReadyChanged ( );
			}
		}

		virtual public AdNetwork GetNetworkType ()
		{
			return net;
		}


		virtual public ProviderParams [] GetNecessaryParams ()
		{
			return necessary_params;
		}

		public void Init (bool ads_enabled, bool children_tagged, bool for_families, object settings)
		{
			this.ads_enabled = ads_enabled;
			LogMessage ( "Initialising with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + children_tagged + ", is_designed_for_families=" + for_families );
		}

		public void SetHomeAdsIDs (ProviderParams param, string [] ids)
		{
			LogMessage ( "Setting " + param + " count = " + ids.Length );
		}

		#endregion

		#region Banner

		public bool IsBannerAdAvailable ()
		{
			return ads_enabled;
		}

		public void RefreshBannerAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
		{
			banner_position = ad_pos;
			banner_size = new Vector2 ( ad_size.w, ad_size.h );
		}

		public void ShowBannerAd (bool show)
		{
			if (show)
			{
				ShowBanner ( );
			}
			else
			{
				HideBanner ( );
			}
		}

		protected void ShowBanner ()
		{
			if (banner_visible = ads_enabled)
			{
				LogMessage ( "ShowBanner" );
			}
		}

		protected void HideBanner ()
		{
			LogMessage ( "HideBanner" );
			banner_visible = false;
		}

		#endregion

		#region Interstitial

		public bool IsInterstitialAdAvailable ()
		{
			return ads_enabled;
		}

		public bool ShowInterstitialAd ()
		{
			LogMessage ( "ShowInterstitialAd" );
			InterstitialShown ( );
			return interstitial_visible = ads_enabled;
		}

		private void InterstitialShown ()
		{
			if (OnInterstitialShown != null)
			{
				OnInterstitialShown ( );
			}
		}

		#endregion

		#region Rewarded

		public bool ShowRewardedAd ()
		{
			LogMessage ( "ShowRewardedAd" );
			RewardedShown ( );
			return rewarded_visible = true;
		}

		private void RewardedShown ()
		{
			if (OnRewardedShown != null)
			{
				OnRewardedShown ( );
			}
		}
		#endregion

		#region Native

		public bool IsNativeAdAvailable ()
		{
			return ads_enabled;
		}


		public void ShowNativeAd (bool show)
		{
			if (show)
			{
				ShowNativeAd ( );
			}
			else
			{
				HideNativeAd ( );
			}
		}

		public void RefreshNativeAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size)
		{
			native_position = ad_pos;
			native_size = new Vector2 ( ad_size.w, ad_size.h );
		}


		protected void ShowNativeAd ()
		{
			if (native_visible = ads_enabled)
			{
				LogMessage ( "ShowNativeAd" );
			}
		}

		protected void HideNativeAd ()
		{
			LogMessage ( "HideNativeAd" );
			native_visible = false;
		}

		#endregion

	}
}
#endif