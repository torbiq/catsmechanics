﻿using System;
using UnityEngine;

namespace PSV_Prototype
{
	public interface IAdProvider
	{
		event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		event Action<string>
			OnEventOccured;
		event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		void Init (bool ads_enabled, bool children_tagged, bool for_families, object settings);

		AdNetwork GetNetworkType ();

		ProviderParams [] GetNecessaryParams ();

		bool IsInterstitialAdAvailable (); //return false if not supported

		bool IsBannerAdAvailable (); //return false if not supported

		bool IsNativeAdAvailable (); //return false if not supported

		bool ShowInterstitialAd (); //return false if not supported

		bool ShowRewardedAd (); //return false if not supported

		void ShowBannerAd (bool show);

		void ShowNativeAd (bool show);

		void RefreshBannerAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size);

		void RefreshNativeAd (AdsInterop.AdPosition ad_pos, AdsInterop.AdSize ad_size);

		void DisableAds ();

		void EnableAds ();

		string ToString ();
	}
}