﻿using System;


namespace PSV_Prototype
{
	public static class AdsInterop
	{
		//events for Game
		public static event Action<bool, bool, bool> OnInitialiseAds;
		public static event Action<bool> OnShowInterstitialAd;
		public static event Action<bool> OnShowRewarded;
		public static event Action<bool> OnShowBannerAd;
		public static event Action<bool> OnShowNativeAd;
		public static event Action OnHideBannerAd;
		public static event Action OnHideNativeAd;
		public static event Action<AdPosition, AdSize> OnRefreshBannerAd;
		public static event Action<AdPosition, AdSize> OnRefreshNativeAd;
		//public static event Action<AdSize> OnResizeBannerAd;
		//public static event Action<AdSize> OnResizeNativeAd;
		public static event Action OnDisableAds;
		public static event Action OnEnableAds;

		//events for AdsMAnager
		public static event Action OnInterstitialShown;
		public static event Action OnInterstitialClosed;
		public static event Action OnRewardedShown;
		public static event Action OnRewardedCompleted;
		public static event Action OnRewardedClosed;

		//debug events for AdsManager
		public static event Action<string, bool> OnBannerEvent;
		public static event Action<string, bool> OnInterstitialEvent;
		public static event Action<string, bool> OnRewardedEvent;
		public static event Action<string, bool> OnNativeEvent;


		public enum AdPosition
		{
			Top_Centered,
			Top_Left,
			Top_Right,
			Bottom_Centered,
			Bottom_Left,
			Bottom_Right,
			Middle_Centered,
			Middle_Left,
			Middle_Right,
			Undefined
		}

		public class AdSize
		{
			public int
				w,
				h;

			public static AdSize
				Smart_banner = new AdSize ( 1, 1 ),
				Standard_banner_320x50 = new AdSize ( 320, 50 ),
				Large_banner_320x100 = new AdSize ( 320, 100 ),
				IAB_medium_rectangle_300x250 = new AdSize ( 300, 250 ),
				IAB_fullsize_banner_468x60 = new AdSize ( 468, 60 ),
				IAB_leaderboard_728x90 = new AdSize ( 728, 90 ),
				IAB_SkyScraper_160x600 = new AdSize ( 160, 600 ),
				Native_banner_minimal_320x80 = new AdSize ( 320, 80 );

			public AdSize (int w, int h)
			{
				this.w = w;
				this.h = h;
			}

			public static bool operator == (AdSize s1, AdSize s2)
			{
				bool res = false;
				if (ReferenceEquals ( s1, null ) && ReferenceEquals ( s2, null ))
				{
					res = true;
				}
				else if (!ReferenceEquals ( s1, null ) && !ReferenceEquals ( s2, null ))
				{
					res = s1.w == s2.w && s1.h == s2.h;
				}
				return res;
			}

			public static bool operator != (AdSize s1, AdSize s2)
			{
				bool res = true;
				if (ReferenceEquals ( s1, null ) && ReferenceEquals ( s2, null ))
				{
					res = false;
				}
				else if (!ReferenceEquals ( s1, null ) && !ReferenceEquals ( s2, null ))
				{
					res = s1.w != s2.w || s1.h != s2.h;
				}
				return res;
			}

			public override bool Equals (object obj)
			{
				return base.Equals ( obj );
			}

			public override int GetHashCode ()
			{
				return base.GetHashCode ( );
			}

			public override string ToString ()
			{
				return "(" + this.w + " : " + this.h + ")";
			}
		}

		#region Methods for Game

		public static void InitialiseAds (bool ads_enabled, bool children_tagged, bool for_families)
		{
			if (OnInitialiseAds != null)
			{
				OnInitialiseAds ( ads_enabled, children_tagged, for_families );
			}
		}

		public static void ShowBannerAd (bool show_home_ads = false)
		{
			if (OnShowBannerAd != null)
			{
				OnShowBannerAd ( show_home_ads );
			}
		}

		public static void HideBannerAd ()
		{
			if (OnHideBannerAd != null)
			{
				OnHideBannerAd ( );
			}
		}

		public static void RefreshBannerAd (AdPosition ad_pos, AdSize ad_size)
		{
			if (OnRefreshBannerAd != null)
			{
				OnRefreshBannerAd ( ad_pos, ad_size );
			}
		}


		public static void ShowNativeAd (bool show_home_ads = false)
		{
			if (OnShowNativeAd != null)
			{
				OnShowNativeAd ( show_home_ads );
			}
		}

		public static void HideNativeAd ()
		{
			if (OnHideNativeAd != null)
			{
				OnHideNativeAd ( );
			}
		}

		public static void RefreshNativeAd (AdPosition ad_pos, AdSize ad_size)
		{
			if (OnRefreshNativeAd != null)
			{
				OnRefreshNativeAd ( ad_pos, ad_size );
			}
		}



		public static void ShowInterstitialAd (bool show_home_ads = false)
		{
			if (OnShowInterstitialAd != null)
			{
				OnShowInterstitialAd ( show_home_ads );
			}
		}

		public static void ShowRewarded (bool show_home_ads = false)
		{
			if (OnShowRewarded != null)
			{
				OnShowRewarded ( show_home_ads );
			}
		}

		public static void DisableAds ()
		{
			if (OnDisableAds != null)
			{
				OnDisableAds ( );
			}
		}

		public static void EnableAds ()
		{
			if (OnEnableAds != null)
			{
				OnEnableAds ( );
			}
		}

		#endregion

		#region  methods for AdsManager

		public static void InterstitialShown ()
		{
			UnityMainThreadDispatcher.Instance.Enqueue ( () =>
			{
				if (OnInterstitialShown != null)
				{
					OnInterstitialShown ( );
				}
			} );
		}

		public static void InterstitialClosed ()
		{
			UnityMainThreadDispatcher.Instance.Enqueue ( () =>
			{
				if (OnInterstitialClosed != null)
				{
					OnInterstitialClosed ( );
				}
			} );
		}

		public static void RewardedShown ()
		{
			UnityMainThreadDispatcher.Instance.Enqueue ( () =>
			{
				if (OnRewardedShown != null)
				{
					OnRewardedShown ( );
				}
			} );
		}

		public static void RewardedCompleted ()
		{
			UnityMainThreadDispatcher.Instance.Enqueue ( () =>
			{
				if (OnRewardedCompleted != null)
				{
					OnRewardedCompleted ( );
				}
			} );
		}

		public static void RewardedClosed ()
		{
			UnityMainThreadDispatcher.Instance.Enqueue ( () =>
			{
				if (OnRewardedClosed != null)
				{
					OnRewardedClosed ( );
				}
			} );
		}
		#endregion

		#region Debug metthods for AdsManager

		public static void BannerEvent (string net, bool available)
		{
			if (OnBannerEvent != null)
			{
				OnBannerEvent ( net, available );
			}
		}

		public static void NativeEvent (string net, bool available)
		{
			if (OnNativeEvent != null)
			{
				OnNativeEvent ( net, available );
			}
		}

		public static void RewardedEvent (string net, bool available)
		{
			if (OnRewardedEvent != null)
			{
				OnRewardedEvent ( net, available );
			}
		}

		public static void InterstitialEvent (string net, bool available)
		{
			if (OnInterstitialEvent != null)
			{
				OnInterstitialEvent ( net, available );
			}
		}
		#endregion
	}
}