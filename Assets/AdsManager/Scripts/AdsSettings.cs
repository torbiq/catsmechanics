﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace PSV_Prototype
{
	public static class AdsSettings
	{
		private const string
			providers_order_param = "PROVIDERS_ORDER";

		private static AdNetwork []
			providers_order = new AdNetwork [] {
				AdNetwork.NeatPlug,
				//AdNetwork.GoogleAds,
				//AdNetwork.HomeAds,
			};


		//set here your settings received from providers
		//comment unused
		static Dictionary<ProviderParams, string> InitialParams = new Dictionary<ProviderParams, string>
		{
#if UNITY_ANDROID

            { ProviderParams.NEATPLUG_BANNER_ID, "ca-app-pub-3940256099942544/6300978111" },			//for debug only - change to your own projet's ids
			{ ProviderParams.NEATPLUG_INTERSTITIAL_ID, "ca-app-pub-3940256099942544/4411468910" },		//for debug only - change to your own projet's ids

			//{ ProviderParams.HOMEADS_REWARDED_ID, "ca-app-pub-1022958838828668/4512156633" },         //HippoPhotostudio id (only for tests)

            { ProviderParams.HOMEADS_BANNER_ID,"ca-app-pub-1022958838828668/3557542231" },              //home ads id (used by default in all projects)
            { ProviderParams.HOMEADS_INTERSTITIAL_ID, "ca-app-pub-1022958838828668/5353405837" },       //home ads id (used by default in all projects)

			// uncomment ids below if using one of ad type listed there
            //{ ProviderParams.HOMEADS_REWARDED_ID, "ca-app-pub-1022958838828668/5149644633" },       //home ads id (used by default in all projects)
            //{ ProviderParams.HOMEADS_NATIVE_ID, "ca-app-pub-1022958838828668/8311479033" },       //home ads id (used by default in all projects)


#elif UNITY_IPHONE
            //{ ProviderParams.NEATPLUG_BANNER_ID, "" },
            //{ ProviderParams.NEATPLUG_INTERSTITIAL_ID, "" },

            //{ ProviderParams.GOOGLEADS_BANNER_ID,"" },
            //{ ProviderParams.GOOGLEADS_INTERSTITIAL_ID, "" },
            //{ ProviderParams.GOOGLEADS_NATIVE_ID, "" },
            //{ ProviderParams.GOOGLEADS_REWARDED_ID, "" },

            //{ ProviderParams.HOMEADS_BANNER_ID,"" },
            //{ ProviderParams.HOMEADS_INTERSTITIAL_ID, "" },
            //{ ProviderParams.HOMEADS_NATIVE_ID, "" },
            //{ ProviderParams.HOMEADS_REWARDED_ID, "" },
#endif
		};

		public static void Init ()
		{
			LogMessage ( "AdsSettings initialize" );
			LoadSavedParams ( );
			FirebaseManager.FetchData ( );
			HomeAdsSettings.Init ( );
		}


		public static void LoadSavedParams ()
		{
			LoadInitialParams ( );
			LoadProvidersOrder ( );
		}

		private static void LoadInitialParams ()
		{
			//taking settings from scriptable object
			ProjectSettingsContainer.ProviderSettings [] settings = null;
#if UNITY_ANDROID
			settings = ProjectSettingsManager.settings.android_provider_settings;
#elif UNITY_IPHONE
			settings = ProjectSettingsManager.settings.ios_provider_settings;
#endif

			if (settings != null)
			{
				for (int i = 0; i < settings.Length; i++)
				{
					SetInitialParam ( settings [i].param, settings [i].value );
				}
			}

			Array A = Enum.GetValues ( typeof ( ProviderParams ) );
			for (int i = 0; i < A.Length; i++)
			{
				ProviderParams param = (ProviderParams) Enum.Parse ( typeof ( ProviderParams ), A.GetValue ( i ).ToString ( ) );
				string val = FirebaseManager.GetRemoteParam ( param.ToString ( ) );
				SetInitialParam ( param, val );
			}
		}


		private static void SetInitialParam (ProviderParams key, string val)
		{
			if (!string.IsNullOrEmpty ( val ))
			{
				if (InitialParams.ContainsKey ( key ))
				{
					InitialParams [key] = val;
				}
				else
				{
					InitialParams.Add ( key, val );
				}
			}
		}



		private static void LoadProvidersOrder ()
		{
			string val = FirebaseManager.GetRemoteParam ( providers_order_param );
			List<AdNetwork> new_order = new List<AdNetwork> ( );
			if (!string.IsNullOrEmpty ( val ))
			{
				string [] vals = val.Split ( ',' );
				for (int i = 0; i < vals.Length; i++)
				{
					new_order.Add ( (AdNetwork) Enum.Parse ( typeof ( AdNetwork ), vals [i].ToString ( ) ) );
				}
			}
			else
			{
				//getting providers order, set through scriptable object
				new_order.AddRange ( ProjectSettingsManager.settings.providers_order );
			}
			if (new_order != null && new_order.Count > 0)
			{
				providers_order = new_order.ToArray ( );
			}
		}


		private static void LogMessage (string message, bool error = false)
		{
			AdsManager.LogMessage ( "AdsSettings: " + message, error );
		}

		#region Getters

		public static AdNetwork [] GetProvidersList ()
		{
#if UNITY_EDITOR
			return new AdNetwork [] { AdNetwork.HomeAds };
#elif UNITY_STANDALONE || UNITY_WEBGL
			return new AdNetwork [0];
#else
			return providers_order;
#endif
		}


		private static string GetLocalParam (string key)
		{
			return PlayerPrefs.GetString ( key, "" );
		}


		private static Dictionary<ProviderParams, string> GetParamsContainer ()
		{
			return InitialParams;
		}

		public static Dictionary<ProviderParams, string> GetSettings (ProviderParams [] _params)
		{
			Dictionary<ProviderParams, string> res = new Dictionary<ProviderParams, string> ( );
			for (int i = 0; i < _params.Length; i++)
			{
				string val = null;
				if (GetParamsContainer ( ).TryGetValue ( _params [i], out val ))
				{
					res.Add ( _params [i], val );
				}
			}
			return res;
		}

		#endregion

		#region Setters

		private static void SetLocalParam (string key, string value)
		{
			LogMessage ( "Setting param " + key + " = " + value );
			PlayerPrefs.SetString ( key, value );
			PlayerPrefs.Save ( );
		}

		#endregion



		public static void ReadFromString (this float target, string val)
		{
			float new_val;
			if (float.TryParse ( val, out new_val ))
			{
				target = new_val;
			}
		}

		public static void ReadFromString (this bool target, string val)
		{
			bool new_val;
			if (!string.IsNullOrEmpty ( val ) && bool.TryParse ( val, out new_val ))
			{
				target = new_val;
			}
		}

		public static void ReadFromString (this int target, string val)
		{
			int new_val;
			if (!string.IsNullOrEmpty ( val ) && int.TryParse ( val, out new_val ))
			{
				target = new_val;
			}
		}
	}
}