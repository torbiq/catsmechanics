﻿using UnityEngine;
using System.Collections.Generic;

namespace PSV_Prototype
{
    public class AdEventsListener :MonoBehaviour
    {
        public static System.Action<string, EventSource, bool> OnAdEvent;

        public static AdEventsListener Instance;

        public enum EventSource
        {
            Banner,
            Interstitial,
            Rewarded,
            Native,
        }


        private Dictionary<string, bool>
            banner_ready = new Dictionary<string, bool> ( ),
            interstitial_ready = new Dictionary<string, bool> ( ),
            native_ready = new Dictionary<string, bool> ( ),
            rewarded_ready = new Dictionary<string, bool> ( );


		//[SerializeField]
		//private EventSource src;
		//[SerializeField]
		//private string net;
		//[SerializeField]
		//private bool available;

		//[ContextMenu ( "MarkToggle" )]
		//void MarkToggle ()
		//{
		//	AdEvent ( net, src, available );
		//}


		private void Awake ()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad ( this.gameObject );
                Subscribe ( );
			}
			else
			{
				Debug.LogWarning ( "Not allowed multiple instances of AdEventsListener" );
				Destroy ( gameObject );
			}
		}


        void Subscribe ()
        {
            AdsInterop.OnBannerEvent += AdsInterop_OnBannerEvent;
            AdsInterop.OnInterstitialEvent += AdsInterop_OnInterEvent;
            AdsInterop.OnNativeEvent += AdsInterop_OnNativeEvent;
            AdsInterop.OnRewardedEvent += AdsInterop_OnRewardedEvent;
        }



        void Unsubscribe ()
        {
            AdsInterop.OnBannerEvent -= AdsInterop_OnBannerEvent;
            AdsInterop.OnInterstitialEvent -= AdsInterop_OnInterEvent;
            AdsInterop.OnNativeEvent -= AdsInterop_OnNativeEvent;
            AdsInterop.OnRewardedEvent -= AdsInterop_OnRewardedEvent;
        }


        private void AdsInterop_OnRewardedEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Rewarded, available );
        }

        private void AdsInterop_OnNativeEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Native, available );
        }

        private void AdsInterop_OnInterEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Interstitial, available );
        }

        private void AdsInterop_OnBannerEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Banner, available );
        }


        private void AdEvent (string net, EventSource src, bool available)
        {
            Dictionary<string, bool> dic = GetAdReady ( src );
            if (!dic.ContainsKey ( net ))
            {
                dic.Add ( net, available );
            }
            else
            {
                dic [net] = available;
            }

            if (OnAdEvent != null)
            {
                OnAdEvent ( net, src, available );
            }
        }


        public bool GetSrcReady (EventSource src)
        {
            bool res = false;
            Dictionary<string, bool> dic = GetAdReady ( src );
            foreach (var item in dic)
            {
                if (item.Value)
                {
                    res = true;
                    break;
                }
            }
            return res;
        }


        public Dictionary<string, bool> GetAdReady (EventSource src)
        {
            Dictionary<string, bool> res = new Dictionary<string, bool> ( );
            switch (src)
            {
                case EventSource.Banner:
                    res = banner_ready;
                    break;
                case EventSource.Interstitial:
                    res = interstitial_ready;
                    break;
                case EventSource.Native:
                    res = native_ready;
                    break;
                case EventSource.Rewarded:
                    res = rewarded_ready;
                    break;
            }
            return res;
        }

       

    }
}