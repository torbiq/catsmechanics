﻿namespace PSV_Prototype
{
    public interface IHomeAdsProvider : IAdProvider
    {
        void SetHomeAdsIDs (ProviderParams param, string [] ids);
    }
}