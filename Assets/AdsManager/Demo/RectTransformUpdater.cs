﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class RectTransformUpdater : MonoBehaviour
{
	public UnityEvent OnDimensionsChanged;


	void OnRectTransformDimensionsChange()
	{
		if (OnDimensionsChanged != null)
		{
			OnDimensionsChanged.Invoke();
		}
	}
}
