﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using PSV_Prototype;

public class AdEventsGUI :MonoBehaviour
{

    public GameObject
        toggle_prefab;

    public Transform
        banner_holder,
        native_holder,
        interstitial_holder,
        rewarded_holder;

    private Dictionary<string, Toggle> toggles;


    private void Awake ()
    {
        toggles = new Dictionary<string, Toggle> ( );
    }

    void OnEnable ()
    {
        System.Array src = System.Enum.GetValues ( typeof ( AdEventsListener.EventSource ) );
        foreach (var s in src)
        {
            AdEventsListener.EventSource _src = (AdEventsListener.EventSource) s;
            Dictionary<string, bool> dic = AdEventsListener.Instance.GetAdReady ( _src );
            foreach (var item in dic)
            {
                MarkToggle ( item.Key, _src, item.Value );
            }
        }
        Subscribe ( );
    }

    void OnDisable ()
    {
        Unsubscribe ( );
    }

    void Subscribe ()
    {
        AdEventsListener.OnAdEvent += MarkToggle;
    }

    void Unsubscribe ()
    {
        AdEventsListener.OnAdEvent -= MarkToggle;
    }


    private void MarkToggle (string net, AdEventsListener.EventSource src, bool available)
    {
        string key = GetKey ( net, src );
        if (!toggles.ContainsKey ( key ))
        {
            toggles.Add ( key, CreateToggle ( net, GetParent ( src ) ) );
        }
        toggles [key].isOn = available;
        if (toggles [key].targetGraphic)
        {
            toggles [key].targetGraphic.color = available ? Color.green : Color.red;
        }
    }

    private Transform GetParent (AdEventsListener.EventSource src)
    {
        Transform res = null;
        switch (src)
        {
            case AdEventsListener.EventSource.Banner:
                res = banner_holder;
                break;
            case AdEventsListener.EventSource.Interstitial:
                res = interstitial_holder;
                break;
            case AdEventsListener.EventSource.Rewarded:
                res = rewarded_holder;
                break;
            case AdEventsListener.EventSource.Native:
                res = native_holder;
                break;
        }
        return res;
    }


    private string GetKey (string net, AdEventsListener.EventSource src)
    {
        return net + "_" + src.ToString ( );
    }

    private Toggle CreateToggle (string label, Transform parent)
    {
        Toggle res = null;
        if (toggle_prefab != null && parent != null)
        {
            GameObject clone = Instantiate ( toggle_prefab, parent.position, parent.rotation ) as GameObject;
            clone.SetActive ( true );
            clone.transform.SetParent ( parent );
            clone.transform.localScale = Vector3.one;
            Text label_component = clone.GetComponentInChildren<Text> ( );
            if (label_component != null)
            {
                label_component.text = label;
            }
            res = clone.GetComponent<Toggle> ( );
        }
        return res;
    }


}
