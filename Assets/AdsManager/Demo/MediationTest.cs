﻿#define GOOGLE_ADS_PRESENT

using System.Collections.Generic;
using UnityEngine;
using PSV_Prototype;
using UnityEngine.UI;


public class MediationTest :MonoBehaviour
{
	private const string
		admob_test_banner = "ca-app-pub-3940256099942544/6300978111",
		admob_test_interstitial = "ca-app-pub-3940256099942544/4411468910",
		leadbolt_interstitial = "ca-app-pub-8211302556993806/1084033972",       //sdk's test id
		leadbolt_rewarded = "ca-app-pub-8211302556993806/4244449976";           //sdk's test id

	public Text
		btn_lbl,
		logger;

	private Dropdown
		dropdown;



	IAdProvider
		ad_provider =
#if UNITY_IPHONE
		new GoogleAdsProvider ( );
#elif UNITY_ANDROID
#if GOOGLE_ADS_PRESENT
		new GoogleAdsProvider ( ); //use thiss to check few sets of IDs during same session
#else
		new NeatPlugProvider ( );  //can start using only one set of IDs per rsession
#endif
#elif UNITY_EDITOR
		new DummyProvider();
#else
	null;
#endif


	public class AdID
	{
		public string
			banner_id,
			inter_id;
		public AdID (string banner, string inter)
		{
			banner_id = banner;
			inter_id = inter;
		}
	}


	Dictionary<string, AdID> AdNetworkParams = new Dictionary<string, AdID>
	{
		{"AdMob", new AdID("","")},
		{ "Inner-Active", new AdID("ca-app-pub-4698438002143937/2736723607","ca-app-pub-4698438002143937/1259990405")},
		{ "Inner-Active-Emergency", new AdID("ca-app-pub-4698438002143937/7259164804","ca-app-pub-4698438002143937/8735898008")},
		{ "Inner-Active-BD", new AdID("ca-app-pub-4698438002143937/1352232009","ca-app-pub-4698438002143937/2828965200")},
		//{"MobFox_p", new AdID("ca-app-pub-1022958838828668/4024035032","ca-app-pub-1022958838828668/1070568633")},		//psv
		{"MobFoxSDK", new AdID ("ca-app-pub-4698438002143937/6497883600","ca-app-pub-4698438002143937/7974616801") },
		{"MobFox_c", new AdID("ca-app-pub-4698438002143937/5565134401","ca-app-pub-4698438002143937/8518600809")},      //challenger
		{"MobFox_supermarket", new AdID("ca-app-pub-4698438002143937/7594118409","ca-app-pub-4698438002143937/9070851604")},      //Supermarket MobFox 
#if UNITY_ANDROID
		{"InMobi_p", new AdID("ca-app-pub-1022958838828668/5843688633","ca-app-pub-1022958838828668/8797155037")},		//psv
		{"InMobi_c", new AdID("ca-app-pub-4698438002143937/4808254802","ca-app-pub-4698438002143937/1854788405")},		//challenger
		{"ChartBoost", new AdID("","ca-app-pub-4698438002143937/9098853604")},		//challenger (appId="4f7b433509b6025804000002" | appSignature="dd2d41b69ac01b80f443f5b6cf06096d457f82bd")
		{"ChartBoost_Live", new AdID("","ca-app-pub-4698438002143937/4344664800")},		//challenger from account
		{"ChartBoost_p", new AdID("","ca-app-pub-1022958838828668/2002891837")},	//psv
		{"UnityAds_c", new AdID("","ca-app-pub-4698438002143937/3344868003")},		//challenger (Game ID: 14851 | Zone ID: defaultZone)
		//{"UnityAds_p", new AdID("","ca-app-pub-1022958838828668/4964861435")},		//psv		
		{"Vungle_p", new AdID("","ca-app-pub-1022958838828668/6135243034")},		//psv
		{"Vungle_c", new AdID("","ca-app-pub-4698438002143937/8193870001")},			//challenger
		{"Vungle_CarService", new AdID("","ca-app-pub-4698438002143937/9340918805")},			//CarService
		{"AdColonySDK", new AdID("","ca-app-pub-4698438002143937/3285882003")},		//challenger
		//{"AdColony_c", new AdID("","ca-app-pub-4698438002143937/3567891600")},		//challenger
		//{"AdColony_live", new AdID("","ca-app-pub-4698438002143937/5369671202")},		//challenger - live ads
		{"AdColony_CarService", new AdID("","ca-app-pub-4698438002143937/8151133207")},     //CarService
		//{"Leadbolt_c", new AdID("","ca-app-pub-4698438002143937/3253809600")},		//challenger
		//{"Leadbolt", new AdID("", leadbolt_interstitial)},							//sdk's mediation test id
		//{"Fyber", new AdID("","ca-app-pub-4698438002143937/4532066402")},			//challenger
		//{"MoPub", new AdID("ca-app-pub-4698438002143937/4868850000","ca-app-pub-4698438002143937/6345583206")},
		//{"MoPubHTML", new AdID("ca-app-pub-4698438002143937/5787180000","ca-app-pub-4698438002143937/1217379609")},		//sdk's IDs
		//{"MoPubMRAID", new AdID("ca-app-pub-4698438002143937/7263913203","ca-app-pub-4698438002143937/8740646402")},		//sdk's IDs
		//{"TapIt_inter", new AdID("ca-app-pub-4698438002143937/7357338008","ca-app-pub-4698438002143937/2787537607")},		//android sdk's IDs
		//{"TapIt_video", new AdID("ca-app-pub-4698438002143937/7357338008","ca-app-pub-4698438002143937/8834071209")},		//android sdk's IDs
		{ "FB_c", new AdID("ca-app-pub-4698438002143937/8227977609","ca-app-pub-4698438002143937/4635382801")},				//challenger
		{ "FB Prankster", new AdID("ca-app-pub-4698438002143937/3475505305","ca-app-pub-4698438002143937/6994102001")},		//com.PSVGamestudio.PranksterDjy
		{ "FB_Kindergarten", new AdID("ca-app-pub-4698438002143937/4554216475","ca-app-pub-4698438002143937/8876604860")},	//com.hippo.kindergartenquest
		{ "FB_Test", new AdID("ca-app-pub-4698438002143937/2181444007","ca-app-pub-4698438002143937/5274511201")},          //'YOUR_PLACEMENT_ID'
		{ "AerServ_SDK", new AdID( "ca-app-pub-4698438002143937/1834464035", "ca-app-pub-4698438002143937/2706183266")},	// '1024876' '1000741'
		{ "AerServ_c", new AdID( "ca-app-pub-4698438002143937/4238100242", "ca-app-pub-4698438002143937/6672691896")},		//challenger '1026597' '1026598'

#elif UNITY_IPHONE		
		{"MobFox", new AdID("ca-app-pub-6224828323195096/5677489566","ca-app-pub-6224828323195096/7876284361")},		//sdk adapter sample
		{"InMobi", new AdID("ca-app-pub-4698438002143937/5087992804","ca-app-pub-4698438002143937/6564726005")},		//challenger from ios sdk (Account ID "4028cb8b2c3a0b45012c406824e800ba" | banner ID "1447912324502" | interstitial ID "1446377525790")
		{"ChartBoost", new AdID("","ca-app-pub-4698438002143937/6005786406")},		//challenger (appId="4f21c409cd1cb2fb7000001b" | appSignature="92e2de2fd7070327bdeb54c15a5295309c6fcd2d")
		{"UnityAds", new AdID("","ca-app-pub-4698438002143937/5501952006")},		//challenger (Game ID: 1384703 | Zone ID: video)
		{"Vungle", new AdID("","ca-app-pub-4698438002143937/9670603206")},			//challenger
		{"AdColony", new AdID("","ca-app-pub-4698438002143937/3484468802")},		//challenger
		{"AdColony live", new AdID("","ca-app-pub-4698438002143937/8040812408")},		//challenger sdk V4VC test ids
		{"AdColony3", new AdID("","ca-app-pub-4698438002143937/7482409201")},		//challenger sdk Basic test ids
		//{"Leadbolt", new AdID("","ca-app-pub-8211302556993806/4037500371")},		//sdk's IDs
		//{"MoPub", new AdID("ca-app-pub-4698438002143937/3250686003","ca-app-pub-4698438002143937/3390286800")},		//sdk's IDs
		//{"MoPubMRAID", new AdID("ca-app-pub-4698438002143937/5787180000","ca-app-pub-4698438002143937/1217379609")},		//sdk's IDs
		//{"MoPubHTML", new AdID("ca-app-pub-4698438002143937/7263913203","ca-app-pub-4698438002143937/8740646402")},		//sdk's IDs
		{"TapIt_inter", new AdID("ca-app-pub-4698438002143937/3985069204","ca-app-pub-4698438002143937/5461802407")},		//IOS sdk's IDs
		{"TapIt_video", new AdID("ca-app-pub-4698438002143937/3985069204","ca-app-pub-4698438002143937/6938535600")},		//IOS sdk's IDs
#endif
	};

	/// <summary>
	/// Chartboost IOS dependencies
	/// - StoreKit, Foundation, CoreGraphics, UIKit
	/// InMobi dependencies
	/// - libsqlite3.0.tbd, libz.tbd, WebKit.framework
	/// - Add the -ObjC flag
	/// AdColony dependencies
	/// - libz.1.2.5.tbd
	/// - AdColony.framework
	/// - AdSupport.framework
	/// - AudioToolbox.framework
	/// - AVFoundation.framework
	/// - CoreTelephony.framework
	/// - EventKit.framework
	/// - JavaScriptCore.framework (Set to Optional)
	/// - MessageUI.framework
	/// - Social.framework
	/// - StoreKit.framework
	/// - SystemConfiguration.framework
	/// - WatchConnectivity.framework (Set to Optional)
	/// - WebKit.framework (Set to Optional)
	/// MoPub dependencies
	/// - AdSupport.framework
	/// - AVFoundation.framework
	/// - CoreMedia.framework
	/// - CoreTelephony.framework
	/// - SystemConfiguration.framework
	/// - StoreKit.framework
	/// - libz.tbd
	/// LeadBolt dependencies
	/// - AdSupport.framework
	/// - AVFoundation.framework
	/// - CoreMedia.framework
	/// - CoreTelephony.framework
	/// - StoreKit.framework
	/// - SystemConfiguration.framework
	/// - libz.tbd
	/// </summary>


	void Awake ()
	{
		dropdown = GetComponentInChildren<Dropdown> ( );
	}

	private void OnEnable ()
	{
		if (DelayedCallHandler.Instance == null)
		{
			gameObject.AddComponent<DelayedCallHandler> ( );
		}
#if UNITY_ANDROID
		if (ad_provider != null && ad_provider.GetNetworkType ( ) == AdNetwork.NeatPlug)
		{
			if (FindObjectOfType<AdmobAdAgent> ( ) == null)
			{
				gameObject.AddComponent<AdmobAdAgent> ( );
			}
		}
#endif
	}


	void InitProvider ()
	{
		if (ad_provider != null)
		{
			ad_provider.OnInterstitialEvent += OnInterstitialEvent;
			ad_provider.OnInterstitialShown += OnInterstitialShown;
			ad_provider.OnInterstitialClosed += OnInterstitialClosed;
		}
	}

	private void OnInterstitialShown ()
	{
		OnMessageLogged ( "*** Interstitial shown ***" );
		HideBanner ( );
	}


	private void OnInterstitialClosed ()
	{
		OnMessageLogged ( "*** Interstitial closed ***" );
	}

	private void OnInterstitialEvent (AdNetwork arg1, bool arg2)
	{
		EnableButton ( arg2 );
	}

	void SetOptions ()
	{

		if (dropdown != null)
		{
			List<Dropdown.OptionData> options = new List<Dropdown.OptionData> ( ) { new Dropdown.OptionData ( "None" ) };
			foreach (var item in AdNetworkParams)
			{
				if (string.IsNullOrEmpty ( item.Value.banner_id ))
					item.Value.banner_id = admob_test_banner;
				if (string.IsNullOrEmpty ( item.Value.inter_id ))
					item.Value.inter_id = admob_test_interstitial;
				options.Add ( new Dropdown.OptionData ( item.Key ) );
			}
			dropdown.ClearOptions ( );
			dropdown.AddOptions ( options );
		}
	}


	void Start ()
	{
		EnableButton ( false );
		InitProvider ( );
		SetOptions ( );
		AdsManager.OnMessageLogged += OnMessageLogged;
		////for demo app
		//dropdown.gameObject.SetActive ( false );
		//SetProviderParams ( "AdMob" );
	}


	void OnMessageLogged (string message)
	{
		if (logger != null)
		{
			CheckLoggerLength ( );
			logger.text += System.DateTime.Now.ToLongTimeString ( ) + " " + message + '\n';
		}
		Debug.Log ( message );
	}


	void CheckLoggerLength ()
	{
		if (logger.text.Length > 16000)
		{
			logger.text.Remove ( 0, 6000 );
		}
	}

	public void ShowInter ()
	{
		if (ad_provider != null)
			ad_provider.ShowInterstitialAd ( );
	}

	public void ShowBanner ()
	{
		if (ad_provider != null)
			ad_provider.ShowBannerAd ( true );
	}

	public void HideBanner ()
	{
		if (ad_provider != null)
			ad_provider.ShowBannerAd ( false );
	}


	void EnableButton (bool param)
	{
		//btn.interactable = param;
		if (btn_lbl)
			btn_lbl.text = param ? "Show inter" : "Ad is loading";
	}



	public void OnDropdownChange (int val)
	{
		if (dropdown != null && val > 0 && val < dropdown.options.Count)
		{
			SetProviderParams ( dropdown.options [val].text );
		}
	}



	void SetProviderParams (string net)
	{
		Debug.Log ( "SetAdmobParams for " + net );
		if (ad_provider != null && AdNetworkParams.ContainsKey ( net ))
		{
			EnableButton ( false );
			//select provider params corresponding to banner and interstitial
			ProviderParams [] supported_params = ad_provider.GetNecessaryParams ( );
			Dictionary<ProviderParams, string> settings = new Dictionary<ProviderParams, string> ( );
			bool
				inter_set = false,
				banner_set = false;
			for (int i = 0; i < supported_params.Length; i++)
			{
				if (!banner_set && supported_params [i].ToString ( ).Contains ( "BANNER_ID" ))
				{
					settings.Add ( supported_params [i], AdNetworkParams [net].banner_id );
					banner_set = true;
				}
				if (!inter_set && supported_params [i].ToString ( ).Contains ( "INTERSTITIAL_ID" ))
				{
					settings.Add ( supported_params [i], AdNetworkParams [net].inter_id );
					inter_set = true;
				}
			}
			ad_provider.Init ( true, true, false, settings );
		}
	}


	public void GoToGame ()
	{
		HideBanner ( );
		Destroy ( gameObject );
		UnityEngine.SceneManagement.SceneManager.LoadScene ( Scenes.InitScene.ToString ( ) );
	}

}
