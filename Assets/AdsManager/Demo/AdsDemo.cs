﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using PSV_Prototype;

public class AdsDemo :MonoBehaviour
{
	public Toggle
		banner_toggle,
		home_toggle;


	public void ShowInterstitial ()
	{
		ManagerGoogle.Instance.ShowFullscreenBanner ( IsHome ( ), true );
	}

	public void ShowRewarded ()
	{
		ManagerGoogle.Instance.ShowRewardedVideoAd ( IsHome ( ) );
	}

	public void ShowBanner ()
	{
		bool is_home = IsHome ( );
		if (IsBanner ( ))
		{
			ManagerGoogle.Instance.ShowSmallBanner ( is_home );
		}
		else
		{
			ManagerGoogle.Instance.ShowNativeBanner ( is_home );
		}
	}

	public void HideBanner ()
	{
		if (IsBanner ( ))
		{
			ManagerGoogle.Instance.HideSmallBanner ( );
		}
		else
		{
			ManagerGoogle.Instance.HideNativeBanner ( );
		}
	}


	public void MoveAdsToTop ()
	{
		if (IsBanner ( ))
		{
			ManagerGoogle.Instance.RefreshSmallBanner ( AdsInterop.AdPosition.Top_Centered, AdsInterop.AdSize.Standard_banner_320x50 );
		}
		else
		{
			ManagerGoogle.Instance.RefreshNativeBanner ( AdsInterop.AdPosition.Top_Centered, new AdsInterop.AdSize ( 320, 150 ) );
		}
	}

	public void MoveAdsToBottom ()
	{
		if (IsBanner ( ))
		{
			ManagerGoogle.Instance.RefreshSmallBanner ( AdsInterop.AdPosition.Bottom_Centered, AdsInterop.AdSize.Standard_banner_320x50 );
		}
		else
		{
			ManagerGoogle.Instance.RefreshNativeBanner ( AdsInterop.AdPosition.Bottom_Centered, AdsInterop.AdSize.Native_banner_minimal_320x80 );
		}
	}

	private bool IsBanner ()
	{
		return banner_toggle.isOn;
	}

	private bool IsHome ()
	{
		return home_toggle.isOn;
	}



}
