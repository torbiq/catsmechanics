﻿#define FIREBASE_PRESENT


using System;
using System.Collections.Generic;
using UnityEditor;


[InitializeOnLoad]
public class NeatPlugDependencies :AssetPostprocessor
{

	public static string
		pugins_version =
		"LATEST";
		//"10.2.0";
		//"11.0.2";

	public static string
		support_version =
		"LATEST";
		//"24.0.0";
		//"25.2.0";


#if UNITY_ANDROID
	/// <summary>Instance of the PlayServicesSupport resolver</summary>
	public static object svcSupport;
#endif  // UNITY_ANDROID

	/// Initializes static members of the class.
	static NeatPlugDependencies ()
	{
		RegisterDependencies ( );
	}



	public static void RegisterDependencies ()
	{
#if UNITY_ANDROID
		RegisterAndroidDependencies ( );
#elif UNITY_IOS
        //RegisterIOSDependencies();
#endif
	}

#if UNITY_ANDROID
	public static void RegisterAndroidDependencies ()
	{

		Type playServicesSupport = Google.VersionHandler.FindClass (
		  "Google.JarResolver", "Google.JarResolver.PlayServicesSupport" );
		if (playServicesSupport == null)
		{
			return;
		}
		svcSupport = svcSupport ?? Google.VersionHandler.InvokeStaticMethod (
		  playServicesSupport, "CreateInstance",
		  new object [] {
		  "NeatPlug",
		  EditorPrefs.GetString("AndroidSdkRoot"),
		  "ProjectSettings"
		  } );

		Google.VersionHandler.InvokeInstanceMethod (
			svcSupport, "DependOn",
			new object [] {
				"com.google.android.gms",
				"play-services-ads",
				pugins_version
			},
			namedArgs: new Dictionary<string, object> ( ) {
				{"packageIds", new string[] {
						"extra-google-m2repository",
						"extra-android-m2repository"} },
				{"repositories", null }
			} );


#if FIREBASE_PRESENT
		Google.VersionHandler.InvokeInstanceMethod (
			svcSupport, "DependOn",
			new object [] {
					"com.google.firebase",
					"firebase-crash",
					pugins_version
			},
			namedArgs: new Dictionary<string, object> ( ) {
					{ "packageIds", new string [] {
						"extra-google-m2repository",
						"extra-android-m2repository"
					} },
					{ "repositories", null }
			} );
#endif

		//Google.VersionHandler.InvokeInstanceMethod (
		//	svcSupport, "DependOn",
		//	new object [] {
		//		"com.android.support",
		//		"support-v4",
		//		support_version
		//	},
		//	namedArgs: new Dictionary<string, object> ( ) {
		//		{"packageIds", new string[] {
		//				"extra-google-m2repository",
		//				"extra-android-m2repository"} },
		//		{"repositories", null }
		//	} );


		//Google.VersionHandler.InvokeInstanceMethod (
		//			svcSupport, "DependOn",
		//			new object [] {
		//		"com.android.support",
		//		"appcompat-v7",
		//		support_version
		//			},
		//			namedArgs: new Dictionary<string, object> ( ) {
		//			{
		//				"packageIds",
		//				new string[] { "extra-android-m2repository" }
		//			}
		//				}
		//			);


		//Google.VersionHandler.InvokeInstanceMethod (
		//			svcSupport, "DependOn",
		//			new object [] {
		//		"com.android.support",
		//		"recyclerview-v7",
		//		support_version
		//			},
		//			namedArgs: new Dictionary<string, object> ( ) {
		//			{
		//				"packageIds",
		//				new string[] { "extra-android-m2repository" }
		//			}
		//				}
		//			);


		//Google.VersionHandler.InvokeInstanceMethod (
		//			svcSupport, "DependOn",
		//			new object [] {
		//		"com.android.support",
		//		"multidex",
		//		"LATEST"
		//			},
		//			namedArgs: new Dictionary<string, object> ( ) {
		//			{
		//				"packageIds",
		//				new string[] { "extra-android-m2repository" }
		//			}
		//				}
		//			);


	}
#endif


#if UNITY_IOS
    //public static void RegisterIOSDependencies ()
    //{

    //    Type iosResolver = Google.VersionHandler.FindClass (
    //        "Google.IOSResolver", "Google.IOSResolver" );
    //    if (iosResolver == null)
    //    {
    //        return;
    //    }


    //    Google.VersionHandler.InvokeStaticMethod (
    //      iosResolver, "AddPod",
    //      new object [] { "GooglePlayGames" },
    //      namedArgs: new Dictionary<string, object> ( ) {
    //  { "version", "5.0+" },
    //  { "bitcodeEnabled", false },
    //      } );
    //}

    //private static void OnPostprocessAllAssets (
    //    string [] importedAssets, string [] deletedAssets,
    //    string [] movedAssets, string [] movedFromPath)
    //{
    //    foreach (string asset in importedAssets)
    //    {
    //        if (asset.Contains ( "IOSResolver" ) ||
    //          asset.Contains ( "JarResolver" ))
    //        {
    //            RegisterDependencies ( );
    //            break;
    //        }
    //    }
    //}
#endif
}