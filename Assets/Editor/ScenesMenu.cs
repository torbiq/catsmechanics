﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public static class ScenesMenu {

    private static void OpenScene(string path)
    {
        OpenSceneMode openMode = OpenSceneMode.Single;
        if (EditorUtility.DisplayDialog("Select open scene mode", "Chose scene opening mode.", "Single", "Additive"))
        {
             Scene[] scenes = new Scene[SceneManager.sceneCount];
             for (int i = 0; i < scenes.Length; i++)
             {
                 scenes[i] = SceneManager.GetSceneAt(i);
             }
             EditorSceneManager.SaveModifiedScenesIfUserWantsTo(scenes);
        }
        else
        {
            openMode = OpenSceneMode.Additive;
        }
        EditorSceneManager.OpenScene(path, openMode);
    }

    [MenuItem("Scenes/MediationAdaptersTest.unity")]
    public static void Scene0()
    {
        OpenScene("Assets/AdsManager/Demo/MediationAdaptersTest.unity");
    }

    [MenuItem("Scenes/InitScene.unity")]
    public static void Scene1()
    {
        OpenScene("Assets/Game/Scenes/InitScene.unity");
    }

    [MenuItem("Scenes/PSV_Splash.unity")]
    public static void Scene2()
    {
        OpenScene("Assets/Game/Scenes/PSV_Splash.unity");
    }

    [MenuItem("Scenes/Melnitsa_Splash.unity")]
    public static void Scene3()
    {
        OpenScene("Assets/Game/Scenes/Melnitsa_Splash.unity");
    }

    [MenuItem("Scenes/MainMenu.unity")]
    public static void Scene4()
    {
        OpenScene("Assets/Game/Scenes/MainMenu.unity");
    }

    [MenuItem("Scenes/NewAdsDemo.unity")]
    public static void Scene5()
    {
        OpenScene("Assets/AdsManager/Demo/NewAdsDemo.unity");
    }

    [MenuItem("Scenes/Push.unity")]
    public static void Scene6()
    {
        OpenScene("Assets/Game/Scenes/Push.unity");
    }

    [MenuItem("Scenes/RateMeScene.unity")]
    public static void Scene7()
    {
        OpenScene("Assets/Game/Scenes/RateMeScene.unity");
    }

}
