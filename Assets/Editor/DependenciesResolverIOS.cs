using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode_PSV_Prototype;
using System.IO;
using System.Collections.Generic;

public class DependenciesResolverIOS :MonoBehaviour
{

	internal static void CopyAndReplaceDirectory (string srcPath, string dstPath)
	{
		if (Directory.Exists ( dstPath ))
			Directory.Delete ( dstPath );
		if (File.Exists ( dstPath ))
			File.Delete ( dstPath );

		Directory.CreateDirectory ( dstPath );

		foreach (var file in Directory.GetFiles ( srcPath ))
			File.Copy ( file, Path.Combine ( dstPath, Path.GetFileName ( file ) ) );

		foreach (var dir in Directory.GetDirectories ( srcPath ))
			CopyAndReplaceDirectory ( dir, Path.Combine ( dstPath, Path.GetFileName ( dir ) ) );
	}

	class IosDependency
	{
		public string framework;
		public bool is_optional;

		public IosDependency (string _framework, bool _is_optional = false)
		{
			framework = _framework;
			is_optional = _is_optional;
		}

		public override string ToString ()
		{
			return framework + ", optional=" + is_optional;
		}
	}


	static IosDependency []
		chartboost_deps = new IosDependency []{
			new IosDependency("StoreKit.framework"),
			new IosDependency("Foundation.framework"),
			new IosDependency("CoreGraphics.framework"),
			new IosDependency("UIKit.framework"),
		},
		inmobi_deps = new IosDependency []{
			new IosDependency("libsqlite3.0.tbd"),
			new IosDependency("libz.tbd"),
			new IosDependency("WebKit.framework", true),
		},
		vungle_deps = new IosDependency []{
			new IosDependency("AdSupport.framework"),
			new IosDependency("AudioToolbox.framework"),
			new IosDependency("AVFoundation.framework"),
			new IosDependency("CFNetwork.framework"),
			new IosDependency("CoreGraphics.framework"),
			new IosDependency("CoreMedia.framework"),
			new IosDependency("Foundation.framework"),
			new IosDependency("libz.tbd"),
			new IosDependency("libz.dylib"),
			new IosDependency("libsqlite3.tbd"),
			new IosDependency("MediaPlayer.framework"),
			new IosDependency("QuartzCore.framework"),
			new IosDependency("StoreKit.framework"),
			new IosDependency("SystemConfiguration.framework"),
			new IosDependency("UIKit.framework"),
			new IosDependency("WebKit.framework"),
		},
		adcolony_deps = new IosDependency []{
			new IosDependency("libz.1.2.5.tbd"),
			new IosDependency("AdColony.framework"),
			new IosDependency("AdSupport.framework"),
			new IosDependency("AudioToolbox.framework"),
			new IosDependency("AVFoundation.framework"),
			new IosDependency("CoreTelephony.framework"),
			new IosDependency("EventKit.framework"),
			new IosDependency("JavaScriptCore.framework",true), //(Set to Optional)
			new IosDependency("MessageUI.framework"),
			new IosDependency("Social.framework"),
			new IosDependency("StoreKit.framework"),
			new IosDependency("SystemConfiguration.framework"),
			new IosDependency("WatchConnectivity.framework",true), //(Set to Optional)
			new IosDependency("WebKit.framework",true), //(Set to Optional)
		},
		leadbolt_deps = new IosDependency []{
			new IosDependency("AdSupport.framework"),
			new IosDependency("AVFoundation.framework"),
			new IosDependency("CoreMedia.framework"),
			new IosDependency("CoreTelephony.framework"),
			new IosDependency("StoreKit.framework"),
			new IosDependency("SystemConfiguration.framework"),
			new IosDependency("libz.tbd"),
		};

	static IosDependency [] [] libs_to_include = new IosDependency [] []{
		chartboost_deps,
		inmobi_deps,
		vungle_deps,
		adcolony_deps,
		//leadbolt_deps,
	};


	//[MenuItem("HelpTools/BuildDependenciesList")]
	private static List<IosDependency> BuildDependenciesList ()
	{
		List<IosDependency> res = new List<IosDependency> ( );
		for (int i = 0; i < libs_to_include.Length; i++)
		{
			for (int j = 0; j < libs_to_include [i].Length; j++)
			{
				IosDependency candidate = libs_to_include [i] [j];
				IosDependency dep = res.Find ( X => X.framework == candidate.framework );
				if (dep == null)
				{
					res.Add ( candidate );
				}
				else
				{
					if (candidate.is_optional && !dep.is_optional)
					{
						dep.is_optional = true;
					}
				}
			}
		}
		Debug.Log ( PSV_Prototype.Utils.ConvertToString( res.ToArray ( )));
		return res;
	}


	[PostProcessBuild]
	public static void OnPostprocessBuild (BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iOS)
		{
			string projPath = PBXProject.GetPBXProjectPath ( path );
			PBXProject proj = new PBXProject ( );

			proj.ReadFromString ( File.ReadAllText ( projPath ) );
			string target = proj.TargetGuidByName ( "Unity-iPhone" );

			List<IosDependency> libs = BuildDependenciesList ( );



			for (int i = 0; i < libs.Count; i++)
			{
				proj.AddFrameworkToProject ( target, libs [i].framework, libs [i].is_optional );
			}

			//CopyAndReplaceDirectory ( "Assets/Lib/mylib.framework", Path.Combine ( path, "Frameworks/mylib.framework" ) );
			//proj.AddFileToBuild ( target, proj.AddFile ( "Frameworks/mylib.framework", "Frameworks/mylib.framework", PBXSourceTree.Source ) );


			//var fileName = "my_file.xml";
			//var filePath = Path.Combine ( "Assets/Lib", fileName );
			//File.Copy ( filePath, Path.Combine ( path, fileName ) );
			//proj.AddFileToBuild ( target, proj.AddFile ( fileName, fileName, PBXSourceTree.Source ) );

			proj.SetBuildProperty ( target, "ENABLE_BITCODE", "No" );


			//proj.SetBuildProperty ( target, "FRAMEWORK_SEARCH_PATHS", "$(inherited)" );
			//proj.AddBuildProperty ( target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Frameworks" );


			File.WriteAllText ( projPath, proj.WriteToString ( ) );


			// Get plist
			string plistPath = path + "/Info.plist";
			PlistDocument plist = new PlistDocument ( );
			plist.ReadFromString ( File.ReadAllText ( plistPath ) );

			// Get root
			PlistElementDict rootDict = plist.root;

			// Change value of CFBundleVersion in Xcode plist
			PlistElementDict NSAppTransportSecurity = rootDict ["NSAppTransportSecurity"].AsDict ( );
			if (NSAppTransportSecurity == null)
			{
				Debug.Log ( "XcodeProjectMod: creating new dict NSAppTransportSecurity" );
				NSAppTransportSecurity = rootDict.CreateDict ( "NSAppTransportSecurity" );
			}
			else
			{
				Debug.Log ( "XcodeProjectMod: dict NSAppTransportSecurity already exists, Count=" + NSAppTransportSecurity.values.Count );
			}
			NSAppTransportSecurity.SetBoolean ( "NSAllowsArbitraryLoads", true );
			NSAppTransportSecurity.SetBoolean ( "NSAllowsArbitraryLoadsInWebContent", true );
			NSAppTransportSecurity.SetBoolean ( "UIViewControllerBasedStatusBarAppearance", false );
			// Write to file
			File.WriteAllText ( plistPath, plist.WriteToString ( ) );
		}
	}
}