﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class ExportManager :MonoBehaviour
{

	private const string menu_path = "HelpTools/Export package/";

	public class PackageSet
	{
		public string []
			FoldersToInclude,
			FilesToInclude;

		public PackageSet (string [] folders, string [] files)
		{
			FoldersToInclude = folders;
			FilesToInclude = files;
		}

		public string [] GetFilesFromPackage ()
		{
			List<string> files = new List<string> ( );
			//scan folders
			if (FoldersToInclude != null)
			{
				for (int i = 0; i < FoldersToInclude.Length; i++)
				{
					files.AddRange ( (string []) Directory.GetFiles ( FoldersToInclude [i], "*.*", SearchOption.AllDirectories ) );
				}
			}
			//check all files
			if (FilesToInclude != null)
			{
				for (int i = 0; i < FilesToInclude.Length; i++)
				{
					string f = FilesToInclude [i];
					if (File.Exists ( f ))
					{
						files.Add ( f );
					}
					else
					{
						Debug.Log ( "File '" + f + "' does not exist" );
					}
				}
			}
			return files.ToArray ( );
		}
	}

	public static PackageSet
		full_set = new PackageSet (
			new string []
				{
					"Assets/AdMobAndAnalytics",
					"Assets/AdsManager",
					"Assets/Demigiant",
					"Assets/EasyStoreKit",
					"Assets/EasyTouchBundle",
					"Assets/Editor",

					"Assets/Game/Animations/GUI",
					"Assets/Game/Font",
					"Assets/Game/Images/GUI",
					"Assets/Game/Images/Loading",
					"Assets/Game/Prefabs/GUI",
					"Assets/Game/Scenes",
					"Assets/Game/Scripts/Helpers",
					"Assets/Game/Scripts/GUI",

					"Assets/PSVPromoPlugin",
					"Assets/PSVPushNews",
					"Assets/RateMeModue",
					"Assets/Resources/PSVPromo",
					"Assets/Resources/Sounds",

					"Assets/Plugins/InMobiAndroid",
					"Assets/Plugins/Android/libs",
					"Assets/Plugins/Android/res/drawable",
					"Assets/Plugins/InMobiAndroid",
					"Assets/Plugins/IOS/Mediation",
					"Assets/Plugins/OpenIAB",
				},
			new string []
				{
					"Assets/Plugins/LitJson.dll",
					"Assets/Plugins/NP_AdmobAd_WP.dll",
					"Assets/Plugins/Android/AndroidManifest.xml",
				}
			),
		lite_set = new PackageSet (
			new string []
				{
					"Assets/AdMobAndAnalytics",
					"Assets/AdsManager",
					"Assets/EasyStoreKit",
					"Assets/Editor",
					"Assets/PSVPromoPlugin",
					"Assets/PSVPushNews",
					"Assets/RateMeModue",
					"Assets/Resources/PSVPromo",
					"Assets/Game/Animations/GUI",
					"Assets/Game/Images/GUI",
					"Assets/Game/Images/Loading",
					"Assets/Plugins/IOS/Mediation",
					"Assets/Plugins/OpenIAB",
					"Assets/Plugins/Android/res/drawable",
					//plugins
					"Assets/Plugins/Android/libs",
					"Assets/Plugins/InMobiAndroid",
				},
			new string []
				{
					//"Assets/Resources/ProjectSettings_Sample.asset",
					"Assets/Game/Prefabs/GUI/BannerPanelCanvas.prefab",
					"Assets/Game/Prefabs/GUI/loading_indicator.prefab",
					"Assets/Game/Scenes/Push.unity",
					"Assets/Game/Scripts/GUI/BannerPanelController.cs",
					"Assets/Game/Scripts/GUI/FadeContentEnabler.cs",
					"Assets/Game/Scripts/GUI/LoadingIndicator.cs",
					"Assets/Game/Scripts/GUI/PurchaseButton.cs",
					"Assets/Game/Scripts/GUI/RestorePurchaseButton.cs",
					"Assets/Game/Scripts/Helpers/AnalyticsManager.cs",
					"Assets/Game/Scripts/Helpers/BillingManager.cs",
					"Assets/Game/Scripts/Helpers/FirebaseManager.cs",
					"Assets/Game/Scripts/Helpers/Initialiser.cs",
					"Assets/Game/Scripts/Helpers/ManagerGoogle.cs",
					"Assets/Game/Scripts/Helpers/PauseManager.cs",
					"Assets/Game/Scripts/Helpers/ProjectSettingsContainer.cs",
					"Assets/Game/Scripts/Helpers/UnityMainThreadDispatcher.cs",
					"Assets/Game/Scripts/Helpers/SceneLoader.cs",
					"Assets/Plugins/Android/AndroidManifest.xml",
					"Assets/Plugins/Android/libs/psvpushservice.jar",
					"Assets/Plugins/LitJson.dll",
					"Assets/Plugins/NP_AdmobAd_WP.dll",
					//"Assets/Plugins/Android/AndroidManifest.xml",
				}
			),
		audio_set = new PackageSet (
			null,
			new string []
				{
					"Assets/Game/Scripts/GUI/AudioToggle.cs",
					"Assets/Game/Scripts/GUI/ToggleButton.cs",
					"Assets/Game/Scripts/Helpers/AudioController.cs",
					"Assets/Game/Scripts/Helpers/GameSettings.cs",
				}
			),
		push_news_set = new PackageSet (
			new string []
				{
					"Assets/PSVPushNews",
				},
			new string []
				{
					"Assets/Editor/EditorHelpTools.cs",
					"Assets/Game/Scenes/Push.unity",
					"Assets/Plugins/Android/libs/psvpushservice.jar",
				}
			),
		gui_set = new
		PackageSet (
			new string []
				{
					"Assets/Game/Animations/GUI",
					"Assets/Game/Images/GUI",
					"Assets/Game/Images/Loading",
					"Assets/Game/Prefabs/GUI",
					"Assets/Game/Scripts/GUI/",
				},
			null
			);

	[MenuItem ( menu_path + "Export Full package" )]
	public static void ExportFullPackage ()
	{
		ExportPackage ( full_set, "full" );
	}


	[MenuItem ( menu_path + "Export All packages" )]
	public static void ExportAllPackages ()
	{
		ExportFullPackage ( );
		ExportLitePackage ( );
		ExportGUIPackage ( );
		ExportPushPackage ( );
		ExportAudioPackage ( );
	}


	[MenuItem ( menu_path + "Export Lite Update package" )]
	public static void ExportLitePackage ()
	{
		ExportPackage ( lite_set, "lite" );
	}

	[MenuItem ( menu_path + "Export GUI package" )]
	public static void ExportGUIPackage ()
	{
		ExportPackage ( gui_set, "gui" );
	}

	[MenuItem ( menu_path + "Export PushNews package" )]
	public static void ExportPushPackage ()
	{
		ExportPackage ( push_news_set, "push" );
	}

	[MenuItem ( menu_path + "Export Audio package" )]
	public static void ExportAudioPackage ()
	{
		ExportPackage ( audio_set, "audio" );
	}


	static void ExportPackage (PackageSet set, string prefix)
	{
		Debug.Log ( "Exporting Audience Network Unity Package..." );

		var path = OutputPath ( prefix );

		try
		{
			string [] files = set.GetFilesFromPackage ( );

			if (files != null && files.Length > 0)
			{
				AssetDatabase.ExportPackage ( files, path, ExportPackageOptions.Recurse );
			}
			else
			{
				Debug.LogError ( "Error exporting package: no files found from package set" );
			}
		}
		catch (System.Exception e)
		{
			Debug.LogError ( "Error exporting package " + e.Message );
		}
		Debug.Log ( "Finished exporting:\n" + path );
	}

	private static string OutputPath (string prefix)
	{
		DirectoryInfo projectRoot = Directory.GetParent ( Directory.GetCurrentDirectory ( ) );
		var outputDirectory = new DirectoryInfo ( Path.Combine ( projectRoot.FullName, "out" ) );

		// Create the directory if it doesn't exist
		outputDirectory.Create ( );
		return Path.Combine ( outputDirectory.FullName, PackageName ( prefix ) );
	}


	private static string PackageName (string prefix)
	{
		System.DateTime date = System.DateTime.Now.Date;
		return string.Format (
			System.Globalization.CultureInfo.InvariantCulture,
			prefix + " {0}.unitypackage", 
			date.Day + "-" + date.Month + "-" + date.Year );
	}
}
